' *****************************************************************
' Generic Data request used by Naples.
'
' It is intended that DataRequests to the BBG API Server (DataServer), 
' the DDE BBG Data Server (DDE_Capture) and any other Data Vendor Objects 
' are constructed using this 'NaplesDataRequest' Object.
'
' A Header message of type '' will usually be accompanied by a
' NaplesDataRequest Object.
'
' Naturally it is essential that this class be serialisable.
' *****************************************************************
<Serializable()> Public Class NaplesDataRequest

  Public RequestID As Guid	' Id of this Request Object
  Public RequestText As String ' Request Text, Format as appropriate.
  Public RequestType As DS_RequestType ' Request Type. e.g. API_Static, DDE_Screenshot etc.

  ' Constructors :-

  Public Sub New(ByVal pRequestText As String, ByVal pRequestType As DS_RequestType, ByVal pRequestID As Guid)
	Me.RequestID = pRequestID
	Me.RequestText = pRequestText
	Me.RequestType = pRequestType
  End Sub

  Public Sub New(ByVal pRequestText As String, ByVal pRequestType As DS_RequestType)
	Me.New(pRequestText, pRequestType, Guid.NewGuid)
  End Sub

  Private Sub New()
	' Enforce the use of Constructor arguments
  End Sub
End Class


' *****************************************************************
' This object is intended to be a General purpose Object for returning Data Requests
' From Naples.
'
' For the Purposes of matching Answers to requests, a copy of the request is included.
' *****************************************************************
<Serializable()> Public Class NaplesDataAnswer

  Public RequestObject As NaplesDataRequest
  Public AnswerType As NaplesAnswerType
  Public AnswerObject As Object
  Public WorkItemObjectID As Guid

  ' Constructors.

  Public Sub New(ByRef pRequestObject As Object, ByVal pAnswerType As NaplesAnswerType, ByRef pAnswerObject As Object, ByRef pWorkItemObjectID As Guid)
    If (TypeOf pRequestObject Is NaplesDataRequest) Then
      Me.RequestObject = CType(pRequestObject, NaplesDataRequest)
      Me.AnswerType = pAnswerType
      Me.AnswerObject = pAnswerObject
      Me.WorkItemObjectID = pWorkItemObjectID
    Else
      Me.RequestObject = Nothing
      Me.AnswerType = pAnswerType
      Me.AnswerObject = pAnswerObject
      Me.WorkItemObjectID = pWorkItemObjectID
    End If
  End Sub

  Public Sub New(ByRef pRequestObject As Object, ByVal pAnswerType As NaplesAnswerType, ByRef pAnswerObject As Object)
    ' pRequestObject must be of type NaplesDataRequest
    Me.New(pRequestObject, pAnswerType, pAnswerObject, Guid.Empty)
  End Sub

  Public Sub New(ByRef pRequestObject As Object, ByVal pAnswerType As NaplesAnswerType)
    ' pRequestObject must be of type NaplesDataRequest
    Me.New(pRequestObject, pAnswerType, Nothing, Guid.Empty)
  End Sub

  Public Sub New(ByRef pRequestObject As Object)
    ' pRequestObject must be of type NaplesDataRequest
    Me.New(pRequestObject, NaplesAnswerType.None, Nothing, Guid.Empty)
  End Sub

  Public Sub New()
    Me.New(Nothing, NaplesAnswerType.None, Nothing, Guid.Empty)
  End Sub

End Class


' *****************************************************************
' DataServer Request Type Enumeration
' 
' Please note that this enumeration is also used by the 'DataServerRequest'
' Object.
' *****************************************************************

Public Enum DS_RequestType
  None
  Cancel
  API_Static
  API_Realtime
  DDE_ScreenShot
  DDE_ScreenCopy
  DDE_GetPortfolio
  DDE_PostCommand
  DDE_RunMacro

  Email_ScreenShot
  Email_ScreenCopy
  Email_EquityInformation

	Debug_RTItems = 1000
	Debug_PendingItems = 1001
	Debug_AggregatorDataTasks = 1002
  Debug_RePostRTRequests = 1003

	Set_DebugLevel = 2000

	BBG_Login = 3000

End Enum



' *****************************************************************
' Type enumeration for the 'NaplesDataAnswer' Object.
'
' *****************************************************************

Public Enum NaplesAnswerType As Integer
  None
  Rejected
  Failed
  Complete
  API_Static
  API_Realtime
  DDE_ScreenShot
	DDE_ScreenCopy
	Debug
End Enum







