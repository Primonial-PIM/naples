Imports System.Threading

'
' Simple Thread Wrapper class.
' 
' Instantiate the Deriving Class and then call 'Start()' to spawn a new thread and run it.
'
' 

Public MustInherit Class ThreadWrapperBase
  ' Task Thread. This is the thread where the task is carried out.

  Public ReadOnly Thread As System.Threading.Thread

  ' Tracks the status of the task
  Private __IsCompleted As Boolean

  Public Sub New()
    ' Create the thread
    Me.Thread = New System.Threading.Thread(AddressOf Me.StartTask)
    Me.Thread.SetApartmentState(ApartmentState.STA)

  End Sub

  ' Start the task on the worker thread
  Public Overridable Sub Start()
    Me.Thread.Start()
  End Sub

  ' Stop the task by aborting the thread
  ' Override this method to use polite stop requests
  Public Overridable Sub [Stop]()
    Me.Thread.Abort()
  End Sub

  ' Thread Task routine
  Private Sub StartTask()
    __IsCompleted = False
    DoTask()
    __IsCompleted = True
  End Sub

  ' Work function.
  Protected MustOverride Sub DoTask()

  ' Status property
  Public ReadOnly Property IsCompleted() As Boolean
    Get
      Return __IsCompleted
    End Get
  End Property

End Class
