Imports System.Threading
Imports Bloomberg
Imports Bloomberg.Api.DataTypes

' *******************************************************
' DataServer Request Object
'
' Data Request Object used by the API Data server - 'DataServer'
' Data Request Object used by the DDE Data server - 'DDE_Capture'
' *******************************************************

<Serializable()> Public Class DataServerRequest
  ' Update Event
  Public Event DataUpdate(ByVal sender As Object, ByVal e As DataServerUpdate)

  ' Unique ID for this Request

  Public RequestId As Guid

  ' Request Details - Type and Substance.

  Public RequestType As DS_RequestType
  Public Request As String

  ' Request Status Properties. 
  ' 'RequestStatus' does what it says : e.g. Submitted, Pending, Complete, Failed etc.
  ' 'Static_RequestID' refers to the GUID of the associated Bloomberg Request Object.
  ' 'RequestTime' refers to the Time that this request was received. May be used to TimeOut stale Requests.

  Public RequestStatus As DS_RequestStatus
  Public Static_RequestID As Guid
  Public RequestTime As Date

  ' Objects in which Data is returned.
  ' Object Type and Data Object. Return Object is most often a hashtable of 'DataServerRTField' Objects.

  Public ReturnType As System.Type
  Public ReturnObject As Object

  ' Constructors :-

  Public Sub New()
    Me.New(DS_RequestType.None, "")
  End Sub

  Public Sub New(ByVal pRequestType As DS_RequestType, ByVal pRequest As String)
    Me.RequestId = Guid.NewGuid
    Me.RequestType = pRequestType
    Me.Request = pRequest

    Me.RequestStatus = DS_RequestStatus.None
    Me.ReturnType = GetType(System.Double)
    Me.ReturnObject = 0
  End Sub

  ' Method to trigger an Update Message.
  ' The Update 'Sender' refers to this object, the Update message returns a reference to 
  ' the RequestID and the Bloomberg Reply object (or Nothing in certain circumstances).

  Public Sub OnDataUpdate(ByVal pReply As Object) ' Bloomberg.Api.Reply)
    If (Me.ReturnObject Is Nothing) Then
      Me.ReturnObject = pReply
    End If

    RaiseEvent DataUpdate(Me, New DataServerUpdate(RequestId, pReply))
  End Sub
End Class


' *******************************************************
' DataServer Realtime Collection Data Object.
'
' The API Data server maintains a hashtable of 'DataServerRTItem' objects for all
' outstanding Realtime Requests. The Hashtable is Keyed on the RealTime Fields BBG RequestID
' So that updates may be quickly applied to te correct object.
'
'
' *******************************************************

<Serializable()> Public Class DataServerRTItem
  Public Event DataUpdate(ByVal sender As Object, ByVal e As DataServerUpdate)

  ' Unique RT Data Item ID
  Public ItemId As Guid

  ' Item Lock : used to allow parallel reads, but only protected writes.
  Public ItemLock As New ReaderWriterLock

  Public ItemStatus As DS_ObjectStatus

  ' Bloomberg Request IDs. May be Null if one is not required.
  Public Realtime_RequestID As Guid
  Public Realtime_Request As Bloomberg.Api.Request
  Public Static_RequestID As Guid

  ' Security to which this request relates.
  Public BBGSecurity As Bloomberg.Api.DataTypes.Security

  ' Collection of Field Data.
  Public Fields As Hashtable

  ' Constructors :-

  Public Sub New()
    Me.ItemId = Guid.NewGuid
    Me.Realtime_Request = Nothing
  End Sub

  ' Method to trigger an Update Message.
  ' The Update 'Sender' refers to this object, the Update message returns a reference to 
  ' the ItemId and the Bloomberg Reply object (or Nothing in certain circumstances).

  Public Sub OnDataUpdate(ByVal pReply As Object) ' Bloomberg.Api.Reply)
    RaiseEvent DataUpdate(Me, New DataServerUpdate(Me.ItemId, pReply))
  End Sub
End Class


' *******************************************************
' DataServer Security Field Object
'
' Designed to Hold the Current Value for a Security Field.
' *******************************************************

<Serializable()> Public Class DataServerRTField

  ' Field Values :-

  Public FieldName As String
  Public DateValue As Date
  Public FieldValue As Object

  ' Constructors :-

  Public Sub New(ByVal pFieldName As String)
	Me.FieldName = pFieldName
	FieldValue = New Double
	DateValue = Now()
  End Sub

  Public Sub New(ByVal pFieldName As String, ByVal pDateValue As Date, ByVal pFieldValue As Object)
	Me.FieldName = pFieldName
	Me.DateValue = pDateValue
	Me.FieldValue = pFieldValue
  End Sub

End Class


' *******************************************************
' DataServer Object Event parameter object
' *******************************************************

Public Class DataServerUpdate
  ' DataServer Data Update Event
  Inherits EventArgs

  ' Event Data
  Private _ReplyObject As Object    ' The Object which supplied the data, e.g. the BBG Reply Object.
  Private _UpdateObjectID As Guid   ' The ID of the Ocject which was updated. e.g. the DataServer Request object.

  ' Property Handlers :-

  Public ReadOnly Property UpdateObjectID() As GUID
    Get
      Return _UpdateObjectID
    End Get
  End Property

  Public ReadOnly Property ReplyType() As System.Type
    Get
      Return _ReplyObject.GetType
    End Get
  End Property

  Public ReadOnly Property ReplyObject() As Object
    Get
      Return _ReplyObject
    End Get
  End Property

  ' Specialised Property, to return BB Reply object if that is what the ReplyObject is.
  Public ReadOnly Property BBgReply() As Bloomberg.Api.Reply
    Get
	  If (Not (_ReplyObject Is Nothing)) AndAlso (_ReplyObject.GetType Is GetType(Bloomberg.Api.Reply)) Then
		Return _ReplyObject
	  Else
		Return Nothing
	  End If
    End Get
  End Property

  ' Constructor :-

  Public Sub New(ByVal pUpdateObjectID As Guid, ByVal pReply As Object)
    _ReplyObject = pReply
    _UpdateObjectID = pUpdateObjectID
  End Sub

End Class


' *******************************************************
' DataServer Request Status Enumeration
' *******************************************************

Public Enum DS_RequestStatus
  None
  RequestSubmitted
  RequestPending
  RequestComplete
  RequestRejected
  RequestFailed
End Enum


' *******************************************************
' DataServer Object Status Enumeration. 
'
' Principally intended for use with the 'DataServerRTItem'
' Class to indicate the status of a RealTime Update object.
' *******************************************************

Public Enum DS_ObjectStatus
  None
  Pending
  Live
  Dead
End Enum

