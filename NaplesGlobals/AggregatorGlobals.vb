

' *******************************************************
' Task Types used by the Aggregator
'
' It is ESSENTIAL that this enumeration matches the tblDataTaskType
' table within the Renaissance Database.
' These Enumerations are duplicated within VeniceGlobals, is is 
' ESSENTIAL that these Enumerations remain in-step.
' *******************************************************

Public Enum DataTaskType As Integer
	NoTask = 0
	PortfolioCapture = 3
	PageCapture = 4

	RealtimeSecurityCapture = 10
	PeriodicSecurityCapture = 11
	RealtimeMarketSecurityCapture = 12
	PeriodicMarketSecurityCapture = 13

	EmailSingleStockChangeNotification = 20
	EmailPortfolioStockChangeNotification = 21

	EmailPeriodicInstrumentInformation = 30
	EmailPage = 31

End Enum


Public Enum DataTaskDay As Byte
	None = 0
	Monday = 1
	Tuesday = 2
	Wednesday = 4
	Thursday = 8
	Friday = 16
	Saturday = 32
	Sunday = 64
	Clear = 128

	EveryDay = 127
	WeekDays = 31
	WeekEnds = 96
End Enum



' *******************************************************
'
'
'
' *******************************************************

Public Class AggregatorDataTask

#Region " Local Variables"

  Private _InstanceGUID As Guid
  Private _TaskID As Integer
  Private _TaskType As DataTaskType
  Private _TaskDescription As String
	Private _TaskComment As String

	'Private _SecurityTicker As String
  Private _Fields As String
	Private _Instrument As String
	Private _InstrumentID As Integer
	Private _Portfolio As String
  Private _PortfolioID As Integer
  Private _PageCode As String

	Private _LimitToMonitorItems As Boolean
  Private _AggregateEmails As Boolean
	Private _EmailPersonIDs(-1) As Integer
	Private _PriceMoveTrigger As Double

  Private _DaysToSaveOn As DataTaskDay
  Private _SaveOnceADay As Boolean ' If this is false, It is assumed capture is at the given Interval between the start and end dates.
  Private _StartTime As Date
  Private _EndTime As Date
  Private _Interval As TimeSpan

  Private _NextUpdateTime As Date
  Private _LastUpdated As Date
  Private _LastUpdateTime As Date

  Private _thisServerRequest(-1) As DataServerRequest
  Private _thisServerRTItem(-1) As DataServerRTItem
  Private _thisReferencePrice(-1) As Double
	Private _thisReferenceDate(-1) As Date
	Private _thisLastTickPrice(-1) As Double
	Private _thisLastTickDate(-1) As Date

#End Region

#Region " Constructors"

  Private Sub New()
    _TaskDescription = ""

    _DaysToSaveOn = DataTaskDay.WeekDays
    _SaveOnceADay = True
    _StartTime = #1/1/3000 12:00:00 PM#
    _EndTime = #1/1/3000 12:00:00 PM#
    _Interval = New TimeSpan(0, 0, 1)

    _LastUpdated = Now()
    _NextUpdateTime = GetNextUpdateTime()

    _InstanceGUID = Guid.NewGuid

    _PortfolioID = 0
  End Sub

  Public Sub New(ByVal pTaskID As Integer)
    Me.New()

    Me._TaskID = pTaskID
  End Sub

#End Region

#Region " Properties"

  Public ReadOnly Property InstanceGUID() As Guid
    Get
      Return _InstanceGUID
    End Get
  End Property

  Public ReadOnly Property TaskID() As Integer
    Get
      Return _TaskID
    End Get
  End Property

  Public Property TaskType() As DataTaskType
    Get
      Return _TaskType
    End Get
    Set(ByVal Value As DataTaskType)
      _TaskType = Value
    End Set
  End Property

  Public Property TaskDescription() As String
    Get
      Return _TaskDescription
    End Get
    Set(ByVal Value As String)
      _TaskDescription = Value
    End Set
  End Property

	Public Property TaskComment() As String
		Get
			Return _TaskComment
		End Get
		Set(ByVal value As String)
			_TaskComment = value
		End Set
	End Property

	Public Property Instrument() As String
		Get
			Return _Instrument
		End Get
		Set(ByVal Value As String)
			_Instrument = Value
		End Set
	End Property

  Public Property InstrumentID() As Integer
    Get
      Return _InstrumentID
    End Get
    Set(ByVal Value As Integer)
      _InstrumentID = Value
    End Set
  End Property

  Public Property Portfolio() As String
    Get
      Return _Portfolio
    End Get
    Set(ByVal Value As String)
      _Portfolio = Value

      ' More Code Here ?

    End Set
  End Property

  Public Property PortfolioID() As Integer
    Get
      Return _PortfolioID
    End Get
    Set(ByVal Value As Integer)
      _PortfolioID = Value
    End Set
  End Property

  Public Property PageCode() As String
    Get
      Return _PageCode
    End Get
    Set(ByVal Value As String)
      _PageCode = Value


      ' More Code Here ?

    End Set
  End Property

  Public Property DaysToSaveOn() As DataTaskDay
    Get
      Return _DaysToSaveOn
    End Get
    Set(ByVal Value As DataTaskDay)
      If (Value And DataTaskDay.Clear) > 0 Then
        _DaysToSaveOn = 0
      End If
      _DaysToSaveOn = Value Or _DaysToSaveOn
      _DaysToSaveOn = (_DaysToSaveOn And (Not (DataTaskDay.Clear)))

      If Not UpdateDue() Then
        _NextUpdateTime = GetNextUpdateTime()
      End If
    End Set
  End Property

	Public Property Fields() As String
		Get
			Return _Fields
		End Get
		Set(ByVal Value As String)
			_Fields = Value


			' More Code Here ?

		End Set
	End Property

  Public Property SaveOnceADay() As Boolean
    Get
      Return _SaveOnceADay
    End Get
    Set(ByVal Value As Boolean)
      _SaveOnceADay = Value

      If Not UpdateDue() Then
        _NextUpdateTime = GetNextUpdateTime()
      End If
    End Set
  End Property

	Public Property LimitToMonitorItems() As Boolean
		Get
			Return _LimitToMonitorItems
		End Get
		Set(ByVal value As Boolean)
			_LimitToMonitorItems = value
		End Set
	End Property

  Public Property AggregateEmails() As Boolean
    Get
      Return _AggregateEmails
    End Get
    Set(ByVal Value As Boolean)
      _AggregateEmails = Value

      If Not UpdateDue() Then
        _NextUpdateTime = GetNextUpdateTime()
      End If
    End Set
  End Property

  Public Property PriceMoveTrigger() As Double
    Get
      Return _PriceMoveTrigger
    End Get
    Set(ByVal value As Double)
      _PriceMoveTrigger = value
    End Set
  End Property

	Public Property EmailPersonIDs(Optional ByVal ItemIndex As Integer = 0) As Integer
		Get
			If (ItemIndex < _EmailPersonIDs.Length) Then
				Return _EmailPersonIDs(ItemIndex)
			Else
				Return 0
			End If
		End Get

		Set(ByVal value As Integer)
			If (_EmailPersonIDs.Length <= ItemIndex) Then
				ReDim Preserve _EmailPersonIDs(ItemIndex)
			End If

			_EmailPersonIDs(ItemIndex) = value
		End Set
	End Property

  Public ReadOnly Property EmailAddressesCount() As Integer
    Get
			Return _EmailPersonIDs.Length
    End Get
  End Property

	Public ReadOnly Property EmailPersonIDsArray() As Integer()
		Get
			Return _EmailPersonIDs
		End Get
	End Property

  Public Property StartTime() As Date
    Get
      Return _StartTime
    End Get
    Set(ByVal Value As Date)
      _StartTime = Value

      If Not UpdateDue() Then
        _NextUpdateTime = GetNextUpdateTime()
      End If
    End Set
  End Property

  Public Property EndTime() As Date
    Get
      Return _EndTime
    End Get
    Set(ByVal Value As Date)
      _EndTime = Value

      If Not UpdateDue() Then
        _NextUpdateTime = GetNextUpdateTime()
      End If
    End Set
  End Property

  Public Property Interval() As TimeSpan
    Get
      Return _Interval
    End Get
    Set(ByVal Value As TimeSpan)
      ' Interval must be a minimum of One second

      If Value.TotalSeconds < 1 Then
        _Interval = New TimeSpan(0, 0, 1)
      Else
        _Interval = Value
      End If

      If Not UpdateDue() Then
        _NextUpdateTime = GetNextUpdateTime()
      End If
    End Set
  End Property

  Public ReadOnly Property NextUpdateTime() As Date
    Get
      Return _NextUpdateTime
    End Get
  End Property

  Public ReadOnly Property LastUpdated() As Date
    Get
      Return _LastUpdated
    End Get
  End Property

  Public ReadOnly Property LastUpdateTime() As Date
    Get
      Return _LastUpdateTime
    End Get
  End Property

  Public Function UpdateDue() As Boolean

    ' Check to see if this item needs updating

    If Now >= NextUpdateTime Then
      Return True
    Else
      Return False
    End If

  End Function

  Public Sub Updated()

    ' Set Next Update Time

    _LastUpdated = Now()
    _LastUpdateTime = _NextUpdateTime
    _NextUpdateTime = GetNextUpdateTime()

  End Sub

	Public Property ServerRequest(Optional ByVal ItemIndex As Integer = 0) As DataServerRequest
		' *********************************************************************
		' Access the Server Request array.
		'
		' For compatability, and because most Data Tasks require only one server request object, 
		' this property takes an optional Index parameter which defaults to zero.
		' The portfolio monitoring Data Tesks may (usually) require multiple server request 
		' objects thus this parameter is also accessable as an array.
		'
		' *********************************************************************

		Get
			If (_thisServerRequest IsNot Nothing) AndAlso (ItemIndex < _thisServerRequest.Length) Then
				Return _thisServerRequest(ItemIndex)
			Else
				Return Nothing
			End If
		End Get
		Set(ByVal Value As DataServerRequest)
			If (_thisServerRequest IsNot Nothing) AndAlso (ItemIndex < _thisServerRequest.Length) Then
				_thisServerRequest(ItemIndex) = Value
			Else
				ReDim Preserve _thisServerRequest(ItemIndex)
				_thisServerRequest(ItemIndex) = Value
			End If
		End Set
	End Property

  Public ReadOnly Property ServerRequestIndex(ByVal thisDataServerRequest As DataServerRequest) As Integer
    Get
      Dim IndexValue As Integer

      If (thisDataServerRequest Is Nothing) Then
        Return -1
      End If

			Try
				For IndexValue = 0 To (_thisServerRequest.Length - 1)
					Try
						If (_thisServerRequest(IndexValue) IsNot Nothing) Then
							If thisDataServerRequest.RequestId.CompareTo(_thisServerRequest(IndexValue).RequestId) = 0 Then
								Return IndexValue
							End If
						End If
					Catch ex As Exception
					End Try
				Next
			Catch ex As Exception
			End Try

      Return -1
    End Get
  End Property

  Public ReadOnly Property ServerRequestCount() As Integer
    Get
      If (_thisServerRequest IsNot Nothing) Then
        Return _thisServerRequest.Length
      End If

      Return 0
    End Get
  End Property

  Public Property ServerRTItem(Optional ByVal ItemIndex As Integer = 0) As DataServerRTItem
    ' *********************************************************************
    ' Access the Server RTItem array.
    '
    ' Each DataTask may be associated with One or more DataServer Requests relating to the 
    ' Market Data Item(s) being watched or recorded.
    ' In parallel to this array is an array of 'DataServerRTItem's. These RT Items are the 
    ' DataServer RT Items relating to the DataServer Requests (if appropriate).
    ' If used, then these RT Items are the items against which Update events occur.
    '
    ' For compatability, and because most Data Tasks require only one server request object, 
    ' this property takes an optional Index parameter which defaults to zero.
    ' The portfolio monitoring Data Tesks may (usually) require multiple server request 
    ' objects thus this parameter is also accessable as an array.
    '
    ' *********************************************************************

    Get
      If (_thisServerRTItem IsNot Nothing) AndAlso (ItemIndex < _thisServerRTItem.Length) Then
        Return _thisServerRTItem(ItemIndex)
      Else
        Return Nothing
      End If
    End Get
    Set(ByVal Value As DataServerRTItem)
      If (_thisServerRTItem IsNot Nothing) AndAlso (ItemIndex < _thisServerRTItem.Length) Then
        _thisServerRTItem(ItemIndex) = Value
      Else
        ReDim Preserve _thisServerRTItem(ItemIndex)
        _thisServerRTItem(ItemIndex) = Value
      End If
    End Set
  End Property

  Public ReadOnly Property ServerRTItemIndex(ByVal thisDataServerRTItem As DataServerRTItem) As Integer
    Get
      Dim IndexValue As Integer

      If (thisDataServerRTItem Is Nothing) Then
        Exit Property
      End If

			Try
				For IndexValue = 0 To (_thisServerRTItem.Length - 1)
					Try
						If (_thisServerRTItem(IndexValue) IsNot Nothing) Then
							If thisDataServerRTItem.ItemId.CompareTo(_thisServerRTItem(IndexValue).ItemId) = 0 Then
								Return IndexValue
							End If
						End If
					Catch ex As Exception
					End Try
				Next
			Catch ex As Exception
			End Try

      Return -1
    End Get
  End Property

  Public ReadOnly Property ServerRTItemCount() As Integer
    Get
      Return _thisServerRTItem.Length
    End Get
  End Property

  Public Property ReferencePrice(Optional ByVal PriceIndex As Integer = 0) As Double
    Get
      If (_thisReferencePrice IsNot Nothing) AndAlso (_thisReferencePrice.Length > PriceIndex) Then
        Return _thisReferencePrice(PriceIndex)
      Else
        Return 0
      End If
    End Get
    Set(ByVal value As Double)
      If (_thisReferencePrice IsNot Nothing) AndAlso (_thisReferencePrice.Length > PriceIndex) Then
        _thisReferencePrice(PriceIndex) = value
      Else
        ReDim Preserve _thisReferencePrice(PriceIndex)
        _thisReferencePrice(PriceIndex) = value
      End If
    End Set
  End Property

	Public Property ReferenceDate(Optional ByVal DateIndex As Integer = 0) As Date
		Get
			If (_thisReferenceDate IsNot Nothing) AndAlso (_thisReferenceDate.Length > DateIndex) Then
				Return _thisReferenceDate(DateIndex)
			Else
				Return Now()
			End If
		End Get
		Set(ByVal value As Date)
			If (_thisReferenceDate IsNot Nothing) AndAlso (_thisReferenceDate.Length > DateIndex) Then
				_thisReferenceDate(DateIndex) = value
			Else
				ReDim Preserve _thisReferenceDate(DateIndex)
				_thisReferenceDate(DateIndex) = value
			End If
		End Set
	End Property

	Public Property LastTickPrice(Optional ByVal PriceIndex As Integer = 0) As Double
		Get
			If (_thisLastTickPrice IsNot Nothing) AndAlso (_thisLastTickPrice.Length > PriceIndex) Then
				Return _thisLastTickPrice(PriceIndex)
			Else
				Return 0
			End If
		End Get
		Set(ByVal value As Double)
			If (_thisLastTickPrice IsNot Nothing) AndAlso (_thisLastTickPrice.Length > PriceIndex) Then
				_thisLastTickPrice(PriceIndex) = value
			Else
				ReDim Preserve _thisLastTickPrice(PriceIndex)
				_thisLastTickPrice(PriceIndex) = value
			End If
      Me.LastTickDate(PriceIndex) = Now()
		End Set
	End Property

	Public Property LastTickDate(Optional ByVal DateIndex As Integer = 0) As Date
		Get
			If (_thisLastTickDate IsNot Nothing) AndAlso (_thisLastTickDate.Length > DateIndex) Then
				Return _thisLastTickDate(DateIndex)
			Else
				Return Now()
			End If
		End Get
		Set(ByVal value As Date)
			If (_thisLastTickDate IsNot Nothing) AndAlso (_thisLastTickDate.Length > DateIndex) Then
				_thisLastTickDate(DateIndex) = value
			Else
				ReDim Preserve _thisLastTickDate(DateIndex)
				_thisLastTickDate(DateIndex) = value
			End If
		End Set
	End Property

#End Region

	Public Overloads Overrides Function Equals(ByVal pOtherObject As Object) As Boolean
		Try

			If Not TypeOf pOtherObject Is AggregatorDataTask Then Return False

			Dim Compare As AggregatorDataTask = CType(pOtherObject, AggregatorDataTask)

			If _TaskType.Equals(Compare.TaskType) AndAlso _
			 Me.Instrument.Equals(Compare.Instrument) AndAlso _
			 Me.InstrumentID.Equals(Compare.InstrumentID) AndAlso _
			 Me.Fields.Equals(Compare.Fields) AndAlso _
			 Me.Portfolio.Equals(Compare.Portfolio) AndAlso _
			 Me.PortfolioID.Equals(Compare.PortfolioID) AndAlso _
			 Me.PageCode.Equals(Compare.PageCode) AndAlso _
			 Me.PriceMoveTrigger.Equals(Compare.PriceMoveTrigger) AndAlso _
			 Me.AggregateEmails.Equals(Compare.AggregateEmails) AndAlso _
			 Me.DaysToSaveOn.Equals(Compare.DaysToSaveOn) AndAlso _
			 Me.SaveOnceADay.Equals(Compare.SaveOnceADay) AndAlso _
			 Me.StartTime.Equals(Compare.StartTime) AndAlso _
			 Me.EndTime.Equals(Compare.EndTime) AndAlso _
			 Me.Interval.Equals(Compare.Interval) AndAlso _
			 Me.LimitToMonitorItems.Equals(Compare.LimitToMonitorItems) AndAlso _
			 Me.TaskComment.Equals(Compare.TaskComment) Then

				' Dim CompareIndex As Integer
				Dim CompareIndexMax As Integer

				' EmailAddresses
				If Me.EmailAddressesCount > Compare.EmailAddressesCount Then
					CompareIndexMax = Me.EmailAddressesCount - 1
				Else
					CompareIndexMax = Compare.EmailAddressesCount - 1
				End If

				'For CompareIndex = 0 To CompareIndexMax
				'	If (Me.EmailPersonIDs(CompareIndex).Equals(Compare.EmailPersonIDs(CompareIndex)) = False) Then
				'		Return (False)
				'	End If
				'Next

				Return True
			End If


		Catch ex As Exception
		End Try

		Return (False)
	End Function

  Private Function GetNextUpdateTime() As Date
    ' *********************************************************************
    ' Return the next Update time.
    '
    ' *********************************************************************

    Dim thisNow As Date
    Dim TestDate As Date
    Dim ReturnDate As Date

    ' Validate.

    ' Is any save day Set ?
    If (Me.DaysToSaveOn And DataTaskDay.EveryDay) = DataTaskDay.None Then
      ' No Day set, return future date
      Return #1/1/3000#
    End If

    ' Use a consistent 'Now' Time and Date
    thisNow = Now()
    TestDate = thisNow

    ' Save Once a Day @ XX:XX
    If Me.SaveOnceADay Then

      ' Save Today ?
      If (DaysToSaveOn And CType(System.Enum.Parse(GetType(DataTaskDay), TestDate.DayOfWeek.ToString), DataTaskDay)) > 0 Then

        ' If Now is before the single Save Time, then the Single Save time is the Next Save Time.

        If TestDate.TimeOfDay.CompareTo(StartTime.TimeOfDay) <= 0 Then
          TestDate = TestDate.Date
          TestDate = TestDate.AddHours(StartTime.Hour)
          TestDate = TestDate.AddMinutes(StartTime.Minute)
          TestDate = TestDate.AddSeconds(StartTime.Second)

          Return TestDate
        End If
      End If

      ' Ok, it is after today's save time,
      ' Find the next valid day

      TestDate = TestDate.Date.AddDays(1) '  Increment Day, setting time to 0 also.
      While (DaysToSaveOn And CType(System.Enum.Parse(GetType(DataTaskDay), TestDate.DayOfWeek.ToString), DataTaskDay)) = 0
        TestDate = TestDate.AddDays(1)

        ' If 'NextDate' is more than a week after 'Now' then assume that
        ' there are no active days and exit with the default far-distant date.
        If (TestDate - thisNow).TotalDays > 10 Then
          Return #1/1/3000#
        End If

      End While

      ' Return The Start time on the next valid Day.

      TestDate = TestDate.AddHours(StartTime.Hour)
      TestDate = TestDate.AddMinutes(StartTime.Minute)
      TestDate = TestDate.AddSeconds(StartTime.Second)

      Return TestDate
    Else
      ' Saved Periodically

      ' Save Today ?

      If (DaysToSaveOn And CType(System.Enum.Parse(GetType(DataTaskDay), TestDate.DayOfWeek.ToString), DataTaskDay)) > 0 Then

        If TestDate.TimeOfDay.CompareTo(StartTime.TimeOfDay) <= 0 Then
          ' Today is a valid Day and The time now is on or before the StartTime

          TestDate = TestDate.Date
          TestDate = TestDate.AddHours(StartTime.Hour)
          TestDate = TestDate.AddMinutes(StartTime.Minute)
          TestDate = TestDate.AddSeconds(StartTime.Second)

          Return TestDate

        ElseIf TestDate.TimeOfDay.CompareTo(EndTime.TimeOfDay) <= 0 Then
          ' Today is a valid Day and The time now is before the EndTime
          ' So, Set ReturnDate to the Start time and increment it by the Interval until it is after the 'Now' time.

          ReturnDate = TestDate.Date
          ReturnDate = ReturnDate.AddHours(StartTime.Hour)
          ReturnDate = ReturnDate.AddMinutes(StartTime.Minute)
          ReturnDate = ReturnDate.AddSeconds(StartTime.Second)

          While ReturnDate.CompareTo(TestDate) < 0
            ReturnDate = ReturnDate.AddSeconds(Interval.TotalSeconds)
          End While

          ' If the Return time is before or Equal to the End time, return it.
          If ReturnDate.TimeOfDay.CompareTo(EndTime.TimeOfDay) <= 0 Then
            Return ReturnDate
          Else
            ' Otherwise return the End Time

            ReturnDate = TestDate.Date
            ReturnDate = ReturnDate.AddHours(EndTime.Hour)
            ReturnDate = ReturnDate.AddMinutes(EndTime.Minute)
            ReturnDate = ReturnDate.AddSeconds(EndTime.Second)
            Return ReturnDate

          End If
        End If
      End If

      ' OK, Today is done with,
      ' Find the next valid day

      TestDate = TestDate.Date.AddDays(1) '  Increment Day, setting time to 0 also.
      While (DaysToSaveOn And CType(System.Enum.Parse(GetType(DataTaskDay), TestDate.DayOfWeek.ToString), DataTaskDay)) = 0
        TestDate = TestDate.AddDays(1)

        ' If 'NextDate' is more than a week after 'Now' then assume that
        ' there are no active days and exit with the default far-distant date.
        If (TestDate - thisNow).TotalDays > 10 Then
          Return #1/1/3000#
        End If
      End While

      ' Return the Start date on that day.

      TestDate = TestDate.Date
      TestDate = TestDate.AddHours(StartTime.Hour)
      TestDate = TestDate.AddMinutes(StartTime.Minute)
      TestDate = TestDate.AddSeconds(StartTime.Second)

      Return TestDate

    End If


  End Function

End Class
