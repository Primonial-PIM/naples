
' *******************************************************
'
'
'
' *******************************************************

Public Class DBTask
  ' *******************************************************
  ' Simple Wrapper object for submitting DB Save requests
  ' to the DataBaseClient object.
  '
  '
  ' *******************************************************

  Private _DataRequestType As DataTaskType
  Private _AggregatorTask As AggregatorDataTask
  Private _DataObject As Object
  Private _TaskID As Guid

  Public Property DataRequestType() As DataTaskType
    Get
      Return _DataRequestType
    End Get
    Set(ByVal Value As DataTaskType)
      _DataRequestType = Value
    End Set
  End Property

  Public Property DataObject() As Object
    Get
      Return _DataObject
    End Get
    Set(ByVal Value As Object)
      _DataObject = Value
    End Set
  End Property

  Public Property AggregatorTask() As AggregatorDataTask
    Get
      Return _AggregatorTask
    End Get
    Set(ByVal Value As AggregatorDataTask)
      _AggregatorTask = Value
    End Set
  End Property

  Public ReadOnly Property TaskID() As Guid
    Get
      Return _TaskID
    End Get
  End Property

  Public Sub New()

    ' Initialise Object 

    _TaskID = System.Guid.NewGuid
    _DataRequestType = DataTaskType.NoTask
    _DataObject = Nothing
    _AggregatorTask = Nothing

  End Sub

  Public Sub New(ByVal pDataRequestType As DataTaskType)
    Me.New()
    _DataRequestType = pDataRequestType
  End Sub

  Public Sub New(ByVal pDataRequestType As DataTaskType, ByVal pDataObject As Object)
    Me.New()
    _DataRequestType = pDataRequestType
    _DataObject = pDataObject
  End Sub

  Public Sub New(ByVal pDataRequestType As DataTaskType, ByVal pAggregatorTask As AggregatorDataTask, ByVal pDataObject As Object)
    Me.New()
    _DataRequestType = pDataRequestType
    _AggregatorTask = pAggregatorTask
    _DataObject = pDataObject
  End Sub

  Public Sub New(ByVal pAggregatorTask As AggregatorDataTask, ByVal pDataObject As Object)
    Me.New()
    _AggregatorTask = pAggregatorTask
    _DataObject = pDataObject
    _DataRequestType = pAggregatorTask.TaskType
  End Sub

End Class
