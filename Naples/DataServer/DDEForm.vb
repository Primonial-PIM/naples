'Option Strict On
'Option Explicit On 
'Option Compare Binary

'Imports System.Net
'Imports System.Net.Sockets
'Imports System.IO
'Imports System.Runtime.Serialization.Formatters.Binary
'Imports System.Runtime.Serialization
'Imports System.Threading
'Imports System.Windows.Forms

'Imports NaplesGlobals
'Imports NaplesGlobals.NaplesGobals

'Imports System.Runtime.InteropServices.LayoutKind
'Imports System.Runtime.InteropServices.Marshal
'Imports System.Windows.Forms.DataFormats



'Public Class DDEForm_Org
'  Inherits System.Windows.Forms.Form

'  Private WithEvents DDEmessages As DDEmessageFilter

'  Private DDEhWnd As System.IntPtr
'  Private DDEApp As String
'  Private DDETopic As String

'  Private DataReceived As Boolean
'  Private ACKReceived As Integer

'  Private __DDETimeout As Long

'  Public ReceivedData As String
'  Private ReadOnly NewLine As String = System.Environment.NewLine
'  Friend WithEvents Timer1 As System.Windows.Forms.Timer
'  Private components As System.ComponentModel.IContainer

'  Public LogTextBox As Windows.Forms.TextBox
'  Public ControlingThreadWrapper As DDE_FormThread

'  Public ReadOnly Property DDE_Window() As System.IntPtr
'    Get
'      Return DDEhWnd
'    End Get
'  End Property

'  'Public Shared Sub Main()
'  '    System.Windows.Forms.Application.Run(New DDEForm)
'  'End Sub

'  Public Sub New()
'    ' Create the form object
'    MyBase.New()

'    Call InitializeComponent()

'    __DDETimeout = 0

'    ' Look for DDE messages
'    DDEmessages = New DDEmessageFilter(Handle)
'  End Sub

'  Private Sub DDEForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'    Me.Visible = False
'  End Sub

'  Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
'    If Not DDEmessages Is Nothing AndAlso DDEmessages.PreFilterMessage(m) Then
'      ' Handled
'    Else
'      MyBase.WndProc(m)
'    End If
'  End Sub

'  Private Sub Log(ByVal pMessage As String)
'    Static LogCounter As Integer

'    LogCounter += 1
'    If Not (LogTextBox Is Nothing) Then
'      LogTextBox.Text = LogTextBox.Text & LogCounter.ToString & ", " & pMessage
'    End If

'  End Sub


'  Public Sub SendInitiate(ByVal App As String, ByVal Topic As String)
'    SendInitiate(BROADCAST, App, Topic)
'  End Sub
'  Private Sub SendInitiate(ByVal hWnd As System.IntPtr, ByVal App As String, ByVal Topic As String)

'    Dim AppAtom As Short
'    Dim TopicAtom As Short

'    Try
'      AppAtom = GlobalAddAtom(App)
'      TopicAtom = GlobalAddAtom(Topic)
'      DDEApp = App
'      DDETopic = Topic

'      Log("   Init send(" & hWnd.ToInt32.ToString("x").ToUpper() & ", " & App & "," & Topic & ")" & NewLine)

'      SendMessage(hWnd, WM_DDE.INITIATE, Handle, MakeInt(AppAtom, TopicAtom))

'      Log("   Init done(" & hWnd.ToInt32.ToString("x").ToUpper() & ", " & App & "," & Topic & ")" & NewLine)
'    Finally
'      If AppAtom <> 0 Then GlobalDeleteAtom(AppAtom)
'      If TopicAtom <> 0 Then GlobalDeleteAtom(TopicAtom)
'    End Try
'  End Sub

'  Public Sub PostRequest(ByVal Item As String)
'    Me.PostRequest(DDEhWnd, Item)
'  End Sub
'  Private Sub PostRequest(ByVal hWnd As System.IntPtr, ByVal Item As String)

'    Dim ItemAtom As Short
'    Dim FormatID As Short

'    Try
'      ItemAtom = GlobalAddAtom(Item)

'      FormatID = CShort(System.Windows.Forms.DataFormats.GetFormat(System.Windows.Forms.DataFormats.Text).Id)

'      DataReceived = False

'      If PostMessage(hWnd, WM_DDE.REQUEST, Handle, PackDDElParam(WM_DDE.REQUEST, FormatID, ItemAtom)) = 0 Then
'        ' Clear the Item atom so we don't free it
'        ItemAtom = 0
'      End If

'      Log("   Request sent(" & hWnd.ToInt32.ToString("x").ToUpper() & ", " & FormatID & "," & Item & ")" & NewLine)
'    Finally
'      If ItemAtom <> 0 Then GlobalDeleteAtom(ItemAtom)
'    End Try
'  End Sub

'  Public Sub PostExecute(ByVal Item As String)
'    Me.PostExecute(DDEhWnd, Item)
'  End Sub
'  Private Sub PostExecute(ByVal hWnd As System.IntPtr, ByVal Item As String)

'    Dim ItemAtom As Short
'    Dim FormatID As Short
'    Dim rval As Integer
'    Dim myIntPtr As System.IntPtr

'    FormatID = CShort(System.Windows.Forms.DataFormats.GetFormat(System.Windows.Forms.DataFormats.Text).Id)

'    Try
'      'ItemAtom = GlobalAddAtom(Item)

'      If IsWindowUnicode(Handle) Then
'        myIntPtr = StringToHGlobalUni(Item)
'      Else
'        myIntPtr = StringToHGlobalAnsi(Item)
'      End If

'      DataReceived = False
'      ACKReceived = 0

'      'rval = PostMessage(hWnd, WM_DDE.EXECUTE, Handle, PackDDElParam(WM_DDE.EXECUTE, FormatID, ItemAtom))
'      rval = PostMessage(hWnd, WM_DDE.EXECUTE, Handle, myIntPtr.ToInt32)
'      If rval = 0 Then
'        ' Clear the Item atom so we don't free it
'        ItemAtom = 0
'        'ACKReceived = -1
'      End If

'      Log("   Execute sent(" & hWnd.ToInt32.ToString("x").ToUpper() & ", " & Item & ")" & NewLine)
'    Finally
'      If ItemAtom <> 0 Then
'        GlobalDeleteAtom(ItemAtom)
'      End If
'    End Try
'  End Sub

'  Public Sub WaitForData()
'    Dim TimeoutCounter As Long
'    Dim TimeoutLimit As Long

'    If (__DDETimeout <= 0) Then
'      TimeoutLimit = 2000
'    Else
'      TimeoutLimit = __DDETimeout
'    End If

'    TimeoutCounter = 0
'    Do
'      If (DataReceived) Or (TimeoutCounter >= TimeoutLimit) Then Exit Sub
'      System.Threading.Thread.Sleep(55)
'      System.Windows.Forms.Application.DoEvents()
'      TimeoutCounter += 55
'    Loop
'  End Sub

'  Public Sub WaitForAck()
'    Dim TimeoutCounter As Long
'    Dim TimeoutLimit As Long

'    If (__DDETimeout <= 0) Then
'      TimeoutLimit = 2000
'    Else
'      TimeoutLimit = __DDETimeout
'    End If

'    TimeoutCounter = 0
'    Do
'      If (ACKReceived <> 0) Or (TimeoutCounter >= TimeoutLimit) Then Exit Sub
'      System.Threading.Thread.Sleep(55)
'      System.Windows.Forms.Application.DoEvents()
'      TimeoutCounter += 55
'    Loop
'  End Sub









'#Region "DDE (Message Received) Events"

'  Private Sub DDEmessages_Initiate(ByVal hWnd As System.IntPtr, ByVal App As String, ByVal Topic As String) Handles DDEmessages.Initiate

'    Log("Init(" & hWnd.ToInt32.ToString("x").ToUpper() & ", " & App & "," & Topic & ")" & NewLine)

'    If (App.Length = 0 OrElse App.ToUpper() = "DDE.NET") AndAlso (Topic.Length = 0 OrElse Topic.ToLower() = "system") Then
'      SendInitACK(hWnd, "DDE.NET", "System")
'    End If
'  End Sub

'  Private Sub DDEmessages_InitACK(ByVal hWnd As System.IntPtr, ByVal App As String, ByVal Topic As String) Handles DDEmessages.InitACK

'    Log("InitACK(" & hWnd.ToInt32.ToString("x").ToUpper() & ", " & App & "," & Topic & ") " & NewLine)

'    If App.ToUpper = DDEApp.ToUpper And Topic.ToUpper = DDETopic.ToUpper Then
'      DDEhWnd = hWnd
'    End If
'  End Sub

'  Private Sub DDEmessages_Terminate(ByVal hWnd As System.IntPtr) Handles DDEmessages.Terminate

'    Log("Terminate " & hWnd.ToInt32.ToString("x").ToUpper() & NewLine)

'    PostTerminate(hWnd)

'  End Sub

'  Private Sub DDEmessages_Advise(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal TransportAdvice As DDEADVISE) Handles DDEmessages.Advise

'    Log("Advise " & hWnd.ToInt32.ToString("x").ToUpper() & ", Item: " & Item & NewLine _
'        & "    " & IIf(TransportAdvice.fAckReq, "AckReq ", "       ") _
'        & IIf(TransportAdvice.fDeferUpd, "DeferUpd ", "         ") _
'        & TransportAdvice.cfFormat.Name _
'        & NewLine)

'    If TransportAdvice.fAckReq Then
'      PostACK(hWnd, Item, DDEmessageFilter.ACKTYPES.OK, 0)
'    End If
'  End Sub

'  Private Sub DDEmessages_Data(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal DataInfo As DDEDATA, ByVal DataPtr As System.IntPtr) Handles DDEmessages.Data

'    Dim result As DDEmessageFilter.ACKTYPES = DDEmessageFilter.ACKTYPES.OK

'    Log("Data " & hWnd.ToInt32.ToString("x").ToUpper() & ", Item: " & Item & NewLine _
'        & "    " & IIf(DataInfo.fAckReq, "AckReq ", "       ") _
'        & IIf(DataInfo.fResponse, "Response ", "         ") _
'        & IIf(DataInfo.fRelease, "Release ", "         ") _
'        & DataInfo.cfFormat.Name _
'        & NewLine)

'    Select Case DataInfo.cfFormat.Name
'      Case System.Windows.Forms.DataFormats.UnicodeText
'        ' Use/assume unicode
'        ReceivedData = PtrToStringAnsi(DataPtr)
'        Log(ReceivedData & NewLine)

'      Case System.Windows.Forms.DataFormats.Text
'        ' Use/assume ansi
'        ReceivedData = PtrToStringAnsi(DataPtr)
'        Log(ReceivedData & NewLine)
'      Case Else
'        ' Unhandled data
'        result = DDEmessageFilter.ACKTYPES.NACK
'        ReceivedData = ""
'    End Select

'    If DataInfo.fAckReq Then
'      PostACK(hWnd, Item, result, 0)
'    End If
'    'If DataInfo.fRelease Then
'    '    ' Handled in caller
'    'End If
'    If DataInfo.fResponse Then
'      DataReceived = True
'    End If
'  End Sub

'  Private Sub DDEmessages_Execute(ByVal hWnd As System.IntPtr, ByVal Command As String, ByVal hCommand As System.IntPtr) Handles DDEmessages.Execute

'    Log("Execute " & hWnd.ToInt32.ToString("x").ToUpper() & ": " & Command & NewLine)

'    ' Always ACK:
'    PostExecACK(hWnd, hCommand, DDEmessageFilter.ACKTYPES.OK, 0)
'  End Sub

'  Private Sub DDEmessages_Poke(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal PokeInfo As DDEPOKE, ByVal PokePtr As System.IntPtr) Handles DDEmessages.Poke

'    Log("Poke " & hWnd.ToInt32.ToString("x").ToUpper() & " Item: " & Item & NewLine _
'        & "    " & "       " _
'        & IIf(PokeInfo.fRelease, "Release ", "         ") _
'        & PokeInfo.cfFormat.Name _
'        & NewLine)
'  End Sub

'  Private Sub DDEmessages_Request(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal Format As System.Windows.Forms.DataFormats.Format) Handles DDEmessages.Request

'    Log("Request " & hWnd.ToInt32.ToString("x").ToUpper() & " Item: " & Item & NewLine _
'        & "    " & "       " _
'        & Format.Name _
'        & NewLine)
'  End Sub

'  Private Sub DDEmessages_UnAdvise(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal Format As System.Windows.Forms.DataFormats.Format) Handles DDEmessages.UnAdvise

'    Log("UnAdvise " & hWnd.ToInt32.ToString("x").ToUpper() & " Item: " & Item & NewLine _
'        & "    " & "       " _
'        & Format.Name _
'        & NewLine)
'  End Sub

'  Private Sub DDEmessages_ACK(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal Response As DDEmessageFilter.ACKTYPES, ByVal AppReturnCode As Byte) Handles DDEmessages.ACK

'    Log("ACK from " & hWnd.ToInt32.ToString("x").ToUpper() & ", Item: " & Item & NewLine _
'        & "    " & " RetCode: " & AppReturnCode.ToString _
'        & Response.ToString _
'        & NewLine)

'    If Response = DDEmessageFilter.ACKTYPES.NACK Then
'      ACKReceived = -1
'    ElseIf Response = DDEmessageFilter.ACKTYPES.OK Then
'      ACKReceived = 1
'    ElseIf Response = DDEmessageFilter.ACKTYPES.Busy Then
'      ACKReceived = -2
'    Else
'      ACKReceived = -3
'    End If
'  End Sub
'#End Region

'#Region "Responses"

'  Private Sub SendInitACK(ByVal hWnd As System.IntPtr, ByVal App As String, ByVal Topic As String)

'    Dim AppAtom As Short
'    Dim TopicAtom As Short

'    Try
'      AppAtom = GlobalAddAtom(App)
'      TopicAtom = GlobalAddAtom(Topic)
'      SendMessage(hWnd, WM_DDE.ACK, Handle, MakeInt(AppAtom, TopicAtom))
'      Log("   Init ACK sent(" & App & "," & Topic & ")" & NewLine)
'    Finally
'      If AppAtom <> 0 Then GlobalDeleteAtom(AppAtom)
'      If TopicAtom <> 0 Then GlobalDeleteAtom(TopicAtom)
'    End Try
'  End Sub

'  Public Sub PostTerminate()
'    PostTerminate(DDEhWnd)
'  End Sub
'  Private Sub PostTerminate(ByVal hWnd As System.IntPtr)

'    Try
'      PostMessage(hWnd, WM_DDE.TERMINATE, Handle, 0)

'      Log("   Terminate Sent)" & NewLine)
'    Finally
'    End Try
'  End Sub

'  Private Sub PostACK(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal Response As DDEmessageFilter.ACKTYPES, ByVal AppReturnCode As Byte)

'    Dim AckData As DDEACK
'    Dim ItemAtom As Short
'    Dim hAckData As System.IntPtr
'    Dim AckDataPtr As System.IntPtr
'    Dim PostResult As Integer = 1

'    Try
'      ItemAtom = GlobalAddAtom(Item)
'      AckData.bAppReturnCode = AppReturnCode
'      Select Case Response
'        Case DDEmessageFilter.ACKTYPES.OK
'          AckData.fAck = True
'        Case DDEmessageFilter.ACKTYPES.Busy
'          AckData.fBusy = True
'        Case Else
'          'AckData.fAck = False
'          'AckData.fBusy = False
'      End Select

'      hAckData = AllocHGlobal(SizeOf(AckData))
'      AckDataPtr = GlobalLock(hAckData)
'      StructureToPtr(AckData, AckDataPtr, False)

'      PostResult = PostMessage(hWnd, WM_DDE.ACK, Handle, PackDDElParam(WM_DDE.ACK, hAckData, ItemAtom))

'      Log("   ACK sent(" & IIf(AckData.fAck, "ACK", "NACK") & ", " & AckData.bAppReturnCode & ")" & NewLine)

'    Finally
'      If ItemAtom <> 0 Then GlobalDeleteAtom(ItemAtom)
'      With System.IntPtr.Zero
'        If Not .Equals(hAckData) Then
'          If Not .Equals(AckDataPtr) Then GlobalUnlock(hAckData)
'          If PostResult <> 0 Then FreeHGlobal(hAckData)
'        End If
'      End With
'    End Try
'  End Sub

'  Private Sub PostExecACK(ByVal hWnd As System.IntPtr, ByVal hCommand As System.IntPtr, ByVal Response As DDEmessageFilter.ACKTYPES, ByVal AppReturnCode As Byte)

'    Dim AckData As DDEACK
'    Dim hAckData As System.IntPtr
'    Dim AckDataPtr As System.IntPtr
'    Dim PostResult As Integer = 1

'    Try
'      AckData.bAppReturnCode = AppReturnCode
'      Select Case Response
'        Case DDEmessageFilter.ACKTYPES.OK
'          AckData.fAck = True
'        Case DDEmessageFilter.ACKTYPES.Busy
'          AckData.fBusy = True
'        Case Else
'          'AckData.fAck = False
'          'AckData.fBusy = False
'      End Select

'      hAckData = AllocHGlobal(SizeOf(AckData))
'      AckDataPtr = GlobalLock(hAckData)
'      StructureToPtr(AckData, AckDataPtr, False)

'      PostResult = PostMessage(hWnd, WM_DDE.ACK, Handle, PackDDElParam(WM_DDE.ACK, hAckData, hCommand))

'      Log("   ACK sent(" & IIf(AckData.fAck, "ACK", "NACK") & ", " & AckData.bAppReturnCode & ")" & NewLine)
'    Finally
'      With System.IntPtr.Zero
'        If Not .Equals(hAckData) Then
'          If Not .Equals(AckDataPtr) Then GlobalUnlock(hAckData)
'          If PostResult <> 0 Then FreeHGlobal(hAckData)
'        End If
'      End With
'    End Try
'  End Sub
'#End Region


'  ' Order corresponds to dde.h - don't change!
'  Friend Enum WM_DDE As Integer
'    FIRST = &H3E0
'    INITIATE = FIRST
'    TERMINATE
'    ADVISE
'    UNADVISE
'    ACK
'    DATA
'    REQUEST
'    POKE
'    EXECUTE
'    LAST = EXECUTE

'  End Enum

'  Public Shared Function IsDDEMsg(ByVal Msg As Integer) As Boolean
'    'Return System.Enum.IsDefined(GetType(WM_DDE), Msg)
'    Return (Msg And Not &HF) = WM_DDE.FIRST
'  End Function

'#Region "DDE Communications Structures"
'  'typedef struct { 
'  '    unsigned short reserved:12, 'MEH adjusted from 14 to compare to DDEDATA
'  '        reserved:1, 
'  '        reserved:1, 
'  '        fDeferUpd:1, 
'  '        fAckReq:1; 
'  '    short cfFormat; 
'  '} DDEADVISE;
'  'typedef struct { 
'  '    unsigned short unused:12, 
'  '        fResponse:1, 
'  '        fRelease:1, 
'  '        reserved:1, 
'  '        fAckReq:1; 
'  '    short cfFormat; 
'  '    BYTE  Value[]; 
'  '} DDEDATA; 

'  <System.Runtime.InteropServices.StructLayout(Sequential)> Private Structure DDEACKPREFIX
'    Private Flags As Short 'PREFIXFlags

'    <System.Flags()> Private Enum PREFIXFlags As Short
'      AckCodeMask = &HFFS
'      Response = &H1000S
'      Release = &H2000S
'      DeferUpd = &H4000S ' ACK:Busy
'      AckReq = &H8000S  ' ACK:Ack
'    End Enum
'    Private Property fFlag(ByVal prefixFlag As PREFIXFlags) As Boolean
'      Get
'        Return (Flags And prefixFlag) <> 0S
'      End Get
'      Set(ByVal Value As Boolean)
'        If Value Then
'          Flags = (Flags Or prefixFlag)
'        Else
'          Flags = (Flags And Not prefixFlag)
'        End If
'      End Set
'    End Property

'    Public Property fResponse() As Boolean
'      Get
'        Return (fFlag(PREFIXFlags.Response))
'      End Get
'      Set(ByVal Value As Boolean)
'        fFlag(PREFIXFlags.Response) = Value
'      End Set
'    End Property
'    Public Property fRelease() As Boolean
'      Get
'        Return fFlag(PREFIXFlags.Release)
'      End Get
'      Set(ByVal Value As Boolean)
'        fFlag(PREFIXFlags.Release) = Value
'      End Set
'    End Property
'    Public Property fDeferUpd() As Boolean
'      Get
'        Return fFlag(PREFIXFlags.DeferUpd)
'      End Get
'      Set(ByVal Value As Boolean)
'        fFlag(PREFIXFlags.DeferUpd) = Value
'      End Set
'    End Property
'    Public Property fAckReq() As Boolean
'      Get
'        Return fFlag(PREFIXFlags.AckReq)
'      End Get
'      Set(ByVal Value As Boolean)
'        fFlag(PREFIXFlags.AckReq) = Value
'      End Set
'    End Property

'    Public Property bAppReturnCode() As Byte

'      Get
'        Return CByte(Flags And PREFIXFlags.AckCodeMask)
'      End Get
'      Set(ByVal Value As Byte)
'        Flags = (CShort(Value) And PREFIXFlags.AckCodeMask) Or (Flags And Not PREFIXFlags.AckCodeMask)
'      End Set
'    End Property
'  End Structure

'  <System.Runtime.InteropServices.StructLayout(Sequential)> Public Structure DDEACK
'    Private Prefix As DDEACKPREFIX
'    Public Property fBusy() As Boolean
'      Get
'        Return Prefix.fDeferUpd
'      End Get
'      Set(ByVal Value As Boolean)
'        Prefix.fDeferUpd = Value
'      End Set
'    End Property
'    Public Property fAck() As Boolean
'      Get
'        Return Prefix.fAckReq
'      End Get
'      Set(ByVal Value As Boolean)
'        Prefix.fAckReq = Value
'      End Set
'    End Property
'    Public Property bAppReturnCode() As Byte
'      Get
'        Return Prefix.bAppReturnCode
'      End Get
'      Set(ByVal Value As Byte)
'        Prefix.bAppReturnCode = Value
'      End Set
'    End Property
'  End Structure

'  <System.Runtime.InteropServices.StructLayout(Sequential)> Private Structure DDEDATAPREFIX
'    Public Flags As DDEACKPREFIX
'    Private Format As Short
'    Public Property cfFormat() As Format
'      Get
'        Return GetFormat(Format)
'      End Get
'      Set(ByVal Value As Format)
'        Format = CShort(Value.Id)
'      End Set
'    End Property
'  End Structure

'  <System.Runtime.InteropServices.StructLayout(Sequential)> Public Structure DDEADVISE
'    Private Prefix As DDEDATAPREFIX
'    Public Property fDeferUpd() As Boolean
'      Get
'        Return Prefix.Flags.fDeferUpd
'      End Get
'      Set(ByVal Value As Boolean)
'        Prefix.Flags.fDeferUpd = Value
'      End Set
'    End Property
'    Public Property fAckReq() As Boolean
'      Get
'        Return Prefix.Flags.fAckReq
'      End Get
'      Set(ByVal Value As Boolean)
'        Prefix.Flags.fAckReq = Value
'      End Set
'    End Property
'    Public Property cfFormat() As Format
'      Get
'        Return Prefix.cfFormat
'      End Get
'      Set(ByVal Value As Format)
'        Prefix.cfFormat = Value
'      End Set
'    End Property
'  End Structure

'  <System.Runtime.InteropServices.StructLayout(Sequential)> Public Structure DDEDATA
'    Private Prefix As DDEDATAPREFIX
'    Public Property fResponse() As Boolean
'      Get
'        Return Prefix.Flags.fResponse
'      End Get
'      Set(ByVal Value As Boolean)
'        Prefix.Flags.fResponse = Value
'      End Set
'    End Property
'    Public Property fRelease() As Boolean
'      Get
'        Return Prefix.Flags.fRelease
'      End Get
'      Set(ByVal Value As Boolean)
'        Prefix.Flags.fRelease = Value
'      End Set
'    End Property
'    Public Property fAckReq() As Boolean
'      Get
'        Return Prefix.Flags.fAckReq
'      End Get
'      Set(ByVal Value As Boolean)
'        Prefix.Flags.fAckReq = Value
'      End Set
'    End Property
'    Public Property cfFormat() As Format
'      Get
'        Return Prefix.cfFormat
'      End Get
'      Set(ByVal Value As Format)
'        Prefix.cfFormat = Value
'      End Set
'    End Property
'  End Structure

'  <System.Runtime.InteropServices.StructLayout(Sequential)> Public Structure DDEPOKE
'    Private Prefix As DDEDATAPREFIX
'    Public Property fRelease() As Boolean
'      Get
'        Return Prefix.Flags.fRelease
'      End Get
'      Set(ByVal Value As Boolean)
'        Prefix.Flags.fRelease = Value
'      End Set
'    End Property
'    Public Property cfFormat() As Format
'      Get
'        Return Prefix.cfFormat
'      End Get
'      Set(ByVal Value As Format)
'        Prefix.cfFormat = Value
'      End Set
'    End Property
'  End Structure
'#End Region

'#Region "API Declarations"
'  'Friend Declare Function GlobalLock Lib "Kernel32" (ByVal hMem As Integer) As System.IntPtr
'  'Friend Declare Function GlobalUnlock Lib "Kernel32" (ByVal hMem As Integer) As System.IntPtr
'  Friend Declare Function GlobalLock Lib "Kernel32" (ByVal hMem As System.IntPtr) As System.IntPtr
'  Friend Declare Function GlobalUnlock Lib "Kernel32" (ByVal hMem As System.IntPtr) As System.IntPtr

'  ' "For Posted DDE messages"...
'  'Friend Declare Function UnpackDDElParam Lib "User32" (ByVal msg As Integer, ByVal lParam As Integer, ByRef LowWord As Short, ByRef HighWord As Short) As Integer
'  'Friend Declare Function UnpackDDElParam Lib "User32" (ByVal msg As Integer, ByVal lParam As Integer, ByRef LowWord As System.IntPtr, ByRef HighWord As Short) As Integer
'  Friend Declare Function UnpackDDElParam Lib "User32" (ByVal msg As Integer, ByVal lParam As System.IntPtr, ByRef LowWord As Short, ByRef HighWord As Short) As Integer
'  Friend Declare Function UnpackDDElParam Lib "User32" (ByVal msg As Integer, ByVal lParam As System.IntPtr, ByRef LowWord As System.IntPtr, ByRef HighWord As Short) As Integer

'#If False Then
'          ' As defined in API documentation
'          'Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByRef LowWord As Short, ByRef HighWord As Short) As Integer
'          Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByRef LowWord As System.IntPtr, ByRef HighWord As Short) As Integer
'          'Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByRef LowWord As Short, ByRef HighWord As System.IntPtr) As Integer
'          Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByRef LowWord As System.IntPtr, ByRef HighWord As System.IntPtr) As Integer
'          'Friend Declare Function ReuseDDElParam Lib "User32" (ByVal lParam As System.IntPtr, ByVal msgIn As Integer, ByVal msgOut As Integer, ByRef LowWord As Short, ByRef HighWord As Short) As Integer
'          'Friend Declare Function ReuseDDElParam Lib "User32" (ByVal lParam As Integer, ByVal msgIn As Integer, ByVal msgOut As Integer, ByRef LowWord As Short, ByRef HighWord As Short) As Integer
'#Else
'  ' As makes sense
'  Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByVal LowWord As Short, ByVal HighWord As Short) As Integer
'  Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByVal LowWord As System.IntPtr, ByVal HighWord As Short) As Integer
'  'Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByVal LowWord As Short, ByVal HighWord As System.IntPtr) As Integer
'  Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByVal LowWord As System.IntPtr, ByVal HighWord As System.IntPtr) As Integer
'  'Friend Declare Function ReuseDDElParam Lib "User32" (ByVal lParam As System.IntPtr, ByVal msgIn As Integer, ByVal msgOut As Integer, ByVal LowWord As Short, ByVal HighWord As Short) As Integer
'  'Friend Declare Function ReuseDDElParam Lib "User32" (ByVal lParam As Integer, ByVal msgIn As Integer, ByVal msgOut As Integer, ByVal LowWord As Short, ByVal HighWord As Short) As Integer
'#End If

'  Friend Declare Function FreeDDElParam Lib "User32" (ByVal msg As Integer, ByVal lParam As System.IntPtr) As Integer
'  'Friend Declare Function FreeDDElParam Lib "User32" (ByVal msg As Integer, ByVal lParam As Integer) As Integer

'  Public ReadOnly BROADCAST As System.IntPtr = New System.IntPtr(-1)  ' Used for hWnd parameter below

'  Friend Overloads Declare Ansi Function SendMessage Lib "User32" Alias "SendMessageA" (ByVal hWnd As System.IntPtr, ByVal Msg As Integer, ByVal wParam As System.IntPtr, ByVal lParam As Integer) As Integer
'  Friend Overloads Declare Ansi Function PostMessage Lib "User32" Alias "PostMessageA" (ByVal hWnd As System.IntPtr, ByVal Msg As Integer, ByVal wParam As System.IntPtr, ByVal lParam As Integer) As Integer
'  'Friend Overloads Declare Ansi Function SendMessage Lib "User32" Alias "SendMessageA" (ByVal hWnd As System.IntPtr, ByVal Msg As Integer, ByVal wParam As Integer, <MarshalAs(UnmanagedType.BStr)> ByVal lParam As String) As Integer
'  'Friend Overloads Declare Ansi Function PostMessage Lib "User32" Alias "PostMessageA" (ByVal hWnd As System.IntPtr, ByVal Msg As Integer, ByVal wParam As Integer, <MarshalAs(UnmanagedType.BStr)> ByVal lParam As String) As Integer
'  'Friend Overloads Declare Ansi Function SendMessage Lib "User32" Alias "SendMessageA" (ByVal hWnd As Integer, ByVal Msg As Integer, ByVal wParam As Integer, ByVal lParam As Integer) As Integer
'  'Friend Overloads Declare Ansi Function PostMessage Lib "User32" Alias "PostMessageA" (ByVal hWnd As Integer, ByVal Msg As Integer, ByVal wParam As Integer, ByVal lParam As Integer) As Integer
'  'Friend Overloads Declare Ansi Function SendMessage Lib "User32" Alias "SendMessageA" (ByVal hWnd As Integer, ByVal Msg As Integer, ByVal wParam As Integer, <MarshalAs(UnmanagedType.BStr)> ByVal lParam As String) As Integer
'  'Friend Overloads Declare Ansi Function PostMessage Lib "User32" Alias "PostMessageA" (ByVal hWnd As Integer, ByVal Msg As Integer, ByVal wParam As Integer, <MarshalAs(UnmanagedType.BStr)> ByVal lParam As String) As Integer

'  Friend Declare Auto Function InSendMessage Lib "User32" Alias "InSendMessage" () As Boolean

'  Friend Declare Auto Function IsWindowUnicode Lib "User32" Alias "IsWindowUnicode" (ByVal hWnd As System.IntPtr) As Boolean

'  Friend Declare Ansi Function GlobalAddAtom Lib "Kernel32" Alias "GlobalAddAtomA" (ByVal Buffer As String) As Short
'  Friend Declare Ansi Function GlobalGetAtomName Lib "Kernel32" Alias "GlobalGetAtomNameA" (ByVal Atom As Short, ByVal Buffer As String, ByVal BufferLen As Integer) As Integer
'  Friend Declare Auto Function GlobalDeleteAtom Lib "Kernel32" Alias "GlobalDeleteAtom" (ByVal Atom As Short) As Short

'#End Region

'  Friend Class DDEmessageFilter
'    Implements System.Windows.Forms.IMessageFilter

'    Public hWnd As System.IntPtr

'    Public Enum ACKTYPES
'      OK
'      Busy
'      NACK
'    End Enum


'    Public Event Initiate(ByVal hWnd As System.IntPtr, ByVal App As String, ByVal Topic As String)
'    Public Event InitACK(ByVal hWnd As System.IntPtr, ByVal App As String, ByVal Topic As String)
'    Public Event Terminate(ByVal hWnd As System.IntPtr)
'    Public Event Advise(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal TransportAdvice As DDEADVISE)
'    Public Event UnAdvise(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal Format As Format)
'    Public Event ACK(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal Response As ACKTYPES, ByVal AppReturnCode As Byte)
'    Public Event Data(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal DataInfo As DDEDATA, ByVal DataPtr As System.IntPtr)
'    Public Event Request(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal Format As Format)
'    Public Event Poke(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal PokeInfo As DDEPOKE, ByVal PokePtr As System.IntPtr)
'    Public Event Execute(ByVal hWnd As System.IntPtr, ByVal Command As String, ByVal hCommand As System.IntPtr)

'    Private Function GetAtomString(ByVal Atom As Short) As String

'      If Atom = 0 Then
'        Return Nothing
'      End If

'      Dim BufferLen As Integer
'      Dim Buffer As String

'      BufferLen = 514
'      Buffer = New String(Microsoft.VisualBasic.ChrW(0), BufferLen)
'      BufferLen = GlobalGetAtomName(Atom, Buffer, BufferLen)
'      If BufferLen = 0 Then
'        System.Diagnostics.Trace.WriteLine("GetAtomString failed with DLL error number " & Microsoft.VisualBasic.Err.LastDllError())
'        Return Nothing
'      Else
'        Return Buffer.Substring(0, BufferLen)
'      End If

'    End Function

'    Protected Overridable Function OnInitiate(ByRef m As System.Windows.Forms.Message) As Boolean
'      ' wparam is the client (window) handle
'      ' lparam.low is atom of app
'      ' lparam.high is atom of topic

'      Dim AppAtom As Short
'      Dim App As String
'      Dim TopicAtom As Short
'      Dim Topic As String

'      UnpackDDElParam(m.Msg, m.LParam, AppAtom, TopicAtom)

'      Topic = GetAtomString(TopicAtom)

'      App = GetAtomString(AppAtom)

'      RaiseEvent Initiate(m.WParam, App, Topic)

'      FreeDDElParam(m.Msg, m.LParam)
'      Return True
'    End Function

'    Protected Overridable Function OnTerminate(ByRef m As System.Windows.Forms.Message) As Boolean


'      RaiseEvent Terminate(m.WParam)

'      FreeDDElParam(m.Msg, m.LParam)
'      Return True
'    End Function

'    Protected Overridable Function OnAdvise(ByRef m As System.Windows.Forms.Message) As Boolean
'      ' wparam is the server (window) handle
'      ' lparam.low is handle to DDEADVISE structure
'      ' lparam.high is atom of item name

'      Dim TransportAdvice As DDEADVISE
'      Dim ItemAtom As Short
'      Dim Item As String
'      Dim hMem As System.IntPtr

'      UnpackDDElParam(m.Msg, m.LParam, hMem, ItemAtom)

'      Item = GetAtomString(ItemAtom)

'      TransportAdvice = CType(PtrToStructure(GlobalLock(hMem), TransportAdvice.GetType()), DDEADVISE)

'      RaiseEvent Advise(m.WParam, Item, TransportAdvice)

'      GlobalUnlock(hMem)

'      FreeDDElParam(m.Msg, m.LParam)
'      Return True
'    End Function

'    Protected Overridable Function OnUnAdvise(ByRef m As System.Windows.Forms.Message) As Boolean
'      Dim Format As Format
'      Dim FormatID As Short
'      Dim ItemAtom As Short
'      Dim Item As String

'      UnpackDDElParam(m.Msg, m.LParam, FormatID, ItemAtom)
'      Format = GetFormat(FormatID)
'      Item = GetAtomString(ItemAtom)
'      RaiseEvent UnAdvise(m.WParam, Item, Format)

'      FreeDDElParam(m.Msg, m.LParam)
'      Return True
'    End Function

'    Protected Overridable Function OnAck(ByRef m As System.Windows.Forms.Message) As Boolean

'      ' Need to know what has been sent!
'      ' Specifically:
'      '  If ACK of Initiate, it is two atoms (SENDMSG)
'      If InSendMessage Then
'        Dim AppAtom As Short
'        Dim App As String
'        Dim TopicAtom As Short
'        Dim Topic As String

'        SplitInt(m.LParam.ToInt32, AppAtom, TopicAtom)

'        Topic = GetAtomString(TopicAtom)
'        App = GetAtomString(AppAtom)
'        RaiseEvent InitACK(m.WParam, App, Topic)

'      Else
'        '  If ACK of Execute, it is DDEACK and command string global object (POST)
'        '  Otherwise, it is DDEACK and an atom (POST)
'        Dim ItemAtom As Short
'        Dim Item As String
'        Dim AckData As DDEACK
'        Dim hMem As System.IntPtr

'        UnpackDDElParam(m.Msg, m.LParam, hMem, ItemAtom)

'        Item = GetAtomString(ItemAtom)

'        ' *** Assuming a command string ***

'        AckData = CType(PtrToStructure(GlobalLock(hMem), AckData.GetType), DDEACK)

'        Dim AckType As ACKTYPES = ACKTYPES.NACK

'        If AckData.fAck Then
'          AckType = ACKTYPES.OK
'        ElseIf AckData.fBusy Then
'          AckType = ACKTYPES.Busy
'        End If

'        RaiseEvent ACK(m.WParam, Item, AckType, AckData.bAppReturnCode)

'        GlobalUnlock(hMem)
'        GlobalDeleteAtom(ItemAtom)
'        FreeDDElParam(m.Msg, m.LParam)
'      End If

'      Return True
'    End Function

'    Protected Overridable Function OnData(ByRef m As System.Windows.Forms.Message) As Boolean
'      ' wparam is the server (window) handle
'      ' lparam.low is handle to DDEDATA structure (or NULL)
'      ' lparam.high is atom of item name
'      Dim ItemAtom As Short
'      Dim Item As String
'      Dim hDDEData As System.IntPtr
'      Dim pDDEData As System.IntPtr
'      Dim DDEData As DDEDATA
'      Dim FreeData As Boolean
'      Try
'        UnpackDDElParam(m.Msg, m.LParam, hDDEData, ItemAtom)

'        Item = GetAtomString(ItemAtom)

'        pDDEData = GlobalLock(hDDEData)

'        DDEData = CType(PtrToStructure(pDDEData, DDEData.GetType()), DDEDATA)

'        FreeData = DDEData.fRelease

'        RaiseEvent Data(m.WParam, Item, DDEData, IncPtr(pDDEData, SizeOf(DDEData)))
'      Finally
'        GlobalUnlock(hDDEData)
'        If FreeData Then
'          FreeHGlobal(hDDEData)
'        End If

'        FreeDDElParam(m.Msg, m.LParam)
'      End Try
'      Return True
'    End Function

'    Protected Overridable Function OnRequest(ByRef m As System.Windows.Forms.Message) As Boolean
'      Dim Format As Format
'      Dim FormatID As Short
'      Dim ItemAtom As Short
'      Dim Item As String
'      UnpackDDElParam(m.Msg, m.LParam, FormatID, ItemAtom)
'      Format = GetFormat(FormatID)
'      Item = GetAtomString(ItemAtom)
'      RaiseEvent Request(m.WParam, Item, Format)

'      FreeDDElParam(m.Msg, m.LParam)
'      Return True
'    End Function

'    Protected Overridable Function OnPoke(ByRef m As System.Windows.Forms.Message) As Boolean
'      ' wparam is the client (window) handle
'      ' lparam.low is handle to DDEPOKE structure (or NULL)
'      ' lparam.high is atom of item name
'      Dim DDEPoke As DDEPOKE
'      Dim ItemAtom As Short
'      Dim Item As String
'      Dim hDDEPoke As System.IntPtr
'      Dim pDDEPoke As System.IntPtr
'      UnpackDDElParam(m.Msg, m.LParam, hDDEPoke, ItemAtom)
'      Item = GetAtomString(ItemAtom)

'      pDDEPoke = GlobalLock(hDDEPoke)
'      PtrToStructure(pDDEPoke, DDEPoke)

'      RaiseEvent Poke(m.WParam, Item, DDEPoke, IncPtr(pDDEPoke, SizeOf(DDEPoke)))

'      GlobalUnlock(hDDEPoke)

'      FreeDDElParam(m.Msg, m.LParam)
'      Return True
'    End Function

'    Protected Overridable Function OnExecute(ByRef m As System.Windows.Forms.Message) As Boolean
'      Dim Exec As String

'      If IsWindowUnicode(m.HWnd) AndAlso IsWindowUnicode(m.WParam) Then
'        ' Use/assume unicode
'        Exec = PtrToStringUni(GlobalLock(m.LParam))
'      Else
'        ' Use/assume ansi
'        Exec = PtrToStringAnsi(GlobalLock(m.LParam))
'      End If

'      RaiseEvent Execute(m.WParam, Exec, m.LParam)

'      GlobalUnlock(m.LParam)

'      FreeDDElParam(m.Msg, m.LParam)
'      Return True
'    End Function

'    Public Function PreFilterMessage(ByRef m As System.Windows.Forms.Message) As Boolean _
'        Implements System.Windows.Forms.IMessageFilter.PreFilterMessage

'      Dim Broadcast As Boolean = (Broadcast.Equals(m.HWnd))

'      If (Broadcast OrElse m.HWnd.Equals(hWnd)) AndAlso IsDDEMsg(m.Msg) Then

'        Select Case m.Msg
'          Case WM_DDE.INITIATE
'            Return OnInitiate(m)
'          Case WM_DDE.TERMINATE
'            Return OnTerminate(m)
'          Case WM_DDE.ADVISE
'            Return OnAdvise(m)
'          Case WM_DDE.UNADVISE
'            Return OnUnAdvise(m)
'          Case WM_DDE.ACK
'            Return OnAck(m)
'          Case WM_DDE.DATA
'            Return OnData(m)
'          Case WM_DDE.REQUEST
'            Return OnRequest(m)
'          Case WM_DDE.POKE
'            Return OnPoke(m)
'          Case WM_DDE.EXECUTE
'            Return OnExecute(m)
'          Case Else
'            Return False
'        End Select
'        Return True
'      Else
'        Return False
'      End If

'    End Function

'    Public Sub New(ByVal hWnd As System.IntPtr)
'      MyBase.New()
'      Me.hWnd = hWnd
'    End Sub

'  End Class

'  <System.Runtime.InteropServices.StructLayout(Explicit)> Private Structure CvtLong
'    <System.Runtime.InteropServices.FieldOffset(0)> Public LongValue As Integer
'    <System.Runtime.InteropServices.FieldOffset(0)> Public LoWord As Short
'    <System.Runtime.InteropServices.FieldOffset(2)> Public HiWord As Short
'  End Structure

'  Public Shared Function MakeInt(ByVal LoWord As Short, ByVal HiWord As Short) As Integer
'    Dim Convert As CvtLong
'    Convert.HiWord = HiWord
'    Convert.LoWord = LoWord
'    Return Convert.LongValue
'  End Function

'  Public Shared Sub SplitInt(ByVal Value As Integer, ByRef LoWord As Short, ByRef HiWord As Short)
'    Dim Convert As CvtLong
'    Convert.LongValue = Value
'    HiWord = Convert.HiWord
'    LoWord = Convert.LoWord
'  End Sub

'  Public Shared Function IncPtr(ByVal Ptr As System.IntPtr, ByVal Offset As Integer) As System.IntPtr
'    Return New System.IntPtr(Ptr.ToInt32 + Offset)
'  End Function

'  Private Overloads Function IIf(ByVal Condition As Boolean, ByVal TrueValue As String, ByVal FalseValue As String) As String
'    If Condition Then
'      Return TrueValue
'    Else
'      Return FalseValue
'    End If
'  End Function

'  Private Sub InitializeComponent()
'    Me.components = New System.ComponentModel.Container
'    Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
'    '
'    'Timer1
'    '
'    '
'    'DDEForm_Org
'    '
'    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
'    Me.ClientSize = New System.Drawing.Size(292, 266)
'    Me.Name = "DDEForm_Org"

'  End Sub

'  Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

'  End Sub
'End Class

'Public Class DDE_FormThread

'  ' *****************************************************************************************************
'  ' DDE Form Thread CLASS
'  ' -----------------
'  '
'  ' 
'  '
'  '
'  ' *****************************************************************************************************

'  Inherits ThreadWrapperBase

'  Private _CloseDownFlag As Boolean
'  Private NaplesInitiator As Initiator
'  Private NewRequests As New Queue                ' Queue object for New Requests

'  Private _DDEForm As DDEForm

'  Public Sub New(ByVal pInitiator As Initiator)
'    MyBase.New()

'    NaplesInitiator = pInitiator
'  End Sub

'  Public Property CloseDown() As Boolean
'    Get
'      Return _CloseDownFlag
'    End Get
'    Set(ByVal Value As Boolean)
'      _CloseDownFlag = Value
'    End Set
'  End Property

'  Public ReadOnly Property DDEForm() As DDEForm
'    Get
'      Return _DDEForm
'    End Get
'  End Property

'  Protected Overrides Sub DoTask()

'    Initialise()

'    RunDataServer()

'    TidyUp()

'  End Sub

'  Private Sub Initialise()
'    ' Initialise Data Server

'    _CloseDownFlag = False

'    _DDEForm = New DDEForm
'    _DDEForm.Visible = False
'    '_DDEForm.ControlingThreadWrapper = Me

'  End Sub

'  Private Sub TidyUp()
'    ' On ending the Server

'    _DDEForm.Close()

'  End Sub

'  Private Sub RunDataServer()
'    Application.Run(_DDEForm)
'  End Sub


'End Class



