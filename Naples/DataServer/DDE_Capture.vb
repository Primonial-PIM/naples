Option Strict On
Option Explicit On 
Option Compare Binary

Imports System.Net
Imports System.Net.Sockets
Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.Serialization
Imports System.Threading
Imports System.Windows.Forms
Imports System.Windows.Forms.Clipboard

Imports ScreenDecoders.ScreenDecoders
Imports NaplesGlobals
Imports RenaissanceGlobals

Imports System.Runtime.InteropServices.LayoutKind
Imports System.Runtime.InteropServices.Marshal
Imports System.Windows.Forms.DataFormats


Public Class DDEForm
  Inherits System.Windows.Forms.Form

  Private WithEvents DDEmessages As DDEmessageFilter

  Private DDEhWnd As System.IntPtr
  Private DDEApp As String
  Private DDETopic As String

  Private DataReceived As Boolean
  Private ACKReceived As Integer

  Private _DDETimeout As Long

  Public ReceivedData As String
  Private ReadOnly NewLine As String = System.Environment.NewLine
  Private components As System.ComponentModel.IContainer

  Public LogTextBox As Windows.Forms.TextBox

  Public ReadOnly Property DDE_Window() As System.IntPtr
    Get
      Return DDEhWnd
    End Get
  End Property


  Public Sub New()
    ' Create the form object
    MyBase.New()

    Call InitializeComponent()

    _DDETimeout = 0

    ' Look for DDE messages
    DDEmessages = New DDEmessageFilter(Handle)

    ' Me.SendInitiate("winblp", "bbk")

  End Sub

  Friend Delegate Sub CloseFormDelegate()

  Friend Sub CloseForm()
    If Me.InvokeRequired Then
      Dim d As New CloseFormDelegate(AddressOf CloseForm)
      Me.BeginInvoke(d, New Object() {})
    Else
      Me.Close()
    End If
  End Sub

  Private Sub DDEForm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    PostTerminate()
    Application.Exit()
  End Sub


  Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
    If Not DDEmessages Is Nothing AndAlso DDEmessages.PreFilterMessage(m) Then
      ' Handled
    Else
      MyBase.WndProc(m)
    End If
  End Sub

  Private Sub Log(ByVal pMessage As String)
    Static LogCounter As Integer

    LogCounter += 1
    If Not (LogTextBox Is Nothing) Then
      LogTextBox.Text = LogTextBox.Text & LogCounter.ToString & ", " & pMessage
    End If

  End Sub

  Private Delegate Sub DelegateSendInitiate(ByVal App As String, ByVal Topic As String)

  Friend Sub SendInitiate(ByVal App As String, ByVal Topic As String)
    If Me.InvokeRequired Then
      Dim d As New DelegateSendInitiate(AddressOf SendInitiate)
      Me.Invoke(d, New Object() {CType(App, Object), CType(Topic, Object)})
    Else
      SendInitiate(BROADCAST, App, Topic)
    End If
  End Sub

  'Public Sub SendInitiate(ByVal App As String, ByVal Topic As String)
  '  SendInitiate(BROADCAST, App, Topic)
  'End Sub

  Private Sub SendInitiate(ByVal hWnd As System.IntPtr, ByVal App As String, ByVal Topic As String)

    Dim AppAtom As Short
    Dim TopicAtom As Short

    Try
      AppAtom = GlobalAddAtom(App)
      TopicAtom = GlobalAddAtom(Topic)
      DDEApp = App
      DDETopic = Topic

      Log("   Init send(" & hWnd.ToInt32.ToString("x").ToUpper() & ", " & App & "," & Topic & ")" & NewLine)

      SendMessage(hWnd, WM_DDE.INITIATE, Handle, MakeInt(AppAtom, TopicAtom))

      Log("   Init done(" & hWnd.ToInt32.ToString("x").ToUpper() & ", " & App & "," & Topic & ")" & NewLine)
    Finally
      If AppAtom <> 0 Then GlobalDeleteAtom(AppAtom)
      If TopicAtom <> 0 Then GlobalDeleteAtom(TopicAtom)
    End Try
  End Sub

  Private Delegate Sub DelegatePostRequest(ByVal Item As String)

  Friend Sub PostRequest(ByVal Item As String)
    If Me.InvokeRequired Then
      Dim d As New DelegatePostRequest(AddressOf PostRequest)
      Me.Invoke(d, New Object() {CType(Item, Object)})
    Else
      Me.PostRequest(DDEhWnd, Item)
    End If
  End Sub

  'Public Sub PostRequest(ByVal Item As String)
  '  Me.PostRequest(DDEhWnd, Item)
  'End Sub

  Private Sub PostRequest(ByVal hWnd As System.IntPtr, ByVal Item As String)

    Dim ItemAtom As Short
    Dim FormatID As Short

    Try
      ItemAtom = GlobalAddAtom(Item)

      FormatID = CShort(System.Windows.Forms.DataFormats.GetFormat(System.Windows.Forms.DataFormats.Text).Id)

      DataReceived = False

      If PostMessage(hWnd, WM_DDE.REQUEST, Handle, PackDDElParam(WM_DDE.REQUEST, FormatID, ItemAtom)) = 0 Then
        ' Clear the Item atom so we don't free it
        ItemAtom = 0
      End If

      Log("   Request sent(" & hWnd.ToInt32.ToString("x").ToUpper() & ", " & FormatID & "," & Item & ")" & NewLine)
    Finally
      If ItemAtom <> 0 Then GlobalDeleteAtom(ItemAtom)
    End Try
  End Sub

  Private Delegate Sub DelegatePostExecute(ByVal Item As String)

  Friend Sub PostExecute(ByVal Item As String)
    If Me.InvokeRequired Then
      Dim d As New DelegatePostExecute(AddressOf PostExecute)
      Me.Invoke(d, New Object() {CType(Item, Object)})
    Else
      Me.PostExecute(DDEhWnd, Item)
    End If
  End Sub

  'Public Sub PostExecute(ByVal Item As String)
  '  Me.PostExecute(DDEhWnd, Item)
  'End Sub

  Private Sub PostExecute(ByVal hWnd As System.IntPtr, ByVal Item As String)

    Dim ItemAtom As Short
    Dim FormatID As Short
    Dim rval As Integer
    Dim myIntPtr As System.IntPtr

    FormatID = CShort(System.Windows.Forms.DataFormats.GetFormat(System.Windows.Forms.DataFormats.Text).Id)

    Try
      'ItemAtom = GlobalAddAtom(Item)

      If IsWindowUnicode(Handle) Then
        myIntPtr = StringToHGlobalUni(Item)
      Else
        myIntPtr = StringToHGlobalAnsi(Item)
      End If

      DataReceived = False
      ACKReceived = 0

      'rval = PostMessage(hWnd, WM_DDE.EXECUTE, Handle, PackDDElParam(WM_DDE.EXECUTE, FormatID, ItemAtom))
      rval = PostMessage(hWnd, WM_DDE.EXECUTE, Handle, myIntPtr.ToInt32)
      If rval = 0 Then
        ' Clear the Item atom so we don't free it
        ItemAtom = 0
        'ACKReceived = -1
      End If

      Log("   Execute sent(" & hWnd.ToInt32.ToString("x").ToUpper() & ", " & Item & ")" & NewLine)
    Finally
      If ItemAtom <> 0 Then
        GlobalDeleteAtom(ItemAtom)
      End If
    End Try
  End Sub

  Public Sub WaitForData()
    Dim TimeoutCounter As Long
    Dim TimeoutLimit As Long

    If (_DDETimeout <= 0) Then
      TimeoutLimit = 2000
    Else
      TimeoutLimit = _DDETimeout
    End If

    TimeoutCounter = 0
    Do
      If (DataReceived) Or (TimeoutCounter >= TimeoutLimit) Then Exit Sub
      System.Threading.Thread.Sleep(55)
      System.Windows.Forms.Application.DoEvents()
      TimeoutCounter += 55
    Loop
  End Sub

  Public Sub WaitForAck()
    Dim TimeoutLimit As Long

    If (_DDETimeout <= 0) Then
      TimeoutLimit = 2000
    Else
      TimeoutLimit = _DDETimeout
    End If

    Call WaitForAck(TimeoutLimit)
  End Sub

  Public Sub WaitForAck(ByVal pTimeout As Long)
    Dim TimeoutCounter As Long
    Dim TimeoutLimit As Long

    TimeoutLimit = pTimeout
    TimeoutCounter = 0

    Do
      If (ACKReceived <> 0) Or (TimeoutCounter >= TimeoutLimit) Then Exit Sub
      System.Threading.Thread.Sleep(55)
      System.Windows.Forms.Application.DoEvents()
      TimeoutCounter += 55
    Loop
  End Sub


#Region "DDE (Message Received) Events"

  Private Sub DDEmessages_Initiate(ByVal hWnd As System.IntPtr, ByVal App As String, ByVal Topic As String) Handles DDEmessages.Initiate

    Log("Init(" & hWnd.ToInt32.ToString("x").ToUpper() & ", " & App & "," & Topic & ")" & NewLine)

    If (App.Length = 0 OrElse App.ToUpper() = "DDE.NET") AndAlso (Topic.Length = 0 OrElse Topic.ToLower() = "system") Then
      SendInitACK(hWnd, "DDE.NET", "System")
    End If
  End Sub

  Private Sub DDEmessages_InitACK(ByVal hWnd As System.IntPtr, ByVal App As String, ByVal Topic As String) Handles DDEmessages.InitACK

    Log("InitACK(" & hWnd.ToInt32.ToString("x").ToUpper() & ", " & App & "," & Topic & ") " & NewLine)

    If App.ToUpper = DDEApp.ToUpper And Topic.ToUpper = DDETopic.ToUpper Then
      DDEhWnd = hWnd
    End If
  End Sub

  Private Sub DDEmessages_Terminate(ByVal hWnd As System.IntPtr) Handles DDEmessages.Terminate

    Log("Terminate " & hWnd.ToInt32.ToString("x").ToUpper() & NewLine)

    PostTerminate(hWnd)

  End Sub

  Private Sub DDEmessages_Advise(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal TransportAdvice As DDEADVISE) Handles DDEmessages.Advise

    Log("Advise " & hWnd.ToInt32.ToString("x").ToUpper() & ", Item: " & Item & NewLine _
        & "    " & IIf(TransportAdvice.fAckReq, "AckReq ", "       ") _
        & IIf(TransportAdvice.fDeferUpd, "DeferUpd ", "         ") _
        & TransportAdvice.cfFormat.Name _
        & NewLine)

    If TransportAdvice.fAckReq Then
      PostACK(hWnd, Item, DDEmessageFilter.ACKTYPES.OK, 0)
    End If
  End Sub

  Private Sub DDEmessages_Data(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal DataInfo As DDEDATA, ByVal DataPtr As System.IntPtr) Handles DDEmessages.Data

    Dim result As DDEmessageFilter.ACKTYPES = DDEmessageFilter.ACKTYPES.OK

    Log("Data " & hWnd.ToInt32.ToString("x").ToUpper() & ", Item: " & Item & NewLine _
        & "    " & IIf(DataInfo.fAckReq, "AckReq ", "       ") _
        & IIf(DataInfo.fResponse, "Response ", "         ") _
        & IIf(DataInfo.fRelease, "Release ", "         ") _
        & DataInfo.cfFormat.Name _
        & NewLine)

    Select Case DataInfo.cfFormat.Name
      Case System.Windows.Forms.DataFormats.UnicodeText
        ' Use/assume unicode
        ReceivedData = PtrToStringAnsi(DataPtr)
        Log(ReceivedData & NewLine)

      Case System.Windows.Forms.DataFormats.Text
        ' Use/assume ansi
        ReceivedData = PtrToStringAnsi(DataPtr)
        Log(ReceivedData & NewLine)
      Case Else
        ' Unhandled data
        result = DDEmessageFilter.ACKTYPES.NACK
        ReceivedData = ""
    End Select

    If DataInfo.fAckReq Then
      PostACK(hWnd, Item, result, 0)
    End If
    'If DataInfo.fRelease Then
    '    ' Handled in caller
    'End If
    If DataInfo.fResponse Then
      DataReceived = True
    End If
  End Sub

  Private Sub DDEmessages_Execute(ByVal hWnd As System.IntPtr, ByVal Command As String, ByVal hCommand As System.IntPtr) Handles DDEmessages.Execute

    Log("Execute " & hWnd.ToInt32.ToString("x").ToUpper() & ": " & Command & NewLine)

    ' Always ACK:
    PostExecACK(hWnd, hCommand, DDEmessageFilter.ACKTYPES.OK, 0)
  End Sub

  Private Sub DDEmessages_Poke(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal PokeInfo As DDEPOKE, ByVal PokePtr As System.IntPtr) Handles DDEmessages.Poke

    Log("Poke " & hWnd.ToInt32.ToString("x").ToUpper() & " Item: " & Item & NewLine _
        & "    " & "       " _
        & IIf(PokeInfo.fRelease, "Release ", "         ") _
        & PokeInfo.cfFormat.Name _
        & NewLine)
  End Sub

  Private Sub DDEmessages_Request(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal Format As System.Windows.Forms.DataFormats.Format) Handles DDEmessages.Request

    Log("Request " & hWnd.ToInt32.ToString("x").ToUpper() & " Item: " & Item & NewLine _
        & "    " & "       " _
        & Format.Name _
        & NewLine)
  End Sub

  Private Sub DDEmessages_UnAdvise(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal Format As System.Windows.Forms.DataFormats.Format) Handles DDEmessages.UnAdvise

    Log("UnAdvise " & hWnd.ToInt32.ToString("x").ToUpper() & " Item: " & Item & NewLine _
        & "    " & "       " _
        & Format.Name _
        & NewLine)
  End Sub

  Private Sub DDEmessages_ACK(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal Response As DDEmessageFilter.ACKTYPES, ByVal AppReturnCode As Byte) Handles DDEmessages.ACK

    Log("ACK from " & hWnd.ToInt32.ToString("x").ToUpper() & ", Item: " & Item & NewLine _
        & "    " & " RetCode: " & AppReturnCode.ToString _
        & Response.ToString _
        & NewLine)

    If Response = DDEmessageFilter.ACKTYPES.NACK Then
      ACKReceived = -1
    ElseIf Response = DDEmessageFilter.ACKTYPES.OK Then
      ACKReceived = 1
    ElseIf Response = DDEmessageFilter.ACKTYPES.Busy Then
      ACKReceived = -2
    Else
      ACKReceived = -3
    End If

  End Sub
#End Region

#Region "Responses"

  Private Sub SendInitACK(ByVal hWnd As System.IntPtr, ByVal App As String, ByVal Topic As String)

    Dim AppAtom As Short
    Dim TopicAtom As Short

    Try
      AppAtom = GlobalAddAtom(App)
      TopicAtom = GlobalAddAtom(Topic)
      SendMessage(hWnd, WM_DDE.ACK, Handle, MakeInt(AppAtom, TopicAtom))
      Log("   Init ACK sent(" & App & "," & Topic & ")" & NewLine)
    Finally
      If AppAtom <> 0 Then GlobalDeleteAtom(AppAtom)
      If TopicAtom <> 0 Then GlobalDeleteAtom(TopicAtom)
    End Try
  End Sub

  Private Delegate Sub DelegatePostTerminate()

  Friend Sub PostTerminate()
    If Me.InvokeRequired Then
      Dim d As New DelegatePostTerminate(AddressOf PostTerminate)
      Me.Invoke(d, New Object() {})
    Else
      If Not (DDEhWnd.Equals(IntPtr.Zero)) Then
        PostTerminate(DDEhWnd)
      End If
      DDEhWnd = IntPtr.Zero
    End If
  End Sub

  
  Private Sub PostTerminate(ByVal hWnd As System.IntPtr)

    Try
      PostMessage(hWnd, WM_DDE.TERMINATE, Handle, 0)

      Log("   Terminate Sent)" & NewLine)
    Finally
    End Try
  End Sub

  Private Sub PostACK(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal Response As DDEmessageFilter.ACKTYPES, ByVal AppReturnCode As Byte)

    Dim AckData As DDEACK
    Dim ItemAtom As Short
    Dim hAckData As System.IntPtr
    Dim AckDataPtr As System.IntPtr
    Dim PostResult As Integer = 1

    Try
      ItemAtom = GlobalAddAtom(Item)
      AckData.bAppReturnCode = AppReturnCode
      Select Case Response
        Case DDEmessageFilter.ACKTYPES.OK
          AckData.fAck = True
        Case DDEmessageFilter.ACKTYPES.Busy
          AckData.fBusy = True
        Case Else
          'AckData.fAck = False
          'AckData.fBusy = False
      End Select

      hAckData = AllocHGlobal(SizeOf(AckData))
      AckDataPtr = GlobalLock(hAckData)
      StructureToPtr(AckData, AckDataPtr, False)

      PostResult = PostMessage(hWnd, WM_DDE.ACK, Handle, PackDDElParam(WM_DDE.ACK, hAckData, ItemAtom))

      Log("   ACK sent(" & IIf(AckData.fAck, "ACK", "NACK") & ", " & AckData.bAppReturnCode & ")" & NewLine)

    Finally
      If ItemAtom <> 0 Then GlobalDeleteAtom(ItemAtom)
      With System.IntPtr.Zero
        If Not .Equals(hAckData) Then
          If Not .Equals(AckDataPtr) Then GlobalUnlock(hAckData)
          If PostResult <> 0 Then FreeHGlobal(hAckData)
        End If
      End With
    End Try
  End Sub

  Private Sub PostExecACK(ByVal hWnd As System.IntPtr, ByVal hCommand As System.IntPtr, ByVal Response As DDEmessageFilter.ACKTYPES, ByVal AppReturnCode As Byte)

    Dim AckData As DDEACK
    Dim hAckData As System.IntPtr
    Dim AckDataPtr As System.IntPtr
    Dim PostResult As Integer = 1

    Try
      AckData.bAppReturnCode = AppReturnCode
      Select Case Response
        Case DDEmessageFilter.ACKTYPES.OK
          AckData.fAck = True
        Case DDEmessageFilter.ACKTYPES.Busy
          AckData.fBusy = True
        Case Else
          'AckData.fAck = False
          'AckData.fBusy = False
      End Select

      hAckData = AllocHGlobal(SizeOf(AckData))
      AckDataPtr = GlobalLock(hAckData)
      StructureToPtr(AckData, AckDataPtr, False)

      PostResult = PostMessage(hWnd, WM_DDE.ACK, Handle, PackDDElParam(WM_DDE.ACK, hAckData, hCommand))

      Log("   ACK sent(" & IIf(AckData.fAck, "ACK", "NACK") & ", " & AckData.bAppReturnCode & ")" & NewLine)
    Finally
      With System.IntPtr.Zero
        If Not .Equals(hAckData) Then
          If Not .Equals(AckDataPtr) Then GlobalUnlock(hAckData)
          If PostResult <> 0 Then FreeHGlobal(hAckData)
        End If
      End With
    End Try
  End Sub
#End Region


  ' Order corresponds to dde.h - don't change!
  Friend Enum WM_DDE As Integer
    FIRST = &H3E0
    INITIATE = FIRST
    TERMINATE
    ADVISE
    UNADVISE
    ACK
    DATA
    REQUEST
    POKE
    EXECUTE
    LAST = EXECUTE

  End Enum

  Public Shared Function IsDDEMsg(ByVal Msg As Integer) As Boolean
    'Return System.Enum.IsDefined(GetType(WM_DDE), Msg)
    Return (Msg And Not &HF) = WM_DDE.FIRST
  End Function

#Region "DDE Communications Structures"
  'typedef struct { 
  '    unsigned short reserved:12, 'MEH adjusted from 14 to compare to DDEDATA
  '        reserved:1, 
  '        reserved:1, 
  '        fDeferUpd:1, 
  '        fAckReq:1; 
  '    short cfFormat; 
  '} DDEADVISE;
  'typedef struct { 
  '    unsigned short unused:12, 
  '        fResponse:1, 
  '        fRelease:1, 
  '        reserved:1, 
  '        fAckReq:1; 
  '    short cfFormat; 
  '    BYTE  Value[]; 
  '} DDEDATA; 

  <System.Runtime.InteropServices.StructLayout(Sequential)> Private Structure DDEACKPREFIX
    Private Flags As Short 'PREFIXFlags

    <System.Flags()> Private Enum PREFIXFlags As Short
      AckCodeMask = &HFFS
      Response = &H1000S
      Release = &H2000S
      DeferUpd = &H4000S ' ACK:Busy
      AckReq = &H8000S  ' ACK:Ack
    End Enum
    Private Property fFlag(ByVal prefixFlag As PREFIXFlags) As Boolean
      Get
        Return (Flags And prefixFlag) <> 0S
      End Get
      Set(ByVal Value As Boolean)
        If Value Then
          Flags = (Flags Or prefixFlag)
        Else
          Flags = (Flags And Not prefixFlag)
        End If
      End Set
    End Property

    Public Property fResponse() As Boolean
      Get
        Return (fFlag(PREFIXFlags.Response))
      End Get
      Set(ByVal Value As Boolean)
        fFlag(PREFIXFlags.Response) = Value
      End Set
    End Property
    Public Property fRelease() As Boolean
      Get
        Return fFlag(PREFIXFlags.Release)
      End Get
      Set(ByVal Value As Boolean)
        fFlag(PREFIXFlags.Release) = Value
      End Set
    End Property
    Public Property fDeferUpd() As Boolean
      Get
        Return fFlag(PREFIXFlags.DeferUpd)
      End Get
      Set(ByVal Value As Boolean)
        fFlag(PREFIXFlags.DeferUpd) = Value
      End Set
    End Property
    Public Property fAckReq() As Boolean
      Get
        Return fFlag(PREFIXFlags.AckReq)
      End Get
      Set(ByVal Value As Boolean)
        fFlag(PREFIXFlags.AckReq) = Value
      End Set
    End Property

    Public Property bAppReturnCode() As Byte

      Get
        Return CByte(Flags And PREFIXFlags.AckCodeMask)
      End Get
      Set(ByVal Value As Byte)
        Flags = (CShort(Value) And PREFIXFlags.AckCodeMask) Or (Flags And Not PREFIXFlags.AckCodeMask)
      End Set
    End Property
  End Structure

  <System.Runtime.InteropServices.StructLayout(Sequential)> Public Structure DDEACK
    Private Prefix As DDEACKPREFIX
    Public Property fBusy() As Boolean
      Get
        Return Prefix.fDeferUpd
      End Get
      Set(ByVal Value As Boolean)
        Prefix.fDeferUpd = Value
      End Set
    End Property
    Public Property fAck() As Boolean
      Get
        Return Prefix.fAckReq
      End Get
      Set(ByVal Value As Boolean)
        Prefix.fAckReq = Value
      End Set
    End Property
    Public Property bAppReturnCode() As Byte
      Get
        Return Prefix.bAppReturnCode
      End Get
      Set(ByVal Value As Byte)
        Prefix.bAppReturnCode = Value
      End Set
    End Property
  End Structure

  <System.Runtime.InteropServices.StructLayout(Sequential)> Private Structure DDEDATAPREFIX
    Public Flags As DDEACKPREFIX
    Private Format As Short
    Public Property cfFormat() As Format
      Get
        Return GetFormat(Format)
      End Get
      Set(ByVal Value As Format)
        Format = CShort(Value.Id)
      End Set
    End Property
  End Structure

  <System.Runtime.InteropServices.StructLayout(Sequential)> Public Structure DDEADVISE
    Private Prefix As DDEDATAPREFIX
    Public Property fDeferUpd() As Boolean
      Get
        Return Prefix.Flags.fDeferUpd
      End Get
      Set(ByVal Value As Boolean)
        Prefix.Flags.fDeferUpd = Value
      End Set
    End Property
    Public Property fAckReq() As Boolean
      Get
        Return Prefix.Flags.fAckReq
      End Get
      Set(ByVal Value As Boolean)
        Prefix.Flags.fAckReq = Value
      End Set
    End Property
    Public Property cfFormat() As Format
      Get
        Return Prefix.cfFormat
      End Get
      Set(ByVal Value As Format)
        Prefix.cfFormat = Value
      End Set
    End Property
  End Structure

  <System.Runtime.InteropServices.StructLayout(Sequential)> Public Structure DDEDATA
    Private Prefix As DDEDATAPREFIX
    Public Property fResponse() As Boolean
      Get
        Return Prefix.Flags.fResponse
      End Get
      Set(ByVal Value As Boolean)
        Prefix.Flags.fResponse = Value
      End Set
    End Property
    Public Property fRelease() As Boolean
      Get
        Return Prefix.Flags.fRelease
      End Get
      Set(ByVal Value As Boolean)
        Prefix.Flags.fRelease = Value
      End Set
    End Property
    Public Property fAckReq() As Boolean
      Get
        Return Prefix.Flags.fAckReq
      End Get
      Set(ByVal Value As Boolean)
        Prefix.Flags.fAckReq = Value
      End Set
    End Property
    Public Property cfFormat() As Format
      Get
        Return Prefix.cfFormat
      End Get
      Set(ByVal Value As Format)
        Prefix.cfFormat = Value
      End Set
    End Property
  End Structure

  <System.Runtime.InteropServices.StructLayout(Sequential)> Public Structure DDEPOKE
    Private Prefix As DDEDATAPREFIX
    Public Property fRelease() As Boolean
      Get
        Return Prefix.Flags.fRelease
      End Get
      Set(ByVal Value As Boolean)
        Prefix.Flags.fRelease = Value
      End Set
    End Property
    Public Property cfFormat() As Format
      Get
        Return Prefix.cfFormat
      End Get
      Set(ByVal Value As Format)
        Prefix.cfFormat = Value
      End Set
    End Property
  End Structure
#End Region

#Region "API Declarations"

  Friend Declare Function GetWinEnv Lib "User32" Alias "GetSystemMetrics" (ByVal int1 As Integer) As Integer

  Friend Declare Function IsWindow Lib "user32" (ByVal hWnd As System.IntPtr) As System.Boolean

  'Friend Declare Function GlobalLock Lib "Kernel32" (ByVal hMem As Integer) As System.IntPtr
  'Friend Declare Function GlobalUnlock Lib "Kernel32" (ByVal hMem As Integer) As System.IntPtr
  Friend Declare Function GlobalLock Lib "Kernel32" (ByVal hMem As System.IntPtr) As System.IntPtr
  Friend Declare Function GlobalUnlock Lib "Kernel32" (ByVal hMem As System.IntPtr) As System.IntPtr

  ' "For Posted DDE messages"...
  'Friend Declare Function UnpackDDElParam Lib "User32" (ByVal msg As Integer, ByVal lParam As Integer, ByRef LowWord As Short, ByRef HighWord As Short) As Integer
  'Friend Declare Function UnpackDDElParam Lib "User32" (ByVal msg As Integer, ByVal lParam As Integer, ByRef LowWord As System.IntPtr, ByRef HighWord As Short) As Integer
  Friend Declare Function UnpackDDElParam Lib "User32" (ByVal msg As Integer, ByVal lParam As System.IntPtr, ByRef LowWord As Short, ByRef HighWord As Short) As Integer
  Friend Declare Function UnpackDDElParam Lib "User32" (ByVal msg As Integer, ByVal lParam As System.IntPtr, ByRef LowWord As System.IntPtr, ByRef HighWord As Short) As Integer

#If False Then
            ' As defined in API documentation
            'Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByRef LowWord As Short, ByRef HighWord As Short) As Integer
            Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByRef LowWord As System.IntPtr, ByRef HighWord As Short) As Integer
            'Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByRef LowWord As Short, ByRef HighWord As System.IntPtr) As Integer
            Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByRef LowWord As System.IntPtr, ByRef HighWord As System.IntPtr) As Integer
            'Friend Declare Function ReuseDDElParam Lib "User32" (ByVal lParam As System.IntPtr, ByVal msgIn As Integer, ByVal msgOut As Integer, ByRef LowWord As Short, ByRef HighWord As Short) As Integer
            'Friend Declare Function ReuseDDElParam Lib "User32" (ByVal lParam As Integer, ByVal msgIn As Integer, ByVal msgOut As Integer, ByRef LowWord As Short, ByRef HighWord As Short) As Integer
#Else
  ' As makes sense
  Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByVal LowWord As Short, ByVal HighWord As Short) As Integer
  Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByVal LowWord As System.IntPtr, ByVal HighWord As Short) As Integer
  'Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByVal LowWord As Short, ByVal HighWord As System.IntPtr) As Integer
  Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByVal LowWord As System.IntPtr, ByVal HighWord As System.IntPtr) As Integer
  'Friend Declare Function ReuseDDElParam Lib "User32" (ByVal lParam As System.IntPtr, ByVal msgIn As Integer, ByVal msgOut As Integer, ByVal LowWord As Short, ByVal HighWord As Short) As Integer
  'Friend Declare Function ReuseDDElParam Lib "User32" (ByVal lParam As Integer, ByVal msgIn As Integer, ByVal msgOut As Integer, ByVal LowWord As Short, ByVal HighWord As Short) As Integer
#End If

  Friend Declare Function FreeDDElParam Lib "User32" (ByVal msg As Integer, ByVal lParam As System.IntPtr) As Integer
  'Friend Declare Function FreeDDElParam Lib "User32" (ByVal msg As Integer, ByVal lParam As Integer) As Integer

  Public ReadOnly BROADCAST As System.IntPtr = New System.IntPtr(-1)  ' Used for hWnd parameter below

  Friend Overloads Declare Ansi Function SendMessage Lib "User32" Alias "SendMessageA" (ByVal hWnd As System.IntPtr, ByVal Msg As Integer, ByVal wParam As System.IntPtr, ByVal lParam As Integer) As Integer
  Friend Overloads Declare Ansi Function PostMessage Lib "User32" Alias "PostMessageA" (ByVal hWnd As System.IntPtr, ByVal Msg As Integer, ByVal wParam As System.IntPtr, ByVal lParam As Integer) As Integer
  'Friend Overloads Declare Ansi Function SendMessage Lib "User32" Alias "SendMessageA" (ByVal hWnd As System.IntPtr, ByVal Msg As Integer, ByVal wParam As Integer, <MarshalAs(UnmanagedType.BStr)> ByVal lParam As String) As Integer
  'Friend Overloads Declare Ansi Function PostMessage Lib "User32" Alias "PostMessageA" (ByVal hWnd As System.IntPtr, ByVal Msg As Integer, ByVal wParam As Integer, <MarshalAs(UnmanagedType.BStr)> ByVal lParam As String) As Integer
  'Friend Overloads Declare Ansi Function SendMessage Lib "User32" Alias "SendMessageA" (ByVal hWnd As Integer, ByVal Msg As Integer, ByVal wParam As Integer, ByVal lParam As Integer) As Integer
  'Friend Overloads Declare Ansi Function PostMessage Lib "User32" Alias "PostMessageA" (ByVal hWnd As Integer, ByVal Msg As Integer, ByVal wParam As Integer, ByVal lParam As Integer) As Integer
  'Friend Overloads Declare Ansi Function SendMessage Lib "User32" Alias "SendMessageA" (ByVal hWnd As Integer, ByVal Msg As Integer, ByVal wParam As Integer, <MarshalAs(UnmanagedType.BStr)> ByVal lParam As String) As Integer
  'Friend Overloads Declare Ansi Function PostMessage Lib "User32" Alias "PostMessageA" (ByVal hWnd As Integer, ByVal Msg As Integer, ByVal wParam As Integer, <MarshalAs(UnmanagedType.BStr)> ByVal lParam As String) As Integer

  Friend Declare Auto Function InSendMessage Lib "User32" Alias "InSendMessage" () As Boolean

  Friend Declare Auto Function IsWindowUnicode Lib "User32" Alias "IsWindowUnicode" (ByVal hWnd As System.IntPtr) As Boolean

  Friend Declare Ansi Function GlobalAddAtom Lib "Kernel32" Alias "GlobalAddAtomA" (ByVal Buffer As String) As Short
  Friend Declare Ansi Function GlobalGetAtomName Lib "Kernel32" Alias "GlobalGetAtomNameA" (ByVal Atom As Short, ByVal Buffer As String, ByVal BufferLen As Integer) As Integer
  Friend Declare Auto Function GlobalDeleteAtom Lib "Kernel32" Alias "GlobalDeleteAtom" (ByVal Atom As Short) As Short

#End Region

  Friend Class DDEmessageFilter
    Implements System.Windows.Forms.IMessageFilter

    Public hWnd As System.IntPtr

    Public Enum ACKTYPES
      OK
      Busy
      NACK
    End Enum


    Public Event Initiate(ByVal hWnd As System.IntPtr, ByVal App As String, ByVal Topic As String)
    Public Event InitACK(ByVal hWnd As System.IntPtr, ByVal App As String, ByVal Topic As String)
    Public Event Terminate(ByVal hWnd As System.IntPtr)
    Public Event Advise(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal TransportAdvice As DDEADVISE)
    Public Event UnAdvise(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal Format As Format)
    Public Event ACK(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal Response As ACKTYPES, ByVal AppReturnCode As Byte)
    Public Event Data(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal DataInfo As DDEDATA, ByVal DataPtr As System.IntPtr)
    Public Event Request(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal Format As Format)
    Public Event Poke(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal PokeInfo As DDEPOKE, ByVal PokePtr As System.IntPtr)
    Public Event Execute(ByVal hWnd As System.IntPtr, ByVal Command As String, ByVal hCommand As System.IntPtr)

    Private Function GetAtomString(ByVal Atom As Short) As String

      If Atom = 0 Then
        Return Nothing
      End If

      Dim BufferLen As Integer
      Dim Buffer As String

      BufferLen = 514
      Buffer = New String(Microsoft.VisualBasic.ChrW(0), BufferLen)
      BufferLen = GlobalGetAtomName(Atom, Buffer, BufferLen)
      If BufferLen = 0 Then
        System.Diagnostics.Trace.WriteLine("GetAtomString failed with DLL error number " & Microsoft.VisualBasic.Err.LastDllError())
        Return Nothing
      Else
        Return Buffer.Substring(0, BufferLen)
      End If

    End Function

    Protected Overridable Function OnInitiate(ByRef m As System.Windows.Forms.Message) As Boolean
      ' wparam is the client (window) handle
      ' lparam.low is atom of app
      ' lparam.high is atom of topic

      Dim AppAtom As Short
      Dim App As String
      Dim TopicAtom As Short
      Dim Topic As String

      UnpackDDElParam(m.Msg, m.LParam, AppAtom, TopicAtom)

      Topic = GetAtomString(TopicAtom)

      App = GetAtomString(AppAtom)

      RaiseEvent Initiate(m.WParam, App, Topic)

      FreeDDElParam(m.Msg, m.LParam)
      Return True
    End Function

    Protected Overridable Function OnTerminate(ByRef m As System.Windows.Forms.Message) As Boolean


      RaiseEvent Terminate(m.WParam)

      FreeDDElParam(m.Msg, m.LParam)
      Return True
    End Function

    Protected Overridable Function OnAdvise(ByRef m As System.Windows.Forms.Message) As Boolean
      ' wparam is the server (window) handle
      ' lparam.low is handle to DDEADVISE structure
      ' lparam.high is atom of item name

      Dim TransportAdvice As DDEADVISE
      Dim ItemAtom As Short
      Dim Item As String
      Dim hMem As System.IntPtr

      UnpackDDElParam(m.Msg, m.LParam, hMem, ItemAtom)

      Item = GetAtomString(ItemAtom)

      TransportAdvice = CType(PtrToStructure(GlobalLock(hMem), TransportAdvice.GetType()), DDEADVISE)

      RaiseEvent Advise(m.WParam, Item, TransportAdvice)

      GlobalUnlock(hMem)

      FreeDDElParam(m.Msg, m.LParam)
      Return True
    End Function

    Protected Overridable Function OnUnAdvise(ByRef m As System.Windows.Forms.Message) As Boolean
      Dim Format As Format
      Dim FormatID As Short
      Dim ItemAtom As Short
      Dim Item As String

      UnpackDDElParam(m.Msg, m.LParam, FormatID, ItemAtom)
      Format = GetFormat(FormatID)
      Item = GetAtomString(ItemAtom)
      RaiseEvent UnAdvise(m.WParam, Item, Format)

      FreeDDElParam(m.Msg, m.LParam)
      Return True
    End Function

    Protected Overridable Function OnAck(ByRef m As System.Windows.Forms.Message) As Boolean

      ' Need to know what has been sent!
      ' Specifically:
      '  If ACK of Initiate, it is two atoms (SENDMSG)
      If InSendMessage Then
        Dim AppAtom As Short
        Dim App As String
        Dim TopicAtom As Short
        Dim Topic As String

        SplitInt(m.LParam.ToInt32, AppAtom, TopicAtom)

        Topic = GetAtomString(TopicAtom)
        App = GetAtomString(AppAtom)
        RaiseEvent InitACK(m.WParam, App, Topic)

      Else
        '  If ACK of Execute, it is DDEACK and command string global object (POST)
        '  Otherwise, it is DDEACK and an atom (POST)
        Dim ItemAtom As Short
        Dim Item As String
        Dim AckData As DDEACK
        Dim hMem As System.IntPtr

        UnpackDDElParam(m.Msg, m.LParam, hMem, ItemAtom)

        Item = GetAtomString(ItemAtom)

        ' *** Assuming a command string ***

        AckData = CType(PtrToStructure(GlobalLock(hMem), AckData.GetType), DDEACK)

        Dim AckType As ACKTYPES = ACKTYPES.NACK

        If AckData.fAck Then
          AckType = ACKTYPES.OK
        ElseIf AckData.fBusy Then
          AckType = ACKTYPES.Busy
        End If

        RaiseEvent ACK(m.WParam, Item, AckType, AckData.bAppReturnCode)

        GlobalUnlock(hMem)
        GlobalDeleteAtom(ItemAtom)
        FreeDDElParam(m.Msg, m.LParam)
      End If

      Return True
    End Function

    Protected Overridable Function OnData(ByRef m As System.Windows.Forms.Message) As Boolean
      ' wparam is the server (window) handle
      ' lparam.low is handle to DDEDATA structure (or NULL)
      ' lparam.high is atom of item name
      Dim ItemAtom As Short
      Dim Item As String
      Dim hDDEData As System.IntPtr
      Dim pDDEData As System.IntPtr
      Dim DDEData As DDEDATA
      Dim FreeData As Boolean
      Try
        UnpackDDElParam(m.Msg, m.LParam, hDDEData, ItemAtom)

        Item = GetAtomString(ItemAtom)

        pDDEData = GlobalLock(hDDEData)

        DDEData = CType(PtrToStructure(pDDEData, DDEData.GetType()), DDEDATA)

        FreeData = DDEData.fRelease

        RaiseEvent Data(m.WParam, Item, DDEData, IncPtr(pDDEData, SizeOf(DDEData)))
      Finally
        GlobalUnlock(hDDEData)
        If FreeData Then
          FreeHGlobal(hDDEData)
        End If

        FreeDDElParam(m.Msg, m.LParam)
      End Try
      Return True
    End Function

    Protected Overridable Function OnRequest(ByRef m As System.Windows.Forms.Message) As Boolean
      Dim Format As Format
      Dim FormatID As Short
      Dim ItemAtom As Short
      Dim Item As String
      UnpackDDElParam(m.Msg, m.LParam, FormatID, ItemAtom)
      Format = GetFormat(FormatID)
      Item = GetAtomString(ItemAtom)
      RaiseEvent Request(m.WParam, Item, Format)

      FreeDDElParam(m.Msg, m.LParam)
      Return True
    End Function

    Protected Overridable Function OnPoke(ByRef m As System.Windows.Forms.Message) As Boolean
      ' wparam is the client (window) handle
      ' lparam.low is handle to DDEPOKE structure (or NULL)
      ' lparam.high is atom of item name
      Dim DDEPoke As DDEPOKE
      Dim ItemAtom As Short
      Dim Item As String
      Dim hDDEPoke As System.IntPtr
      Dim pDDEPoke As System.IntPtr
      UnpackDDElParam(m.Msg, m.LParam, hDDEPoke, ItemAtom)
      Item = GetAtomString(ItemAtom)

      pDDEPoke = GlobalLock(hDDEPoke)
      PtrToStructure(pDDEPoke, DDEPoke)

      RaiseEvent Poke(m.WParam, Item, DDEPoke, IncPtr(pDDEPoke, SizeOf(DDEPoke)))

      GlobalUnlock(hDDEPoke)

      FreeDDElParam(m.Msg, m.LParam)
      Return True
    End Function

    Protected Overridable Function OnExecute(ByRef m As System.Windows.Forms.Message) As Boolean
      Dim Exec As String

      If IsWindowUnicode(m.HWnd) AndAlso IsWindowUnicode(m.WParam) Then
        ' Use/assume unicode
        Exec = PtrToStringUni(GlobalLock(m.LParam))
      Else
        ' Use/assume ansi
        Exec = PtrToStringAnsi(GlobalLock(m.LParam))
      End If

      RaiseEvent Execute(m.WParam, Exec, m.LParam)

      GlobalUnlock(m.LParam)

      FreeDDElParam(m.Msg, m.LParam)
      Return True
    End Function

    Public Function PreFilterMessage(ByRef m As System.Windows.Forms.Message) As Boolean _
        Implements System.Windows.Forms.IMessageFilter.PreFilterMessage

      Dim Broadcast As Boolean = (Broadcast.Equals(m.HWnd))

      If (Broadcast OrElse m.HWnd.Equals(hWnd)) AndAlso IsDDEMsg(m.Msg) Then

        Select Case m.Msg
          Case WM_DDE.INITIATE
            Return OnInitiate(m)
          Case WM_DDE.TERMINATE
            Return OnTerminate(m)
          Case WM_DDE.ADVISE
            Return OnAdvise(m)
          Case WM_DDE.UNADVISE
            Return OnUnAdvise(m)
          Case WM_DDE.ACK
            Return OnAck(m)
          Case WM_DDE.DATA
            Return OnData(m)
          Case WM_DDE.REQUEST
            Return OnRequest(m)
          Case WM_DDE.POKE
            Return OnPoke(m)
          Case WM_DDE.EXECUTE
            Return OnExecute(m)
          Case Else
            Return False
        End Select
        Return True
      Else
        Return False
      End If

    End Function

    Public Sub New(ByVal hWnd As System.IntPtr)
      MyBase.New()
      Me.hWnd = hWnd
    End Sub

  End Class

  <System.Runtime.InteropServices.StructLayout(Explicit)> Private Structure CvtLong
    <System.Runtime.InteropServices.FieldOffset(0)> Public LongValue As Integer
    <System.Runtime.InteropServices.FieldOffset(0)> Public LoWord As Short
    <System.Runtime.InteropServices.FieldOffset(2)> Public HiWord As Short
  End Structure

  Public Shared Function MakeInt(ByVal LoWord As Short, ByVal HiWord As Short) As Integer
    Dim Convert As CvtLong
    Convert.HiWord = HiWord
    Convert.LoWord = LoWord
    Return Convert.LongValue
  End Function

  Public Shared Sub SplitInt(ByVal Value As Integer, ByRef LoWord As Short, ByRef HiWord As Short)
    Dim Convert As CvtLong
    Convert.LongValue = Value
    HiWord = Convert.HiWord
    LoWord = Convert.LoWord
  End Sub

  Public Shared Function IncPtr(ByVal Ptr As System.IntPtr, ByVal Offset As Integer) As System.IntPtr
    Return New System.IntPtr(Ptr.ToInt32 + Offset)
  End Function

  Private Overloads Function IIf(ByVal Condition As Boolean, ByVal TrueValue As String, ByVal FalseValue As String) As String
    If Condition Then
      Return TrueValue
    Else
      Return FalseValue
    End If
  End Function

  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    '
    'DDEForm
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(292, 266)
    Me.Name = "DDEForm"

  End Sub

#Region " Marshalled commands"

  Private Delegate Function DelegateSetFormVisible(ByRef Value As Boolean) As Boolean

  Friend Function SetFormVisible(ByRef Value As Boolean) As Boolean
    If Me.InvokeRequired Then
      Dim d As New DelegateSetFormVisible(AddressOf SetFormVisible)
      Return CBool(Me.Invoke(d, New Object() {CType(Value, Object)}))
    Else
      Me.Visible = Value
      Return Me.Visible
    End If
  End Function

  Private Delegate Function DelegateGetFormVisible() As Boolean

  Friend Function GetFormVisible() As Boolean
    If Me.InvokeRequired Then
      Dim d As New DelegateGetFormVisible(AddressOf GetFormVisible)
      Return CBool(Me.Invoke(d, New Object() {}))
    Else
      Return Me.Visible
    End If
  End Function

#End Region

End Class



Public Class DDE_Capture


  ' *****************************************************************************************************
  ' DDE Capture CLASS
  ' -----------------
  '
  ' 
  '
  '
  ' *****************************************************************************************************

  Inherits ThreadWrapperBase

  Friend WithEvents _DDEForm As DDEForm
  Private DDEFormMessageThread As Thread

  Private _CloseDownFlag As Boolean
  Private NaplesInitiator As Initiator
  Private NewRequests As New Queue                ' Queue object for New Requests

  Friend Declare Function EmptyClipboard Lib "user32" Alias "EmptyClipboard" () As Long

  Public Sub New(ByVal pInitiator As Initiator)
    MyBase.New()

    Me.Thread.Name = "DDE_Capture"
    NaplesInitiator = pInitiator
  End Sub

  Public Property CloseDown() As Boolean
    Get
      Return _CloseDownFlag
    End Get
    Set(ByVal Value As Boolean)
      _CloseDownFlag = Value
    End Set
  End Property

  Public ReadOnly Property DDEForm() As DDEForm
    Get
      Return _DDEForm
    End Get
  End Property

  Protected Overrides Sub DoTask()

    Initialise()

    RunDataServer()

    TidyUp()

  End Sub

  Private Sub Initialise()
    ' Initialise Data Server

    _CloseDownFlag = False

    ' _DDEForm = New DDEForm
    ' _DDEForm.Visible = False

    _DDEForm = Nothing

    DDEFormMessageThread = New Thread(AddressOf Me.RunDDEFormMessageLoop)
    DDEFormMessageThread.SetApartmentState(ApartmentState.STA) '  = ApartmentState.STA
    DDEFormMessageThread.Name = "DDEFormMessageLoop"
    DDEFormMessageThread.Start()
  End Sub

  Private Sub TidyUp()
    Dim Counter As Integer

    ' On ending the Server :-

    ' Shut Down the DDE Form, This should also clear the Message Loop thread.

    If Not (_DDEForm Is Nothing) Then
      If _DDEForm.InvokeRequired Then
        Dim d As New DDEForm.CloseFormDelegate(AddressOf _DDEForm.CloseForm)
        _DDEForm.BeginInvoke(d, New Object() {})
      Else
        _DDEForm.CloseForm()
      End If
    End If

    For Counter = 0 To 10
      If (DDEFormMessageThread.ThreadState And ThreadState.Running) = ThreadState.Running Then
        Application.DoEvents()
        Threading.Thread.Sleep(20)
      Else
        Exit For
      End If
    Next

    If (Not (DDEFormMessageThread Is Nothing)) Then
      If (DDEFormMessageThread.ThreadState And ThreadState.Running) = ThreadState.Running Then
        DDEFormMessageThread.Abort()
      End If
    End If

  End Sub


  ' *******************************************************************
  ' Post New DDEServer Request
  ' *******************************************************************
  Public Sub PostNewRequest(ByRef pServerRequest As DataServerRequest)
    SyncLock NewRequests
      NewRequests.Enqueue(pServerRequest)
    End SyncLock
  End Sub


  ' *******************************************************************
  '
  ' *******************************************************************
  Private Sub RunDataServer()
    Dim LoopWorkDone As Boolean
    Dim thisDSRequest As DataServerRequest
    Dim LastMessage As Date
    Dim tempString As String
    Dim NewRequestsCount As Integer

    LastMessage = Now()
    '     Me.SendInitiate("winblp", "bbk")

    ' Dummy work loop.
    While (_CloseDownFlag = False)
      Try

        LoopWorkDone = False
        Try
          If (Not (Me._DDEForm Is Nothing)) Then
            If (Me._DDEForm.GetFormVisible = True) Then
              Me._DDEForm.SetFormVisible(False)
            End If
          End If
        Catch ex As Exception
        End Try

        SyncLock NewRequests
          NewRequestsCount = NewRequests.Count
        End SyncLock

        If (NewRequestsCount > 0) Then
          LoopWorkDone = True

          Try

            ' ***************************************
            ' Retrieve Request
            ' ***************************************

            SyncLock NewRequests
              thisDSRequest = CType(NewRequests.Dequeue, DataServerRequest)
            End SyncLock
          Catch ex As Exception
            LogError(DebugLevels.Minimal, "DDE_Capture, RunDataServer", LOG_LEVELS.Error, ex.Message, "Error Dequeueing DDE Request", ex.StackTrace)
            thisDSRequest = Nothing
          End Try

          If (Not (thisDSRequest Is Nothing)) And (Not (_DDEForm Is Nothing)) Then
            Try
              Me._DDEForm.SendInitiate("winblp", "bbk")
            Catch ex As Exception
              LogError(DebugLevels.Minimal, "DDE_Capture, RunDataServer", LOG_LEVELS.Error, ex.Message, "Error SendInitiate", ex.StackTrace)
            End Try

            Try
              tempString = "Log DDE Request : " & System.Enum.GetName(GetType(NaplesGlobals.DS_RequestType), thisDSRequest.RequestType) & ", `" & thisDSRequest.Request.ToString & "`."
              LogError(DebugLevels.Medium, "DDE_Capture, RunDataServer", LOG_LEVELS.Audit, "", tempString, "")
            Catch ex As Exception
            End Try

            Select Case thisDSRequest.RequestType

							Case DS_RequestType.BBG_Login
								' Trigger a login process on Bloomberg.

								Dim ReturnObject As Object
								ReturnObject = "Not OK, Login failed."
								thisDSRequest.RequestStatus = DS_RequestStatus.RequestFailed

								Try
									If thisDSRequest.Request.Length > 0 Then
										Dim Params() As String = thisDSRequest.Request.Split(New Char() {CChar(" "), CChar(","), CChar("|")})
										Dim ParameterString As String
										Dim ParameterName As String

										Dim UserName As String
										Dim Password As String

										If Params.Length >= 2 Then
											UserName = Params(0)
											Password = Params(1)

											For Each ParameterString In Params
												Try
													If InStr(ParameterString, "=") > 0 Then
														ParameterName = ParameterString.Substring(0, InStr(ParameterString, "=") - 1)
														ParameterString = ParameterString.Substring(InStr(ParameterString, "="), (ParameterString.Length - InStr(ParameterString, "=")))

														Select Case ParameterName.ToUpper
															Case "USERNAME"
																UserName = ParameterString.Trim

															Case "PASSWORD"
																Password = ParameterString.Trim

														End Select
													End If
												Catch ex As Exception
												End Try
											Next

											Call DoAutoLogin(UserName, Password)

											ReturnObject = "OK"
											thisDSRequest.RequestStatus = DS_RequestStatus.RequestComplete

										End If
									End If

								Catch ex As Exception
								Finally
									Try
										thisDSRequest.ReturnObject = ReturnObject
										thisDSRequest.OnDataUpdate(ReturnObject)
									Catch ex As Exception
									End Try
								End Try

							Case DS_RequestType.DDE_ScreenShot


							Case DS_RequestType.DDE_ScreenCopy
								Dim ReturnObject As Object
								ReturnObject = Nothing

								If thisDSRequest.Request.Length > 0 Then
									Try
										'Me.DDEForm.PostExecute(BBGAutoLoginString)
										'Me.DDEForm.WaitForAck()
										Call DoAutoLogin()

										Me.DDEForm.PostExecute(BBGScreenToUse & "<home>")
										Me.DDEForm.WaitForAck()
										Threading.Thread.Sleep(50)
										Me.DDEForm.PostExecute(BBGScreenToUse & thisDSRequest.Request & "<Go>")
										Me.DDEForm.WaitForAck()

										ReturnObject = GetScreenText(Me.DDEForm, BBGClipboardCopyTimeout * 2)

										thisDSRequest.RequestStatus = DS_RequestStatus.RequestComplete

									Catch ex As Exception
										ReturnObject = Nothing
										thisDSRequest.RequestStatus = DS_RequestStatus.RequestFailed
										LogError(DebugLevels.Minimal, "DDE_Capture, RunDataServer", LOG_LEVELS.Error, ex.Message, "Error processing `DDE_ScreenCopy`", ex.StackTrace)

									Finally
										Try
											thisDSRequest.ReturnObject = ReturnObject
											thisDSRequest.OnDataUpdate(ReturnObject)
										Catch ex As Exception
										End Try
									End Try
								End If

              Case DS_RequestType.DDE_GetPortfolio
                Dim ReturnObject As Object
                Dim ReturnString As String = ""
                Dim ScreenString As String
                Dim ExecuteSting As String

                ReturnObject = Nothing

                If thisDSRequest.Request.Length > 0 Then
                  Try
                    'Me.DDEForm.PostExecute(BBGAutoLoginString)
                    'Me.DDEForm.WaitForAck()
                    LogError(DebugLevels.Maximum, "DDE_Capture, RunDataServer , DDE_GetPortfolio", LOG_LEVELS.Audit, "", "DoAutoLogin()", "")

                    Call DoAutoLogin()

                    Me.DDEForm.PostExecute(BBGScreenToUse & "<home>")
                    Me.DDEForm.WaitForAck()
                    Threading.Thread.Sleep(50)

                    ExecuteSting = BBGScreenToUse & thisDSRequest.Request & "<Client>LPRT<Go>PDSP<Go>"
                    LogError(DebugLevels.Maximum, "DDE_Capture, RunDataServer , DDE_GetPortfolio", LOG_LEVELS.Audit, "", "PostCommand : " & ExecuteSting, "")
                    Me.DDEForm.PostExecute(BBGScreenToUse & ExecuteSting)
                    Me.DDEForm.WaitForAck()

                    LogError(DebugLevels.Maximum, "DDE_Capture, RunDataServer , DDE_GetPortfolio", LOG_LEVELS.Audit, "", "PostCommand : <copy>", "")

                    ScreenString = GetScreenText(Me.DDEForm, BBGClipboardCopyTimeout * 2)

                    If BBGScreenDecoder(BBGScreenType.PortfolioPDSP, BBGReturnValue.PDSP_Ticker, ScreenString).ToUpper = "S" Then
                      LogError(DebugLevels.Maximum, "DDE_Capture, RunDataServer , DDE_GetPortfolio", LOG_LEVELS.Audit, "", "Set portfolio to show Stock Tickers.", "")

                      Me.DDEForm.PostExecute(BBGScreenToUse & "<down><down>T<Go>")
                      Me.DDEForm.WaitForAck()

                      ScreenString = GetScreenText(Me.DDEForm, BBGClipboardCopyTimeout * 2)

                    End If

                    LogError(DebugLevels.Maximum, "DDE_Capture, RunDataServer , DDE_GetPortfolio", LOG_LEVELS.Audit, "", "Parse Portfolio Header and First Page of Data.", "")

                    ReturnString = BBGScreenDecoder(BBGScreenType.PortfolioPDSP, BBGReturnValue.PDSP_Header, ScreenString)
                    ReturnString &= BBGScreenDecoder(BBGScreenType.PortfolioPDSP, BBGReturnValue.PDSP_InstrumentLines, ScreenString)

                    Dim PageCount As Integer
                    Dim TotalPages As Integer

                    If ScreenString.Length > 0 Then
                      TotalPages = CInt(BBGScreenDecoder(BBGScreenType.PortfolioPDSP, BBGReturnValue.PDSP_TotalPages, ScreenString))
                      If TotalPages > 1 Then
                        For PageCount = 2 To TotalPages
                          Try

                            LogError(DebugLevels.Maximum, "DDE_Capture, RunDataServer , DDE_GetPortfolio", LOG_LEVELS.Audit, "", "Parse Portfolio, Get Page " & PageCount.ToString, "")

                            Me.DDEForm.PostExecute(BBGScreenToUse & "<pagefwd>")
                            Me.DDEForm.WaitForAck()

                            ScreenString = GetScreenText(Me.DDEForm)

                            LogError(DebugLevels.Maximum, "DDE_Capture, RunDataServer , DDE_GetPortfolio", LOG_LEVELS.Audit, "", "Parse Portfolio, Parse Page " & PageCount.ToString, "")

                            ReturnString &= BBGScreenDecoder(BBGScreenType.PortfolioPDSP, BBGReturnValue.PDSP_InstrumentLines, ScreenString)

                          Catch ex As Exception
                            LogError(DebugLevels.Minimal, "DDE_Capture, RunDataServer , DDE_GetPortfolio", LOG_LEVELS.Error, ex.Message, "Error getting Portfolio, Page " & PageCount.ToString, ex.StackTrace)
                          End Try
                        Next
                      End If
                    End If

                    Me.DDEForm.PostExecute(BBGScreenToUse & "<home>")
                    Me.DDEForm.WaitForAck()
                    Threading.Thread.Sleep(100)

                    ReturnObject = ReturnString
                    thisDSRequest.RequestStatus = DS_RequestStatus.RequestComplete

                  Catch ex As Exception
                    ReturnObject = Nothing
                    thisDSRequest.RequestStatus = DS_RequestStatus.RequestFailed
                    LogError(DebugLevels.Minimal, "DDE_Capture, RunDataServer", LOG_LEVELS.Error, ex.Message, "Error processing `DDE_GetPortfolio`", ex.StackTrace)

                  Finally
                    Try
                      thisDSRequest.ReturnObject = ReturnObject
                      thisDSRequest.OnDataUpdate(ReturnObject)
                    Catch ex As Exception
                    End Try
                  End Try
                End If

              Case DS_RequestType.DDE_PostCommand

                If thisDSRequest.Request.Length > 0 Then
                  Try
                    'Me.DDEForm.PostExecute(BBGAutoLoginString)
                    'Me.DDEForm.WaitForAck()
                    Call DoAutoLogin()

                    Me.DDEForm.PostExecute(BBGScreenToUse & "<home>")
                    Me.DDEForm.WaitForAck()
                    Threading.Thread.Sleep(50)
                    Me.DDEForm.PostExecute(BBGScreenToUse & thisDSRequest.Request)
                    Me.DDEForm.WaitForAck()
                    thisDSRequest.RequestStatus = DS_RequestStatus.RequestComplete

                  Catch ex As Exception

                    LogError(DebugLevels.Minimal, "DDE_Capture, RunDataServer", LOG_LEVELS.Error, ex.Message, "Error processing `DDE_PostCommand`", ex.StackTrace)
                    thisDSRequest.RequestStatus = DS_RequestStatus.RequestFailed

                  Finally
                    Try
                      thisDSRequest.OnDataUpdate(Nothing)
                    Catch ex As Exception
                    End Try
                  End Try
                End If

            End Select

            Me._DDEForm.PostTerminate()

          End If
        End If ' NewRequests.Count > 0

        If (LoopWorkDone = False) Then
          Threading.Thread.Sleep(25)
        End If

      Catch ex As Exception
        LogError(DebugLevels.Minimal, "DDE_Capture, RunDataServer", LOG_LEVELS.Error, ex.Message, "Error running in main DDE Server loop", ex.StackTrace)
      End Try
    End While

  End Sub

	Private Function DoAutoLogin(Optional ByVal pUsername As String = BBGAutoLoginDefaultUser, Optional ByVal pPassword As String = BBGAutoLoginDefaultPassword) As Boolean
		Dim ClipBoardText As String = ""
		Dim ClipBoardObject As Object = Nothing
		Dim Timeout As Integer
		Dim FinalTimer As Integer
		Dim TimerStart As Date
		Dim tempval As Double
		Dim TSConCount As Integer = 0
		Dim CompleteLoginCommand As String = ""

		Try
			CompleteLoginCommand = BBGAutoLoginHeader & Left(pUsername, Math.Min(pUsername.Length, 12))

			If (pUsername.Length < 12) Then
				CompleteLoginCommand &= "<tab>"
			End If


			CompleteLoginCommand &= Left(pPassword, Math.Min(pPassword.Length, 12))
			CompleteLoginCommand &= BBGAutoLoginTail

		Catch ex As Exception
			Exit Function
		End Try

		Try
			TSConCount = 0

RetryPoint:

			Timeout = 5000
			FinalTimer = 3000

			TimerStart = Now()

			Me.DDEForm.PostExecute(CompleteLoginCommand) ' BBGAutoLoginString)
			Me.DDEForm.WaitForAck()

			Call ClearClipboard()
			Me.DDEForm.PostExecute("<blp-0><copy>")
			Me.DDEForm.WaitForAck()

			ClipBoardObject = GetFromClipboard(Timeout)
			While ClipBoardObject Is Nothing
				Try
					Timeout += 5000

					Me.DDEForm.PostExecute(CompleteLoginCommand) ' BBGAutoLoginString)
					Me.DDEForm.WaitForAck()

					Call ClearClipboard()
					Me.DDEForm.PostExecute("<blp-0><copy>")
					Me.DDEForm.WaitForAck()

					If Timeout > 50000 Then
						LogError(DebugLevels.Minimal, "DDE_Capture, DoAutoLogin", LOG_LEVELS.Warning, "", "Error in AutoLogin() - Timed Out.", "")
						Return False
						Exit Function
					End If
				Catch ex As Exception
					LogError(DebugLevels.Minimal, "DDE_Capture, DoAutoLogin", LOG_LEVELS.Error, ex.Message, "Error in AutoLogin() loop.", ex.StackTrace)
				End Try
			End While

			If (Not (ClipBoardObject Is Nothing)) Then
				ClipBoardText = ClipBoardObject.ToString

				If (ClipBoardText.Contains("login error:") OrElse ClipBoardText.Contains("L O G I N   E R R O R")) Then
					If (TSConCount = 0) Then
						TSConCount += 1

						Try
							Shell("tscon 0 /DEST:console", AppWinStyle.Hide, True, 20000)
						Catch ex As Exception
						End Try

						GoTo RetryPoint
					End If

					Return False
				End If
			End If

			tempval = Now().Subtract(TimerStart).TotalMilliseconds
			If Now().Subtract(TimerStart).TotalMilliseconds >= 1000 Then
				Threading.Thread.Sleep(FinalTimer)
			End If

		Catch ex As Exception
			LogError(DebugLevels.Minimal, "DDE_Capture, DoAutoLogin", LOG_LEVELS.Error, ex.Message, "Error in AutoLogin().", ex.StackTrace)
		End Try

		Return True

		Exit Function
	End Function

  Private Function ClearClipboard() As Boolean
    ' **************************************************************************
    ' AAARRRGGGHHHH !!!!!!!
    '
    ' OK, now that's off my chest - We can continue.
    '
    ' Seemingly a recurring problem with Portfolio Capture failing is due to an error being
    ' repeatedly thrown by the 'Clipboard.SetDataObject("")' command.
    ' There seems to be no concensus in the wider community as to Why or When this error occurrs, Neither
    ' Does there seem to be a concensus on how to deal with it, thus this is my solution :-
    '
    ' For the first attempt, use the .Net Managed Code, for Subsequent attempts, use the API call to clear the 
    ' Clipboard.
    '
    ' Hope this works !
    '
    ' **************************************************************************
    Dim RetryCount As Integer
    Dim SuccessFlag As Boolean

    Try
      SuccessFlag = False
      RetryCount = 0

      While (SuccessFlag = False) AndAlso (RetryCount < 10)
        SuccessFlag = True

        Try
          If (RetryCount <= 0) Then
            Clipboard.SetDataObject("")
          Else
            If EmptyClipboard() = 0 Then
              SuccessFlag = False
            End If
          End If

        Catch ex As Exception
          LogError(DebugLevels.Minimal, "ClearClipboard", LOG_LEVELS.Error, ex.Message, "Error clearing Clipboard, RetryCount = " & RetryCount.ToString, ex.StackTrace)
          SuccessFlag = False
          Threading.Thread.Sleep(50)
          Application.DoEvents()
        End Try

        RetryCount += 1
      End While
    Catch ex As Exception
      LogError(DebugLevels.Minimal, "ClearClipboard", LOG_LEVELS.Error, ex.Message, "Error in ClearClipboard().", ex.StackTrace)
    End Try

    Return SuccessFlag
  End Function

  Private Function GetScreenText(ByRef pDDEForm As Naples.DDEForm, Optional ByVal InitialTimeout As Integer = BBGClipboardCopyTimeout) As String
    Dim RetryCount As Integer
    Dim ScreenString As String
    Dim ThisTimeout As Integer

    Try
      RetryCount = 0
      ThisTimeout = InitialTimeout
      Call ClearClipboard()

      While RetryCount < 10
        Me.DDEForm.PostExecute(BBGScreenToUse & "<copy>")
        Me.DDEForm.WaitForAck()

        Try
          ScreenString = CType(GetFromClipboard(ThisTimeout), String)

          If ScreenString.Length > 0 Then
            Return ScreenString
          End If

          Call ClearClipboard()
        Catch ex As Exception
        End Try

        ThisTimeout = BBGClipboardCopyTimeout
        RetryCount += 1
      End While

    Catch ex As Exception
      LogError(DebugLevels.Minimal, "GetScreenText", LOG_LEVELS.Error, ex.Message, "Error in GetScreenText().", ex.StackTrace)
    End Try

    Return ""
  End Function

  Private Function GetFromClipboard() As Object
    Return GetFromClipboard(BBGClipboardCopyTimeout)
  End Function

  Private Function GetFromClipboard(ByVal pTimeout As Integer) As Object
    ' pTimeout is specified in Milliseconds

    Dim iData As IDataObject
    Dim ReturnObject As Object
    ReturnObject = Nothing

    Dim ClipCount As Integer = 0

    Try

      While (ClipCount < pTimeout)
        Application.DoEvents()
        Threading.Thread.Sleep(50)
        Application.DoEvents()

        iData = Nothing
        Try
          iData = Clipboard.GetDataObject
        Catch ex As Exception
        End Try

        If (Not (iData Is Nothing)) Then
          Try
            If iData.GetDataPresent(DataFormats.Text) Then
              ReturnObject = iData.GetData(DataFormats.Text)
              If CType(ReturnObject, String).Length > 0 Then
                LogError(DebugLevels.Maximum, "DDE_Capture, GetFromClipboard", LOG_LEVELS.Audit, "", "Text Retrieved from Clipboard.", "")

                Return ReturnObject
              End If
            ElseIf iData.GetDataPresent(DataFormats.Bitmap) Then
              LogError(DebugLevels.Maximum, "DDE_Capture, GetFromClipboard", LOG_LEVELS.Audit, "", "Bitmap Retrieved from Clipboard.", "")

              ReturnObject = iData.GetData(DataFormats.Bitmap)
              Return ReturnObject
            End If
          Catch ex As Exception
            LogError(DebugLevels.Minimal, "DDE_Capture, GetFromClipboard", LOG_LEVELS.Error, ex.Message, "Error in GetFromClipboard().", ex.StackTrace)
            ReturnObject = Nothing
          End Try
        End If

        ClipCount += 50
      End While

      ' For debug purposes only !
      If ClipCount < (pTimeout + pTimeout) Then
        ClipCount = ClipCount
      End If

      LogError(DebugLevels.Maximum, "DDE_Capture, GetFromClipboard", LOG_LEVELS.Audit, "", "Timeout retrieving from the Clipboard.", "")

      Return ReturnObject

    Catch ex As Exception

      LogError(DebugLevels.Minimal, "DDE_Capture, GetFromClipboard", LOG_LEVELS.Error, ex.Message, "Error retrieving from the Clipboard.", ex.StackTrace)

    End Try

    Return ReturnObject
  End Function

  Private Sub RunDDEFormMessageLoop()
    Dim DdeFormAppContext As New ApplicationContext

    While (_CloseDownFlag = False)
      Try
        If Me._DDEForm Is Nothing Then
          Me._DDEForm = New DDEForm
          Me._DDEForm.Show()
          Me._DDEForm.Visible = False
        End If

        DdeFormAppContext.MainForm = Me._DDEForm
        Application.Run(DdeFormAppContext)
      Catch ex As Exception
      End Try
    End While

    Dim Dummy As Integer
    Dummy = 0

  End Sub


  Private Function LogError( _
  ByVal pDebugLevel As DebugLevels, _
  ByVal p_Error_Location As String, _
  ByVal p_System_Error_Number As Integer, _
  ByVal p_System_Error_Description As String, _
  Optional ByVal p_Error_Message As String = "", _
  Optional ByVal p_Error_StackTrace As String = "", _
  Optional ByVal p_Log_to As Integer = 0) As Integer

    Try
      If Not (NaplesInitiator.Naples_DataBaseClient Is Nothing) Then
        Return NaplesInitiator.Naples_DataBaseClient.LogError(pDebugLevel, p_Error_Location, p_System_Error_Number, p_System_Error_Description, p_Error_Message, p_Error_StackTrace, p_Log_to)
      End If

    Catch ex As Exception
      Return (-1)
    End Try
  End Function

End Class

