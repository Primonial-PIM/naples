Imports System.Net
Imports System.Net.Sockets
Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.Serialization
Imports System.Threading

Imports Bloomberg.Api
Imports Bloomberg.Api.DataTypes
Imports Bloomberg.Api.DataTypes.MetaData

Imports NaplesGlobals
Imports RenaissanceGlobals




Public Class DataServer
  ' *****************************************************************************************************
  ' DATA SERVER CLASS
  ' -----------------
  '
  ' Dataserver in brief :
  '
  ' The Data Server class exists to process One-Off (DS_RequestType.API_Static) requests for data and 
  ' Updated (DS_RequestType.API_Realtime) Ongoing data requests.
  ' Static data requests are held briefly until the request is fulfilled whilst Ongoing Realtime requests are
  ' moved to an internal hashtable collection of 'DataServerRTItem' objects where they are updated on an ongoing basis.
  '
  ' Requests are placed by calling the 'PostNewRequest' method to this object.
  ' All requests are constructed using the 'DataServerRequest' object as follows :
  '			  .RequestType as DS_RequestType  - API_Static or API_Realtime
  '			  .Request As String			        - String format : <Security>|<FieldName>[,<FieldName>]
  '											                  - e.g. 'IBM US Equity|NAME,,BID,ASK,LAST
  '
  ' Static requests are returned through the request object, whilst dynamic requests pass back a reference to
  ' the internal DataServer 'DataServerRTItem' object which is updated as RT data arrives from BBG.
  '
  ' Static Requests :
  '		Once the request is accepted, the Request object 'RequestStatus' will be set to 'Pending'. When the Static
  '		data is received by the server, the request object is updated and the Status is set to 'Complete'. At that time
  '		the 'ReturnObject' property is set to a data object of some type (usually a hashtable of 'DataServerRTField'
  '		objects and the ObjectType of the Returned data is set in the 'ReturnType' Request Property.
  '
  '		If the request fails for some reason then the Request status will be set to either 'Rejected' (Usually some
  '		kind of formatting error which meant that the request could not be initiated, or 'Failed' (An error was
  '		trapped). In the event of the request failing then the error message is usually returned in the 'ReturnObject'
  '		property.
  '
  '		The RequestObject has an event 'DataUpdate' that is fired when the Static data is delivered successfully.
  '
  '
  ' RealTime Requests.
  '
  '		Once the request is accepted, the Request object 'RequestStatus' will be set to 'Submitted' and a reference to 
  '		the internal DataServer 'DataServerRTItem' object relating to this request will be returned in the 
  '		Request's 'ReturnObject' property. 
  '		When Static or Realtime data updates are received, the 'DataServerRTItem' is updated and the objects 'DataUpdate'
  '		Event is trigered.
  '		The data in the Update object is held in Two fields :
  '			  BBGSecurity As Bloomberg.Api.DataTypes.Security	: The Suscribed security, held in a Bloomberg format.
  '			  Fields As Hashtable								: The Field Data held as a hashtable of 'DataServerRTField' objects.
  '
  '		Whilst updates are ongoing, the 'DataServerRTItem' object's 'ItemStatus' property will be set to 'Live'. Other
  '		values such as 'Pending' or 'Dead' indicate an inactive item.
  '
  '		If the request fails for some reason then the Request status will be set to either 'Rejected' (Usually some
  '		kind of formatting error which meant that the request could not be initiated, or 'Failed' (An error was
  '		trapped). In the event of the request failing then the error message is usually returned in the 'ReturnObject'
  '		property.
  '
  '
  ' Request object :
  '
  '		Public Event DataUpdate(ByVal sender As Object, ByVal e As DataServerUpdate)
  '		Public RequestId As Guid					        : (Out) Unique GUID for this DataServerRequest object.
  '		Public RequestType As DS_RequestType	  	: (In)  Static, Realtime, various Debug
  '		Public Request As String					        : (In)  Data Request, '<Security>|<FieldName>[,<FieldName>]'
  '		Public RequestStatus As DS_RequestStatus	: (Out) Current request Status.
  '		Public Static_RequestID As Guid			    	: (Out) GUID of the related BBG Request object.
  '		Public RequestTime As Date				      	: (Out) Initiation time of this Request.
  '		Public ReturnType As System.Type		    	: (Out) Object type of the ReturnObject.
  '		Public ReturnObject As Object			      	: (Out) Return object. e.g. Error String, Field Hashtable, etc.
  '
  ' DataServerRTItem object :
  '
  '		Public Event DataUpdate(ByVal sender As Object, ByVal e As DataServerUpdate)
  '		
  '		Public ItemLock As New ReaderWriterLock   : Update locking object
  '		Public ItemStatus As DS_ObjectStatus      : (Out) None, Pending, Live, Dead
  '		Public ItemId As Guid                     : (Out) Unique GUID for this DataServerRTItem object.
  '		
  '		Public Realtime_RequestID As Guid         : (Out) GUID of the related BBG RT Request object.
  '		Public Static_RequestID As Guid           : (Out) GUID of the related BBG Static Request object.
  '		
  '		Public BBGSecurity As Bloomberg.Api.DataTypes.Security : Security to which this object relates.
  '		Public Fields As Hashtable                : Hashtable of 'DataServerRTField' objects representing current data.
  '
  ' *****************************************************************************************************

  Inherits ThreadWrapperBase

  Private NaplesInitiator As Initiator            ' Reference back to central Initiator object.

  Private NewRequests As New Queue                ' Queue object for New Requests
  Private PendingRequests As New Hashtable        ' Collection of Pending Static requests.
  Private RTDataItems As New Hashtable            ' Collection of Realtime (Ongoing) data objects.
  Private StaticDataItems As New Hashtable        ' Complimentary, temporary, collection of Realtime objects, used to more quickly update RT objects with their initial static data.
  Private ProcessingRequest As New ReaderWriterLock  ' Lock used to prevent Updates while Requests are being processed. This is designed to prevent a request update before the request is fully processed.

  Private _CloseDownFlag As Boolean               ' Clean shutdown mechanism, TRUE results in exit from the main work loop.
  Dim ftbl As FieldTable                          ' Bloomberg FieldTable taken from the MarketDataAdaptor object.
  Private TimeOfLastBloombergStartAttempt As Date ' 
  Friend LiveDebugs As Boolean                    ' 


  Public Sub New(ByVal pInitiator As Initiator)
    ' ************************************************************
    ' Simple Class constructor.
    ' ************************************************************

    MyBase.New()

    Me.Thread.Name = "DataServer"
    NaplesInitiator = pInitiator

  End Sub

  Public Property CloseDown() As Boolean
    ' ************************************************************
    ' CloseDown Property. Used to initiate clean shutdown.
    ' ************************************************************

    Get
      Return _CloseDownFlag
    End Get
    Set(ByVal Value As Boolean)
      _CloseDownFlag = Value
    End Set
  End Property

  Protected Overrides Sub DoTask()
    ' ************************************************************
    ' Overrided, Top level thread work process.
    ' ************************************************************

    Initialise()

    RunDataServer()

    TidyUp()

  End Sub

  Private Sub Initialise()
    ' ************************************************************
    ' Object Initialiser.
    ' ************************************************************
    ' Initialise Data Server

    _CloseDownFlag = False
    LiveDebugs = False

    ' Initialise
    TimeOfLastBloombergStartAttempt = CDate("1 Jan 1900")

    Call StartBloombergService()

  End Sub

  Private Sub TidyUp()
    ' ************************************************************
    ' On ending the Server
    ' ************************************************************

    ' Stop the BBG Server gracefully
    If MarketDataAdapter.IsAdapterStarted = True Then
      MarketDataAdapter.Shutdown()
    End If

  End Sub

  Private Sub RunDataServer()
    ' ************************************************************
    ' Main Work Loop.
    '
    ' Principle task is to process new data requests.
    '
    ' ************************************************************

    Dim thisDSRequest As DataServerRequest
    Dim thisField As String
    Dim BBG_RtRequest As RequestForRealtime = Nothing
    Dim BBG_StRequest As Request = Nothing '  RequestForStatic
    Dim BBGField As Bloomberg.Api.DataTypes.Field

    Dim SecurityName As String
    Dim FieldNames(-1) As String
    Dim RequestOK As Boolean
    Dim LoopWorkDone As Boolean
    Dim NewRequestsCount As Integer

    Dim RTFields As Hashtable


    ' Work loop.
    While (_CloseDownFlag = False)
      Try


        LoopWorkDone = False

        ' ***************************************
        ' Check For new Server Requests
        ' ***************************************

        ' retrieve next request
        NewRequestsCount = 0
        SyncLock NewRequests
          NewRequestsCount = NewRequests.Count
        End SyncLock

        If (StartBloombergService() = False) And (NewRequestsCount > 0) Then
          ' ***************************************
          ' Bloomberg Service not Started.
          ' Return Requests with Failed status.
          ' ***************************************

          LoopWorkDone = True
					ProcessingRequest.AcquireWriterLock(1000)

          Try
            ' ***************************************
            ' Retrieve Request
            ' ***************************************

            SyncLock NewRequests
              thisDSRequest = CType(NewRequests.Dequeue, DataServerRequest)
            End SyncLock

            ' ***************************************
            ' Modify the request to reflect the failed Bloomberg Server.
            ' ***************************************

            thisDSRequest.ReturnObject = String.Copy("Bloomberg Server has failed to start.")
            thisDSRequest.ReturnType = GetType(System.String)
            thisDSRequest.RequestStatus = DS_RequestStatus.RequestFailed

          Catch ex As Exception
            ' ***************************************
            ' In the event of an error build a dummy request to fall through the loop.
            ' Modify the request to reflect the error message.
            ' ***************************************

            thisDSRequest = New DataServerRequest(DS_RequestType.None, "")
            thisDSRequest.ReturnObject = String.Copy(ex.Message)
            thisDSRequest.ReturnType = GetType(System.String)
            thisDSRequest.RequestStatus = DS_RequestStatus.RequestFailed

            LogError(DebugLevels.Minimal, "DataServer, RunDataServer", LOG_LEVELS.Error, ex.Message, "Error De-Queueing Request (BBG Service not started)", ex.StackTrace)
          End Try


        ElseIf NewRequestsCount > 0 Then
          ' ***************************************
          ' BBG Service Seems OK. Continue ....
          ' ***************************************

          Try
            LoopWorkDone = True
						ProcessingRequest.AcquireWriterLock(1000)

            ' ***************************************
            ' Retrieve Request.
            ' ***************************************

            SyncLock NewRequests
              thisDSRequest = CType(NewRequests.Dequeue, DataServerRequest)
            End SyncLock

          Catch ex As Exception
            ' ***************************************
            ' In the event of an error build a dummy request to fall through the loop.
            ' ***************************************

            thisDSRequest = New DataServerRequest(DS_RequestType.None, "")
            thisDSRequest.ReturnObject = String.Copy(ex.Message)
            thisDSRequest.ReturnType = GetType(System.String)
            thisDSRequest.RequestStatus = DS_RequestStatus.RequestFailed

            LogError(DebugLevels.Minimal, "DataServer, RunDataServer", LOG_LEVELS.Error, ex.Message, "Error De-Queueing Request.", ex.StackTrace)
          End Try


          ' ***************************************
          ' Process request....
          '
          ' For each Request Type .....
          ' ***************************************
          Select Case thisDSRequest.RequestType

            Case DS_RequestType.API_Static, DS_RequestType.API_Realtime
              ' ***************************************
              ' Static or Realtime Data request.
              '
              ' ***************************************

              Try

                ' Initial Request status.
                thisDSRequest.RequestStatus = DS_RequestStatus.None

                ' ***************************************
                ' Resolve Security and Field names
                ' Format : <Security Mnemonic>|<Field Name>[[,<Field Name>]...]
                ' ***************************************

                SecurityName = thisDSRequest.Request
                If InStr(SecurityName, "|") > 0 Then
                  FieldNames = SecurityName.Substring(InStr(SecurityName, "|")).Split(CChar(","))
                  SecurityName = SecurityName.Substring(0, InStr(SecurityName, "|") - 1)
                End If

                ' ***************************************
                ' Build Initial Static Request
                ' ***************************************
                ' Validate Security

                Try
                  BBG_StRequest = New Request    '  RequestForStatic
                  BBG_StRequest.Monitor = False
                  BBG_StRequest.Securities.Add(SecurityName)
                  BBG_StRequest.SubscriptionMode = SubscriptionMode.ByRequest
                  AddHandler BBG_StRequest.ReplyEvent, AddressOf BBG_ReplyEvent
                Catch ex As Exception
                  thisDSRequest.ReturnObject = String.Copy(ex.Message)
                  thisDSRequest.ReturnType = GetType(System.String)
                  thisDSRequest.RequestStatus = DS_RequestStatus.RequestFailed

                  LogError(DebugLevels.Minimal, "DataServer, RunDataServer", LOG_LEVELS.Error, ex.Message, "Error building initial Static request (API).", ex.StackTrace)
                End Try

                ' ***************************************
                ' Build Iniital RT Request
                ' ***************************************

                If thisDSRequest.RequestType = DS_RequestType.API_Realtime Then
                  Try
                    BBG_RtRequest = New RequestForRealtime
                    BBG_RtRequest.Securities.Add(SecurityName)
                    BBG_RtRequest.SubscriptionMode = SubscriptionMode.ByRequest
                    AddHandler BBG_RtRequest.ReplyEvent, AddressOf BBG_ReplyEvent
									Catch ex As Exception
										Try
											thisDSRequest.ReturnObject = String.Copy(ex.Message)
											thisDSRequest.ReturnType = GetType(System.String)
											thisDSRequest.RequestStatus = DS_RequestStatus.RequestFailed

											RemoveHandler BBG_RtRequest.ReplyEvent, AddressOf BBG_ReplyEvent
										Catch Inner_Ex As Exception
										End Try

										LogError(DebugLevels.Minimal, "DataServer, RunDataServer", LOG_LEVELS.Error, ex.Message, "Error building Realtime request (API) : " & SecurityName, ex.StackTrace)
                  End Try
                End If

                ' ***************************************
                ' Now add fields to the Static and dynamic requests as appropriate.
                ' Note that BBG Fields are denoted as either static or dynamic fields and must be a member of the 
                ' appropriate request type. Not withstanding that all requests are of the same base type - you can't mix
                ' and match.
                ' ***************************************

                RTFields = New Hashtable
                For Each thisField In FieldNames
                  Try
                    If (thisDSRequest.RequestStatus <> DS_RequestStatus.RequestFailed) Then
                      BBGField = ftbl(thisField.Trim)

                      ' Build Hashtable used in the RT Object to store field values.
                      RTFields.Add(BBGField.Mnemonic, New DataServerRTField(BBGField.Mnemonic))

                      ' Add Field to the appropriate BBG Request 
                      If (BBGField.IsRealTime) And (thisDSRequest.RequestType = DS_RequestType.API_Realtime) Then
                        ' RT Request
                        If (BBG_RtRequest IsNot Nothing) Then
                          BBG_RtRequest.Fields.Add(BBGField)
                        End If
                      Else
                        ' Static Request, Either a static field or a Static DataServer Request type

                        If (BBG_StRequest IsNot Nothing) Then
                          BBG_StRequest.Fields.Add(BBGField)
                        End If

                        '' If the BBG_StRequest is a specific BBG Static Request object then it will not accept
                        '' RT Fields, however if a generic Request object is used then this limitation does not apply.
                        ''
                        ''If (BBGField.IsRealTime) Then

                        ''  ' Static requests will not allow RT Fields, Convert to Static Version if possible
                        ''  ' Seemingly Static fields start with 'Px_' and truncate RT fields where they have an '_'.
                        ''  ' Not all RT fields seem to have Static equivalents. Some Fields will be lost here.

                        ''  Dim NewFieldName As String

                        ''  If InStr(thisField.Trim, "_") > 0 Then
                        ''	NewFieldName = "PX_" & thisField.Substring(0, InStr(thisField.Trim, "_") - 1)
                        ''  Else
                        ''	NewFieldName = "PX_" & thisField.Trim
                        ''  End If

                        ''  Try
                        ''	BBGField = ftbl(NewFieldName.Trim)
                        ''	If (BBGField.IsStatic) Then
                        ''	  BBG_StRequest.Fields.Add(BBGField)
                        ''	End If
                        ''  Catch ex As Exception
                        ''  End Try
                        ''Else
                        ''  BBG_StRequest.Fields.Add(BBGField)
                        ''End If
                      End If
                    End If
                  Catch ex As Exception
                    thisDSRequest.ReturnObject = String.Copy(ex.Message)
                    thisDSRequest.ReturnType = GetType(System.String)
                    thisDSRequest.RequestStatus = DS_RequestStatus.RequestFailed

                    LogError(DebugLevels.Minimal, "DataServer, RunDataServer", LOG_LEVELS.Error, ex.Message, "Error adding fields to Data Request (API Request).", ex.StackTrace)
                  End Try
                Next


                ' ***************************************
                ' Send the Static request to the MarketDataAdaptor if the Static Request exists, Has a security 
                ' and has any Fields
                ' ***************************************

                RequestOK = False

                If (thisDSRequest.RequestStatus <> DS_RequestStatus.RequestFailed) Then
                  If (BBG_StRequest IsNot Nothing) AndAlso (BBG_StRequest.Securities.Count > 0) AndAlso (BBG_StRequest.Fields.Count > 0) Then
                    Try
                      MarketDataAdapter.SendRequest(BBG_StRequest.ToRequest)
                      RequestOK = True
                      thisDSRequest.RequestStatus = DS_RequestStatus.RequestPending
                    Catch ex As Exception
                      RequestOK = False

                      Try
                        RemoveHandler BBG_StRequest.ReplyEvent, AddressOf BBG_ReplyEvent
                      Catch Inner_Ex As Exception
                      End Try

                      BBG_StRequest = Nothing
                      thisDSRequest.ReturnObject = String.Copy(ex.Message)
                      thisDSRequest.ReturnType = GetType(System.String)
                      thisDSRequest.RequestStatus = DS_RequestStatus.RequestFailed

                      LogError(DebugLevels.Minimal, "DataServer, RunDataServer", LOG_LEVELS.Error, ex.Message, "Error posting Static MarketDataAdapter.SendRequest(). (API).", ex.StackTrace)
                    End Try
                  Else
                    Try
                      RemoveHandler BBG_StRequest.ReplyEvent, AddressOf BBG_ReplyEvent
                    Catch Inner_Ex As Exception
                    End Try

                    BBG_StRequest = Nothing
                    thisDSRequest.RequestStatus = DS_RequestStatus.RequestRejected
                  End If

                  ' ***************************************
                  ' Send the Realtime request to the MarketDataAdaptor if the Realtime Request exists, Has a security 
                  ' and has any Fields
                  ' ***************************************
                  If (BBG_RtRequest IsNot Nothing) AndAlso (BBG_RtRequest.Securities.Count > 0) AndAlso (BBG_RtRequest.Fields.Count > 0) Then
                    Try
                      MarketDataAdapter.SendRequest(BBG_RtRequest.ToRequest)
                      RequestOK = True
                      thisDSRequest.RequestStatus = DS_RequestStatus.RequestPending
                    Catch ex As Exception
                      RequestOK = False

                      Try
                        RemoveHandler BBG_RtRequest.ReplyEvent, AddressOf BBG_ReplyEvent
                      Catch Inner_Ex As Exception
                      End Try

                      BBG_RtRequest = Nothing
                      thisDSRequest.ReturnObject = String.Copy(ex.Message)
                      thisDSRequest.ReturnType = GetType(System.String)
                      thisDSRequest.RequestStatus = DS_RequestStatus.RequestFailed

                      LogError(DebugLevels.Minimal, "DataServer, RunDataServer", LOG_LEVELS.Error, ex.Message, "Error posting Realtime MarketDataAdapter.SendRequest(). (API).", ex.StackTrace)
                    End Try
                  Else
                    If thisDSRequest.RequestStatus = DS_RequestStatus.None Then
                      thisDSRequest.RequestStatus = DS_RequestStatus.RequestRejected

                      Try
                        RemoveHandler BBG_RtRequest.ReplyEvent, AddressOf BBG_ReplyEvent
                      Catch Inner_Ex As Exception
                      End Try

                      BBG_RtRequest = Nothing
                      RequestOK = False
                    End If
                  End If
                End If

                ' ***************************************
                ' At this point : The static and dynamic requests have been constructed and posted if appropriate.
                ' Now : Static requests will remain as request items in the Pending collection until updated by the 
                ' BBG Request response.
                ' RT Requests will be added the the RTData collection and the RTData object will be returned by the request.
                ' the RTData object will raise events as it is updated.
                '
                ' ***************************************
                If (RequestOK = True) Then
                  thisDSRequest.RequestTime = Now()

                  If (thisDSRequest.RequestType = DS_RequestType.API_Realtime) Then
                    ' ******************************************
                    ' RT Request
                    ' 
                    ' Construct new 'DataServerRTItems' Object and add it to the 'RTDataItems' collection.
                    ' ******************************************

                    Dim thisRtItem As New DataServerRTItem
                    Dim RtGUID As Guid
                    Dim RTItemOK As Boolean

                    ' Resolve BBG Static Request Information for DataServerRTItem
                    RTItemOK = False
                    RtGUID = Guid.Empty

                    If (BBG_StRequest IsNot Nothing) Then
                      thisRtItem.Static_RequestID = BBG_StRequest.RequestId
                      RtGUID = BBG_StRequest.RequestId

                      If BBG_StRequest.Securities.Count > 0 Then
                        thisRtItem.BBGSecurity = BBG_StRequest.Securities(0)
                      End If

                      RTItemOK = True
                    Else
                      thisRtItem.Static_RequestID = Guid.Empty
                    End If

                    ' ***************************************
                    '
                    ' Resolve BBG Realtime Request Information for DataServerRTItem
                    ' It is relevant that the static request is processed first since I want to end up with the 
                    ' BBG RT Request GUID in the DataServerRTItem GUID field if a RT Request is present.
                    ' This GUID will be used to with the collection (Hashtable) index to quickly locate
                    ' Entries for the application of data updates.
                    '
                    ' ***************************************

                    If (BBG_RtRequest IsNot Nothing) Then
                      thisRtItem.Realtime_RequestID = BBG_RtRequest.RequestId
                      RtGUID = BBG_RtRequest.RequestId

                      If BBG_RtRequest.Securities.Count > 0 Then
                        thisRtItem.BBGSecurity = BBG_RtRequest.Securities(0)
                      End If

                      RTItemOK = True
                    Else
                      thisRtItem.Realtime_RequestID = Guid.Empty
                    End If

                    ' Resolve GUID Information for DataServerRTItem, if not already resolved. (It should be !)

                    If (RtGUID.Equals(Guid.Empty)) Then
                      RtGUID = Guid.NewGuid
                    End If

                    ' Apply the Fields Hashtable to the RT Data item.

                    If (RTItemOK = True) Then
                      RTItemOK = False

                      If (RTFields.Count > 0) Then
                        thisRtItem.Fields = RTFields
                        RTItemOK = True
                      End If
                    End If

                    ' Post new data item to the RT Data HashTable collection.

                    If (RTItemOK = True) Then
                      Try
                        thisRtItem.ItemStatus = DS_ObjectStatus.Pending
                        thisDSRequest.ReturnObject = thisRtItem
                        thisDSRequest.ReturnType = GetType(DataServerRTItem)
                        thisDSRequest.RequestStatus = DS_RequestStatus.RequestSubmitted

                        SyncLock RTDataItems
                          RTDataItems.Add(RtGUID, thisRtItem)
                        End SyncLock

                        ' Add RtItem to StaticDataItems if a static request is being used. 

                        If (thisRtItem.Static_RequestID.Equals(Guid.Empty) = False) Then
                          Try
                            SyncLock StaticDataItems
                              StaticDataItems.Add(BBG_StRequest.RequestId, thisRtItem)
                            End SyncLock
                          Catch ex As Exception
                            thisDSRequest.ReturnObject = String.Copy(ex.Message)
                            thisDSRequest.ReturnType = GetType(System.String)
                            thisDSRequest.RequestStatus = DS_RequestStatus.RequestFailed
                          End Try
                        End If

                      Catch ex As Exception

                        thisDSRequest.ReturnObject = String.Copy(ex.Message)
                        thisDSRequest.ReturnType = GetType(System.String)
                        thisDSRequest.RequestStatus = DS_RequestStatus.RequestFailed

                        LogError(DebugLevels.Minimal, "DataServer, RunDataServer", LOG_LEVELS.Error, ex.Message, "Error adding Item to one of the DataItems collection. (API).", ex.StackTrace)
                      End Try

                    Else
                      thisDSRequest.RequestStatus = DS_RequestStatus.RequestRejected
                    End If
                  Else

                    ' ******************************************
                    ' Static Request
                    ' ******************************************

                    If (BBG_StRequest IsNot Nothing) AndAlso (BBG_StRequest.Securities.Count > 0) AndAlso (BBG_StRequest.Fields.Count > 0) Then
                      thisDSRequest.Static_RequestID = BBG_StRequest.RequestId

                      Try
                        SyncLock PendingRequests
                          PendingRequests.Add(BBG_StRequest.RequestId, thisDSRequest)
                        End SyncLock
                        thisDSRequest.RequestStatus = DS_RequestStatus.RequestSubmitted

                      Catch ex As Exception
                        thisDSRequest.RequestStatus = DS_RequestStatus.RequestFailed
                        thisDSRequest.ReturnObject = String.Copy(ex.Message)
                        thisDSRequest.ReturnType = GetType(System.String)

                        LogError(DebugLevels.Minimal, "DataServer, RunDataServer", LOG_LEVELS.Error, ex.Message, "Error adding Item to the Pending requests collection. (API).", ex.StackTrace)
                      End Try
                    Else
                      thisDSRequest.RequestStatus = DS_RequestStatus.RequestRejected
                    End If
                  End If
                Else
                  ' RequestOK = False

                  If thisDSRequest.RequestStatus = DS_RequestStatus.None Then
                    thisDSRequest.RequestStatus = DS_RequestStatus.RequestRejected
                    RequestOK = False
                  End If
                End If

              Catch ex As Exception
                thisDSRequest.RequestStatus = DS_RequestStatus.RequestFailed
                thisDSRequest.ReturnObject = String.Copy(ex.Message)
                thisDSRequest.ReturnType = GetType(System.String)

                LogError(DebugLevels.Minimal, "DataServer, RunDataServer", LOG_LEVELS.Error, ex.Message, "Error processing API Request.", ex.StackTrace)
              End Try


            Case DS_RequestType.Debug_PendingItems
              ' ***************************************
              '  Debug request to return a list or securities in the 'Pending' collection.
              ' ***************************************






            Case DS_RequestType.Debug_RTItems
              ' ***************************************
              ' Debug request to return a list of securities in the 'RTItems' collection
              ' ***************************************







            Case Else    ' Unhandled Request Type
              thisDSRequest.RequestStatus = DS_RequestStatus.RequestRejected

          End Select

          ' Default Request Response if not handled above...
          If (thisDSRequest.RequestStatus = DS_RequestStatus.None) Or (thisDSRequest.RequestStatus = DS_RequestStatus.RequestPending) Then
            thisDSRequest.RequestStatus = DS_RequestStatus.RequestRejected
          End If

          ' Trigger Event as necesary.
          If thisDSRequest.RequestType = DS_RequestType.API_Realtime Then
            thisDSRequest.OnDataUpdate(Nothing)
          ElseIf (thisDSRequest.RequestStatus <> DS_RequestStatus.RequestPending) And (thisDSRequest.RequestStatus <> DS_RequestStatus.RequestSubmitted) Then
            thisDSRequest.OnDataUpdate(Nothing)
          End If

        End If ' If NewRequests.Count > 0

        If (LoopWorkDone = False) Then
          Threading.Thread.Sleep(25)
        End If

      Catch ex As Exception
				LogError(DebugLevels.Minimal, "DataServer, RunDataServer", LOG_LEVELS.Error, ex.Message, "Error in `RunDataServer()`, caught by the most generic error trap.", ex.StackTrace)
      Finally
        If ProcessingRequest.IsWriterLockHeld Then
          ProcessingRequest.ReleaseLock()
        End If
      End Try

		End While
  End Sub

  Public Sub RemoveRequest(ByRef pServerRequest As DataServerRequest)
    ' ***************************************************************************
    ' Routine to remove all aspects of the given request from the API Data Server
    '
    ' Request Should be removed from the Static Pending Queue and the Realtime Queues
    ' If necessary.
    '
    ' Any Associated Bloomberg Requests should be cancelled.
    ' ***************************************************************************

    ' Remove Static request

    If (Not (Guid.Empty.Equals(pServerRequest.Static_RequestID))) Then
      Try
        MarketDataAdapter.CancelRequest(pServerRequest.Static_RequestID)
      Catch ex As Exception
      End Try

      Try
        If Me.PendingRequests.Contains(pServerRequest.Static_RequestID) Then
          SyncLock PendingRequests
            Me.PendingRequests.Remove(pServerRequest.Static_RequestID)
          End SyncLock
        End If
      Catch ex As Exception
      End Try
    End If

    ' If the ReturnObject is a RT Item remove that too.

    If (pServerRequest.ReturnObject IsNot Nothing) Then
      If (TypeOf pServerRequest.ReturnObject Is DataServerRTItem) Then
        Try
          Call RemoveRequest(CType(pServerRequest.ReturnObject, DataServerRTItem))
        Catch ex As Exception
        End Try
      End If
    End If

  End Sub

  Public Sub RemoveRequest(ByRef pRTItem As DataServerRTItem)
    ' ***************************************************************************
    ' Routine to remove all aspects of the given RT Data Item from the API Data Server
    '
    ' The Item Should be remove from the RealTime Items Collection and the temporary Static
    ' StaticDataItems Collection If necessary.
    '
    ' Any Associated Bloomberg Requests should be cancelled.
    ' ***************************************************************************

    If (Not (Guid.Empty.Equals(pRTItem.Realtime_RequestID))) Then
      Try
        MarketDataAdapter.CancelRequest(pRTItem.Realtime_RequestID)
      Catch ex As Exception
      End Try

      Try
        If Me.PendingRequests.Contains(pRTItem.Realtime_RequestID) Then
          SyncLock PendingRequests
            Me.PendingRequests.Remove(pRTItem.Realtime_RequestID)
          End SyncLock
        End If
      Catch ex As Exception
      End Try
    End If

    If (Not (Guid.Empty.Equals(pRTItem.Static_RequestID))) Then
      Try
        MarketDataAdapter.CancelRequest(pRTItem.Static_RequestID)
      Catch ex As Exception
      End Try

      Try
        If Me.PendingRequests.Contains(pRTItem.Static_RequestID) Then
          SyncLock PendingRequests
            Me.PendingRequests.Remove(pRTItem.Static_RequestID)
          End SyncLock
        End If
      Catch ex As Exception
      End Try

      Try
        If Me.StaticDataItems.Contains(pRTItem.Static_RequestID) Then
          SyncLock StaticDataItems
            Me.StaticDataItems.Remove(pRTItem.Static_RequestID)
          End SyncLock
        End If
      Catch ex As Exception
      End Try

    End If

  End Sub

	Public Sub PostNewRequest(ByRef pServerRequest As DataServerRequest)
		' *******************************************************************
		' Post New DataServer Request
		' *******************************************************************
		Try
			SyncLock NewRequests
				NewRequests.Enqueue(pServerRequest)
			End SyncLock
		Catch ex As Exception
			LogError(DebugLevels.Minimal, "DataServer, PostNewRequest()", LOG_LEVELS.Error, ex.Message, "Error posting new DataServer Request.", ex.StackTrace)
		End Try
	End Sub

	Private Function StartBloombergService() As Boolean
		' *******************************************************************
		' Function to initiate Bloomberg Data service.
		'
		' Designed to be called repeatedly in the Request Process loop in order to 
		' recover from DataServer shutdowns.
		' *******************************************************************

		Try

			' Check the Adaptor is not already startyed
			If MarketDataAdapter.IsAdapterStarted = True Then
				Return True
				Exit Function
			End If

			' Check that we have not tried to start (and presumably failed) the service recently
			If Now().Subtract(TimeOfLastBloombergStartAttempt).TotalSeconds < BBGServerRetryTimeout Then
				Return False
				Exit Function
			End If

      'start initialization process, while requests are being prepared
      Try
        MarketDataAdapter.Startup()
      Catch ex As Exception
      End Try
      ftbl = MarketDataAdapter.FieldTable

			TimeOfLastBloombergStartAttempt = Now()

			' Subscribe for any status messages
			' Since this code may be run multiple times if the Adaptor stops and starts, try to remove an 
			' existing handler first.
			Try
				RemoveHandler MarketDataAdapter.StatusEvent, AddressOf OnDataServices_StatusEvent
			Catch ex As Exception
			End Try

			AddHandler MarketDataAdapter.StatusEvent, AddressOf OnDataServices_StatusEvent

		Catch ex As Exception
			LogError(DebugLevels.Minimal, "DataServer, StartBloombergService", LOG_LEVELS.Error, ex.Message, "Error Starting Bloomberg Market Data Adaptor.", ex.StackTrace)
			Return False
			Exit Function
		End Try

    ' Call LogBBGFields()

		Return True

	End Function

  'Private Sub LogBBGFields()
  '  Dim BBGField As Bloomberg.Api.DataTypes.Field
  '  Dim FieldCount As Integer


  '  For FieldCount = 0 To (ftbl.FieldCount - 1)
  '    Try
  '      BBGField = ftbl.Item(FieldCount)

  '      If (BBGField.IsRealTime) AndAlso BBGField.Mnemonic.Contains("PRICE") Then
  '        Debug.Print(BBGField.Mnemonic & ", " & BBGField.IsRealTime.ToString)
  '      ElseIf BBGField.Mnemonic.StartsWith("PX_") Then
  '        Debug.Print(BBGField.Mnemonic & ", " & BBGField.IsRealTime.ToString)
  '      End If
  '    Catch ex As Exception
  '    End Try
  '  Next

  '  ' ftbl
  'End Sub

  Private Sub OnDataServices_StatusEvent(ByVal status As StatusCode, ByVal description As String)
    ' *******************************************************************
    'NOTE: this callback happens on a background thread.
    'When dealing with GUI elements please use 
    'Control.Invoke/Control.BeginInvoke to marshal the call back to 
    'the thread that created the Control.
    ' *******************************************************************

    Dim tempString As String
    Try

      tempString = "BBG Data Adaptor Status Event : " & System.Enum.GetName(GetType(Bloomberg.Api.StatusCode), status)

      LogError(DebugLevels.Medium, "DataServer, OnDataServices_StatusEvent", LOG_LEVELS.Error, "", tempString, "")

      Select Case status
        Case StatusCode.SessionDisconnected
          ' If the Session Disconnects, do not wait to re-connect.
          TimeOfLastBloombergStartAttempt = CDate("1 Jan 1900")

        Case StatusCode.SessionConnected
          If (MarketDataAdapter.IsAdapterStarted) Then
            Try
              MarketDataAdapter.InvalidateRealtimeCache()
              MarketDataAdapter.SynchInvalidateRealtimeCache()
            Catch ex As Exception
            End Try

            Try
              RePostAllRTDataItemRequests()
            Catch ex As Exception
            End Try
          End If

        Case Else

      End Select

    Catch ex As Exception
      LogError(DebugLevels.Minimal, "DataServer, OnDataServices_StatusEvent", LOG_LEVELS.Error, ex.Message, "Error processing Event.", ex.StackTrace)
    End Try

  End Sub

  Private Sub BBG_ReplyEvent(ByVal Reply As Reply)
    ' ***********************************************
    ' Process BBG Data reply
    '
    ' Process all data replies. Both for Static and Realtime
    ' Data subscriptions.
    ' ***********************************************

    Dim RequestGUID As Guid
    Dim secItem As SecurityDataItem
    Dim fldItem As FieldDataItem
    Dim point As DataPoint

    Dim FieldsData As Hashtable

    Dim thisDataServerItem As DataServerRTItem

    Try

      ' ***********************************************
      ' Get an exclusive lock on the 'ProcessingRequest' lock.
      ' This prevents Reply Events being processed before the Request is fully actioned.
      '
      ' ***********************************************
			ProcessingRequest.AcquireWriterLock(1000)

      ' Resolve What Pending or RTDataItem this applies to.

      RequestGUID = Reply.Request.RequestId

      ' ***********************************************
      ' Process any related Pending Requests
      ' ***********************************************

      If PendingRequests.ContainsKey(RequestGUID) Then
        Dim thisDataServerRequest As DataServerRequest = Nothing

				Try
					SyncLock PendingRequests
						thisDataServerRequest = CType(PendingRequests(RequestGUID), DataServerRequest)
					End SyncLock

					FieldsData = New Hashtable

					'go through securities
					For Each secItem In Reply.GetSecurityDataItems()
						'Console.Write(ControlChars.Tab)
						'Console.WriteLine("Security: {0}", secItem.Security.FullName)

						'go through fields
						For Each fldItem In secItem.FieldsData
							'Console.Write(ControlChars.Tab + ControlChars.Tab)
							'Console.Write(fldItem.Field.Mnemonic + ": ")

							'go through field values
							For Each point In fldItem.DataPoints
								If (point.IsError) Then
									'Console.WriteLine(point.ReplyError.DisplayName)
								Else
									FieldsData.Add(fldItem.Field.Mnemonic, New DataServerRTField(fldItem.Field.Mnemonic, Now(), point.Value))
									'Console.WriteLine(point.Value)
								End If
							Next point
						Next fldItem

						' Process only the first security. There should be no more than one !
						Exit For
					Next secItem

					' ***********************************************
					' Update the Request Object with the Field Data.
					' ***********************************************

					If (thisDataServerRequest IsNot Nothing) Then
						SyncLock thisDataServerRequest
							thisDataServerRequest.ReturnObject = FieldsData
							thisDataServerRequest.ReturnType = GetType(Hashtable)
							thisDataServerRequest.RequestStatus = DS_RequestStatus.RequestComplete
						End SyncLock
					End If

				Catch ex As Exception

					' ***********************************************
					' In the event of an error, Return the error message in the Request Object.
					' ***********************************************

					If (thisDataServerRequest IsNot Nothing) Then
						SyncLock thisDataServerRequest
							thisDataServerRequest.ReturnObject = String.Copy(ex.Message)
							thisDataServerRequest.ReturnType = GetType(System.String)
							thisDataServerRequest.RequestStatus = DS_RequestStatus.RequestFailed
						End SyncLock
					End If
				Finally

					' ***********************************************
					' Trigger the OnUpdate Event
					' ***********************************************

					Try
						If (thisDataServerRequest IsNot Nothing) Then
							thisDataServerRequest.OnDataUpdate(Reply)
						End If
					Catch ex As Exception
					End Try

					' ***********************************************
					' Remove from the PendingRequests Collection
					' ***********************************************

					SyncLock PendingRequests
						PendingRequests.Remove(RequestGUID)
					End SyncLock

					Try
						' Remove Request from Bloomberg Server
						MarketDataAdapter.CancelRequest(RequestGUID)
					Catch ex As Exception
					End Try

				End Try
      End If

      ' ***********************************************
      ' Process Realtime Data Items
      ' ***********************************************

      thisDataServerItem = Nothing

      If RTDataItems.ContainsKey(RequestGUID) Then
        ' Is it an ongoing RT Update ?

        Try
          SyncLock RTDataItems
            thisDataServerItem = CType(RTDataItems(RequestGUID), DataServerRTItem)
          End SyncLock

          SyncLock thisDataServerItem
            thisDataServerItem.Realtime_Request = Reply.Request
          End SyncLock
        Catch ex As Exception
        End Try

      ElseIf StaticDataItems.ContainsKey(RequestGUID) Then
        ' Is it a one-off static update ?

        Try
          thisDataServerItem = CType(StaticDataItems(RequestGUID), DataServerRTItem)
          SyncLock StaticDataItems
            StaticDataItems.Remove(RequestGUID)
          End SyncLock
          Try
            ' Remove Request from Bloomberg Server
            MarketDataAdapter.CancelRequest(RequestGUID)
          Catch ex As Exception
          End Try
        Catch ex As Exception
        End Try

      Else ' Orphan Update ?
        Try
          ' Remove Request from Bloomberg Server
          MarketDataAdapter.CancelRequest(RequestGUID)
        Catch ex As Exception
        End Try

      End If


      If (thisDataServerItem IsNot Nothing) Then
        ' ***********************************************
        ' A related DataServer Item was found, Update it...
        ' ***********************************************
        Dim FieldChanged As Boolean = False
        Dim OldField As DataServerRTField
        ' Dim NewField As DataServerRTField

        Try
					thisDataServerItem.ItemLock.AcquireWriterLock(1000)

          thisDataServerItem.ItemStatus = DS_ObjectStatus.Live
          thisDataServerItem.Realtime_Request = Reply.Request

          FieldsData = thisDataServerItem.Fields

          'go through securities
          For Each secItem In Reply.GetSecurityDataItems()

            ' If this security matches the DataServerItem then ....
            If thisDataServerItem.BBGSecurity.Equals(secItem.Security) Then

              'go through fields
              For Each fldItem In secItem.FieldsData

                'go through field values
                For Each point In fldItem.DataPoints
                  If (point.IsError) Then
                  Else

                    ' Update Fields Hatshtable.
                    If (FieldsData.ContainsKey(fldItem.Field.Mnemonic)) Then
                      OldField = CType(FieldsData(fldItem.Field.Mnemonic), DataServerRTField)
                      If (OldField.FieldValue.GetType Is GetType(Date)) Then
                        If Not (CType(OldField.FieldValue, Date).Equals(CType(point.Value, Date))) Then
                          OldField.FieldValue = point.Value
                          FieldChanged = True
                        End If
                      ElseIf (OldField.FieldValue.GetType Is GetType(String)) Then
                        If Not (CType(OldField.FieldValue, String).Equals(CType(point.Value, String))) Then
                          OldField.FieldValue = point.Value
                          FieldChanged = True
                        End If
                      ElseIf (OldField.FieldValue.GetType Is GetType(Double)) Then
                        If Not (CType(OldField.FieldValue, Double).Equals(CType(point.Value, Double))) Then
                          OldField.FieldValue = point.Value
                          FieldChanged = True
                        End If
                      ElseIf (OldField.FieldValue.GetType Is GetType(Integer)) Then
                        If Not (CType(OldField.FieldValue, Integer).Equals(CType(point.Value, Integer))) Then
                          OldField.FieldValue = point.Value
                          FieldChanged = True
                        End If
                      Else
                        OldField.FieldValue = point.Value
                        FieldChanged = True
                      End If

                      ' Record time of Update.
                      OldField.DateValue = Now()
                    End If
                  End If
                Next point
              Next fldItem
            End If
          Next secItem

        Catch ex As Exception
        Finally
          thisDataServerItem.ItemLock.ReleaseWriterLock()
          Try
            If (FieldChanged = True) Then
              thisDataServerItem.OnDataUpdate(Reply)
            End If
          Catch ex As Exception
          End Try

        End Try

      End If

    Catch ex As Exception
    Finally

      ' Release Lock
      Try
        If ProcessingRequest.IsWriterLockHeld Then
          ProcessingRequest.ReleaseLock()
        End If
      Catch ex As Exception
      End Try
    End Try


  End Sub

  Public Sub RePostAllRTDataItemRequests()
    ' **************************************************************************
    ' Routine to cancel and resubmit all realtime bloomberg requests.
    '
    ' **************************************************************************

    Dim KeysCollection As ICollection
    Dim ThisKey As Object
    Dim thisDataServerItem As DataServerRTItem
    Dim thisBBGRequest As Bloomberg.Api.Request

		If (True) Then
			' NPP 14 May 2007. 
			' Not sure this works, so am diabling it for the moment.
			Exit Sub
		End If

    SyncLock RTDataItems
      KeysCollection = RTDataItems.Keys
    End SyncLock

    Try
      LogError(DebugLevels.Minimal, "DataServer, RePostAllRTDataItemRequests", LOG_LEVELS.Audit, "", "Running RePostAllRTDataItemRequests()", "")
    Catch ex As Exception
    End Try

    Try
      For Each ThisKey In KeysCollection
        Try
          SyncLock RTDataItems
            If RTDataItems.Contains(ThisKey) Then
              thisDataServerItem = TryCast(RTDataItems(ThisKey), DataServerRTItem)
            Else
              thisDataServerItem = Nothing
            End If

            If (thisDataServerItem IsNot Nothing) AndAlso (thisDataServerItem.Realtime_Request IsNot Nothing) Then
              thisBBGRequest = thisDataServerItem.Realtime_Request

              Try
                MarketDataAdapter.CancelRequest(thisBBGRequest.RequestId)
              Catch ex As Exception
                LogError(DebugLevels.Minimal, "DataServer, RePostAllRTDataItemRequests", LOG_LEVELS.Error, ex.Message, "Error Canceling BBG Request", ex.StackTrace)
              End Try

              Try
                MarketDataAdapter.SendRequest(thisBBGRequest)
              Catch ex As Exception
                LogError(DebugLevels.Minimal, "DataServer, RePostAllRTDataItemRequests", LOG_LEVELS.Error, ex.Message, "Error Posting BBG Request", ex.StackTrace)
              End Try
            End If

          End SyncLock

        Catch ex As Exception
          LogError(DebugLevels.Minimal, "DataServer, RePostAllRTDataItemRequests", LOG_LEVELS.Error, ex.Message, "Error in RePostAllRTDataItemRequests()", ex.StackTrace)
        End Try
      Next
    Catch ex As Exception
      LogError(DebugLevels.Minimal, "DataServer, RePostAllRTDataItemRequests", LOG_LEVELS.Error, ex.Message, "Error in RePostAllRTDataItemRequests()", ex.StackTrace)
    End Try


  End Sub

  Private Function LogError( _
    ByVal pDebugLevel As DebugLevels, _
    ByVal p_Error_Location As String, _
    ByVal p_System_Error_Number As Integer, _
    ByVal p_System_Error_Description As String, _
    Optional ByVal p_Error_Message As String = "", _
    Optional ByVal p_Error_StackTrace As String = "", _
    Optional ByVal p_Log_to As Integer = 0) As Integer

    Try
      If Not (NaplesInitiator Is Nothing) Then
        If Not (NaplesInitiator.Naples_DataBaseClient Is Nothing) Then
          Return NaplesInitiator.Naples_DataBaseClient.LogError(pDebugLevel, p_Error_Location, p_System_Error_Number, p_System_Error_Description, p_Error_Message, p_Error_StackTrace, p_Log_to)
        End If
      End If

    Catch ex As Exception
      Return (-1)
    End Try
  End Function

#Region " Debug"

	Friend Function Debug_GetRTItemDetails() As ArrayList
		Dim RValArraylist As New ArrayList

		Dim KeyCollection As ICollection
		Dim ThisKey As Object
		Dim thisDataServerItem As DataServerRTItem
		Dim RTItemCount As Integer = 1

		SyncLock RTDataItems
			KeyCollection = RTDataItems.Keys
		End SyncLock
		For Each ThisKey In KeyCollection
			thisDataServerItem = Nothing
			Try
				SyncLock RTDataItems
					If (RTDataItems.ContainsKey(ThisKey)) Then
						thisDataServerItem = RTDataItems(ThisKey)
					End If
				End SyncLock
			Catch ex As Exception
				thisDataServerItem = Nothing
			End Try

			If (thisDataServerItem IsNot Nothing) Then
				Try
					thisDataServerItem.ItemLock.AcquireReaderLock(1000)

					RValArraylist.Add(CStr("  "))
					RValArraylist.Add(CStr("RT Data Item " & RTItemCount.ToString & " of " & KeyCollection.Count.ToString))
					RValArraylist.Add(CStr("  ItemStatus = " & thisDataServerItem.ItemStatus.ToString))
					RValArraylist.Add(CStr("  FullName = " & thisDataServerItem.BBGSecurity.FullName.ToString))

					Dim FieldItem As DataServerRTField
					Dim Item As DictionaryEntry
					SyncLock thisDataServerItem
						If (thisDataServerItem.Fields IsNot Nothing) Then
							For Each Item In thisDataServerItem.Fields
								If (Item.Value IsNot Nothing) Then
									Try
										FieldItem = CType(Item.Value, DataServerRTField)
										RValArraylist.Add(CStr("    " & FieldItem.FieldName.ToString & " = " & FieldItem.FieldValue.ToString & ", Date = " & FieldItem.DateValue.ToString(DISPLAYMEMBER_LONGDATEFORMAT)))
									Catch ex As Exception
									End Try
								End If
							Next
						Else
							RValArraylist.Add(CStr("  Fields = 'Nothing'"))
						End If
					End SyncLock

				Catch ex As Exception
				Finally
					Try
						If (thisDataServerItem.ItemLock.IsReaderLockHeld) Then
							thisDataServerItem.ItemLock.ReleaseReaderLock()
						End If
					Catch ex As Exception
					End Try
				End Try

			End If

			RTItemCount += 1
		Next

		Return RValArraylist
	End Function

	Friend Function Debug_GetPendingItemDetails() As ArrayList
		Dim RValArraylist As New ArrayList

		Dim KeyCollection As ICollection
		Dim ThisKey As Object
		Dim thisDSRequest As DataServerRequest
		Dim RequestCounter As Integer = 1

		SyncLock PendingRequests
			KeyCollection = PendingRequests.Keys
		End SyncLock
		For Each ThisKey In KeyCollection
			thisDSRequest = Nothing

			SyncLock PendingRequests
				Try
					If (PendingRequests.ContainsKey(ThisKey)) Then
						thisDSRequest = PendingRequests(ThisKey)
					End If
				Catch ex As Exception
					thisDSRequest = Nothing
				End Try
			End SyncLock

			If (thisDSRequest IsNot Nothing) Then
				SyncLock thisDSRequest
					Try

						RValArraylist.Add(CStr("  "))
						RValArraylist.Add(CStr("Pending Request " & RValArraylist.ToString & " of " & KeyCollection.Count.ToString))
						RValArraylist.Add(CStr("  RequestType = " & thisDSRequest.RequestType.ToString))
						RValArraylist.Add(CStr("  RequestStatus = " & thisDSRequest.RequestStatus.ToString))
						RValArraylist.Add(CStr("  Request = " & thisDSRequest.Request.ToString))
						RValArraylist.Add(CStr("  RequestTime = " & thisDSRequest.RequestTime.ToString(DISPLAYMEMBER_LONGDATEFORMAT)))

					Catch ex As Exception
					End Try
				End SyncLock
			End If

			RequestCounter += 1
		Next

		Return RValArraylist
	End Function

#End Region

End Class

