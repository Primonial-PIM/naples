Imports System.Net
Imports System.Net.Sockets
Imports System.Net.Mail
Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.Serialization

Imports NaplesGlobals
Imports RenaissanceGlobals

Public Class EMail_Server

  ' *****************************************************************************************************
  ' EMail Server CLASS
  ' -----------------
  '
	' General Purpose Class to process incoming and outgoing EMails. 
  '
  '
  ' *****************************************************************************************************

  Inherits ThreadWrapperBase

#Region " Locals"

	Private _CloseDownFlag As Boolean
	Private _CloseDownFlagLock As New Object

	Private NaplesInitiator As Initiator

	Private _EMailWorkQueue As New Queue

	Private _EMAIL_FromAddress As String = "Naples@primonial.com"
	Private _EMail_SMTPServer As String = "mailer.patrimoine.net"	' Not correct ?!?

#End Region

#Region " Properties "

	Friend Property EMAIL_FromAddress() As String
		' *****************************************************************************************************
		' "From" Address to use when sending EMails.
		'
		' *****************************************************************************************************

		Get
			Return _EMAIL_FromAddress
		End Get
		Set(ByVal value As String)
			If value.Length > 0 Then
				_EMAIL_FromAddress = value
			End If
		End Set
	End Property

	Friend Property EMail_SMTPServer() As String
		' *****************************************************************************************************
		' SMTP Server to use to send EMails.
		'
		' *****************************************************************************************************

		Get
			Return _EMail_SMTPServer
		End Get
		Set(ByVal value As String)
			If value.Length > 0 Then
				_EMail_SMTPServer = value
			End If
		End Set
	End Property

	Public Property CloseDown() As Boolean
		' *****************************************************************************************************
		' 
		'
		' *****************************************************************************************************

		Get
			SyncLock _CloseDownFlagLock
				Return _CloseDownFlag
			End SyncLock
		End Get
		Set(ByVal Value As Boolean)
			SyncLock _CloseDownFlagLock
				_CloseDownFlag = Value
			End SyncLock
		End Set
	End Property

#End Region

#Region " Public Classes"

	Public Class EMailTask_DataTaskUpdate
		Private _ThisDataTask As AggregatorDataTask
		Private _ServerRequestIndex As Integer
		Private _LastPrice As Double
		Private _ReferencePrice As Double
		Private _ReferenceDate As Date
		Private _UpdateTime As Date

		Public Sub New(ByVal pThisDataTask As AggregatorDataTask, ByVal pServerRequestIndex As Integer, ByVal pLastPrice As Double, ByVal pReferencePrice As Double, ByVal pReferenceDate As Date, ByVal pUpdateTime As Date)
			Try
				_ThisDataTask = pThisDataTask
				ServerRequestIndex = pServerRequestIndex
				_LastPrice = pLastPrice
				_ReferencePrice = pReferencePrice
				_ReferenceDate = pReferenceDate
				_UpdateTime = pUpdateTime
			Catch ex As Exception
			End Try
		End Sub

		Public Property DataTask() As AggregatorDataTask
			Get
				Return _ThisDataTask
			End Get
			Set(ByVal value As AggregatorDataTask)
				Try
					If (value IsNot Nothing) Then
						_ThisDataTask = value
					End If
				Catch ex As Exception
				End Try
			End Set
		End Property

		Public Property ServerRequestIndex() As Integer
			Get
				Return _ServerRequestIndex
			End Get
			Set(ByVal value As Integer)
				_ServerRequestIndex = value
			End Set
		End Property

		Public Property LastPrice() As Double
			Get
				Return _LastPrice
			End Get
			Set(ByVal value As Double)
				_LastPrice = value
			End Set
		End Property

		Public Property ReferencePrice() As Double
			Get
				Return _ReferencePrice
			End Get
			Set(ByVal value As Double)
				_ReferencePrice = value
			End Set
		End Property

		Public Property ReferenceDate() As Date
			Get
				Return _ReferenceDate
			End Get
			Set(ByVal value As Date)
				_ReferenceDate = value
			End Set
		End Property

		Public Property UpdateTime() As Date
			Get
				Return _UpdateTime
			End Get
			Set(ByVal value As Date)
				_UpdateTime = value
			End Set
		End Property

	End Class

	Public Class EMailTask_SendDataTaskPendingEmails
		Private _ThisDataTask As AggregatorDataTask

		Public Sub New(ByVal pThisDataTask As AggregatorDataTask)
			Try
				_ThisDataTask = pThisDataTask
			Catch ex As Exception
			End Try
		End Sub

		Public Property DataTask() As AggregatorDataTask
			Get
				Return _ThisDataTask
			End Get
			Set(ByVal value As AggregatorDataTask)
				Try
					If (value IsNot Nothing) Then
						_ThisDataTask = value
					End If
				Catch ex As Exception
				End Try
			End Set
		End Property

	End Class

#End Region

#Region " Class / Thread Initialisation Functions"

	Public Sub New(ByVal pInitiator As Initiator)
		' *****************************************************************************************************
		'
		'
		' *****************************************************************************************************

		MyBase.New()

		Me.Thread.Name = "EMail_Server"
		NaplesInitiator = pInitiator
	End Sub

	Protected Overrides Sub DoTask()
		' *****************************************************************************************************
		'
		'
		' *****************************************************************************************************

		Initialise()

		RunDataServer()

		TidyUp()

	End Sub

	Private Sub Initialise()
		' *****************************************************************************************************
		'
		'
		' *****************************************************************************************************

		_CloseDownFlag = False

		_EMAIL_FromAddress = EMAIL_DefaultFromAddress
		_EMail_SMTPServer = EMAIL_DefaultSMTPServer

	End Sub

	Private Sub TidyUp()
		' *****************************************************************************************************
		' On ending the Server
		'
		'
		' *****************************************************************************************************



	End Sub

#End Region

#Region " Run Data Server"

	Private Sub RunDataServer()
		' *****************************************************************************************************
		'
		'
		' *****************************************************************************************************
		Dim ThisCloseDownFlag As Boolean = False
		Dim thisEmailTask As Object

		Dim WorkDone As Boolean = False

		' Work loop.
		While (ThisCloseDownFlag = False)
			Try

				If WorkDone Then WorkDone = False


				' Monitor Incoming EMails


				' Monitor EMail Tasks
				thisEmailTask = PopEmailTask()

				If (thisEmailTask IsNot Nothing) Then
					Try
						WorkDone = True

						If (TypeOf thisEmailTask Is EMailTask_DataTaskUpdate) Then
							Call ProcessDataTaskUpdate(CType(thisEmailTask, EMailTask_DataTaskUpdate))

						ElseIf (TypeOf thisEmailTask Is EMailTask_SendDataTaskPendingEmails) Then
							Call ProcessDataTaskPendingEmails(CType(thisEmailTask, EMailTask_SendDataTaskPendingEmails))

							'ElseIf (TypeOf thisEmailTask Is XXXXXXX) Then


						End If



					Catch ex As Exception
						LogError(DebugLevels.Minimal, "EmailServer, RunDataServer()", LOG_LEVELS.Error, ex.Message, "Error in RunDataServer().", ex.StackTrace)
					End Try
				End If

			Catch ex As Exception
				LogError(DebugLevels.Minimal, "EmailServer, RunDataServer()", LOG_LEVELS.Error, ex.Message, "Error in RunDataServer().", ex.StackTrace)
			End Try

			' End Of Loop 

			Try
				SyncLock _CloseDownFlagLock
					ThisCloseDownFlag = _CloseDownFlag
				End SyncLock

				If (Not WorkDone) AndAlso (Not ThisCloseDownFlag) Then
					Threading.Thread.Sleep(25)
				End If
			Catch ex As Exception
			End Try

		End While

	End Sub

#End Region

#Region " Work Queue Functions "

	Public Sub PostEmailTask(ByRef pEMailTask As Object)
		' *******************************************************************
		' Post New Email Request
		' *******************************************************************

		Try
			SyncLock _EMailWorkQueue
				_EMailWorkQueue.Enqueue(pEMailTask)
			End SyncLock
		Catch ex As Exception
			LogError(DebugLevels.Minimal, "EmailServer, PostEmailTask()", LOG_LEVELS.Error, ex.Message, "Error posting new DataServer Request.", ex.StackTrace)
		End Try
	End Sub

	Public Function PopEmailTask() As Object
		' *******************************************************************
		' Pop New Email Request
		' *******************************************************************

		Try
			SyncLock _EMailWorkQueue
				If _EMailWorkQueue.Count > 0 Then
					Dim RVal As Object

					RVal = _EMailWorkQueue.Dequeue

					Return RVal
				End If
			End SyncLock
		Catch ex As Exception
			LogError(DebugLevels.Minimal, "EmailServer, PopEmailTask()", LOG_LEVELS.Error, ex.Message, "Error posting new DataServer Request.", ex.StackTrace)
		End Try

		Return Nothing
	End Function
#End Region

#Region " Work Functions "

	Private Sub ProcessDataTaskUpdate(ByVal thisDataTaskUpdate As EMailTask_DataTaskUpdate)
    ' *****************************************************************************************************
    '
    '
    ' *****************************************************************************************************

		If (thisDataTaskUpdate Is Nothing) Then
			Exit Sub
		End If

		Dim thisDataTask As AggregatorDataTask = thisDataTaskUpdate.DataTask
		Dim ServerRequestIndex As Integer = thisDataTaskUpdate.ServerRequestIndex
		Dim LastValue As Double = thisDataTaskUpdate.LastPrice
		Dim thisReferencePrice As Double = thisDataTaskUpdate.ReferencePrice
		Dim thisReferenceDate As Date = thisDataTaskUpdate.ReferenceDate

		' Send EMail or Cache Email Update.
		Try
			Dim thisEmailPersonIDs(-1) As Integer
			Dim thisTaskDescription As String
			Dim thisTaskType As String
			Dim thisTicker As String
			Dim thisRequest As String
      Dim thisInstrumentName As String
      Dim thisPortfolioName As String = ""
      Dim thisPercentChange As Double
			Dim thisSubject As String
			Dim thisBody As String

			' Get Basic information.

			thisEmailPersonIDs = thisDataTask.EmailPersonIDsArray
			thisTaskDescription = thisDataTask.TaskDescription
			thisTaskType = thisDataTask.TaskType.ToString
			thisRequest = thisDataTask.ServerRequest(ServerRequestIndex).Request
			If (thisRequest.Contains("|")) Then
				thisTicker = thisRequest.Substring(0, thisRequest.IndexOf("|"))
			Else
				thisTicker = thisRequest
			End If
			thisInstrumentName = thisTicker
			thisPercentChange = ((LastValue / thisReferencePrice) - 1.0)

			' Lookup Market Instrument Details

			Try
				Dim MktInstrumentsDS As New RenaissanceDataClass.DSMarketInstruments
				Dim SelectedMktInstrumentRows() As RenaissanceDataClass.DSMarketInstruments.tblMarketInstrumentsRow

				MktInstrumentsDS = Me.NaplesInitiator.Naples_DataBaseClient.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblMarketInstruments, True)
				If (MktInstrumentsDS IsNot Nothing) AndAlso (MktInstrumentsDS.tblMarketInstruments IsNot Nothing) AndAlso (MktInstrumentsDS.tblMarketInstruments.Rows.Count > 0) Then
					SelectedMktInstrumentRows = MktInstrumentsDS.tblMarketInstruments.Select("Ticker='" & thisTicker & "'")
					If (SelectedMktInstrumentRows IsNot Nothing) AndAlso (SelectedMktInstrumentRows.Length > 0) Then
						thisInstrumentName = SelectedMktInstrumentRows(0).Description
					End If
				End If
			Catch ex As Exception
				LogError(DebugLevels.Minimal, "EMail_Server, ProcessDataTaskUpdate()", RenaissanceGlobals.LOG_LEVELS.Error, ex.Message, "Error resolving Instrument name.", ex.StackTrace, 0)
			End Try

			If (thisDataTask.AggregateEmails) Then
				thisBody = "(" & thisTicker & ") " & thisInstrumentName & ControlChars.NewLine & _
				"    Last Price " & LastValue.ToString("#,##0.00") & ControlChars.NewLine & _
				"    % Change = " & thisPercentChange.ToString("#,##0.00%") & " since " & thisReferenceDate.ToString(DISPLAYMEMBER_LONGDATEFORMAT)

				Me.NaplesInitiator.Naples_DataBaseClient.SaveDataTaskEmail(thisDataTask.TaskID, thisBody)
			Else
				Try
					' Resolve Portfolio Name.

					Dim PortfolioIndexDS As New RenaissanceDataClass.DSPortfolioIndex
					Dim SelectedPortfolioIndexRows() As RenaissanceDataClass.DSPortfolioIndex.tblPortfolioIndexRow
					thisPortfolioName = thisDataTask.Portfolio

					If (thisDataTask.PortfolioID > 0) Then
						PortfolioIndexDS = Me.NaplesInitiator.Naples_DataBaseClient.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblPortfolioIndex, True)

						If (PortfolioIndexDS IsNot Nothing) AndAlso (PortfolioIndexDS.tblPortfolioIndex IsNot Nothing) AndAlso (PortfolioIndexDS.tblPortfolioIndex.Rows.Count > 0) Then
							SelectedPortfolioIndexRows = PortfolioIndexDS.tblPortfolioIndex.Select("PortfolioID=" & thisDataTask.PortfolioID)
							If (SelectedPortfolioIndexRows IsNot Nothing) AndAlso (SelectedPortfolioIndexRows.Length > 0) Then
								thisPortfolioName = SelectedPortfolioIndexRows(0).PortfolioDescription
							End If
						End If
					End If
				Catch ex As Exception
					LogError(DebugLevels.Minimal, "EMail_Server, ProcessDataTaskUpdate()", RenaissanceGlobals.LOG_LEVELS.Error, ex.Message, "Error resolving Portfolio name.", ex.StackTrace, 0)
				End Try

				thisSubject = thisPortfolioName & ", " & thisPercentChange.ToString("#,##0.00%") & ", " & thisInstrumentName
				thisBody = "Naples Update Notification" & vbCrLf
				thisBody &= "Portfolio : " & thisPortfolioName & vbCrLf
				thisBody &= "" & vbCrLf
				thisBody &= "(" & thisTicker & ") " & thisInstrumentName & vbCrLf
				thisBody &= "Last Price " & LastValue.ToString("#,##0.00") & vbCrLf
				thisBody &= "Reference Price " & thisReferencePrice.ToString("#,##0.00") & vbCrLf
				thisBody &= thisPercentChange.ToString("#,##0.00%") & " move since " & thisReferenceDate.ToString(DISPLAYMEMBER_LONGDATEFORMAT) & vbCrLf
				thisBody &= "" & vbCrLf

				' Append Comment
				Try
					If thisDataTask.TaskComment.Length > 0 Then
						Dim CommentString As String = thisDataTask.TaskComment

						While CommentString.Length > 0
							If CommentString.Length > 50 Then
								thisBody &= CommentString.Substring(0, 50) & vbCrLf
								CommentString = CommentString.Substring(50)
							Else
								thisBody &= CommentString & vbCrLf
								CommentString = ""
							End If
						End While

					End If
				Catch ex As Exception
				End Try

				SendEMail(thisEmailPersonIDs, thisSubject, thisBody)
			End If
		Catch ex As Exception
			LogError(DebugLevels.Minimal, "EMail_Server, ProcessDataTaskUpdate()", RenaissanceGlobals.LOG_LEVELS.Error, ex.Message, "Error in ProcessDataTaskUpdate()", ex.StackTrace, 0)
		End Try

	End Sub

	Friend Sub ProcessDataTaskPendingEmails(ByVal thisDataTaskUpdate As EMailTask_SendDataTaskPendingEmails)
    ' *****************************************************************************************************
    '
    '
    ' *****************************************************************************************************

		If (thisDataTaskUpdate Is Nothing) Then
			Exit Sub
		End If

		Dim PendingEmails As DataTable
		Dim thisEmailPersonIDs(-1) As Integer
		Dim thisSubject As String = ""
		Dim thisBody As String = ""
		Dim thisDataRow As DataRow = Nothing
    Dim thisPortfolioName As String = ""
		Dim MaxRN As Integer = 0

    Try
      PendingEmails = Me.NaplesInitiator.Naples_DataBaseClient.GetDataTaskEmail(thisDataTaskUpdate.DataTask.TaskID)

      If (PendingEmails.Rows.Count <= 0) Then
        Exit Sub
      End If
		Catch ex As Exception
			Try
				LogError(DebugLevels.Minimal, "EMail_Server, ProcessDataTaskPendingEmails()", RenaissanceGlobals.LOG_LEVELS.Error, ex.Message, "Error calling Naples_DataBaseClient.GetDataTaskEmail(" & thisDataTaskUpdate.DataTask.TaskID.ToString & ")", ex.StackTrace, 0)
			Catch Inner_Ex As Exception
			End Try
			Exit Sub
    End Try

		Try
			' Resolve Portfolio

			Dim PortfolioIndexDS As New RenaissanceDataClass.DSPortfolioIndex
			Dim SelectedPortfolioIndexRows() As RenaissanceDataClass.DSPortfolioIndex.tblPortfolioIndexRow
			thisPortfolioName = thisDataTaskUpdate.DataTask.Portfolio

			If (thisDataTaskUpdate.DataTask.PortfolioID > 0) Then
				PortfolioIndexDS = Me.NaplesInitiator.Naples_DataBaseClient.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblPortfolioIndex, True)
				If (PortfolioIndexDS IsNot Nothing) AndAlso (PortfolioIndexDS.tblPortfolioIndex IsNot Nothing) AndAlso (PortfolioIndexDS.tblPortfolioIndex.Rows.Count > 0) Then
					SelectedPortfolioIndexRows = PortfolioIndexDS.tblPortfolioIndex.Select("PortfolioID=" & thisDataTaskUpdate.DataTask.PortfolioID)
					If (SelectedPortfolioIndexRows IsNot Nothing) AndAlso (SelectedPortfolioIndexRows.Length > 0) Then
						thisPortfolioName = SelectedPortfolioIndexRows(0).PortfolioDescription
					End If
				End If
			End If
		Catch ex As Exception
			LogError(DebugLevels.Minimal, "EMail_Server, ProcessDataTaskPendingEmails()", RenaissanceGlobals.LOG_LEVELS.Error, ex.Message, "Error resolving Portfolio name.", ex.StackTrace, 0)
		End Try

		Try
			' Build Email Body
			thisEmailPersonIDs = thisDataTaskUpdate.DataTask.EmailPersonIDsArray

			Select Case thisDataTaskUpdate.DataTask.TaskType
				Case DataTaskType.EmailSingleStockChangeNotification
					thisSubject = thisDataTaskUpdate.DataTask.TaskDescription & ", " & thisDataTaskUpdate.DataTask.Instrument
					thisBody = ""

				Case DataTaskType.EmailPortfolioStockChangeNotification
					thisSubject = thisDataTaskUpdate.DataTask.TaskDescription & ", " & thisDataTaskUpdate.DataTask.Portfolio

					thisBody = "Portfolio : " & thisPortfolioName & vbCrLf

				Case Else
					thisSubject = "Naples Update."
					thisBody = ""
			End Select

			thisBody &= vbCrLf
			MaxRN = 0
			For Each thisDataRow In PendingEmails.Rows
				thisBody &= CStr(thisDataRow("EMailLine")).Trim & vbCrLf
				If CInt(thisDataRow("RN")) > MaxRN Then
					MaxRN = CInt(thisDataRow("RN"))
				End If
			Next

			' Append Comment
			Try
				If thisDataTaskUpdate.DataTask.TaskComment.Length > 0 Then
					Dim CommentString As String = thisDataTaskUpdate.DataTask.TaskComment

					While CommentString.Length > 0
						If CommentString.Length > 50 Then
							thisBody &= CommentString.Substring(0, 50) & vbCrLf
							CommentString = CommentString.Substring(50)
						Else
							thisBody &= CommentString & vbCrLf
							CommentString = ""
						End If
					End While

				End If
			Catch ex As Exception
			End Try

		Catch ex As Exception
			LogError(DebugLevels.Minimal, "EMail_Server, ProcessDataTaskPendingEmails()", RenaissanceGlobals.LOG_LEVELS.Error, ex.Message, "Error building EMail body.", ex.StackTrace, 0)
		End Try

		Try
			SendEMail(thisEmailPersonIDs, thisSubject, thisBody)
		Catch ex As Exception
			LogError(DebugLevels.Minimal, "EMail_Server, ProcessDataTaskPendingEmails()", RenaissanceGlobals.LOG_LEVELS.Error, ex.Message, "Error calling SendEMail().", ex.StackTrace, 0)
		End Try

		Try
			Call NaplesInitiator.Naples_DataBaseClient.ClearPendingEmails(thisDataTaskUpdate.DataTask.TaskID, MaxRN)
		Catch ex As Exception
			LogError(DebugLevels.Minimal, "EMail_Server, ProcessDataTaskPendingEmails()", RenaissanceGlobals.LOG_LEVELS.Error, ex.Message, "Error calling Naples_DataBaseClient.ClearPendingEmails().", ex.StackTrace, 0)
		End Try
	End Sub

#End Region

#Region " Basic Send-Email Functions"

	Friend Sub SendEMail(ByVal pToPersonIDs() As Integer, ByVal pSubject As String, ByVal pBody As String)
		' *****************************************************************************************************
		'
		'
		' *****************************************************************************************************

		Try
			Call SendEMail(Me.NaplesInitiator.Naples_DataBaseClient.GetPersonIdEmailAddresses(pToPersonIDs), pSubject, pBody)
		Catch ex As Exception
			LogError(DebugLevels.Minimal, "EMail_Server, SendEmail()", RenaissanceGlobals.LOG_LEVELS.Error, ex.Message, "Error Sending EMail.", ex.StackTrace, 0)
		End Try
	End Sub

	Friend Sub SendEMail(ByVal pToAddress() As String, ByVal pSubject As String, ByVal pBody As String)
		' *****************************************************************************************************
		'
		'
		' *****************************************************************************************************

		Try

			Dim myMessage As New System.Net.Mail.MailMessage
			Dim AddressCounter As Integer

			If (pToAddress Is Nothing) OrElse (pToAddress.Length <= 0) Then
				Exit Sub
			End If

			For AddressCounter = 0 To (pToAddress.Length - 1)
				myMessage.To.Add(pToAddress(AddressCounter).Trim)
			Next

			myMessage.From = New MailAddress(EMAIL_FromAddress)
			myMessage.Subject = pSubject
			myMessage.Priority = MailPriority.High
			myMessage.Body = pBody
			myMessage.IsBodyHtml = False

			Dim SMTPClient As New SmtpClient(EMail_SMTPServer)
			SMTPClient.EnableSsl = False

			SMTPClient.Send(myMessage)

			myMessage.Dispose()

		Catch ex As Exception
			LogError(DebugLevels.Minimal, "EMail_Server, SendEmail()", RenaissanceGlobals.LOG_LEVELS.Error, ex.Message, "Error Sending EMail.", ex.StackTrace, 0)
		End Try

	End Sub

#End Region

#Region " LogError "

	Private Function LogError( _
	 ByVal pDebugLevel As DebugLevels, _
	 ByVal p_Error_Location As String, _
	 ByVal p_System_Error_Number As Integer, _
	 ByVal p_System_Error_Description As String, _
	 Optional ByVal p_Error_Message As String = "", _
	 Optional ByVal p_Error_StackTrace As String = "", _
	 Optional ByVal p_Log_to As Integer = 0) As Integer

		Try
			If Not (NaplesInitiator Is Nothing) Then
				If Not (NaplesInitiator.Naples_DataBaseClient Is Nothing) Then
					Return NaplesInitiator.Naples_DataBaseClient.LogError(pDebugLevel, p_Error_Location, p_System_Error_Number, p_System_Error_Description, p_Error_Message, p_Error_StackTrace, p_Log_to)
				End If
			End If

		Catch ex As Exception
			Return (-1)
		End Try
	End Function

#End Region

End Class

