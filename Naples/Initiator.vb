Imports Microsoft.Win32
Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports System.Windows.Forms
Imports System.Threading

Imports NaplesGlobals
Imports RenaissanceGlobals

Public Class Initiator

  Public Naples_TrayComponent As TrayComponent

  Private WithEvents _Naples_TCPClient As TCP_Client
  Private _Naples_DataServer As DataServer
  Private _Naples_Aggregator As Aggregator
  Private _Naples_EmailServer As EMail_Server
  Private _Naples_DDECapture As DDE_Capture
  Private _Naples_DataBaseClient As DataBaseClient
  Private _CloseDownFlag As Boolean

  Private _MessageServerName As String
  Private _MessageServerAddress As IPAddress

  Private _SQLServerName As String
  Private _SQLDatabaseName As String

  Private _SMTPServer As String
  Private _SMTPUser As String
  Private _SMTPPassword As String

  Private _DebugLevel As Integer
	Private DebugLevel_Lock As New Object

#Region " Initiator Properties."

  Public ReadOnly Property MessageServerName() As String
    Get
      Return _MessageServerName
    End Get
  End Property

  Public ReadOnly Property MessageServerAddress() As IPAddress
    Get
      Return _MessageServerAddress
    End Get
  End Property

  Public ReadOnly Property Naples_DDECapture() As DDE_Capture
    Get
      Return _Naples_DDECapture
    End Get
  End Property

  Public ReadOnly Property Naples_Aggregator() As Aggregator
    Get
      Return _Naples_Aggregator
    End Get
  End Property

  Public ReadOnly Property Naples_DataServer() As DataServer
    Get
      Return _Naples_DataServer
    End Get
  End Property

  Public ReadOnly Property Naples_TCPClient() As TCP_Client
    Get
      Return _Naples_TCPClient
    End Get
  End Property

  Public ReadOnly Property Naples_DataBaseClient() As DataBaseClient
    Get
      Return _Naples_DataBaseClient
    End Get
  End Property

  Public ReadOnly Property Naples_EmailServer() As EMail_Server
    Get
      Return _Naples_EmailServer
    End Get
  End Property

  Public ReadOnly Property SQLServerName() As String
    Get
      Return _SQLServerName
    End Get
  End Property

  Public ReadOnly Property SQLDatabaseName() As String
    Get
      Return _SQLDatabaseName
    End Get
  End Property

  Public ReadOnly Property CloseDownFlag() As Boolean
    Get
      Return _CloseDownFlag
    End Get
  End Property

  Public ReadOnly Property SMTPServer() As String
    Get
      Return _SMTPServer
    End Get
  End Property

  Public ReadOnly Property SMTPUser() As String
    Get
      Return _SMTPUser
    End Get
  End Property

  Public ReadOnly Property SMTPPassword() As String
    Get
      Return _SMTPPassword
    End Get
  End Property

  Public Property DebugLevel() As Integer
		Get
			SyncLock DebugLevel_Lock
				Return _DebugLevel
			End SyncLock
		End Get
    Set(ByVal Value As Integer)
			SyncLock DebugLevel_Lock
				_DebugLevel = Value
			End SyncLock
		End Set

  End Property
#End Region

  Public Shared Sub Main(ByVal CmdArgs() As String)
    ' *******************************************************************************
    ' Initiate Naples
    ' 
    ' 
    ' *******************************************************************************

    ' *******************************************************************************
    ' Check for previous instances
    ' *******************************************************************************

    Dim Proc() As Process

    Dim ModuleName As String
    Dim ProcName As String

    Try

      ' Get Current Process details
      ModuleName = Process.GetCurrentProcess.MainModule.ModuleName
      ProcName = System.IO.Path.GetFileNameWithoutExtension(ModuleName)


      ' Find all processes with this name
      Proc = Process.GetProcessesByName(ProcName)
      If Proc.Length > 1 Then
        Exit Sub
      End If

    Catch ex As Exception

    End Try

    ' *******************************************************************************
    ' Start up
    ' *******************************************************************************

    Try

      Thread.CurrentThread.SetApartmentState(ApartmentState.STA) ' = ApartmentState.STA

      Application.EnableVisualStyles()

      Dim NaplesInitiator As New Initiator
      Call NaplesInitiator.UserInitialise(CmdArgs)

      Application.Run()

    Catch ex As Exception
    End Try

  End Sub

  Public Sub UserInitialise(ByVal CmdArgs() As String)

    ' Startup Parameters
    _MessageServerName = ""
    _MessageServerAddress = IPAddress.None
    _SMTPServer = "mailer.friendsis.net"
    _SMTPUser = ""
    _SMTPPassword = ""

    DebugLevel = DebugLevels.Maximum

    _SQLServerName = ""
    _CloseDownFlag = False

    If Not (CmdArgs Is Nothing) Then
      If (CmdArgs.Length > 0) Then
        Dim ParameterString As String
        Dim ParameterName As String
        ' Dim ParameterValue As String

        For Each ParameterString In CmdArgs
          Try

            If InStr(ParameterString, "=") > 0 Then
              ParameterName = ParameterString.Substring(0, InStr(ParameterString, "=") - 1)
              ParameterString = ParameterString.Substring(InStr(ParameterString, "="), (ParameterString.Length - InStr(ParameterString, "=")))

              Select Case ParameterName.ToUpper

                Case "MESSAGESERVER"
                  _MessageServerName = ParameterString
                  Try
                    _MessageServerAddress = TCPServer_Common.NetworkFunctions.GetServerAddress(MessageServerName)
                  Catch ex As Exception
                    _MessageServerName = ""
                    _MessageServerAddress = IPAddress.None
                  End Try

                Case "SQLSERVER"
                  If ParameterString.Trim.Length > 0 Then
                    _SQLServerName = ParameterString.Trim

                    If (_SQLDatabaseName Is Nothing) Then
                      _SQLDatabaseName = "Renaissance"
                    End If
                  End If

                Case "SQLDATABASENAME"
                  If ParameterString.Trim.Length > 0 Then
                    _SQLDatabaseName = ParameterString.Trim
                  End If

                Case "SMTPSERVER"
                  If ParameterString.Trim.Length > 0 Then
                    _SMTPServer = ParameterString.Trim
                  End If

                Case "SMTPUSER"
                  If ParameterString.Trim.Length > 0 Then
                    _SMTPUser = ParameterString.Trim
                  End If

                Case "SMTPPASSWORD"
                  If ParameterString.Trim.Length > 0 Then
                    _SMTPPassword = ParameterString.Trim
                  End If

              End Select
            End If

          Catch ex As Exception
          End Try
        Next

      End If
    End If

    ' Resolve SQL Server, if necessary

    Try

      If (SQLServerName Is Nothing) OrElse (SQLServerName.Length <= 0) Then
        Dim ComputerName As String

        ' Registry ?

        _SQLServerName = CType(Get_RegistryItem(Registry.CurrentUser, REGISTRY_BASE & "\SQLServer", "ServerName", ""), String)

        If _SQLServerName.Length <= 0 Then
          ComputerName = Environment.GetEnvironmentVariable("COMPUTERNAME")

          If ComputerName Is Nothing Then
            ComputerName = ""
          End If

          If ComputerName.StartsWith("UKPRWO") Then
            If ComputerName.StartsWith("UKPRWO000457") Then
              _SQLServerName = "UKPRWO000457"
              _SQLDatabaseName = "InvestMaster_Test1"
            Else
              _SQLServerName = "EXMSDB53"
              _SQLDatabaseName = "Renaissance"
            End If
          Else
            _SQLServerName = ComputerName
            _SQLDatabaseName = "InvestMaster_Test1"
          End If
        End If
      End If

    Catch ex As Exception
    End Try

    If (SQLDatabaseName Is Nothing) OrElse (SQLDatabaseName.Length <= 0) Then
      _SQLDatabaseName = "Renaissance"
    End If

    ' Initiate components

    Try

      ' Create Tray Component
      Naples_TrayComponent = New TrayComponent
      Naples_TrayComponent.InitiatorObject = Me

      ' Start DataServer
      _Naples_DataServer = New DataServer(Me)
      Naples_DataServer.Start()

      ' Start DDE_Capture
      _Naples_DDECapture = New DDE_Capture(Me)
      Naples_DDECapture.Start()

      Threading.Thread.Sleep(4000)

      ' Start Tcp Client
      _Naples_TCPClient = New TCP_Client(Me)
      Naples_TCPClient.Start()

      ' Start DB Client ' Must be after TCP Client - In order to establish ApplicationUpdate Event handler.
      _Naples_DataBaseClient = New DataBaseClient(Me)
      Naples_DataBaseClient.Start()

      Threading.Thread.Sleep(4000)

      ' Start EmailServer
      _Naples_EmailServer = New EMail_Server(Me)
      Naples_EmailServer.Start()

      ' Start Aggregator - Must be after the DBClient & TCP Client
      _Naples_Aggregator = New Aggregator(Me)
      Naples_Aggregator.Start()

      Call LogError(DebugLevels.Minimal, "Naples Initiator", 0, "", "Starting Naples Initiator.")

    Catch ex As Exception
      Call LogError(DebugLevels.Minimal, "Naples, Initiator", LOG_LEVELS.Error, ex.Message, "Error Starting Naples Initiator.", ex.StackTrace)
    End Try


  End Sub

  Public Sub ShutDown()
    Dim TimeCounter As Integer

    Call LogError(DebugLevels.Minimal, "Naples Initiator, Shutdown()", 0, "", "Shutting down Naples.")

    ' Shutdown Email Server
    Try
      Naples_EmailServer.CloseDown = True
      TimeCounter = 20
      While (TimeCounter > 0) And (Naples_EmailServer.IsCompleted = False)
        Application.DoEvents()
        'Threading.Thread.Sleep(20)
        Naples_EmailServer.Thread.Join(20)
        TimeCounter -= 1
      End While
      If (Naples_EmailServer.IsCompleted = False) Then
        Naples_EmailServer.Stop()
      End If
    Catch ex As Exception
    End Try

    ' ShutDown TCP_Client
    Try
      Naples_TCPClient.CloseDown = True
        TimeCounter = 20
        While (TimeCounter > 0) And (Naples_TCPClient.IsCompleted = False)
          Application.DoEvents()
        'Threading.Thread.Sleep(20)
          Naples_TCPClient.Thread.Join(20)
          TimeCounter -= 1
        End While
        If (Naples_TCPClient.IsCompleted = False) Then
          Naples_TCPClient.Stop()
        End If
    Catch ex As Exception
    End Try


    ' ShutDown Aggregator, Allow it 200ms to close
    Try
      Naples_Aggregator.CloseDown = True
      TimeCounter = 20
      While (TimeCounter > 0) And (Naples_Aggregator.IsCompleted = False)
        Application.DoEvents()
        Threading.Thread.Sleep(20)
        TimeCounter -= 1
      End While
      If (Naples_Aggregator.IsCompleted = False) Then
        Naples_Aggregator.Stop()
      End If
    Catch ex As Exception
    End Try


    ' ShutDown DDECapture, Allow it 200ms to close
    Try
      Naples_DDECapture.CloseDown = True
      TimeCounter = 20
      While (TimeCounter > 0) And (Naples_DDECapture.IsCompleted = False)
        Application.DoEvents()
        Threading.Thread.Sleep(20)
        TimeCounter -= 1
      End While
      If (Naples_DDECapture.IsCompleted = False) Then
        Naples_DDECapture.Stop()
      End If
    Catch ex As Exception
    End Try


    ' ShutDown DataServer, Allow it 200ms to close
    Try
      Naples_DataServer.CloseDown = True
      TimeCounter = 20
      While (TimeCounter > 0) And (Naples_DataServer.IsCompleted = False)
        Application.DoEvents()
        Threading.Thread.Sleep(20)
        TimeCounter -= 1
      End While
      If (Naples_DataServer.IsCompleted = False) Then
        Naples_DataServer.Stop()
      End If
    Catch ex As Exception
    End Try

    ' ShutDown DBClient, Allow it 200ms to close
    Try
      Naples_DataBaseClient.CloseDown = True
      TimeCounter = 20
      While (TimeCounter > 0) And (Naples_DataBaseClient.IsCompleted = False)
        Application.DoEvents()
        Threading.Thread.Sleep(20)
        TimeCounter -= 1
      End While
      If (Naples_DataBaseClient.IsCompleted = False) Then
        Naples_DataBaseClient.Stop()
      End If
    Catch ex As Exception
    End Try

    ' Exit
    Try
      Application.Exit()
    Catch ex As Exception
    End Try

  End Sub

  Private Sub ApplicationAutoUpdate(ByVal sender As System.Object, ByVal e As TCP_Client.FCPApplicationUpdateEventArgs) Handles _Naples_TCPClient.FCPApplicationUpdate
    Dim ChangeID As NaplesChangeID

    Try

      If e.MessageHeader Is Nothing Then Exit Sub

      Select Case e.MessageHeader.MessageType
        Case TCPServer_Common.TCP_ServerMessageType.Client_ApplicationUpdateMessage, TCPServer_Common.TCP_ServerMessageType.Server_ApplicationUpdateMessage

          Select Case e.MessageHeader.Application
            Case TCPServer_Common.FCP_Application.Naples

              If e.MessageHeader.MessageString.Length > 0 Then
                Try
                  If IsNumeric(e.MessageHeader.MessageString) Then
                    ChangeID = CInt(e.MessageHeader.MessageString)
                  Else
                    ChangeID = System.Enum.Parse(GetType(NaplesChangeID), e.MessageHeader.MessageString)
                  End If

                  If ChangeID = NaplesChangeID.DeathToNaples Then
                    Try
                      _CloseDownFlag = True

                    Catch ex As Exception
                    End Try
                  End If
                Catch ex As Exception
                End Try

              End If
          End Select
      End Select

    Catch ex As Exception

    End Try

  End Sub

  Private Function LogError( _
  ByVal pDebugLevel As DebugLevels, _
  ByVal p_Error_Location As String, _
  ByVal p_System_Error_Number As Integer, _
  ByVal p_System_Error_Description As String, _
  Optional ByVal p_Error_Message As String = "", _
  Optional ByVal p_Error_StackTrace As String = "", _
  Optional ByVal p_Log_to As Integer = 0) As Integer

    Try
      If Not (Naples_DataBaseClient Is Nothing) Then
        Return Naples_DataBaseClient.LogError(pDebugLevel, p_Error_Location, p_System_Error_Number, p_System_Error_Description, p_Error_Message, p_Error_StackTrace, p_Log_to)
      End If

    Catch ex As Exception
      Return (-1)
    End Try
  End Function

End Class
