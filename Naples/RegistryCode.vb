Imports Microsoft.Win32

Module RegistryCode


  Public Function Get_RegistryItem(ByRef pRegistryArea As RegistryKey, ByVal pKey As String, ByVal pItem As String, ByVal pDefault As Object) As Object
    ' **********************************************************************
    ' Retrieve an item from the registry.
    '
    ' **********************************************************************

    Dim Key As RegistryKey
    Dim ItemValue As Object

    Key = pRegistryArea.OpenSubKey(pKey, False)

    If Key Is Nothing Then
      ItemValue = pDefault
    Else
      ItemValue = Key.GetValue(pItem, pDefault)
    End If

    Return ItemValue

  End Function

  Public Function Get_RegistryItem(ByVal pKey As String, ByVal pItem As String, ByVal pDefault As Object) As Object
    ' **********************************************************************
    ' Retrieve an item from the registry.
    '
    ' **********************************************************************

    Return Get_RegistryItem(Registry.CurrentUser, pKey, pItem, pDefault)

    'Dim Key As RegistryKey
    'Dim ItemValue As Object

    'Key = Registry.CurrentUser.OpenSubKey(pKey, False)

    'If Key Is Nothing Then
    '  ItemValue = pDefault
    'Else
    '  ItemValue = Key.GetValue(pItem, pDefault)
    'End If

    'Return ItemValue

  End Function


  Public Function Set_RegistryItem(ByRef pRegistryArea As RegistryKey, ByVal pKey As String, ByVal pItem As String, ByVal pItemValue As Object) As Boolean
    ' **********************************************************************
    ' Save an item to the registry.
    '
    ' **********************************************************************
    Dim Key As RegistryKey
    ' Dim ItemValue As Object

    Try
      Key = pRegistryArea.OpenSubKey(pKey, True)

      If Key Is Nothing Then
        Key = pRegistryArea.CreateSubKey(pKey)
      End If

      Key.SetValue(pItem, pItemValue)

    Catch ex As Exception
      Return False
      Exit Function

    End Try

    Return True
  End Function

  Public Function Set_RegistryItem(ByVal pKey As String, ByVal pItem As String, ByVal pItemValue As Object) As Boolean
    ' **********************************************************************
    ' Save an item to the registry.
    '
    ' **********************************************************************

    Return Set_RegistryItem(Registry.CurrentUser, pKey, pItem, pItemValue)

    'Dim Key As RegistryKey
    'Dim ItemValue As Object

    'Try
    '  Key = Registry.CurrentUser.OpenSubKey(pKey, True)

    '  If Key Is Nothing Then
    '    Key = Registry.CurrentUser.CreateSubKey(pKey)
    '  End If

    '  Key.SetValue(pItem, pItemValue)

    'Catch ex As Exception
    '  Return False
    '  Exit Function

    'End Try

    'Return True

  End Function

End Module
