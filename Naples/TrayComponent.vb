Imports NaplesGlobals

Public Class TrayComponent
  Inherits System.ComponentModel.Component

#Region " Component Designer generated code "

  Public Sub New(ByVal Container As System.ComponentModel.IContainer)
    MyClass.New()

    'Required for Windows.Forms Class Composition Designer support
    Container.Add(Me)
  End Sub

  Public Sub New()
    MyBase.New()

    'This call is required by the Component Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Component overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Component Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Component Designer
  'It can be modified using the Component Designer.
  'Do not modify it using the code editor.
  Friend WithEvents NaplesNotifyIcon As System.Windows.Forms.NotifyIcon
  Friend WithEvents CM_Naples As System.Windows.Forms.ContextMenu
  Friend WithEvents CM_CloseNaples As System.Windows.Forms.MenuItem
  Friend WithEvents CM_ViewNaples As System.Windows.Forms.MenuItem
  Friend WithEvents Timer_Quit As System.Timers.Timer
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(TrayComponent))
    Me.NaplesNotifyIcon = New System.Windows.Forms.NotifyIcon(Me.components)
    Me.CM_Naples = New System.Windows.Forms.ContextMenu
    Me.CM_CloseNaples = New System.Windows.Forms.MenuItem
    Me.CM_ViewNaples = New System.Windows.Forms.MenuItem
    Me.Timer_Quit = New System.Timers.Timer
    CType(Me.Timer_Quit, System.ComponentModel.ISupportInitialize).BeginInit()
    '
    'NaplesNotifyIcon
    '
    Me.NaplesNotifyIcon.ContextMenu = Me.CM_Naples
    Me.NaplesNotifyIcon.Icon = CType(resources.GetObject("NaplesNotifyIcon.Icon"), System.Drawing.Icon)
    Me.NaplesNotifyIcon.Text = "Naples"
    Me.NaplesNotifyIcon.Visible = True
    '
    'CM_Naples
    '
    Me.CM_Naples.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.CM_CloseNaples, Me.CM_ViewNaples})
    '
    'CM_CloseNaples
    '
    Me.CM_CloseNaples.Index = 0
    Me.CM_CloseNaples.Text = "Close Naples"
    '
    'CM_ViewNaples
    '
    Me.CM_ViewNaples.Index = 1
    Me.CM_ViewNaples.Text = "View Naples"
    '
    'Timer_Quit
    '
    Me.Timer_Quit.Enabled = True
    Me.Timer_Quit.Interval = 1000
    CType(Me.Timer_Quit, System.ComponentModel.ISupportInitialize).EndInit()

  End Sub

#End Region

  Public InitiatorObject As Initiator
  Dim thisRequest As DataServerRequest
  Dim thisRTItem As DataServerRTItem


  Private Sub CM_Naples_Popup(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CM_Naples.Popup

  End Sub

  Private Sub CM_CloseNaples_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CM_CloseNaples.Click
    Try
      InitiatorObject.ShutDown()
      Me.NaplesNotifyIcon.Visible = False
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Timer_Quit_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles Timer_Quit.Elapsed
    Static ShutDownCount As Integer = 0

    Try
      If (Not (InitiatorObject Is Nothing)) AndAlso (InitiatorObject.CloseDownFlag = True) Then

        ' If the CloseDownFlag is set then the idea is that this timer will notice and shut Naples down
        ' gracefully. It is done this way because there were issues with triggering the shutdown from
        ' within Naples itself.
        ' If the Shutdown takes more than the Timer Interval to complete then this routine will be
        ' called again, thus the timer is slowed down.
        ' It is allowed to call repeatedly as the shutdown process has not shown itself to execute 
        ' reliably when called in this way.
        ' Non-the-less the number of times that the Shutdown process may be called has been limited
        ' to 10, by use of the 'ShutDownCount' static variable.

        If (ShutDownCount < 10) Then
          ShutDownCount += 1

          InitiatorObject.ShutDown()
          Me.NaplesNotifyIcon.Visible = False
          Timer_Quit.Interval = 5000 ' Slow the timer down if CloseDownFlag = True
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub CM_ViewNaples_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CM_ViewNaples.Click

    'Try
    '  thisRequest = New DataServerRequest(DS_RequestType.API_Realtime, "UKX Index|Last_Price,Name")
    '  AddHandler thisRequest.DataUpdate, AddressOf RequestUpdate
    '  thisRTItem = Nothing
    '  InitiatorObject.Naples_DataServer.PostNewRequest(thisRequest)
    'Catch ex As Exception
    'End Try

  End Sub

  Private Sub RequestUpdate(ByVal sender As Object, ByVal e As DataServerUpdate)

    Try
      If thisRequest.RequestStatus = DS_RequestStatus.RequestSubmitted Then
        If thisRequest.ReturnType Is GetType(DataServerRTItem) Then
          thisRTItem = CType(thisRequest.ReturnObject, DataServerRTItem)

          AddHandler thisRTItem.DataUpdate, AddressOf DataUpdate
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub DataUpdate(ByVal sender As Object, ByVal e As DataServerUpdate)
    Dim FieldsData As Hashtable
    Dim HashTableEntry As DictionaryEntry
    Dim FieldValue As Object
    Dim FieldString As String
    Dim FieldNumber As Double

    ' Dim FieldItem As DataServerRTField

    Try

      If thisRTItem.Static_RequestID.Equals(e.BBgReply.Request.RequestId) Then
        FieldsData = thisRTItem.Fields

        For Each HashTableEntry In FieldsData
          FieldValue = CType(HashTableEntry.Value, DataServerRTField).FieldValue

          If FieldValue.GetType Is GetType(System.String) Then
            FieldString = CType(FieldValue, System.String)
          Else
            FieldNumber = CType(FieldValue, System.Double)
          End If
        Next
      End If
      If thisRTItem.Realtime_RequestID.Equals(e.BBgReply.Request.RequestId) Then
        FieldsData = thisRTItem.Fields

        For Each HashTableEntry In FieldsData
          FieldValue = CType(HashTableEntry.Value, DataServerRTField).FieldValue

          If FieldValue.GetType Is GetType(System.String) Then
            FieldString = CType(FieldValue, System.String)
          Else
            FieldNumber = CType(FieldValue, System.Double)
          End If
        Next
      End If

    Catch ex As Exception

    End Try


  End Sub

End Class
