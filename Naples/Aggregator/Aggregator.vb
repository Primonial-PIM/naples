Imports System.Net
Imports System.Net.Sockets
Imports System.IO
Imports System.Data.SqlClient
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.Serialization
Imports System.Xml

Imports NaplesGlobals
Imports RenaissanceDataClass
Imports RenaissanceGlobals

Public Class Aggregator

  ' *****************************************************************************************************
  ' Data Aggregator CLASS
  ' ---------------------
  '
  ' 
  '
  '
  ' *****************************************************************************************************

  Inherits ThreadWrapperBase

  Private _CloseDownFlag As Boolean
  Private _NaplesInitiator As Initiator

  Private AggregatorDataTasks As New ArrayList
  Private RTItemUpdateQueue As New Queue(100, 1)

  Private WithEvents Naples_TCPClient As TCP_Client

  Public Const KNOWLEDGEDATE_NOW As Date = #1/1/1900#

  Private Class RTItemUpdateQueueClass
    Private _TaskType As NaplesGlobals.DataTaskType
    Private _RTItem As DataServerRTItem
    Private _CreationTime As Date

    Public Sub New(ByVal pTaskType As NaplesGlobals.DataTaskType, ByVal pRTItem As DataServerRTItem)
      _TaskType = pTaskType
      _RTItem = pRTItem
      _CreationTime = Now()
    End Sub

    Public Property TaskType() As NaplesGlobals.DataTaskType
      Get
        Return _TaskType
      End Get
      Set(ByVal value As NaplesGlobals.DataTaskType)
        _TaskType = value
      End Set
    End Property

    Public Property RTItem() As DataServerRTItem
      Get
        Return _RTItem
      End Get
      Set(ByVal value As DataServerRTItem)
        If (value IsNot Nothing) Then
          _RTItem = value
        End If
      End Set
    End Property
  End Class

#Region " Constructor"

  Public Sub New(ByVal pInitiator As Initiator)
    MyBase.New()

    Me.Thread.Name = "Aggregator"

    Try
      _NaplesInitiator = pInitiator
      Naples_TCPClient = pInitiator.Naples_TCPClient
    Catch ex As Exception
    End Try

  End Sub

#End Region

#Region " Properties"

  Public ReadOnly Property NaplesInitiator() As Initiator
    Get
      Return _NaplesInitiator
    End Get
  End Property

	Private ReadOnly Property thisDataHandler() As RenaissanceDataClass.DataHandler
		Get
			If (NaplesInitiator.Naples_DataBaseClient Is Nothing) Then
				Return Nothing
			Else
				Return NaplesInitiator.Naples_DataBaseClient.thisDataHandler
			End If
		End Get
	End Property

	Private ReadOnly Property thisAdaptorHandler() As RenaissanceDataClass.AdaptorHandler
		Get
			If (NaplesInitiator.Naples_DataBaseClient Is Nothing) Then
				Return Nothing
			Else
				Return NaplesInitiator.Naples_DataBaseClient.thisAdaptorHandler
			End If
		End Get
	End Property

  Public Property CloseDown() As Boolean
    Get
      Return _CloseDownFlag
    End Get
    Set(ByVal Value As Boolean)
      _CloseDownFlag = Value
    End Set
  End Property

#End Region

  Protected Overrides Sub DoTask()
    ' **********************************************************************
    '
    ' **********************************************************************

    Threading.Thread.Sleep(5000)

    Initialise()

    RunDataServer()

    TidyUp()

  End Sub

  Private Sub Initialise()
    ' **********************************************************************
    ' Initialise Data Server
    ' **********************************************************************

    _CloseDownFlag = False

    ' Initialise Data Tasks.

    Try

      Call UpdateDataTaskCollection()

    Catch ex As Exception

    End Try

  End Sub

  Private Sub TidyUp()
    ' **********************************************************************
    ' On ending the Server
    ' **********************************************************************

    Dim thisDataTask As AggregatorDataTask = Nothing
    Dim TaskCounter As Integer

    ' Cancel existing Data Tasks.

    SyncLock AggregatorDataTasks
      For TaskCounter = 0 To (AggregatorDataTasks.Count - 1)

        Try
          thisDataTask = Nothing
          If TaskCounter < AggregatorDataTasks.Count Then
            thisDataTask = CType(AggregatorDataTasks(TaskCounter), AggregatorDataTask)

            CancelDataTask(thisDataTask)
          End If
        Catch ex As Exception
        End Try
      Next
    End SyncLock
  End Sub

  Private Sub RunDataServer()
    ' **********************************************************************
    ' Principal Work loop for the Aggregator Object.
    '
    ' Loop throught the available Data Tasks, Processing the task if an update
    ' is indicated to be due.
    '
    ' **********************************************************************
    Dim thisDataTask As AggregatorDataTask = Nothing
    Dim TaskCounter As Integer
    Dim DataServerWorkDone As Boolean
		Dim AggregatorDataTasksCount As Integer

    While (_CloseDownFlag = False)
      thisDataTask = Nothing
      TaskCounter = 0
      DataServerWorkDone = False

      ' **********************************************************************
      ' Loop through data tasks
      ' **********************************************************************

			Try
				SyncLock AggregatorDataTasks
					AggregatorDataTasksCount = AggregatorDataTasks.Count
				End SyncLock

				For TaskCounter = 0 To (AggregatorDataTasksCount - 1)
					Try
						thisDataTask = Nothing

						' Get Task.

						SyncLock AggregatorDataTasks
							If TaskCounter < AggregatorDataTasks.Count Then
								thisDataTask = CType(AggregatorDataTasks(TaskCounter), AggregatorDataTask)
							End If
						End SyncLock
					Catch ex As Exception
						thisDataTask = Nothing
					End Try

					If (thisDataTask Is Nothing) Then
						Exit For
					End If

					SyncLock thisDataTask
						' Check Update Status

						If thisDataTask.UpdateDue Then
							' Update task in a fasion appropriate to it's Task Type.
							DataServerWorkDone = True

							Select Case thisDataTask.TaskType
								Case NaplesGlobals.DataTaskType.PageCapture, NaplesGlobals.DataTaskType.PortfolioCapture
									' Portfilio Capture

									Try
										If Not (thisDataTask.ServerRequest Is Nothing) Then
											thisDataTask.ServerRequest.RequestStatus = DS_RequestStatus.None
											thisDataTask.ServerRequest.ReturnObject = Nothing
											thisDataTask.ServerRequest.ReturnType = Nothing
										Else

										End If
									Catch ex As Exception
									End Try

									Me.NaplesInitiator.Naples_DDECapture.PostNewRequest(thisDataTask.ServerRequest)

								Case NaplesGlobals.DataTaskType.PeriodicSecurityCapture
									' Periodic Security Capture

								Case NaplesGlobals.DataTaskType.RealtimeSecurityCapture
									' Realtime Security Capture

								Case NaplesGlobals.DataTaskType.PeriodicMarketSecurityCapture
									' Periodic Security Capture

								Case NaplesGlobals.DataTaskType.RealtimeMarketSecurityCapture
									' Realtime Security Capture

								Case NaplesGlobals.DataTaskType.EmailPage
									' EmailPage

								Case NaplesGlobals.DataTaskType.EmailPeriodicInstrumentInformation
									' EmailPeriodicInstrumentInformation

								Case NaplesGlobals.DataTaskType.EmailPortfolioStockChangeNotification, _
								 NaplesGlobals.DataTaskType.EmailSingleStockChangeNotification
									' EmailPortfolioStockChangeNotification
									' EmailSingleStockChangeNotification

									Call NaplesInitiator.Naples_EmailServer.PostEmailTask(New EMail_Server.EMailTask_SendDataTaskPendingEmails(thisDataTask))

							End Select

							thisDataTask.Updated()
						End If
					End SyncLock
				Next

			Catch ex As Exception
			End Try


      ' **********************************************************************
      ' Process RT Item Updates
      ' 
      ' RT Updates result from a subset of DataTasks that maintain persistent
      ' RT Data requests.
      ' **********************************************************************
      thisDataTask = Nothing
      TaskCounter = 0

      Try
        Dim RTItemCount As Integer
        Dim thisRTItemUpdateItem As RTItemUpdateQueueClass
        Dim RTUpdateStartTime As Date

        SyncLock RTItemUpdateQueue
          RTItemCount = RTItemUpdateQueue.Count
        End SyncLock
        RTUpdateStartTime = Now

        ' Loop while there are RT Items to process, Limit this section to chunks of n Seconds.
        While (RTItemCount > 0) AndAlso ((Now() - RTUpdateStartTime).TotalSeconds <= 5.0)
          DataServerWorkDone = True

          SyncLock RTItemUpdateQueue
            If RTItemUpdateQueue.Count > 0 Then
              thisRTItemUpdateItem = CType(RTItemUpdateQueue.Dequeue, RTItemUpdateQueueClass)
            Else
              thisRTItemUpdateItem = Nothing
            End If
          End SyncLock

          If (thisRTItemUpdateItem IsNot Nothing) Then
            Dim thisDataServerRequest As DataServerRequest = Nothing
            Dim TaskIndex As Integer = 0
            Dim ServerRequestIndex As Integer = 0

            ' Resolve DataTask and DataServerRequest relating to this RTItem
            ' Loopthrough Aggregator Data Tasks of a matching type looking for a matching RTItem.

            SyncLock AggregatorDataTasks
              For TaskIndex = 0 To (AggregatorDataTasks.Count - 1)
                thisDataTask = CType(AggregatorDataTasks(TaskIndex), AggregatorDataTask)
                If (thisDataTask.TaskType = thisRTItemUpdateItem.TaskType) Then
                  ServerRequestIndex = thisDataTask.ServerRTItemIndex(thisRTItemUpdateItem.RTItem)
                  If (ServerRequestIndex >= 0) Then
                    Exit For
                  End If
                End If
                thisDataTask = Nothing
              Next
            End SyncLock

            ' Process RT Item Update

            If (thisDataTask IsNot Nothing) Then

              Select Case thisRTItemUpdateItem.TaskType
                Case NaplesGlobals.DataTaskType.RealtimeSecurityCapture



								Case NaplesGlobals.DataTaskType.EmailPortfolioStockChangeNotification, NaplesGlobals.DataTaskType.EmailSingleStockChangeNotification

									Try
										thisRTItemUpdateItem.RTItem.ItemLock.AcquireReaderLock(1000)

										' Check vs Reference Price
										Dim FieldsHashtable As Hashtable = Nothing
										Dim LastValue As Double = 0
										Dim UpdateTriggered As Boolean = False
										Dim DebugString As String = ""

										If (TypeOf thisRTItemUpdateItem.RTItem.Fields Is Hashtable) Then
											FieldsHashtable = CType(thisRTItemUpdateItem.RTItem.Fields, Hashtable)
										End If

										If (FieldsHashtable IsNot Nothing) Then
											Try
												If (FieldsHashtable.Contains(BBG_LAST_PRICE)) AndAlso IsNumeric(CType(FieldsHashtable(BBG_LAST_PRICE), DataServerRTField).FieldValue) Then
													LastValue = CDbl(CType(FieldsHashtable(BBG_LAST_PRICE), DataServerRTField).FieldValue)
												End If

											Catch ex As Exception
											End Try

											If (LastValue <> 0) Then

												If thisDataTask.ReferencePrice(ServerRequestIndex) = 0 Then
													thisDataTask.ReferencePrice(ServerRequestIndex) = LastValue

													' Update Reference Price Table.

													NaplesInitiator.Naples_DataBaseClient.SetReferencePrice(thisDataTask.TaskID, thisDataTask.ServerRequest(ServerRequestIndex).Request, LastValue)

												Else

													' Compare LastValue vs ReferencePrice
													If Math.Abs((LastValue / thisDataTask.ReferencePrice(ServerRequestIndex)) - 1.0) >= thisDataTask.PriceMoveTrigger Then
														UpdateTriggered = True
														Try
															DebugString = thisDataTask.TaskDescription & ", " & thisDataTask.ServerRequest(ServerRequestIndex).Request & ", " & thisDataTask.ReferencePrice(ServerRequestIndex).ToString & " -> " & LastValue.ToString
														Catch ex As Exception
														End Try
													End If

												End If

												thisDataTask.LastTickPrice(ServerRequestIndex) = LastValue

												If (UpdateTriggered) Then

													' Send EMail or Cache Email Update.

													Try
														Me.LogError(DebugLevels.Maximum, "Aggregator, RunDataServer()", LOG_LEVELS.Audit, DebugString, "Save Email Notification ", "", 0)
													Catch ex As Exception
													End Try

													Me.NaplesInitiator.Naples_EmailServer.PostEmailTask(New EMail_Server.EMailTask_DataTaskUpdate(thisDataTask, ServerRequestIndex, LastValue, thisDataTask.ReferencePrice(ServerRequestIndex), thisDataTask.ReferenceDate(ServerRequestIndex), Now()))

													' Update Notification reference price.

													NaplesInitiator.Naples_DataBaseClient.SetReferencePrice(thisDataTask.TaskID, thisDataTask.ServerRequest(ServerRequestIndex).Request, LastValue)

													thisDataTask.ReferencePrice(ServerRequestIndex) = LastValue

												End If

											End If
										End If

									Catch ex As Exception
										Me.LogError(DebugLevels.Minimal, "Aggregator, RunDataServer()", LOG_LEVELS.Error, ex.Message, "Error Processing RT Update", ex.StackTrace, 0)
									Finally
										thisRTItemUpdateItem.RTItem.ItemLock.ReleaseReaderLock()
									End Try

							End Select

            Else

            End If ' (thisDataTask IsNot Nothing) 

          End If

          SyncLock RTItemUpdateQueue
            RTItemCount = RTItemUpdateQueue.Count
          End SyncLock
        End While
      Catch ex As Exception
      End Try

      ' Pause thread if no work is taking place.
      If (Not DataServerWorkDone) Then
        Threading.Thread.Sleep(50)
      End If
    End While

  End Sub

  Public Sub UpdateDataTaskCollection(Optional ByVal UpdateTaskType As NaplesGlobals.DataTaskType = NaplesGlobals.DataTaskType.NoTask)
    ' **********************************************************************
    ' Force an Update of the Data Tasks collection.
    '
    ' For use if the DataTask Table has been updated.
    '
    ' To facilitate the update of selective task types, this routine now takes an optional
    ' parameter specifying the task type to refresh. This functionallity was introduced
    ' specifically so that PortfolioChangeNotification tasks could be selectively 
    ' refreshed from the AutoUpdate routine.
    ' **********************************************************************

		Dim dsDataTasks As RenaissanceDataClass.DSDataTask
		Dim tblDataTasks As RenaissanceDataClass.DSDataTask.tblDataTaskDataTable

    Dim ExistingDataTasks As New ArrayList
    Dim TaskCounter As Integer
		Dim thisDataTaskRow As RenaissanceDataClass.DSDataTask.tblDataTaskRow
    Dim tempDataTask As AggregatorDataTask
    Dim ActiveDataTask As AggregatorDataTask

    Try
      ' **********************************************************************
      ' Retrieve new Data Task details
      ' **********************************************************************
      tblDataTasks = Nothing

			dsDataTasks = CType(NaplesInitiator.Naples_DataBaseClient.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblDataTask, False), RenaissanceDataClass.DSDataTask)

      If Not (dsDataTasks Is Nothing) Then
        tblDataTasks = dsDataTasks.tblDataTask
      End If

    Catch ex As Exception
      Call LogError(DebugLevels.Minimal, "Aggregator, UpdateDataTaskCollection", LOG_LEVELS.Error, ex.Message, "Error retrieving new Data Tasks Table.", ex.StackTrace)
      Exit Sub
    End Try

    ' If we failed to retrieve the Data Tasks Table, then exit.

    If (tblDataTasks Is Nothing) Then
      Exit Sub
    End If


    ' If there are no Tasks in the Data Task Table, then Clear existing tasks and exit.

    If (tblDataTasks.Rows.Count = 0) Then
      ' Clear Existing Tasks and Exit.

      If (Not (AggregatorDataTasks Is Nothing)) AndAlso (AggregatorDataTasks.Count > 0) Then
        Try
          SyncLock AggregatorDataTasks
            For TaskCounter = 0 To (AggregatorDataTasks.Count - 1)
              ' Cancel outstanding requests

              CancelDataTask(AggregatorDataTasks(TaskCounter))
            Next

            AggregatorDataTasks.Clear()
          End SyncLock
        Catch ex As Exception
        End Try
      End If

      Exit Sub
    End If

    ' Remove Some Temporary Datasets so that they are refreshed.
		'Try
		'  NaplesInitiator.Naples_DataBaseClient.thisDataHandler.Drop_Dataset(TEMP_Get_MI_tblPortfolioIndex)
		'  NaplesInitiator.Naples_DataBaseClient.thisDataHandler.Drop_Dataset(TEMP_Get_MI_tblPortfolioData)
		'Catch ex As Exception
		'End Try

    Try

      ' **********************************************************************
      ' Compare Existing Data Tasks with those present in the Database
      ' **********************************************************************

      SyncLock AggregatorDataTasks

        ' 1) Build Temporary list of Existing Tasks

        For TaskCounter = 0 To (AggregatorDataTasks.Count - 1)
          Try
            If (UpdateTaskType = NaplesGlobals.DataTaskType.NoTask) OrElse (CType(AggregatorDataTasks(TaskCounter), AggregatorDataTask).TaskType = UpdateTaskType) Then
              ExistingDataTasks.Add(AggregatorDataTasks(TaskCounter))
            End If
          Catch ex As Exception
          End Try
        Next

        ' Compare each TaskRow with the existing AggregatorDataTasks
        ' If the Task Already exists then Ignore the TaskRow otherwise
        ' Add the new task row to the AggregatorDataTasks set.
        '
        For Each thisDataTaskRow In tblDataTasks
          Try
            ActiveDataTask = Nothing
            tempDataTask = ParseDataTask(Nothing, thisDataTaskRow)

            ' Are we running for all Task Types, or just a specific task type ?

            If (tempDataTask IsNot Nothing) AndAlso ((UpdateTaskType = NaplesGlobals.DataTaskType.NoTask) OrElse (tempDataTask.TaskType = UpdateTaskType)) Then

              For TaskCounter = 0 To (AggregatorDataTasks.Count - 1)
                If (UpdateTaskType = NaplesGlobals.DataTaskType.NoTask) OrElse (CType(AggregatorDataTasks(TaskCounter), AggregatorDataTask).TaskType = UpdateTaskType) Then
                  ActiveDataTask = CType(AggregatorDataTasks(TaskCounter), AggregatorDataTask)

                  If (tempDataTask.TaskID = ActiveDataTask.TaskID) Then
                    ' TaskIDs are that same, Check for changes in Task Properties that do not demand a compete re-build of this task.

                    If (ActiveDataTask.PriceMoveTrigger.Equals(tempDataTask.PriceMoveTrigger) = False) Then
                      ActiveDataTask.PriceMoveTrigger = tempDataTask.PriceMoveTrigger
                    End If

                    If (ActiveDataTask.DaysToSaveOn.Equals(tempDataTask.DaysToSaveOn) = False) Then
                      ActiveDataTask.DaysToSaveOn = tempDataTask.DaysToSaveOn
                    End If

                    If (ActiveDataTask.SaveOnceADay.Equals(tempDataTask.SaveOnceADay) = False) Then
                      ActiveDataTask.SaveOnceADay = tempDataTask.SaveOnceADay
                    End If

                    If (ActiveDataTask.AggregateEmails.Equals(tempDataTask.AggregateEmails) = False) Then
                      ActiveDataTask.AggregateEmails = tempDataTask.AggregateEmails
                    End If

                    If (ActiveDataTask.StartTime.Equals(tempDataTask.StartTime) = False) Then
                      ActiveDataTask.StartTime = tempDataTask.StartTime
                    End If

                    If (ActiveDataTask.EndTime.Equals(tempDataTask.EndTime) = False) Then
                      ActiveDataTask.EndTime = tempDataTask.EndTime
                    End If

                    If (ActiveDataTask.Interval.Equals(tempDataTask.Interval) = False) Then
                      ActiveDataTask.Interval = tempDataTask.Interval
                    End If

										If (ActiveDataTask.LimitToMonitorItems.Equals(tempDataTask.LimitToMonitorItems) = False) Then
											ActiveDataTask.LimitToMonitorItems = tempDataTask.LimitToMonitorItems
										End If

										If (ActiveDataTask.TaskComment.Equals(tempDataTask.TaskComment) = False) Then
											ActiveDataTask.TaskComment = tempDataTask.TaskComment
										End If

										Try
											' Update Email IDs if necessary
											Dim PersonIDCounter As Integer = 0

											For PersonIDCounter = (tempDataTask.EmailAddressesCount - 1) To 0 Step -1	' (Reverse order to Minimise possibe Array ReDims)
												If ActiveDataTask.EmailPersonIDs(PersonIDCounter) <> tempDataTask.EmailPersonIDs(PersonIDCounter) Then
													ActiveDataTask.EmailPersonIDs(PersonIDCounter) = tempDataTask.EmailPersonIDs(PersonIDCounter)
												End If
											Next
											If ActiveDataTask.EmailAddressesCount > tempDataTask.EmailAddressesCount Then
												For PersonIDCounter = tempDataTask.EmailAddressesCount To (ActiveDataTask.EmailAddressesCount - 1)
													ActiveDataTask.EmailPersonIDs(PersonIDCounter) = 0
												Next
											End If
										Catch ex As Exception
										End Try
									End If

									If tempDataTask.Equals(ActiveDataTask) Then

										' Type Specific Change Checks (not requiring a complete rebuild)
										Select Case tempDataTask.TaskType
											Case DataTaskType.EmailPortfolioStockChangeNotification
												VerifyPortfolioDataRequests(ActiveDataTask)

										End Select

										' tempDataTask already exists in AggregatorDataTasks
										' So, Remove it from the ExistingDataTasks list (So it does not get tidied away later)
										' And set tempDataTask = Nothing so that it is not added to AggregatorDataTasks

										ExistingDataTasks.Remove(ActiveDataTask)
										tempDataTask = Nothing
										Exit For
									End If
								End If

							Next

              If Not (tempDataTask Is Nothing) Then
                ' This means that tempDataTask does not exist in AggregatorDataTasks
                ' Activate the Server Tasks and add it AggregatorDataTasks.

                AggregatorDataTasks.Add(tempDataTask)
                SetServerRequests(tempDataTask)

              End If

            End If
          Catch ex As Exception
            Call LogError(DebugLevels.Minimal, "Aggregator, UpdateDataTaskCollection", LOG_LEVELS.Error, ex.Message, "Error checking task in tblDataTasks", ex.StackTrace)
          End Try
        Next

        ' Now, Any Tasks remaining in ExistingDataTasks are obsolete and should be removed from 
        ' AggregatorDataTasks

        For TaskCounter = 0 To (ExistingDataTasks.Count - 1)
          Try
            tempDataTask = CType(ExistingDataTasks(TaskCounter), AggregatorDataTask)

            AggregatorDataTasks.Remove(tempDataTask)

            CancelDataTask(tempDataTask)

          Catch ex As Exception
          End Try
        Next

      End SyncLock

    Catch ex As Exception
      Call LogError(DebugLevels.Minimal, "Aggregator, UpdateDataTaskCollection", LOG_LEVELS.Error, ex.Message, "Error updating DataTask Collection", ex.StackTrace)
    End Try

		'' Remove Some Temporary Datasets so that they are refreshed.
		'NaplesInitiator.Naples_DataBaseClient.thisDataHandler.Drop_Dataset(TEMP_Get_MI_tblPortfolioIndex)
		'NaplesInitiator.Naples_DataBaseClient.thisDataHandler.Drop_Dataset(TEMP_Get_MI_tblPortfolioData)


  End Sub

	Friend Sub VerifyAllPortfolioDataRequests()

		Dim TaskCounter As Integer
		Dim ActiveDataTask As AggregatorDataTask

		For TaskCounter = 0 To (AggregatorDataTasks.Count - 1)
			Try
				ActiveDataTask = Nothing

				SyncLock AggregatorDataTasks
					If TaskCounter < AggregatorDataTasks.Count Then
						ActiveDataTask = CType(AggregatorDataTasks(TaskCounter), AggregatorDataTask)
					End If
				End SyncLock

				If (ActiveDataTask IsNot Nothing) Then
					Select Case ActiveDataTask.TaskType
						Case DataTaskType.EmailPortfolioStockChangeNotification
							VerifyPortfolioDataRequests(ActiveDataTask)

					End Select
				End If

			Catch ex As Exception
			End Try

		Next

	End Sub

	Private Function InitialiseDataTask(ByRef pThisDataTask As AggregatorDataTask, ByRef DataTaskRow As RenaissanceDataClass.DSDataTask.tblDataTaskRow) As AggregatorDataTask
		' ************************************************************************************
		'
		'
		'
		' ************************************************************************************
		Dim NewDataTask As AggregatorDataTask = Nothing

		Try

			' Do we need to Cancel an existing Request ?
			NewDataTask = CancelDataTask(pThisDataTask)

			' Get Data Task Details
			NewDataTask = ParseDataTask(NewDataTask, DataTaskRow)

			' Create and Post Server Requests as necessary.
			Call SetServerRequests(NewDataTask)

			Return NewDataTask

		Catch ex As Exception

			Call CancelDataTask(NewDataTask)
			Return Nothing
		End Try

	End Function

  Private Function CancelDataTask(ByRef pThisDataTask As Object) As AggregatorDataTask
    If (TypeOf pThisDataTask Is AggregatorDataTask) Then
      Return CancelDataTask(CType(pThisDataTask, AggregatorDataTask))
    Else
      Return CType(pThisDataTask, AggregatorDataTask)
    End If
  End Function

  Private Function CancelDataTask(ByRef pThisDataTask As AggregatorDataTask) As AggregatorDataTask

    Dim IndexCounter As Integer

    ' Cancel all the DataRequests, this will also cancel the embedded Static or RT Items.

    If (pThisDataTask IsNot Nothing) Then
      SyncLock pThisDataTask
        For IndexCounter = 0 To (pThisDataTask.ServerRequestCount - 1)
          If (pThisDataTask.ServerRequest(IndexCounter) IsNot Nothing) Then
            Try
              Me.NaplesInitiator.Naples_DataServer.RemoveRequest(pThisDataTask.ServerRequest(IndexCounter))
            Catch ex As Exception
						End Try

						Try
							RemoveHandler pThisDataTask.ServerRequest(IndexCounter).DataUpdate, AddressOf Me.DataUpdate
						Catch ex As Exception
						End Try

						Try
							pThisDataTask.ServerRequest(IndexCounter) = Nothing
							pThisDataTask.ServerRTItem(IndexCounter) = Nothing
							pThisDataTask.ReferencePrice(IndexCounter) = 0
						Catch ex As Exception
						End Try
					End If

				Next
      End SyncLock
    End If

    'If (Not (pThisDataTask Is Nothing)) AndAlso (Not (pThisDataTask.ServerRequest Is Nothing)) Then
    '  SyncLock pThisDataTask

    '    Select Case pThisDataTask.TaskType
    '      Case NaplesGlobals.DataTaskType.RealtimeSecurityCapture, NaplesGlobals.DataTaskType.PeriodicSecurityCapture
    '        If (pThisDataTask.ServerRequest.RequestType = DS_RequestType.API_Static) Or _
    '        (pThisDataTask.ServerRequest.RequestType = DS_RequestType.API_Realtime) Then

    '          Try
    '            Me.NaplesInitiator.Naples_DataServer.RemoveRequest(pThisDataTask.ServerRequest)
    '          Catch ex As Exception
    '          End Try

    '          If Not (pThisDataTask.ServerRTItem Is Nothing) Then
    '            Try
    '              Me.NaplesInitiator.Naples_DataServer.RemoveRequest(pThisDataTask.ServerRTItem)
    '            Catch ex As Exception
    '            End Try
    '          End If

    '        End If

    '      Case NaplesGlobals.DataTaskType.PortfolioCapture

    '      Case NaplesGlobals.DataTaskType.PageCapture

    '      Case Else

    '    End Select

    '    pThisDataTask.ServerRequest = Nothing
    '    pThisDataTask.ServerRTItem = Nothing

    '  End SyncLock
    'End If

    Return pThisDataTask

  End Function

	Private Function ParseDataTask(ByRef pThisDataTask As AggregatorDataTask, ByVal DataTaskRow As RenaissanceDataClass.DSDataTask.tblDataTaskRow) As AggregatorDataTask
		' ************************************************************************************
		'
		'
		'
		' ************************************************************************************

		' Dim NewServerRequest As DataServerRequest
		Dim NewDataTask As AggregatorDataTask
		Dim XML_TaskDetails As XmlDocument

		Try
			NewDataTask = pThisDataTask

			If (DataTaskRow Is Nothing) OrElse (DataTaskRow.IsTaskIDNull) OrElse (DataTaskRow.TaskDefinition.Length = 0) Then
				Return Nothing
			End If

			If pThisDataTask Is Nothing Then
				NewDataTask = New AggregatorDataTask(DataTaskRow.TaskID)
			End If

			If (DataTaskRow.TaskID <> NewDataTask.TaskID) Then
				Return Nothing
			End If

			' Get Data Task Details

			Try
				XML_TaskDetails = New XmlDocument
				XML_TaskDetails.LoadXml(DataTaskRow.TaskDefinition)
			Catch ex As Exception
				Return Nothing
			End Try

			' Set Task Details :-

			Dim nodelist As XmlNodeList

			NewDataTask.InstrumentID = DataTaskRow.InstrumentID

			' Task Type

			Try
				nodelist = XML_TaskDetails.GetElementsByTagName("RequestType")
				If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) Then
					NewDataTask.TaskType = System.Enum.Parse(GetType(NaplesGlobals.DataTaskType), nodelist(0).InnerText)
				Else
					NewDataTask.TaskType = NaplesGlobals.DataTaskType.NoTask
				End If
			Catch ex As Exception
				NewDataTask.TaskType = NaplesGlobals.DataTaskType.NoTask
			End Try

			' Task Description

			NewDataTask.TaskDescription = DataTaskRow.TaskDescription.ToString

			' NewDataTask.LimitToMonitorItems
			'       

			Try
				nodelist = XML_TaskDetails.GetElementsByTagName("LimitToMonitorItems")
				If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) Then
					If CBool(nodelist(0).InnerText) Then
						NewDataTask.LimitToMonitorItems = True
					Else
						NewDataTask.LimitToMonitorItems = False
					End If
				Else
					NewDataTask.LimitToMonitorItems = False
				End If
			Catch ex As Exception
				NewDataTask.LimitToMonitorItems = False
			End Try

			' Task Comment
			'       

			Try
				nodelist = XML_TaskDetails.GetElementsByTagName("TaskComment")
				If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) Then
					NewDataTask.TaskComment = CStr(nodelist(0).InnerText)
				Else
					NewDataTask.TaskComment = ""
				End If
			Catch ex As Exception
				NewDataTask.TaskComment = ""
			End Try

			' Aggregate Emails
			'       

			Try
				nodelist = XML_TaskDetails.GetElementsByTagName("AggregateEmails")
				If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) Then
					NewDataTask.AggregateEmails = CBool(nodelist(0).InnerText)
				Else
					NewDataTask.AggregateEmails = False
				End If
			Catch ex As Exception
				NewDataTask.AggregateEmails = False
			End Try

			Try
				nodelist = XML_TaskDetails.GetElementsByTagName("PriceMoveTrigger")
				If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) AndAlso (IsNumeric(nodelist(0).InnerText)) Then
					NewDataTask.PriceMoveTrigger = CDbl(nodelist(0).InnerText)
				Else
					NewDataTask.PriceMoveTrigger = 0.1
				End If
			Catch ex As Exception
				NewDataTask.PriceMoveTrigger = 0.1
			End Try

			' Email Addresses

			Try
				Dim AddressCount As Integer = 0
				Dim AddressIndex As Integer = 0
				Dim EMailAddress As String

				nodelist = XML_TaskDetails.GetElementsByTagName("EMail_AddressCount")
				If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) AndAlso (IsNumeric(nodelist(0).InnerText)) Then
					AddressCount = CInt(nodelist(0).InnerText)
				End If

				For AddressIndex = 1 To AddressCount
					EMailAddress = ""

					nodelist = XML_TaskDetails.GetElementsByTagName("EMail_PersonID_" & AddressIndex.ToString)
					If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) AndAlso (IsNumeric(nodelist(0).InnerText)) Then
						NewDataTask.EmailPersonIDs(AddressIndex - 1) = CInt(nodelist(0).InnerText)
					End If
				Next

			Catch ex As Exception
			End Try

			' SaveDays

			Try
				If NewDataTask.AggregateEmails Then
					Try
						nodelist = XML_TaskDetails.GetElementsByTagName("SaveDays")
						If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) AndAlso (IsNumeric(nodelist(0).InnerText)) Then
							NewDataTask.DaysToSaveOn = CInt(nodelist(0).InnerText)
						Else
							NewDataTask.DaysToSaveOn = NaplesGlobals.DataTaskDay.WeekDays
						End If
					Catch ex As Exception
						NewDataTask.DaysToSaveOn = NaplesGlobals.DataTaskDay.WeekDays
					End Try
				Else
					NewDataTask.DaysToSaveOn = NaplesGlobals.DataTaskDay.None
				End If
			Catch ex As Exception
				NewDataTask.DaysToSaveOn = NaplesGlobals.DataTaskDay.WeekDays
			End Try

			' Frequency - 'Once' or 'Interval'

			Try
				nodelist = XML_TaskDetails.GetElementsByTagName("Frequency")
				If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) Then
					If nodelist(0).InnerText.StartsWith("O") Then
						NewDataTask.SaveOnceADay = True
					Else
						If (NewDataTask.TaskType = NaplesGlobals.DataTaskType.PortfolioCapture) Then
							' Portfolio Capture is always 'Once-a-Day'
							NewDataTask.SaveOnceADay = True
						Else
							NewDataTask.SaveOnceADay = False
						End If
					End If
				Else
					NewDataTask.SaveOnceADay = True
				End If
			Catch ex As Exception
				NewDataTask.SaveOnceADay = True
			End Try

			' Start Time

			Try
				nodelist = XML_TaskDetails.GetElementsByTagName("StartTime")
				If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) AndAlso (IsDate(nodelist(0).InnerText)) Then
					NewDataTask.StartTime = CDate(nodelist(0).InnerText)
				Else
					NewDataTask.StartTime = New Date(3000, 1, 1, 12, 0, 0)
				End If
			Catch ex As Exception
				NewDataTask.StartTime = New Date(3000, 1, 1, 12, 0, 0)
			End Try

			' End Time

			Try
				nodelist = XML_TaskDetails.GetElementsByTagName("EndTime")
				If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) AndAlso (IsDate(nodelist(0).InnerText)) Then
					NewDataTask.EndTime = CDate(nodelist(0).InnerText)
				Else
					NewDataTask.EndTime = New Date(3000, 1, 1, 12, 0, 0)
				End If
			Catch ex As Exception
				NewDataTask.EndTime = New Date(3000, 1, 1, 12, 0, 0)
			End Try

			' Interval

			Try
				nodelist = XML_TaskDetails.GetElementsByTagName("Interval")
				If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) AndAlso (IsNumeric(nodelist(0).InnerText)) Then
					NewDataTask.Interval = New TimeSpan(0, 0, CInt(nodelist(0).InnerText))
				Else
					NewDataTask.Interval = New TimeSpan(0, 0, 3600)
				End If
			Catch ex As Exception
				NewDataTask.Interval = New TimeSpan(0, 0, 3600)
			End Try

			'' Security

			'Try
			'  nodelist = XML_TaskDetails.GetElementsByTagName("Security")
			'  If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) Then
			'    NewDataTask.SecurityTicker = nodelist(0).InnerText
			'  Else
			'    NewDataTask.SecurityTicker = ""
			'    If (NewDataTask.TaskType = NaplesGlobals.DataTaskType.PeriodicSecurityCapture) Or (NewDataTask.TaskType = NaplesGlobals.DataTaskType.RealtimeSecurityCapture) Then
			'      NewDataTask.TaskType = NaplesGlobals.DataTaskType.NoTask
			'    End If
			'  End If
			'Catch ex As Exception
			'  NewDataTask.SecurityTicker = ""
			'  NewDataTask.TaskType = NaplesGlobals.DataTaskType.NoTask
			'End Try

			' Instrument

			Try
				nodelist = XML_TaskDetails.GetElementsByTagName("Instrument")
				If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) Then
					NewDataTask.Instrument = nodelist(0).InnerText
				Else
					NewDataTask.Instrument = ""
				End If
			Catch ex As Exception
				NewDataTask.Instrument = ""
			End Try

			' InstrumentID

			Try
				nodelist = XML_TaskDetails.GetElementsByTagName("InstrumentID")
				If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) AndAlso (IsNumeric(nodelist(0).InnerText)) Then
					NewDataTask.InstrumentID = CInt(nodelist(0).InnerText)
				Else
					NewDataTask.InstrumentID = 0
				End If
			Catch ex As Exception
				NewDataTask.Instrument = ""
			End Try

			' Portfolio

			Try
				nodelist = XML_TaskDetails.GetElementsByTagName("Portfolio")
				If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) Then
					NewDataTask.Portfolio = nodelist(0).InnerText
				Else
					NewDataTask.Portfolio = ""
					If (NewDataTask.TaskType = NaplesGlobals.DataTaskType.PortfolioCapture) Then
						NewDataTask.TaskType = NaplesGlobals.DataTaskType.NoTask
					End If
				End If
			Catch ex As Exception
				NewDataTask.Portfolio = ""
				If (NewDataTask.TaskType = NaplesGlobals.DataTaskType.PortfolioCapture) Then
					NewDataTask.TaskType = NaplesGlobals.DataTaskType.NoTask
				End If
			End Try

			' PortfolioID

			Try
				nodelist = XML_TaskDetails.GetElementsByTagName("PortfolioID")
				If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) AndAlso (IsNumeric(nodelist(0).InnerText)) Then
					NewDataTask.PortfolioID = CInt(nodelist(0).InnerText)
				Else
					NewDataTask.PortfolioID = 0
					If (NewDataTask.TaskType = NaplesGlobals.DataTaskType.EmailPortfolioStockChangeNotification) Then	' OrElse (NewDataTask.TaskType = NaplesGlobals.DataTaskType.PortfolioCapture) Then
						NewDataTask.TaskType = NaplesGlobals.DataTaskType.NoTask
					End If
				End If
			Catch ex As Exception
				NewDataTask.Portfolio = ""
				If (NewDataTask.TaskType = NaplesGlobals.DataTaskType.PortfolioCapture) Then
					NewDataTask.TaskType = NaplesGlobals.DataTaskType.NoTask
				End If
			End Try

			' PageCode

			Try
				nodelist = XML_TaskDetails.GetElementsByTagName("PageCode")
				If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) Then
					NewDataTask.PageCode = nodelist(0).InnerText
				Else
					NewDataTask.PageCode = ""
					If (NewDataTask.TaskType = NaplesGlobals.DataTaskType.PageCapture) Then
						NewDataTask.TaskType = NaplesGlobals.DataTaskType.NoTask
					End If
				End If
			Catch ex As Exception
				NewDataTask.PageCode = ""
				If (NewDataTask.TaskType = NaplesGlobals.DataTaskType.PageCapture) Then
					NewDataTask.TaskType = NaplesGlobals.DataTaskType.NoTask
				End If
			End Try

			' Fields

			Try
				nodelist = XML_TaskDetails.GetElementsByTagName("Fields")
				If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) Then
					NewDataTask.Fields = nodelist(0).InnerText
				Else
					NewDataTask.Fields = ""
					If (NewDataTask.TaskType = NaplesGlobals.DataTaskType.PeriodicSecurityCapture) Or (NewDataTask.TaskType = NaplesGlobals.DataTaskType.RealtimeSecurityCapture) Then
						NewDataTask.TaskType = NaplesGlobals.DataTaskType.NoTask
					End If
					If (NewDataTask.TaskType = NaplesGlobals.DataTaskType.PeriodicMarketSecurityCapture) Or (NewDataTask.TaskType = NaplesGlobals.DataTaskType.RealtimeMarketSecurityCapture) Then
						NewDataTask.TaskType = NaplesGlobals.DataTaskType.NoTask
					End If
				End If
			Catch ex As Exception
				NewDataTask.Fields = ""
				If (NewDataTask.TaskType = NaplesGlobals.DataTaskType.PeriodicSecurityCapture) Or (NewDataTask.TaskType = NaplesGlobals.DataTaskType.RealtimeSecurityCapture) Then
					NewDataTask.TaskType = NaplesGlobals.DataTaskType.NoTask
				End If
				If (NewDataTask.TaskType = NaplesGlobals.DataTaskType.PeriodicMarketSecurityCapture) Or (NewDataTask.TaskType = NaplesGlobals.DataTaskType.RealtimeMarketSecurityCapture) Then
					NewDataTask.TaskType = NaplesGlobals.DataTaskType.NoTask
				End If
			End Try

		Catch ex As Exception
			Me.LogError(DebugLevels.Minimal, "Aggregator, ParseDataTask()", LOG_LEVELS.Error, ex.Message, "Error Parsing Data Task", ex.StackTrace, 0)
			Return Nothing
		End Try

		Return NewDataTask

	End Function

  Private Function SetServerRequests(ByRef NewDataTask As AggregatorDataTask) As AggregatorDataTask
    Dim NewServerRequest As DataServerRequest

    Try

      ' Check NewDataTask is not Nothing.

      If (NewDataTask Is Nothing) Then
        Return NewDataTask
      End If

      SyncLock NewDataTask

        ' Post Server Requests as necessary.
        Select Case NewDataTask.TaskType
          Case NaplesGlobals.DataTaskType.RealtimeSecurityCapture, NaplesGlobals.DataTaskType.PeriodicSecurityCapture

            NewServerRequest = New DataServerRequest
            NewDataTask.ServerRequest = NewServerRequest
            AddHandler NewServerRequest.DataUpdate, AddressOf Me.DataUpdate

            NewServerRequest.RequestStatus = DS_RequestStatus.None
            NewServerRequest.RequestType = DS_RequestType.API_Realtime
						NewServerRequest.Request = NewDataTask.Instrument & "|" & NewDataTask.Fields

            ' More code here

            Me.NaplesInitiator.Naples_DataServer.PostNewRequest(NewServerRequest)

					Case NaplesGlobals.DataTaskType.RealtimeMarketSecurityCapture, NaplesGlobals.DataTaskType.PeriodicMarketSecurityCapture

						NewServerRequest = New DataServerRequest
						NewDataTask.ServerRequest = NewServerRequest
						AddHandler NewServerRequest.DataUpdate, AddressOf Me.DataUpdate

						NewServerRequest.RequestStatus = DS_RequestStatus.None
						NewServerRequest.RequestType = DS_RequestType.API_Realtime
						NewServerRequest.Request = NewDataTask.Instrument & "|" & NewDataTask.Fields

						' More code here

						Me.NaplesInitiator.Naples_DataServer.PostNewRequest(NewServerRequest)

					Case NaplesGlobals.DataTaskType.PortfolioCapture

						NewServerRequest = New DataServerRequest
						NewDataTask.ServerRequest = NewServerRequest
						AddHandler NewServerRequest.DataUpdate, AddressOf Me.DataUpdate

						NewServerRequest.RequestStatus = DS_RequestStatus.None
						NewServerRequest.RequestType = DS_RequestType.DDE_GetPortfolio
						NewServerRequest.Request = NewDataTask.Portfolio

						'  Me.NaplesInitiator.Naples_DDECapture.PostNewRequest(NewServerRequest)

          Case NaplesGlobals.DataTaskType.PageCapture, DataTaskType.EmailPage

            NewServerRequest = New DataServerRequest
            NewDataTask.ServerRequest = NewServerRequest
            AddHandler NewServerRequest.DataUpdate, AddressOf Me.DataUpdate

            NewServerRequest.RequestStatus = DS_RequestStatus.None
            NewServerRequest.RequestType = DS_RequestType.DDE_ScreenCopy
            NewServerRequest.Request = NewDataTask.PageCode

            ' Me.NaplesInitiator.Naples_DDECapture.PostNewRequest(NewServerRequest)

          Case DataTaskType.EmailPeriodicInstrumentInformation

          Case DataTaskType.EmailPortfolioStockChangeNotification

            Dim PortfolioTickers() As String
            Dim TickerCounter As Integer

						PortfolioTickers = NaplesInitiator.Naples_DataBaseClient.GetPortfolioMarketInstruments(NewDataTask.PortfolioID, NewDataTask.LimitToMonitorItems, True, True)

						If (PortfolioTickers IsNot Nothing) AndAlso (PortfolioTickers.Length > 0) Then
							For TickerCounter = (PortfolioTickers.Length - 1) To 0 Step -1 ' Start at the End so the DataTask arrays only redim once.
								Try
									NewServerRequest = New DataServerRequest
									NewDataTask.ServerRequest(TickerCounter) = NewServerRequest
									AddHandler NewServerRequest.DataUpdate, AddressOf Me.DataUpdate

									NewServerRequest.RequestStatus = DS_RequestStatus.None
									NewServerRequest.RequestType = DS_RequestType.API_Realtime
									NewServerRequest.Request = PortfolioTickers(TickerCounter) & "|" & BBG_LAST_PRICE	' LAST_TRADE

									' Get Reference price 
									Dim thisReferencePrice As Double
									Dim thisReferenceDate As Date

									NewDataTask.ReferencePrice(TickerCounter) = NaplesInitiator.Naples_DataBaseClient.GetReferencePrice(NewDataTask.TaskID, PortfolioTickers(TickerCounter), thisReferencePrice, thisReferenceDate)
									NewDataTask.ReferenceDate(TickerCounter) = thisReferenceDate

									' Post Data Request
									Me.NaplesInitiator.Naples_DataServer.PostNewRequest(NewServerRequest)
								Catch ex As Exception
									Me.LogError(DebugLevels.Minimal, "Aggregator, SetServerRequests()", LOG_LEVELS.Error, ex.Message, "Error setting Portfolio server requests.", ex.StackTrace, 0)
								End Try
							Next
						End If

          Case DataTaskType.EmailSingleStockChangeNotification

            NewServerRequest = New DataServerRequest
            NewDataTask.ServerRequest = NewServerRequest
            AddHandler NewServerRequest.DataUpdate, AddressOf Me.DataUpdate

            NewServerRequest.RequestStatus = DS_RequestStatus.None
            NewServerRequest.RequestType = DS_RequestType.API_Realtime
						NewServerRequest.Request = NewDataTask.Instrument & "|" & BBG_LAST_PRICE ' LAST_TRADE

            ' Get Reference price 
						Dim thisReferencePrice As Double
						Dim thisReferenceDate As Date

						NewDataTask.ReferencePrice = NaplesInitiator.Naples_DataBaseClient.GetReferencePrice(NewDataTask.TaskID, NewDataTask.Instrument, thisReferencePrice, thisReferenceDate)
						NewDataTask.ReferenceDate = thisReferenceDate

            ' Post Data Request
            Me.NaplesInitiator.Naples_DataServer.PostNewRequest(NewServerRequest)

          Case Else
            Return Nothing
        End Select

      End SyncLock

      Return NewDataTask

		Catch ex As Exception
			Me.LogError(DebugLevels.Minimal, "Aggregator, SetServerRequests()", LOG_LEVELS.Error, ex.Message, "Error setting server requests.", ex.StackTrace, 0)

			Return NewDataTask
    End Try

  End Function

  Private Sub VerifyPortfolioDataRequests(ByRef ActiveDataTask As AggregatorDataTask)

    Select Case ActiveDataTask.TaskType

      Case DataTaskType.EmailPortfolioStockChangeNotification

        Dim PortfolioTickers() As String
        Dim TickerCounter As Integer
        Dim ServerRequestCounter As Integer
        Dim ServerRequestNullEntryIndex As Integer
        Dim FoundItFlag As Boolean
        Dim NewServerRequest As DataServerRequest

				PortfolioTickers = NaplesInitiator.Naples_DataBaseClient.GetPortfolioMarketInstruments(ActiveDataTask.PortfolioID, ActiveDataTask.LimitToMonitorItems, True, True)

        If (PortfolioTickers Is Nothing) OrElse ((PortfolioTickers.Length <= 0) AndAlso (ActiveDataTask.ServerRequestCount <= 0)) Then
          ' There are no Stocks in the portfolio and there are no requests in the Data Task.
          ' OK, exit
          Exit Sub
        End If

        SyncLock ActiveDataTask

          ' Check that there are no ServerRequests for Items not in the Tickers List.
          For ServerRequestCounter = 0 To (ActiveDataTask.ServerRequestCount - 1)
            Try
              FoundItFlag = False

              If (ActiveDataTask.ServerRequest(ServerRequestCounter) IsNot Nothing) Then
                For TickerCounter = 0 To (PortfolioTickers.Length - 1)
                  If (ActiveDataTask.ServerRequest(ServerRequestCounter).Request.StartsWith(PortfolioTickers(TickerCounter) & "|")) Then
                    FoundItFlag = True
                    Exit For
                  End If
                Next

                If (FoundItFlag = False) Then
                  ' Remove Server Request
									Try
										RemoveHandler ActiveDataTask.ServerRequest(ServerRequestCounter).DataUpdate, AddressOf Me.DataUpdate
									Catch ex As Exception
									End Try

									Try
										Me.NaplesInitiator.Naples_DataServer.RemoveRequest(ActiveDataTask.ServerRequest(ServerRequestCounter))
									Catch ex As Exception
									End Try

									Try
										ActiveDataTask.ServerRequest(ServerRequestCounter) = Nothing
										ActiveDataTask.ServerRTItem(ServerRequestCounter) = Nothing
										ActiveDataTask.ReferencePrice(ServerRequestCounter) = 0
									Catch ex As Exception
									End Try

								End If
              End If
            Catch ex As Exception
            End Try
          Next

          ' Check that there are no Items in the Tickers List not in the ServerRequest array.
          For TickerCounter = 0 To (PortfolioTickers.Length - 1)
            Try
              ServerRequestNullEntryIndex = (-1)
              FoundItFlag = False

              If PortfolioTickers(TickerCounter).Length > 0 Then
                For ServerRequestCounter = (ActiveDataTask.ServerRequestCount - 1) To 0 Step -1
                  If (ActiveDataTask.ServerRequest(ServerRequestCounter) Is Nothing) Then
                    ServerRequestNullEntryIndex = ServerRequestCounter
                  End If

                  If (ActiveDataTask.ServerRequest(ServerRequestCounter).Request.StartsWith(PortfolioTickers(TickerCounter) & "|")) Then
                    FoundItFlag = True
                    Exit For
                  End If
                Next

                If (FoundItFlag = False) Then
                  ' Add Server Request
                  If (ServerRequestNullEntryIndex < 0) Then
                    ServerRequestNullEntryIndex = ActiveDataTask.ServerRequestCount
                  End If


                  NewServerRequest = New DataServerRequest
                  ActiveDataTask.ServerRequest(ServerRequestNullEntryIndex) = NewServerRequest
                  AddHandler NewServerRequest.DataUpdate, AddressOf Me.DataUpdate

                  NewServerRequest.RequestStatus = DS_RequestStatus.None
                  NewServerRequest.RequestType = DS_RequestType.API_Realtime
                  NewServerRequest.Request = PortfolioTickers(TickerCounter) & "|" & BBG_LAST_PRICE ' LAST_TRADE

                  ' Set Reference price 
									Dim thisReferencePrice As Double
									Dim thisReferenceDate As Date

									ActiveDataTask.ReferencePrice(TickerCounter) = NaplesInitiator.Naples_DataBaseClient.GetReferencePrice(ActiveDataTask.TaskID, PortfolioTickers(TickerCounter), thisReferencePrice, thisReferenceDate)
									ActiveDataTask.ReferenceDate(TickerCounter) = thisReferenceDate

                  ' Post Data Request
                  Me.NaplesInitiator.Naples_DataServer.PostNewRequest(NewServerRequest)
                End If

              End If
            Catch ex As Exception
            End Try
          Next
        End SyncLock



    End Select


  End Sub

  Private Sub DataUpdate(ByVal sender As Object, ByVal e As DataServerUpdate)
    ' ************************************************************
    ' Sender is the DataServer Request
    ' e.ReplyObject Is the Relpying Data as is NewServerRequest.ReturnObject
    ' e.UpdateObjectID = NewServerRequest.RequestId - A Unique GUID
    '
    '
    ' ************************************************************

    Dim thisServerRequest As DataServerRequest

    ' ************************************************************
    ' Don't Process Events if we are trying to close down

    If (CloseDown) Then Exit Sub

    ' ************************************************************
    ' Validate the Sender Types

    If Not (TypeOf sender Is DataServerRequest) Then
      Exit Sub
    End If

    thisServerRequest = CType(sender, DataServerRequest)

    ' ************************************************************
    ' Locate related data task

    Dim thisDataTask As AggregatorDataTask = Nothing
    Dim TaskCounter As Integer
    Dim ServerRequestIndex As Integer = 0

    For TaskCounter = 0 To (AggregatorDataTasks.Count - 1)
      thisDataTask = CType(AggregatorDataTasks(TaskCounter), AggregatorDataTask)
      ServerRequestIndex = thisDataTask.ServerRequestIndex(thisServerRequest)

      If (ServerRequestIndex >= 0) Then
        Exit For
      End If
      'If thisDataTask.ServerRequest.RequestId.Equals(thisServerRequest.RequestId) Then
      '  Exit For
      'End If
      thisDataTask = Nothing
      ServerRequestIndex = 0
    Next

    If (thisDataTask Is Nothing) Then
      ' Related Data task was not found. 

      Exit Sub
    End If

    ' ************************************************************
    ' Process Data Request dependent upon the request type.

    Select Case thisDataTask.TaskType
      Case NaplesGlobals.DataTaskType.PageCapture


      Case NaplesGlobals.DataTaskType.PortfolioCapture
        ' Aggregator Portfolio Capture 

        If thisServerRequest.RequestType = DS_RequestType.DDE_GetPortfolio Then
          ' DataServer request is as expected.

          ' Check the Request status is OK.
          Select Case thisServerRequest.RequestStatus
            Case DS_RequestStatus.RequestComplete
              ' Post Database Update request for this Portfolio Update
              Me.NaplesInitiator.Naples_DataBaseClient.PostNewRequest(New DBTask(thisDataTask.TaskType, thisDataTask, thisServerRequest.ReturnObject))

            Case DS_RequestStatus.RequestFailed

            Case DS_RequestStatus.RequestPending

            Case DS_RequestStatus.RequestRejected

            Case DS_RequestStatus.RequestSubmitted

            Case Else

          End Select

        Else
          ' DataServer Request type is not compatible with this DataTaskType


        End If

      Case NaplesGlobals.DataTaskType.PeriodicSecurityCapture
        If TypeOf thisServerRequest.ReturnObject Is DataServerRTItem Then
          Dim thisRTItem As DataServerRTItem = CType(thisServerRequest.ReturnObject, DataServerRTItem)
          thisDataTask.ServerRTItem = thisRTItem

          '' Add Callback...
					'AddHandler thisRTItem.DataUpdate, AddressOf RTDataUpdate_PeriodicSecurityCapture

          ' More Code Here
					More_Code_Here()

        Else
          thisDataTask.ServerRTItem = Nothing
        End If


      Case NaplesGlobals.DataTaskType.RealtimeSecurityCapture
        If TypeOf thisServerRequest.ReturnObject Is DataServerRTItem Then
          Dim thisRTItem As DataServerRTItem = CType(thisServerRequest.ReturnObject, DataServerRTItem)
          thisDataTask.ServerRTItem(ServerRequestIndex) = thisRTItem
          'thisDataTask.ServerRequest(ServerRequestIndex) = thisServerRequest

          ' Add Callback...
          AddHandler thisRTItem.DataUpdate, AddressOf RTDataUpdate_RealtimeSecurityCapture

          ' More Code Here

        Else
          thisDataTask.ServerRTItem = Nothing
        End If

			Case NaplesGlobals.DataTaskType.PeriodicMarketSecurityCapture
				If TypeOf thisServerRequest.ReturnObject Is DataServerRTItem Then
					Dim thisRTItem As DataServerRTItem = CType(thisServerRequest.ReturnObject, DataServerRTItem)
					thisDataTask.ServerRTItem = thisRTItem

					'' Add Callback...
					'AddHandler thisRTItem.DataUpdate, AddressOf RTDataUpdate_PeriodicMarketSecurityCapture

					' More Code Here
					More_Code_Here()

				Else
					thisDataTask.ServerRTItem = Nothing
				End If


			Case NaplesGlobals.DataTaskType.RealtimeMarketSecurityCapture
				If TypeOf thisServerRequest.ReturnObject Is DataServerRTItem Then
					Dim thisRTItem As DataServerRTItem = CType(thisServerRequest.ReturnObject, DataServerRTItem)
					thisDataTask.ServerRTItem(ServerRequestIndex) = thisRTItem
					'thisDataTask.ServerRequest(ServerRequestIndex) = thisServerRequest

					' Add Callback...
					'AddHandler thisRTItem.DataUpdate, AddressOf RTDataUpdate_RealtimeMarketSecurityCapture

					' More Code Here
					More_Code_Here()

				Else
					thisDataTask.ServerRTItem = Nothing
				End If

      Case NaplesGlobals.DataTaskType.EmailPage
        If thisServerRequest.RequestType = DS_RequestType.DDE_ScreenCopy Then
          ' DataServer request is as expected.

          ' Check the Request status is OK.
          Select Case thisServerRequest.RequestStatus
            Case DS_RequestStatus.RequestComplete
              ' Email Page
              ' Me.NaplesInitiator.Naples_DataBaseClient.PostNewRequest(New DBTask(thisDataTask.TaskType, thisDataTask, thisServerRequest.ReturnObject))

              ' More Code Here

            Case DS_RequestStatus.RequestFailed

            Case DS_RequestStatus.RequestPending

            Case DS_RequestStatus.RequestRejected

            Case DS_RequestStatus.RequestSubmitted

            Case Else

          End Select

        Else
          ' DataServer Request type is not compatible with this DataTaskType

        End If

      Case NaplesGlobals.DataTaskType.EmailPeriodicInstrumentInformation

      Case NaplesGlobals.DataTaskType.EmailPortfolioStockChangeNotification
        If TypeOf thisServerRequest.ReturnObject Is DataServerRTItem Then
          Dim thisRTItem As DataServerRTItem = CType(thisServerRequest.ReturnObject, DataServerRTItem)

          If (ServerRequestIndex >= 0) Then
            thisDataTask.ServerRTItem(ServerRequestIndex) = thisRTItem
            ' thisDataTask.ServerRequest(ExistingIndex) = thisServerRequest

            ' Add Callback...
            Try
              ' Just in case
              RemoveHandler thisRTItem.DataUpdate, AddressOf RTDataUpdate_EmailPortfolioStockChange
            Catch ex As Exception
            End Try
            AddHandler thisRTItem.DataUpdate, AddressOf RTDataUpdate_EmailPortfolioStockChange

            ' More Code Here

          Else
            ' Unknown ServerRequest
            ' Remove RT Item

            NaplesInitiator.Naples_DataServer.RemoveRequest(thisRTItem)
          End If
        Else
          thisDataTask.ServerRTItem = Nothing
        End If

      Case NaplesGlobals.DataTaskType.EmailSingleStockChangeNotification
        If TypeOf thisServerRequest.ReturnObject Is DataServerRTItem Then
          Dim thisRTItem As DataServerRTItem = CType(thisServerRequest.ReturnObject, DataServerRTItem)

          thisDataTask.ServerRTItem(ServerRequestIndex) = thisRTItem
          ' thisDataTask.ServerRequest = thisServerRequest

          ' Add Callback...
          AddHandler thisRTItem.DataUpdate, AddressOf RTDataUpdate_EmailSingleStockChange

          ' More Code Here

        Else
          thisDataTask.ServerRTItem = Nothing
        End If

    End Select



  End Sub

#Region " RT Item Update Handlers"

  ' Using a Queue for RT Updates which are processed in the Aggregator Work loop is effectively a method
  ' for marshaling the Update processing back to the Aggregator thread.

  Friend Sub RTDataUpdate_EmailSingleStockChange(ByVal sender As Object, ByVal e As DataServerUpdate)
    Try
      SyncLock RTItemUpdateQueue
        RTItemUpdateQueue.Enqueue(New RTItemUpdateQueueClass(NaplesGlobals.DataTaskType.EmailSingleStockChangeNotification, CType(sender, DataServerRTItem)))
      End SyncLock
    Catch ex As Exception
    End Try
  End Sub

  Friend Sub RTDataUpdate_EmailPortfolioStockChange(ByVal sender As Object, ByVal e As DataServerUpdate)
    Try
      SyncLock RTItemUpdateQueue
        RTItemUpdateQueue.Enqueue(New RTItemUpdateQueueClass(NaplesGlobals.DataTaskType.EmailPortfolioStockChangeNotification, CType(sender, DataServerRTItem)))
      End SyncLock
    Catch ex As Exception
    End Try
  End Sub

  Friend Sub RTDataUpdate_RealtimeSecurityCapture(ByVal sender As Object, ByVal e As DataServerUpdate)
    Try
      SyncLock RTItemUpdateQueue
        RTItemUpdateQueue.Enqueue(New RTItemUpdateQueueClass(NaplesGlobals.DataTaskType.RealtimeSecurityCapture, CType(sender, DataServerRTItem)))
      End SyncLock
    Catch ex As Exception
    End Try
  End Sub

#End Region

#Region " Log Error"

  Private Function LogError( _
    ByVal pDebugLevel As DebugLevels, _
    ByVal p_Error_Location As String, _
    ByVal p_System_Error_Number As Integer, _
    ByVal p_System_Error_Description As String, _
    Optional ByVal p_Error_Message As String = "", _
    Optional ByVal p_Error_StackTrace As String = "", _
    Optional ByVal p_Log_to As Integer = 0) As Integer

    Try
      If Not (NaplesInitiator Is Nothing) Then
        If Not (NaplesInitiator.Naples_DataBaseClient Is Nothing) Then
          Return NaplesInitiator.Naples_DataBaseClient.LogError(pDebugLevel, p_Error_Location, p_System_Error_Number, p_System_Error_Description, p_Error_Message, p_Error_StackTrace, p_Log_to)
        End If
      End If

    Catch ex As Exception
      Return (-1)
    End Try
  End Function

#End Region

#Region " Debug Code"

	Friend Function Debug_GetDataTaskDetails(Optional ByVal TaskIDString As String = "") As ArrayList
		Dim RValArraylist As New ArrayList

		Dim thisDataTask As AggregatorDataTask = Nothing
		Dim AggregatorDataTasksCount As Integer
		Dim TaskCounter As Integer
		Dim ItemCounter As Integer
		Dim thisDataServerRequest As DataServerRequest
		Dim thisDataServerRTItem As DataServerRTItem
		Dim DebugTaskID As Integer = 0

		Try
			If IsNumeric(TaskIDString) Then
				DebugTaskID = CInt(TaskIDString)
			End If
		Catch ex As Exception
			DebugTaskID = 0
		End Try

		Try
			SyncLock AggregatorDataTasks
				AggregatorDataTasksCount = AggregatorDataTasks.Count
			End SyncLock

			For TaskCounter = 0 To (AggregatorDataTasksCount - 1)
				Try
					thisDataTask = Nothing

					' Get Task.

					SyncLock AggregatorDataTasks
						If TaskCounter < AggregatorDataTasks.Count Then
							thisDataTask = CType(AggregatorDataTasks(TaskCounter), AggregatorDataTask)
						End If
					End SyncLock
				Catch ex As Exception
					thisDataTask = Nothing
				End Try

				If (thisDataTask IsNot Nothing) AndAlso ((DebugTaskID = 0) Or (thisDataTask.TaskID = DebugTaskID)) Then
					Try
						SyncLock thisDataTask

							RValArraylist.Add(CStr("  "))
							RValArraylist.Add(CStr("DataTask " & (TaskCounter + 1).ToString & " of " & AggregatorDataTasksCount.ToString))
							RValArraylist.Add(CStr("  TaskID = " & thisDataTask.TaskID.ToString))
							RValArraylist.Add(CStr("  TaskType = " & thisDataTask.TaskType.ToString))
							RValArraylist.Add(CStr("  TaskDescription = '" & thisDataTask.TaskDescription.ToString & "'"))
							RValArraylist.Add(CStr("  Instrument = '" & thisDataTask.Instrument.ToString & "'"))
							RValArraylist.Add(CStr("  InstrumentID = " & thisDataTask.InstrumentID.ToString))
							RValArraylist.Add(CStr("  Portfolio = '" & thisDataTask.Portfolio.ToString & "'"))
							RValArraylist.Add(CStr("  PortfolioID = " & thisDataTask.PortfolioID.ToString))
							RValArraylist.Add(CStr("  Fields = '" & thisDataTask.Fields.ToString & "'"))
							RValArraylist.Add(CStr("  PageCode = '" & thisDataTask.PageCode.ToString & "'"))
							RValArraylist.Add(CStr("  LimitToMonitorItems = " & thisDataTask.LimitToMonitorItems.ToString))
							RValArraylist.Add(CStr("  AggregateEmails = " & thisDataTask.AggregateEmails.ToString))
							RValArraylist.Add(CStr("  PriceMoveTrigger = " & thisDataTask.PriceMoveTrigger.ToString("#,##0.00##%")))
							RValArraylist.Add(CStr("  DaysToSaveOn = " & thisDataTask.DaysToSaveOn.ToString))
							RValArraylist.Add(CStr("  SaveOnceADay = " & thisDataTask.SaveOnceADay.ToString))
							RValArraylist.Add(CStr("  StartTime = " & thisDataTask.StartTime.ToString(DISPLAYMEMBER_TIMEFORMAT)))
							RValArraylist.Add(CStr("  EndTime = " & thisDataTask.EndTime.ToString(DISPLAYMEMBER_TIMEFORMAT)))
							RValArraylist.Add(CStr("  Interval = " & thisDataTask.Interval.ToString))
							RValArraylist.Add(CStr("  NextUpdateTime = " & thisDataTask.NextUpdateTime.ToString(DISPLAYMEMBER_LONGDATEFORMAT)))
							RValArraylist.Add(CStr("  TaskLastUpdated = " & thisDataTask.LastUpdated.ToString(DISPLAYMEMBER_LONGDATEFORMAT)))
							RValArraylist.Add(CStr("  TaskLastUpdateTime = " & thisDataTask.LastUpdateTime.ToString(DISPLAYMEMBER_LONGDATEFORMAT)))
							RValArraylist.Add(CStr("  UpdateDue = " & thisDataTask.UpdateDue.ToString))
							RValArraylist.Add(CStr("  "))

							For ItemCounter = 0 To (thisDataTask.EmailPersonIDsArray.Length - 1)
								RValArraylist.Add(CStr("  EMailID " & (ItemCounter + 1).ToString & " = " & thisDataTask.EmailPersonIDsArray(ItemCounter).ToString))
							Next

							RValArraylist.Add(CStr("  "))

							For ItemCounter = 0 To (thisDataTask.ServerRequestCount - 1)
								thisDataServerRequest = thisDataTask.ServerRequest(ItemCounter)
								thisDataServerRTItem = thisDataTask.ServerRTItem(ItemCounter)

								RValArraylist.Add(CStr("  ServerRequest " & (ItemCounter + 1).ToString))
								If (thisDataServerRequest Is Nothing) Then
									RValArraylist.Add(CStr("    is 'Nothing'"))
								Else
									RValArraylist.Add(CStr("    RequestType = " & thisDataServerRequest.RequestType.ToString))
									RValArraylist.Add(CStr("    RequestStatus = " & thisDataServerRequest.RequestStatus.ToString))
									RValArraylist.Add(CStr("    Request = '" & thisDataServerRequest.Request.ToString & "'"))
								End If

								RValArraylist.Add(CStr("  ServerRTItem " & (ItemCounter + 1).ToString))
								If (thisDataServerRTItem Is Nothing) Then
									RValArraylist.Add(CStr("    is 'Nothing'"))
								Else
									Try
										thisDataServerRTItem.ItemLock.AcquireReaderLock(1000)

										RValArraylist.Add(CStr("    ItemStatus = " & thisDataServerRTItem.ItemStatus.ToString))
										RValArraylist.Add(CStr("    FullName = '" & thisDataServerRTItem.BBGSecurity.FullName.ToString & "'"))

										Dim FieldItem As DataServerRTField
										Dim Item As DictionaryEntry
										SyncLock thisDataServerRTItem
											For Each Item In thisDataServerRTItem.Fields
												If (Item.Value IsNot Nothing) Then
													Try
														FieldItem = CType(Item.Value, DataServerRTField)
														RValArraylist.Add(CStr("    " & FieldItem.FieldName.ToString & " = " & FieldItem.FieldValue.ToString & ", Date = " & FieldItem.DateValue.ToString(DISPLAYMEMBER_LONGDATEFORMAT)))
													Catch ex As Exception
													End Try
												End If
											Next
										End SyncLock

									Catch ex As Exception
									Finally
										If thisDataServerRTItem.ItemLock.IsReaderLockHeld Then
											thisDataServerRTItem.ItemLock.ReleaseReaderLock()
										End If

									End Try

								End If

                RValArraylist.Add(CStr("   ReferencePrice " & (ItemCounter + 1).ToString & " = " & thisDataTask.ReferencePrice(ItemCounter).ToString))
                RValArraylist.Add(CStr("   ReferenceDate " & (ItemCounter + 1).ToString & " = " & thisDataTask.ReferenceDate(ItemCounter).ToString(DISPLAYMEMBER_LONGDATEFORMAT)))
                RValArraylist.Add(CStr("   LastTickPrice " & (ItemCounter + 1).ToString & " = " & thisDataTask.LastTickPrice(ItemCounter).ToString))
                RValArraylist.Add(CStr("   LastTickDate " & (ItemCounter + 1).ToString & " = " & thisDataTask.LastTickDate(ItemCounter).ToString(DISPLAYMEMBER_LONGDATEFORMAT)))
							Next
						End SyncLock

					Catch ex As Exception

					End Try
				End If

			Next

		Catch ex As Exception

		End Try

		Return RValArraylist
	End Function

#End Region

	Private Sub More_Code_Here()

	End Sub
End Class

