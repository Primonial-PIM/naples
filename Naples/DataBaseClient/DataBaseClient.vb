Imports System.Net
Imports System.Net.Sockets
Imports System.IO
Imports System.Data.SqlClient
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.Serialization
Imports System.Threading

Imports Bloomberg.Api
Imports Bloomberg.Api.DataTypes
Imports Bloomberg.Api.DataTypes.MetaData

Imports NaplesGlobals

Imports RenaissanceGlobals

Public Class DataBaseClient
  ' *****************************************************************************************************
  ' DataBase Client
  ' -----------------
  '
  '
  ' *****************************************************************************************************

  Inherits ThreadWrapperBase

#Region " Locals "

  Private NaplesInitiator As Initiator            ' Reference back to central Initiator object.

	Private _thisDataHandler As New RenaissanceDataClass.DataHandler
	Private _thisAdaptorHandler As New RenaissanceDataClass.AdaptorHandler

  Private NewRequests As New Queue                ' Queue object for New Requests

  Public ProcessingRequest As New ReaderWriterLock  ' Lock used to prevent Updates while Requests are being processed. This is designed to prevent a request update before the request is fully processed.

  Private _CloseDownFlag As Boolean               ' Clean shutdown mechanism, TRUE results in exit from the main work loop.

	' LockObjects
	Private SetReferencePrice_Lockobject As New Object
	Private GetReferencePrice_Lockobject As New Object

  Private WithEvents Naples_TCPClient As TCP_Client

#End Region

#Region " Properties"

	Friend ReadOnly Property thisDataHandler() As RenaissanceDataClass.DataHandler
		Get
			Return _thisDataHandler
		End Get
	End Property

	Friend ReadOnly Property thisAdaptorHandler() As RenaissanceDataClass.AdaptorHandler
		Get
			Return _thisAdaptorHandler
		End Get
	End Property

#End Region

#Region " Constructor and 'ThreadClass' Overoads"

  Public Sub New(ByVal pInitiator As Initiator)
    ' ************************************************************
    ' Simple Class constructor.
    ' ************************************************************

    MyBase.New()

    Try
      Me.Thread.Name = "DataBaseClient"
      NaplesInitiator = pInitiator
    Catch ex As Exception
    End Try

    Try
      Naples_TCPClient = pInitiator.Naples_TCPClient
      AddHandler Naples_TCPClient.FCPApplicationUpdate, AddressOf Me.ApplicationAutoUpdate

      '    Private Sub ApplicationAutoUpdate(ByVal sender As System.Object, ByVal e As TCP_Client.FCPApplicationUpdateEventArgs) ' Handles Naples_TCPClient.FCPApplicationUpdate

    Catch ex As Exception
    End Try

    ' Initialise Database Connection

    'Try
    '  Call GetNaplesConnection()
    'Catch ex As Exception
    'End Try

  End Sub

  Public Property CloseDown() As Boolean
    ' ************************************************************
    ' CloseDown Property. Used to initiate clean shutdown.
    ' ************************************************************

    Get
      Return _CloseDownFlag
    End Get
    Set(ByVal Value As Boolean)
      _CloseDownFlag = Value
    End Set
  End Property


  Protected Overrides Sub DoTask()
    ' ************************************************************
    ' Overrided, Top level thread work process.
    ' ************************************************************

    Initialise()

    RunDBClient()

    TidyUp()

  End Sub

  Private Sub Initialise()
    ' ************************************************************
    ' Object Initialiser.
    ' ************************************************************
    ' Initialise Data Server

    _CloseDownFlag = False

    ' Initialise





  End Sub

  Private Sub TidyUp()
    ' ************************************************************
    ' On ending the Server
    ' ************************************************************





  End Sub

#End Region

#Region " Class definitions"
  ' PortfolioDetails

  Private Class PortfolioDetailsArrayList
    Inherits ArrayList

    ' *****************************************************************************************************
    ' Simple ArrayList overlay.
    ' 
    ' The sole purpose of this is to adjust the behavior associated  with adding a new PortfolioData
    ' data item. If a similar (Same Ticker) item already exists in the array, then the new Add
    ' operation simply adds the new values to the existing object.
    '
    ' *****************************************************************************************************

    Public Overloads Function Add(ByRef pNewDetails As PortfolioData) As PortfolioData
      Dim TempPortfolioData As PortfolioData
      Dim TempCounter As Integer


      If Me.Count > 0 Then
        For TempCounter = 0 To (Me.Count - 1)
          TempPortfolioData = Me(TempCounter)

          If (TempPortfolioData.Ticker = pNewDetails.Ticker) Then
            TempPortfolioData.Position += pNewDetails.Position
            If (TempPortfolioData.Price = 0) Or ((pNewDetails.Position <> 0) And (pNewDetails.Price <> 0)) Then
              TempPortfolioData.Price += pNewDetails.Price
            End If
            Return TempPortfolioData
          End If
        Next
      End If

      MyBase.Add(pNewDetails)

      Return pNewDetails
    End Function


  End Class

  Private Class PortfolioData
    ' *****************************************************************************************************
    '
    '
    ' *****************************************************************************************************

    Public Ticker As String
    Public Exchange As String
    Public MarketInstrumentType As Integer ' Public Enum DataProviderInstrumentType As Integer
    Public MarketInstrumentID As Integer
    Public PortfolioItemType As Integer ' Public Enum PortfolioItemType As Integer
    Public Position As Double
    Public Price As Double
    Public PriceSource As String

    Public Sub New()
      Ticker = ""
      Exchange = ""
      MarketInstrumentType = RenaissanceGlobals.DataProviderInstrumentType.None
      MarketInstrumentID = 0
      PortfolioItemType = RenaissanceGlobals.PortfolioItemType.Default
      Position = 0
      Price = 0
      PriceSource = ""
    End Sub

    Public Sub New(ByVal pPortfolioLine As String)
      Me.New()

      ' Validate for empty or inadequate strings 
      If pPortfolioLine.Length <= 40 Then
        Exit Sub
      End If

      ' Parse string :-
      Dim InitialLine As String

      ' Remove any stray CR or LF characters.

      InitialLine = pPortfolioLine.Trim(New Char() {Chr(10), Chr(13)})

      If String.Compare(InitialLine.Substring(2, 1), ")") <> 0 Then
        Exit Sub
      End If

      Dim thisTicker As String
      Dim thisExchange As String
      Dim thisPriceSource As String
      Dim thisMarketInstrumentType As RenaissanceGlobals.DataProviderInstrumentType
      Dim thisPortfolioItemType As RenaissanceGlobals.PortfolioItemType
      Dim thisPosition As Double
      Dim thisPrice As Double

      Dim ExchangeOffset As Integer
      ExchangeOffset = InitialLine.LastIndexOf(" US ")
      If ExchangeOffset < 0 Then
        ExchangeOffset = 18
      End If

      InitialLine = InitialLine.PadRight(80, Chr(32))
      thisTicker = InitialLine.Substring(3, ExchangeOffset - 2).Trim
      thisExchange = InitialLine.Substring(ExchangeOffset, 4).Trim
      If Not (IsNumeric(InitialLine.Substring(ExchangeOffset + 4, 10).Trim)) Then
        thisPosition = 0
      Else
        thisPosition = CDbl(InitialLine.Substring(ExchangeOffset + 4, 10).Trim)
      End If

      Dim SourceOffset As Integer
      SourceOffset = -1
      SourceOffset = InitialLine.LastIndexOf("EXCH ")
      If (SourceOffset < ExchangeOffset) Then
        SourceOffset = -1
      End If

      If (SourceOffset < 0) Then
        SourceOffset = InitialLine.LastIndexOf("TRAC ")

        If (SourceOffset < ExchangeOffset) Then
          SourceOffset = -1
        End If
      End If

      If (SourceOffset < 0) Then
        SourceOffset = InitialLine.LastIndexOf("BGN ")

        If (SourceOffset < ExchangeOffset) Then
          SourceOffset = -1
        End If
      End If

      If (SourceOffset < 0) Then
        SourceOffset = 50
      End If

      thisPriceSource = InitialLine.Substring(SourceOffset, 4).Trim

      thisPortfolioItemType = RenaissanceGlobals.PortfolioItemType.MarketInstrument
      thisMarketInstrumentType = RenaissanceGlobals.DataProviderInstrumentType.Corp

      '' Somtimes the Ticker area seems to contain the first letter of the exchange code.
      '' Try to avoid this.
      'If thisTicker.Length = 16 Then
      '  If thisTicker.Substring(12, 3) = "   " Then
      '    thisTicker = thisTicker.Substring(0, 15).Trim
      '  End If
      'End If

      ' Establish Correct Instrument Type and Market Instrument Type

      Select Case thisPriceSource.ToUpper
        Case "EXCH", "BGN"
          If thisTicker.IndexOfAny(New Char() {CChar("%")}) >= 0 Then
            ' Adjust Ticker Values for 'Preferred' Instruments.
            ' A pain in the Ass.

            thisTicker = thisTicker.Substring(0, InStr(thisTicker, "%") - 1)

            thisMarketInstrumentType = RenaissanceGlobals.DataProviderInstrumentType.Pfd
          Else
            If thisTicker.Trim.EndsWith(" " & thisExchange.Trim) Then
              thisMarketInstrumentType = RenaissanceGlobals.DataProviderInstrumentType.Equity
            End If
          End If

        Case "TRAC"
          ' Assumed to be Fixed Income.

        Case Else

      End Select

      ' Add Market Instrument Tag to the Ticker

      Select Case thisMarketInstrumentType
        Case RenaissanceGlobals.DataProviderInstrumentType.M_Mkt
          thisTicker &= " M-Mkt"

        Case Else
          thisTicker &= " " & System.Enum.GetName(GetType(RenaissanceGlobals.DataProviderInstrumentType), thisMarketInstrumentType)

      End Select

      If Not (IsNumeric(InitialLine.Substring(SourceOffset - 17, 16).Trim)) Then
        thisPrice = 0
      Else
        thisPrice = CDbl(InitialLine.Substring(SourceOffset - 17, 16).Trim)
      End If

      Me.Ticker = thisTicker
      Me.Exchange = thisExchange
      Me.Position = thisPosition
      Me.Price = thisPrice
      Me.PriceSource = thisPriceSource
      Me.MarketInstrumentType = thisMarketInstrumentType
      Me.PortfolioItemType = thisPortfolioItemType

    End Sub
  End Class

#End Region

#Region " GetNaplesConnection()"

  Private Function GetNaplesConnection() As SqlConnection
    ' *****************************************************************************************************
    ' Returns a pooled connection top the Naples database.
    '
    '
    ' *****************************************************************************************************
    Dim ConnectString As String

    ConnectString = "SERVER=" & NaplesInitiator.SQLServerName & ";DATABASE=" & NaplesInitiator.SQLDatabaseName & ";User ID=Naples;Password=Naples;Trusted_Connection=False;Pooling=true;Min Pool Size=1;Max Pool Size=100"
    Dim RVal As New SqlConnection(ConnectString)

    Try
      If (RVal.State And ConnectionState.Open) <> ConnectionState.Open Then
        RVal.Open()
      End If

      Return RVal

    Catch ex As Exception
      Try
        RVal.Close()
      Catch Inner_Ex As Exception
      End Try
    End Try

    Return Nothing


    '    ConnectString = "SERVER=" & NaplesInitiator.SQLServerName & ";DATABASE=" & NaplesInitiator.SQLDatabaseName & ";User ID=Naples;Password=Naples;Trusted_Connection=False"

    'Dim thisConnection As SqlConnection = Nothing
    'qqq()
    'Try
    '  thisConnection = thisDataHandler.Get_Connection(NAPLES_CONNECTION)
    '  If thisConnection Is Nothing Then
    '    Dim ConnectString As String
    '    ConnectString = "SERVER=" & NaplesInitiator.SQLServerName & ";DATABASE=" & NaplesInitiator.SQLDatabaseName & ";User ID=Naples;Password=Naples;Trusted_Connection=False"
    '    thisConnection = thisDataHandler.Add_Connection(NAPLES_CONNECTION, ConnectString)
    '  End If

    '  Try
    '    If (thisConnection.State <> ConnectionState.Open) Then
    '      thisConnection.Open()
    '    End If
    '  Catch ex As Exception
    '  End Try
    '  Return thisConnection
    'Catch ex As Exception
    '  Try
    '    If (thisConnection IsNot Nothing) Then
    '      thisConnection.Close()
    '    End If
    '  Catch Inner_Ex As Exception
    '  End Try
    'End Try

    'Return Nothing
  End Function

  Private Function GetNewNaplesConnection() As SqlConnection

    Try
      Dim ConnectString As String
      ConnectString = "SERVER=" & NaplesInitiator.SQLServerName & ";DATABASE=" & NaplesInitiator.SQLDatabaseName & ";User ID=Naples;Password=Naples;Trusted_Connection=False"

      Dim RVal As New SqlConnection(ConnectString)

      RVal.Open()

      Return RVal
    Catch ex As Exception
    End Try

    Return Nothing
  End Function
#End Region

#Region " Error Logging "

  ' *****************************************************************************************************
  '
  '
  '
  ' *****************************************************************************************************

  '     ByVal pDebugLevel As NaplesGlobals.DebugLevels, _
  ' DebugLevels.Minimal

  Friend Function LogError( _
    ByVal pDebugLevel As DebugLevels, _
    ByVal p_Error_Location As String, _
    ByVal p_System_Error_Number As Integer, _
    ByVal p_System_Error_Description As String, _
    Optional ByVal p_Error_Message As String = "", _
    Optional ByVal p_Error_StackTrace As String = "", _
    Optional ByVal p_Log_to As Integer = 0) As Integer

    '**************************************************************************************************
    ' Public function returning public variables
    '
    ' Purpose:  Save Error messages to Database / File if required
    '
    ' Accepts:  p_Error_Location as String            : Location of Error (for debugging and logging purposes)
    '           p_System_Error_Number As Integer      : System 'Err'
    '           p_System_Error_Description As String  : System 'Error'
    '           p_Error_Message as String (Optional)  : Meaningful User Error message
    '           p_Error_StackTrace as String (Optional)  : StackTrace Property of Error message
    '           p_Log_to As Integer (Optional)        : Write log conditions
    '
    ' Returns:  0 or error number on fail, only returns an error if logged to File / Table and the update fails
    '**************************************************************************************************


    ' Log to DB, if possible.

    Dim InsertCommand As New SqlCommand
    Dim myConnection As SqlConnection = Nothing

		Try
			If CInt(pDebugLevel) > CInt(NaplesInitiator.DebugLevel) Then
				Return 0
			End If
		Catch ex As Exception
		End Try

    Try
      myConnection = GetNewNaplesConnection()

      If (myConnection IsNot Nothing) Then
        InsertCommand.CommandText = "[adp_tblNaplesErrorLog_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = myConnection
        InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@WindowsUserName", System.Data.SqlDbType.NVarChar, 100))
        InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ErrorNumber", System.Data.SqlDbType.Int, 4))
        InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ErrorLocation", System.Data.SqlDbType.NVarChar, 250))
        InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SystemErrorDescription", System.Data.SqlDbType.NVarChar, 250))
        InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UserErrorDescription", System.Data.SqlDbType.NVarChar, 450))
        InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CallStack", System.Data.SqlDbType.Text))

        InsertCommand.Parameters("@WindowsUserName").Value = Environment.UserName
        InsertCommand.Parameters("@ErrorNumber").Value = p_System_Error_Number
        InsertCommand.Parameters("@ErrorLocation").Value = p_Error_Location
        InsertCommand.Parameters("@SystemErrorDescription").Value = p_System_Error_Description
        InsertCommand.Parameters("@UserErrorDescription").Value = p_Error_Message
        InsertCommand.Parameters("@CallStack").Value = p_Error_StackTrace

        SyncLock myConnection
          InsertCommand.ExecuteNonQuery()
        End SyncLock
      End If

      Return 0

    Catch ex As Exception
    Finally
      Try
        If (myConnection IsNot Nothing) Then
          myConnection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

  End Function

#End Region

#Region " Application Update Event Handling"

  Private Sub ApplicationAutoUpdate(ByVal sender As System.Object, ByVal e As TCP_Client.FCPApplicationUpdateEventArgs) ' Handles Naples_TCPClient.FCPApplicationUpdate
    ' **************************************************************************************
    ' 
    ' **************************************************************************************

    Select Case e.MessageHeader.MessageType

      Case TCPServer_Common.TCP_ServerMessageType.Server_ApplicationUpdateMessage, TCPServer_Common.TCP_ServerMessageType.Client_ApplicationUpdateMessage

        ' **********************************************************************
        ' If this is an Update Message....
        ' **********************************************************************

        If (e.MessageHeader.Application And TCPServer_Common.FCP_Application.Naples) <> TCPServer_Common.FCP_Application.None Then
          ' **********************************************************************
          '
          ' **********************************************************************





        End If

        If (e.MessageHeader.Application And (TCPServer_Common.FCP_Application.Renaissance Or TCPServer_Common.FCP_Application.Venice)) <> TCPServer_Common.FCP_Application.None Then
          ' **********************************************************************
          ' And it is for Venice...
          ' **********************************************************************

          If (e.MessageHeader.FollowingMessageCount > 0) AndAlso _
            (Not (e.FollowingObjects Is Nothing)) AndAlso _
            (e.FollowingObjects.Length > 0) AndAlso _
            (e.FollowingObjects(0).GetType Is GetType(RenaissanceGlobals.RenaissanceUpdateClass)) Then

            ' **********************************************************************
            ' Multiple Updates have been sent as in a 'RenaissanceUpdateClass' class
            ' **********************************************************************

            ' Dim thisVeniceUpdateEventArgs As New RenaissanceGlobals.VeniceUpdateEventArgs
            Dim thisRenaissanceUpdateClass As New RenaissanceGlobals.RenaissanceUpdateClass
            Dim thisChangeID As RenaissanceGlobals.RenaissanceChangeID

            ' Construct the local UpdateEvent Argument class

            thisRenaissanceUpdateClass = CType(e.FollowingObjects(0), RenaissanceGlobals.RenaissanceUpdateClass)

            For Each thisChangeID In thisRenaissanceUpdateClass.ChangeIDs

              ' Don't propogate Knowledgedate or DBConnection changes from other applications.

              If (thisChangeID <> RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) And (thisChangeID <> RenaissanceGlobals.RenaissanceChangeID.Connection) Then

                ' ReLoad the related dataset
                Call ReloadTable(thisChangeID, False)

              End If

              ' Specific Updates
              Call SpecificUpdates(thisChangeID)

            Next

          Else

            ' **********************************************************************
            ' Only a single Update has been set, encoded in the MessageHeader String
            ' The Message string may either be an Integer relating to the RenaissanceChangeID Enum,
            ' OR the text representation of the Enum value, e.g. 'tblFund'.
            ' **********************************************************************

            Dim ChangeID As RenaissanceGlobals.RenaissanceChangeID    ' RenaissanceGlobals.RenaissanceChangeID

            If IsNumeric(e.MessageHeader.MessageString) Then
              ChangeID = CType(CInt(e.MessageHeader.MessageString), RenaissanceGlobals.RenaissanceChangeID)
            Else
              ChangeID = System.Enum.Parse(GetType(RenaissanceGlobals.RenaissanceChangeID), e.MessageHeader.MessageString)
            End If

            If (ChangeID > 0) And (ChangeID <> RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) Then
              Try
                ' **********************************************************************
                ' ReLoad the related dataset
                ' Trigger the local Changed Event
                '
                ' This code now assumes that the ChangeID given in this way might be a bitmap, thus
                ' it checks against every item in the RenaissanceChangeID enumeration.
                ' **********************************************************************

                ' If a tblTransaction update is received, then reload the SelectTransactions 'Table'.
                ' This is because this table is never reloaded anywhere else - It is not edited directly
                ' and there should not be any updates received for this table directly.

                Call ReloadTable(ChangeID, True)

                ' Raise Event.

                Call SpecificUpdates(ChangeID)

              Catch ex As Exception
              End Try
            End If

          End If

        Else

        End If


      Case TCPServer_Common.TCP_ServerMessageType.Server_NaplesDataAnswer, TCPServer_Common.TCP_ServerMessageType.Client_NaplesDataAnswer
        ' **********************************************************************
        '
        ' **********************************************************************




      Case Else

    End Select

  End Sub

  Private Sub SpecificUpdates(ByVal pChangeID As RenaissanceGlobals.RenaissanceChangeID)

    Try

      Select Case pChangeID

        Case RenaissanceGlobals.RenaissanceChangeID.tblDataTask
          ' Update Aggregator Task collection

          If Not (NaplesInitiator.Naples_Aggregator Is Nothing) Then
            NaplesInitiator.Naples_Aggregator.UpdateDataTaskCollection()
          End If

				Case RenaissanceChangeID.tblPortfolioData

					If Not (NaplesInitiator.Naples_Aggregator Is Nothing) Then
						NaplesInitiator.Naples_Aggregator.VerifyAllPortfolioDataRequests()
					End If

			End Select

    Catch ex As Exception
    End Try

  End Sub

#End Region

#Region " Establish Standard table datasets "

  Public Function Load_Table(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset) As DataSet
    ' Get Dataset, forcing a refresh if Noted IDs have changed.

    Return Load_Table(pDatasetDetails, pDatasetDetails.NotedIDsHaveChanged, True)
  End Function

  Public Function Load_Table(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset, ByVal pForceRefresh As Boolean) As DataSet
    Return Load_Table(pDatasetDetails, pForceRefresh, True)
  End Function

  Public Function Load_Table(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset, ByVal pForceRefresh As Boolean, ByVal pRaiseEvent As Boolean) As DataSet
    ' **********************************************************************
    ' Routine to Load a standard dataset or retrieve an existing one if it already
    ' exists.
    ' **********************************************************************

    Dim myDataset As DataSet
    Dim myConnection As SqlConnection = Nothing
    Dim myAdaptor As SqlDataAdapter

    If (pDatasetDetails Is Nothing) Then
      Return Nothing
    End If

    Dim THIS_TABLENAME As String = pDatasetDetails.TableName
    Dim THIS_ADAPTORNAME As String = pDatasetDetails.Adaptorname
    Dim THIS_DATASETNAME As String = pDatasetDetails.DatasetName

    ' StandardDatasetNames
    ' Get / Establish DataHandler cached connection

    Try
      myConnection = thisDataHandler.Get_Connection(NAPLES_CONNECTION)
      If myConnection Is Nothing Then
        Dim ConnectString As String
        ConnectString = "SERVER=" & NaplesInitiator.SQLServerName & ";DATABASE=" & NaplesInitiator.SQLDatabaseName & ";User ID=Naples;Password=Naples;Trusted_Connection=False"
        myConnection = thisDataHandler.Add_Connection(NAPLES_CONNECTION, ConnectString)
      End If

      Try
        If (myConnection.State <> ConnectionState.Open) Then
          myConnection.Open()
        End If
      Catch ex As Exception
      End Try
    Catch ex As Exception
      Try
        If (myConnection IsNot Nothing) Then
          myConnection.Close()
        End If
      Catch Inner_Ex As Exception
      End Try
      myConnection = Nothing
    End Try

    If myConnection Is Nothing Then
      Return Nothing
    End If

    myAdaptor = thisDataHandler.Get_Adaptor(THIS_ADAPTORNAME, NAPLES_CONNECTION, THIS_TABLENAME)
    myDataset = thisDataHandler.Get_Dataset(THIS_DATASETNAME)

    'AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
    'AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
    'AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    If myDataset Is Nothing Then

      myDataset = thisDataHandler.Get_Dataset(THIS_DATASETNAME, True)

      SyncLock myConnection
        SyncLock myDataset

          Try
            If (myDataset.Tables.Count > 0) Then
              myDataset.Tables(0).Clear()
            End If
          Catch ex As Exception
          End Try
          Try
            myAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = KNOWLEDGEDATE_NOW
          Catch ex As Exception
          End Try

          Try
            If (myDataset.Tables.Count > 0) Then
              myAdaptor.Fill(myDataset.Tables(0))
            Else
              myAdaptor.Fill(myDataset, pDatasetDetails.TableName)
            End If
          Catch ex As Exception
            If (myDataset.Tables.Count > 0) Then
              Call LogError(DebugLevels.Minimal, "DataBaseClient, Load_Table", LOG_LEVELS.Error, ex.Message, "Error Filling Table " & myDataset.Tables(0).TableName, ex.StackTrace)
            Else
              Call LogError(DebugLevels.Minimal, "DataBaseClient, Load_Table", LOG_LEVELS.Error, ex.Message, "Error Filling Table ", ex.StackTrace)
            End If
          End Try

        End SyncLock
      End SyncLock

      ' Reset the IDs have Changed Flag, as the table has been re-loaded.
      pDatasetDetails.NotedIDsHaveChanged = False

    ElseIf (pForceRefresh = True) Or (pDatasetDetails.NotedIDsHaveChanged = True) Then

      SyncLock myConnection
        SyncLock myDataset

          Try
            Try
              If (myDataset.Tables.Count > 0) Then
                myDataset.Tables(0).Clear()
              End If
            Catch ex As Exception
            End Try

            Try
              myAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = KNOWLEDGEDATE_NOW
            Catch ex As Exception
            End Try

            If (myDataset.Tables.Count > 0) Then
              myAdaptor.Fill(myDataset.Tables(0))
            Else
              myAdaptor.Fill(myDataset, pDatasetDetails.TableName)
            End If

          Catch ex As Exception
            Call LogError(DebugLevels.Medium, "DataBaseClient, Load_Table", LOG_LEVELS.Error, ex.Message, "Error Filling (Refresh) Table " & myDataset.Tables(0).TableName, ex.StackTrace)
          End Try

        End SyncLock
      End SyncLock

      ' Reset the IDs have Changed Flag, as the table has been re-loaded.
      pDatasetDetails.NotedIDsHaveChanged = False
    End If

    Return myDataset
  End Function

  Public Function ReloadTable(ByVal pTableID As RenaissanceGlobals.RenaissanceChangeID, ByVal pRaiseEvent As Boolean) As Boolean
    ' **********************************************************************
    ' Function to Re-Load a standard dataset if it is already loaded.
    '
    ' If the dataset is not already loaded, then the call to 'Get_Dataset' will return
    ' Nothing since insufficient of the constructor parameters are given.
    ' If a dataset IS returned, then the Load_Dataset funcion is used to refresh it.
    ' **********************************************************************
    Dim ThisStandardDataset As RenaissanceGlobals.StandardDataset = RenaissanceGlobals.RenaissanceStandardDatasets.GetStandardDataset(pTableID)
    Dim myDataset As DataSet

    Try
      If Not (ThisStandardDataset Is Nothing) Then
        myDataset = thisDataHandler.Get_Dataset(ThisStandardDataset.DatasetName)
        If (Not (myDataset Is Nothing)) Then
          If (Load_Table(ThisStandardDataset, True, pRaiseEvent) Is Nothing) Then
            Return False
          Else
            Return True
          End If
        End If
      End If
    Catch ex As Exception
    End Try

    Return False
  End Function

  Public Function Get_SeparateTable(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset) As DataTable
    Dim myDataset As DataSet = Nothing

    myDataset = Get_SeparateDataSet(pDatasetDetails)

    If (myDataset IsNot Nothing) Then
      Return (myDataset.Tables(0))
    End If

    Return Nothing

  End Function

  Public Function Get_SeparateDataSet(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset) As DataSet
    ' **********************************************************************
    ' Routine to Load a fresh copy of a standard table
    ' **********************************************************************

    Dim myDataset As DataSet = Nothing
    Dim myConnection As SqlConnection = Nothing
    Dim myAdaptor As New SqlDataAdapter

    If (pDatasetDetails Is Nothing) Then
      Return Nothing
    End If

    Dim THIS_TABLENAME As String = pDatasetDetails.TableName
    Dim THIS_ADAPTORNAME As String = pDatasetDetails.Adaptorname
    Dim THIS_DATASETNAME As String = pDatasetDetails.DatasetName

    ' StandardDatasetNames

    Try
      myConnection = GetNewNaplesConnection()

      myDataset = thisDataHandler.Instantiate_Dataset(THIS_DATASETNAME)

      thisAdaptorHandler.Set_AdaptorCommands(myConnection, myAdaptor, THIS_TABLENAME)

      Try
        myAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = KNOWLEDGEDATE_NOW
      Catch ex As Exception
      End Try

      Try
        If (myDataset.Tables.Count > 0) Then
          myAdaptor.Fill(myDataset.Tables(0))
        Else
          myAdaptor.Fill(myDataset, THIS_TABLENAME)
        End If
      Catch ex As Exception
        If (myDataset.Tables.Count > 0) Then
          Call LogError(DebugLevels.Minimal, "DataBaseClient, Get_SeparateTable", LOG_LEVELS.Error, ex.Message, "Error Filling Table " & myDataset.Tables(0).TableName, ex.StackTrace)
        Else
          Call LogError(DebugLevels.Minimal, "DataBaseClient, Get_SeparateTable", LOG_LEVELS.Error, ex.Message, "Error Filling Table ", ex.StackTrace)
        End If
      End Try

    Catch ex As Exception

    Finally
      Try
        If (myConnection IsNot Nothing) Then
          myConnection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    If (myDataset Is Nothing) Then
      Return Nothing
    End If

    If (myDataset.Tables.Count > 0) Then
      Return myDataset
    End If

    Return Nothing

  End Function

#End Region

#Region " AdaptorUpdate"

  Public Function AdaptorUpdate(ByRef pAdaptor As SqlDataAdapter, ByRef pTable As DataTable) As Integer
    Return thisDataHandler.AdaptorUpdate(pAdaptor, pTable)
  End Function

  Public Function AdaptorUpdate(ByRef pAdaptor As SqlDataAdapter, ByRef pDataSet As DataSet) As Integer
    Return thisDataHandler.AdaptorUpdate(pAdaptor, pDataSet)
  End Function

  Public Function AdaptorUpdate(ByRef pAdaptor As SqlDataAdapter, ByRef pDataRows() As DataRow) As Integer
    Return thisDataHandler.AdaptorUpdate(pAdaptor, pDataRows)
  End Function

  Public Function AdaptorUpdate(ByRef pAdaptor As SqlDataAdapter, ByRef pDataSet As DataSet, ByVal pTableName As String) As Integer
    Return thisDataHandler.AdaptorUpdate(pAdaptor, pDataSet, pTableName)
  End Function

#End Region

#Region " Process DB requests"

  ' *******************************************************************
  ' Post New DataServer Request
  ' *******************************************************************
  Public Sub PostNewRequest(ByRef pDBTask As DBTask)

    If (pDBTask Is Nothing) Then Exit Sub
    If (pDBTask.DataObject Is Nothing) Then Exit Sub

    SyncLock NewRequests
      NewRequests.Enqueue(pDBTask)
    End SyncLock
  End Sub

  Private Sub RunDBClient()
    ' ************************************************************
    ' Main Work Loop.
    '
    ' Principle task is to process new DB tasks.
    '
    ' ************************************************************

    Dim LoopWorkDone As Boolean
    Dim thisDBTask As DBTask

    ' Work loop.
    While (_CloseDownFlag = False)
      LoopWorkDone = False

      ' ***************************************
      ' Check For new Server Requests
      ' ***************************************

      ' retrieve next request

      If NewRequests.Count > 0 Then
        ' ***************************************
        ' BBG Service Seems OK. Continue ....
        ' ***************************************

        LoopWorkDone = True
        ProcessingRequest.AcquireWriterLock(Timeout.Infinite)

        ' ***************************************
        ' Retrieve Request.
        ' ***************************************

        Try
          SyncLock NewRequests
            thisDBTask = CType(NewRequests.Dequeue, DBTask)
          End SyncLock

        Catch ex As Exception
          ' ***************************************
          ' In the event of an error build a dummy task to fall through the loop.
          ' ***************************************

          thisDBTask = Nothing
        End Try


        ' ***************************************
        ' Process request....
        '
        ' For each Request Type .....
        ' ***************************************
        If Not (thisDBTask Is Nothing) Then
          Dim thisRequestType As DataTaskType
          thisRequestType = NaplesGlobals.DataTaskType.NoTask

          If (thisDBTask.DataRequestType <> NaplesGlobals.DataTaskType.NoTask) Then
            thisRequestType = thisDBTask.DataRequestType
          ElseIf (Not (thisDBTask.AggregatorTask Is Nothing)) Then
            thisRequestType = thisDBTask.AggregatorTask.TaskType
          End If


          Select Case thisRequestType
            Case NaplesGlobals.DataTaskType.PortfolioCapture

              ProcessPortfolioUpdate(thisDBTask)

            Case NaplesGlobals.DataTaskType.PageCapture


            Case NaplesGlobals.DataTaskType.RealtimeSecurityCapture, NaplesGlobals.DataTaskType.PeriodicSecurityCapture


            Case NaplesGlobals.DataTaskType.NoTask
              ' Special Case OR Nothing.


            Case Else


          End Select

        End If

      End If ' If NewRequests.Count > 0

      If ProcessingRequest.IsWriterLockHeld Then
        ProcessingRequest.ReleaseLock()
      End If

      If (LoopWorkDone = False) Then
        Threading.Thread.Sleep(25)
      End If
    End While
  End Sub



  ' *******************************************************************
  ' *******************************************************************
  '
  ' Data Update Routines
  '
  ' *******************************************************************
  ' *******************************************************************



  ' *******************************************************************
  ' Process Portfolio Update
  '
  ' *******************************************************************
  Public Sub ProcessPortfolioUpdate(ByVal thisDBTask As DBTask)
    Dim thisConnection As SqlConnection = Nothing
    Dim InitialPortfolioText As String
    Dim PortfolioStringArray As String()
    Dim PortfolioDetails As New PortfolioDetailsArrayList

    Dim DetailsCounter As Integer
    Dim DataCounter As Integer

    Dim PortfolioTicker As String
    Dim PortfolioFilingDate As Date
    Dim PricingSettlementDate As Date

    Dim thisPortfolioData As PortfolioData
    ' Dim tempPortfolioData As PortfolioData
    Dim LineCounter As Integer

    ' Dim tempStr As String

    Dim MissingTickers As New ArrayList
    Dim TickerCount As Integer

    ' Overarching Error trap.
    Try

      thisConnection = GetNewNaplesConnection()
      If (thisConnection Is Nothing) Then
        Exit Sub
      End If

      If Not (TypeOf (thisDBTask.DataObject) Is String) Then
        Exit Sub
      End If
      InitialPortfolioText = CType(thisDBTask.DataObject, String)

      ' *********************************************************
      ' First Parse the Portfolio Text
      ' *********************************************************

      If Not (thisDBTask.AggregatorTask Is Nothing) Then
        PortfolioTicker = thisDBTask.AggregatorTask.Portfolio
      Else
        PortfolioTicker = ""
      End If
      PortfolioFilingDate = #1/1/1900#
      PricingSettlementDate = Now.Date

      Try

        PortfolioStringArray = InitialPortfolioText.Split(New Char() {Chr(10)})
        If (PortfolioStringArray Is Nothing) OrElse (PortfolioStringArray.Length <= 0) Then
          Exit Sub
        End If

        For LineCounter = 0 To (PortfolioStringArray.Length - 1)

          ' Parse Portfolio Filing Date.
          If (PortfolioFilingDate = #1/1/1900#) AndAlso (InStr(PortfolioStringArray(LineCounter), "Filing Date") > 0) Then
            Dim DateString As String
            Try
              DateString = PortfolioStringArray(LineCounter).Substring(InStr(PortfolioStringArray(LineCounter), "Filing Date") + 10)
              If DateString.Length >= 5 Then
                Dim Day As Integer
                Dim Month As Integer
                Dim Year As Integer
                Dim SlashIndex As Integer

                SlashIndex = DateString.IndexOf("/")
                If SlashIndex > 0 Then
                  Month = CInt(DateString.Substring(0, SlashIndex))
                  DateString = DateString.Substring(SlashIndex + 1)
                End If
                SlashIndex = DateString.IndexOf("/")
                If SlashIndex > 0 Then
                  Day = CInt(DateString.Substring(0, SlashIndex))
                  DateString = DateString.Substring(SlashIndex + 1)
                End If
                Year = CInt(DateString) + 2000

                PortfolioFilingDate = New Date(Year, Month, Day)
              End If
            Catch ex As Exception
            End Try
          End If

          ' Parse Portfolio Ticker.
          If (PortfolioTicker = "") AndAlso (InStr(PortfolioStringArray(LineCounter), "Port:") > 0) Then
            Try
              PortfolioTicker = PortfolioStringArray(LineCounter).Substring(InStr(PortfolioStringArray(LineCounter), "Port:") + 4).Trim
            Catch ex As Exception
            End Try
          End If

          If (InStr(PortfolioStringArray(LineCounter), " Filing Date") > 0) Then
            Try
              Dim TempDateString As String
              TempDateString = PortfolioStringArray(LineCounter).Substring(InStr(PortfolioStringArray(LineCounter), " Settle On") + 9).Trim

              PricingSettlementDate = New Date(CInt("20" & TempDateString.Substring(6, 2)), CInt(TempDateString.Substring(0, 2)), CInt(TempDateString.Substring(3, 2)))

            Catch ex As Exception
            End Try
          End If

          ' Parse Portfolio Data
          thisPortfolioData = New PortfolioData(PortfolioStringArray(LineCounter))


          If thisPortfolioData.Ticker.Length > 0 Then
            PortfolioDetails.Add(thisPortfolioData)
          End If
        Next

        ' ***************************************************************
        ' Reject any updates without a valid Filing Date
        ' ***************************************************************
        If (PortfolioFilingDate <= #1/1/1900#) Then
          Exit Sub
        End If

        ' ***************************************************************
        ' OK, now Check the Market Instruments Exist then Save Price.
        ' ***************************************************************

        ' Load tblMarketInstruments

				Dim MktInstrumentsDS As New RenaissanceDataClass.DSMarketInstruments
				Dim MktInstrumentsTbl As RenaissanceDataClass.DSMarketInstruments.tblMarketInstrumentsDataTable
				Dim MktInstrumentsRow As RenaissanceDataClass.DSMarketInstruments.tblMarketInstrumentsRow
        Dim MktInstrumentsAdaptor As SqlDataAdapter
        Dim MktInstrumentsUpdateFlag As Boolean

				' Dim MktPricesDS As New RenaissanceDataClass.DSMarketPrices
				' Dim MktPricesTbl As RenaissanceDataClass.DSMarketPrices.tblMarketPricesDataTable
				Dim MktPricesRow As RenaissanceDataClass.DSMarketPrices.tblMarketPricesRow
        Dim MktPricesAdaptor As SqlDataAdapter
        Dim MktPricesUpdateFlag As Boolean

				Dim TmpMktPricesDS As New RenaissanceDataClass.DSMarketPrices
				Dim TmpMktPricesTbl As RenaissanceDataClass.DSMarketPrices.tblMarketPricesDataTable
        TmpMktPricesTbl = TmpMktPricesDS.tblMarketPrices

				Dim SelectedMktInstrumentRows() As RenaissanceDataClass.DSMarketInstruments.tblMarketInstrumentsRow

        MktInstrumentsUpdateFlag = False
        MktPricesUpdateFlag = False
        MktInstrumentsTbl = Nothing

        Try

          ' thisAdaptorHandler is a global object.

          MktInstrumentsDS = Me.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblMarketInstruments, True)
          MktInstrumentsAdaptor = thisDataHandler.Get_Adaptor(RenaissanceGlobals.RenaissanceStandardDatasets.tblMarketInstruments.Adaptorname, NAPLES_CONNECTION, RenaissanceGlobals.RenaissanceStandardDatasets.tblMarketInstruments.TableName)
          MktInstrumentsAdaptor.InsertCommand.Parameters("@KnowledgeDate").Value = KNOWLEDGEDATE_NOW
          MktInstrumentsAdaptor.UpdateCommand.Parameters("@KnowledgeDate").Value = KNOWLEDGEDATE_NOW

					' MktPricesDS = Me.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblMarketPrices)
          MktPricesAdaptor = thisDataHandler.Get_Adaptor(RenaissanceGlobals.RenaissanceStandardDatasets.tblMarketPrices.Adaptorname, NAPLES_CONNECTION, RenaissanceGlobals.RenaissanceStandardDatasets.tblMarketPrices.TableName)
          MktPricesAdaptor.InsertCommand.Parameters("@KnowledgeDate").Value = KNOWLEDGEDATE_NOW
          MktPricesAdaptor.UpdateCommand.Parameters("@KnowledgeDate").Value = KNOWLEDGEDATE_NOW

          MktInstrumentsTbl = Nothing
					' MktPricesTbl = Nothing

          If (Not (MktInstrumentsDS.tblMarketInstruments Is Nothing)) Then

            MktInstrumentsTbl = MktInstrumentsDS.tblMarketInstruments
						' MktPricesTbl = MktPricesDS.tblMarketPrices

            ' Check Existence

            For DetailsCounter = 0 To (PortfolioDetails.Count - 1)
              ' Check Instrument Exists
              MktInstrumentsRow = Nothing

              Try
                thisPortfolioData = PortfolioDetails(DetailsCounter)

                SelectedMktInstrumentRows = MktInstrumentsTbl.Select("Ticker='" & thisPortfolioData.Ticker & "'")

                If (SelectedMktInstrumentRows Is Nothing) OrElse (SelectedMktInstrumentRows.Length <= 0) Then
                  ' Add Instrument
                  MktInstrumentsRow = MktInstrumentsTbl.NewtblMarketInstrumentsRow

                  MktInstrumentsRow.MarketInstrumentID = 0
                  MktInstrumentsRow.Description = thisPortfolioData.Ticker & " <To Update>"
                  MktInstrumentsRow.InstrumentType = thisPortfolioData.MarketInstrumentType
                  MktInstrumentsRow.Ticker = thisPortfolioData.Ticker

                  MktInstrumentsTbl.Rows.Add(MktInstrumentsRow)

                  ' Update Table
                  ' MktInstrumentsAdaptor.Update(MktInstrumentsTbl)
                  AdaptorUpdate(MktInstrumentsAdaptor, MktInstrumentsTbl)

                  MktInstrumentsUpdateFlag = True

                  ' Set up DataRequest to get Instrument Name.

                  MissingTickers.Add(thisPortfolioData.Ticker)

                Else
                  MktInstrumentsRow = SelectedMktInstrumentRows(0)
                End If
              Catch ex As Exception
                LogError(DebugLevels.Minimal, "DataBaseClient, ProcessPortfolioUpdate, CheckMktInstruments", LOG_LEVELS.Error, ex.Message, "Error Checking or Adding Market Instrument", ex.StackTrace)
                MktInstrumentsRow = Nothing
                thisPortfolioData = Nothing
              End Try

              Try
                If (Not (thisPortfolioData Is Nothing)) Then
                  thisPortfolioData.MarketInstrumentID = MktInstrumentsRow.MarketInstrumentID
                End If
              Catch ex As Exception
              End Try

              ' Save Price
              '
              ' Rather than check in .NET Code if an exact Price match exists for this Price
              ' All Market Prices are added as new Prices and the SQL Insert stored procedure
              ' checks for duplicates. This was done for simplicity at this level and because the
              ' Insert Code has to make this check anyway !
              ' If an existing Price exists, then the Existing Row is returned by the SP, this
              ' was resulting in Duplicate RN Errors being returned from the Table object.
              ' Thus prices are now added without adding the Row to the MktPricesTbl table, and 
              ' the MktPricesTbl Table is re-loaded at the end.

              Try
                If Not (MktInstrumentsRow Is Nothing) AndAlso (Not (thisPortfolioData Is Nothing)) Then
                  MktPricesRow = TmpMktPricesTbl.NewtblMarketPricesRow

                  MktPricesRow.MarketInstrumentID = MktInstrumentsRow.MarketInstrumentID
                  MktPricesRow.PriceDataType = 0 ' LAST
                  MktPricesRow.PriceDate = PricingSettlementDate
                  MktPricesRow.PriceValue = thisPortfolioData.Price
                  MktPricesRow.TextValue = ""

                  ' MktPricesTbl.Rows.Add(MktPricesRow)
                  ' MktPricesAdaptor.Update(MktPricesTbl)
                  ' MktPricesAdaptor.Update(New DataRow() {MktPricesRow})
                  TmpMktPricesTbl.Rows.Add(MktPricesRow) ' Temporary Table is used as the Adaptor will not Update rows that do not belong to a Table.
                  AdaptorUpdate(MktPricesAdaptor, (New DataRow() {MktPricesRow}))

                  MktPricesUpdateFlag = True
                End If

              Catch ex As Exception
                LogError(DebugLevels.Minimal, "DataBaseClient, ProcessPortfolioUpdate, SavePrice", LOG_LEVELS.Error, ex.Message, "Error Saving Market price", ex.StackTrace)
              End Try

            Next

          End If

          If (MktInstrumentsUpdateFlag = True) Then
            ' Post Update Message
            MktInstrumentsDS = Me.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblMarketInstruments, True)

            If Not (NaplesInitiator.Naples_TCPClient Is Nothing) Then
              Try
                NaplesInitiator.Naples_TCPClient.PostApplicationUpdate((TCPServer_Common.FCP_Application.Renaissance Or TCPServer_Common.FCP_Application.Venice), RenaissanceGlobals.RenaissanceChangeID.tblMarketInstruments.ToString)
              Catch ex As Exception
              End Try
            End If
          End If
          If (MktPricesUpdateFlag = True) Then
            ' Refresh Market Prices DataSet
						Try
							Me.ReloadTable(RenaissanceChangeID.tblMarketPrices, False)
							' MktPricesDS = Me.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblMarketPrices, True)

							'If (Not (MktPricesDS Is Nothing)) Then
							'	MktPricesTbl = MktPricesDS.tblMarketPrices
							'  Else
							'	MktPricesTbl = Nothing
							' End If
						Catch ex As Exception
						End Try

            ' Post Update Message

            If Not (NaplesInitiator.Naples_TCPClient Is Nothing) Then
              Try
                NaplesInitiator.Naples_TCPClient.PostApplicationUpdate((TCPServer_Common.FCP_Application.Renaissance Or TCPServer_Common.FCP_Application.Venice), RenaissanceGlobals.RenaissanceChangeID.tblMarketPrices.ToString)
              Catch ex As Exception
              End Try
            End If
          End If

        Catch ex As Exception

        End Try


        ' *********************************************************
        ' Now we have details of the freshly retrieved portfolio, Lets see if it has changed.
        ' *********************************************************

        ' 1) Get Portfolio Index

				Dim IndexDS As New RenaissanceDataClass.DSPortfolioIndex
        Dim IndexAdaptor As New SqlDataAdapter
        Dim PortfolioID As Integer
        Dim PortfolioDataID As Integer

        Dim PortfolioIndexUpdateFlag As Boolean
        Dim PortfolioDataUpdateFlag As Boolean
        Dim thisMarketInstrumentID As Integer

        PortfolioIndexUpdateFlag = False
        PortfolioDataUpdateFlag = False

        ' thisAdaptorHandler is a global object.

        Call thisAdaptorHandler.Set_AdaptorCommands(thisDataHandler.Get_Connection(NAPLES_CONNECTION), IndexAdaptor, "tblPortfolioIndex")

        SyncLock IndexAdaptor.SelectCommand.Connection
          IndexAdaptor.Fill(IndexDS.tblPortfolioIndex)
        End SyncLock

        PortfolioID = (-1)

        If (Not (IndexDS.tblPortfolioIndex Is Nothing)) AndAlso (IndexDS.tblPortfolioIndex.Rows.Count > 0) Then
					Dim SelectedPortfolioRows() As RenaissanceDataClass.DSPortfolioIndex.tblPortfolioIndexRow

          SelectedPortfolioRows = IndexDS.tblPortfolioIndex.Select("(PortfolioTicker='" & PortfolioTicker & "') AND (PortfolioFilingDate='" & PortfolioFilingDate.ToString(QUERY_SHORTDATEFORMAT) & "')")

          If (SelectedPortfolioRows IsNot Nothing) AndAlso (SelectedPortfolioRows.Length > 0) Then
            Try
              PortfolioID = CInt(SelectedPortfolioRows(0)("PortfolioID"))
              PortfolioDataID = CInt(SelectedPortfolioRows(0)("PortfolioDataID"))
            Catch ex As Exception
            End Try
          End If
        End If

        ' If a Portfolio Index was not found, then add one.

				Dim PortfolioDataDS As New RenaissanceDataClass.DSPortfolioData
        Dim PortfolioDataAdaptor As New SqlDataAdapter
				Dim PortfolioDataSelectedRows() As RenaissanceDataClass.DSPortfolioData.tblPortfolioDataRow
				Dim PortfolioDataRow As RenaissanceDataClass.DSPortfolioData.tblPortfolioDataRow

        If (PortfolioID <= 0) Then
          ' Portfolio does not exist, Add a new one

					Dim NewIndexRow As RenaissanceDataClass.DSPortfolioIndex.tblPortfolioIndexRow

          Try
            NewIndexRow = IndexDS.tblPortfolioIndex.NewtblPortfolioIndexRow

            NewIndexRow.PortfolioTicker = PortfolioTicker
            NewIndexRow.PortfolioDescription = thisDBTask.AggregatorTask.TaskDescription
            NewIndexRow.PortfolioFilingDate = PortfolioFilingDate
            NewIndexRow.PortfolioType = RenaissanceGlobals.PortfolioType.SEC_Filing
            NewIndexRow.InstrumentID = thisDBTask.AggregatorTask.InstrumentID

            IndexDS.tblPortfolioIndex.Rows.Add(NewIndexRow)
            ' IndexAdaptor.Update(IndexDS)
            AdaptorUpdate(IndexAdaptor, IndexDS)

            PortfolioIndexUpdateFlag = True

            PortfolioID = NewIndexRow.PortfolioID
            PortfolioDataID = NewIndexRow.PortfolioDataID
          Catch ex As Exception
          End Try

          ' Now Add Portfolio Data

          Call thisAdaptorHandler.Set_AdaptorCommands(thisDataHandler.Get_Connection(NAPLES_CONNECTION), PortfolioDataAdaptor, "tblPortfolioData")

          For DetailsCounter = 0 To (PortfolioDetails.Count - 1)
            thisPortfolioData = CType(PortfolioDetails(DetailsCounter), PortfolioData)
            PortfolioDataRow = PortfolioDataDS.tblPortfolioData.NewtblPortfolioDataRow

            thisMarketInstrumentID = 0
            Try
              If Not (MktInstrumentsTbl Is Nothing) Then
                SelectedMktInstrumentRows = MktInstrumentsTbl.Select("Ticker='" & thisPortfolioData.Ticker & "'")
                If SelectedMktInstrumentRows.Length > 0 Then
                  thisMarketInstrumentID = SelectedMktInstrumentRows(0).MarketInstrumentID
                End If
              End If
            Catch ex As Exception
            End Try

            PortfolioDataRow.PortfolioDataID = PortfolioDataID
            PortfolioDataRow.PortfolioItemID = 0
            PortfolioDataRow.Ticker = thisPortfolioData.Ticker
            PortfolioDataRow.FilingDate = PortfolioFilingDate
            PortfolioDataRow.MarketInstrumentID = thisMarketInstrumentID

            PortfolioDataRow.PortfolioItemType = RenaissanceGlobals.PortfolioItemType.MarketInstrument

            PortfolioDataRow.Holding = thisPortfolioData.Position
            PortfolioDataRow.Weight = 0
            PortfolioDataRow.XMLData = ""

            PortfolioDataDS.tblPortfolioData.AddtblPortfolioDataRow(PortfolioDataRow)
          Next

          ' Commit Changes

          PortfolioDataAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = DBNull.Value
          PortfolioDataAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = DBNull.Value

          ' PortfolioDataAdaptor.Update(PortfolioDataDS.tblPortfolioData)
          AdaptorUpdate(PortfolioDataAdaptor, PortfolioDataDS.tblPortfolioData)

          PortfolioDataUpdateFlag = True

        Else
          ' Portfolio does exist, compare and update if necessary

          ' Retrieve Portfolio Data 

          Call thisAdaptorHandler.Set_AdaptorCommands(thisDataHandler.Get_Connection(NAPLES_CONNECTION), PortfolioDataAdaptor, "tblPortfolioData")
					Try
						If (PortfolioDataAdaptor.SelectCommand.Parameters.Contains("@PortfolioDataID") = False) Then
							PortfolioDataAdaptor.SelectCommand.Parameters.Add(New SqlParameter("@PortfolioDataID", System.Data.SqlDbType.Int, 4))
						End If
						PortfolioDataAdaptor.SelectCommand.Parameters("@PortfolioDataID").Value = PortfolioDataID
					Catch ex As Exception
					End Try
          SyncLock PortfolioDataAdaptor.SelectCommand.Connection
            PortfolioDataAdaptor.Fill(PortfolioDataDS.tblPortfolioData)
          End SyncLock

          'If PortfolioDataDS.tblPortfolioData.Rows.Count > 0 Then
          PortfolioDataSelectedRows = PortfolioDataDS.tblPortfolioData.Select("PortfolioDataID=" & PortfolioDataID.ToString)
          ' End If

          ' Compare Portfolio Details New BBG vs Stored DB

          Dim PortfoliosAreTheSame As Boolean

          PortfoliosAreTheSame = True

          ' Check Basic similarity : Number of elements.

          If (PortfolioDetails.Count <= 0) Then
            If (PortfolioDataSelectedRows IsNot Nothing) AndAlso (PortfolioDataSelectedRows.Length > 0) Then
              ' PortfolioDataSelectedRows.Length > 0 and PortfolioDetails.Count = 0

              PortfoliosAreTheSame = False
            End If
          ElseIf (PortfolioDataSelectedRows Is Nothing) OrElse (PortfolioDetails.Count <> PortfolioDataSelectedRows.Length) Then
            ' PortfolioDataSelectedRows is empty and PortfolioDetails.Count > 0

            PortfoliosAreTheSame = False
          End If

          ' If the Portfolios have the same number of elements then perform a detailed comparison.

          If (PortfoliosAreTheSame = True) AndAlso (PortfolioDetails.Count > 0) Then

            For DetailsCounter = 0 To (PortfolioDetails.Count - 1)
              ' Just captured Details
              thisPortfolioData = CType(PortfolioDetails(DetailsCounter), PortfolioData)
              PortfoliosAreTheSame = False

							If (PortfolioDataSelectedRows IsNot Nothing) Then
								For DataCounter = 0 To (PortfolioDataSelectedRows.Length - 1)
									PortfolioDataRow = PortfolioDataSelectedRows(DataCounter)

									If (PortfolioDataRow.FilingDate = PortfolioFilingDate) AndAlso _
									(PortfolioDataRow.Holding = thisPortfolioData.Position) AndAlso _
									(PortfolioDataRow.Ticker = thisPortfolioData.Ticker) Then
										PortfoliosAreTheSame = True
									End If
								Next
							End If

              If (PortfoliosAreTheSame = False) Then
                Exit For
              End If
            Next
          End If

          ' If portfolios do not match, then Add new details

          If (PortfoliosAreTheSame = True) Then
            ' Portfolios are the same, no need to continue

            Exit Sub
          Else
            ' Overwite Existing lines, Replace with new lines
						If (PortfolioDataSelectedRows IsNot Nothing) Then
							For DataCounter = 0 To (PortfolioDataSelectedRows.Length - 1)
								PortfolioDataRow = PortfolioDataSelectedRows(DataCounter)

								If (PortfolioDataRow.Holding = 0) And (PortfolioDataRow.Weight = 0) Then
									' Flag positions with an existing position of Zero for deletion

									PortfolioDataRow.DateDeleted = Now()
								Else
									PortfolioDataRow.Holding = 0
									PortfolioDataRow.Weight = 0
								End If
							Next
						End If
          End If


          Dim ItemUpdated As Boolean

					Try

						For DetailsCounter = 0 To (PortfolioDetails.Count - 1)
							ItemUpdated = False

							' Just captured Details
							thisPortfolioData = CType(PortfolioDetails(DetailsCounter), PortfolioData)
							PortfoliosAreTheSame = False

							If (PortfolioDataSelectedRows IsNot Nothing) Then
								For DataCounter = 0 To (PortfolioDataSelectedRows.Length - 1)
									PortfolioDataRow = PortfolioDataSelectedRows(DataCounter)

									If (PortfolioDataRow.FilingDate = PortfolioFilingDate) AndAlso _
								 (PortfolioDataRow.Ticker = thisPortfolioData.Ticker) Then

										ItemUpdated = True

										PortfolioDataRow.Holding = thisPortfolioData.Position

										' Prevent Deletion of this row (if necessary)
										If (Not PortfolioDataRow.IsDateDeletedNull) Then
											PortfolioDataRow.SetDateDeletedNull()
										End If

										Exit For
									End If
								Next
							End If

							If (ItemUpdated = False) Then
								' Add New Row

								PortfolioDataRow = PortfolioDataDS.tblPortfolioData.NewtblPortfolioDataRow

								thisMarketInstrumentID = 0
								Try
									If (MktInstrumentsTbl IsNot Nothing) Then
										SelectedMktInstrumentRows = MktInstrumentsTbl.Select("Ticker='" & thisPortfolioData.Ticker & "'")
										If SelectedMktInstrumentRows.Length > 0 Then
											thisMarketInstrumentID = SelectedMktInstrumentRows(0).MarketInstrumentID
										End If
									End If
								Catch ex As Exception
								End Try

								PortfolioDataRow.PortfolioDataID = PortfolioDataID
								PortfolioDataRow.PortfolioItemID = 0
								PortfolioDataRow.Ticker = thisPortfolioData.Ticker
								PortfolioDataRow.FilingDate = PortfolioFilingDate
								PortfolioDataRow.MarketInstrumentID = thisMarketInstrumentID

								PortfolioDataRow.PortfolioItemType = thisPortfolioData.PortfolioItemType

								PortfolioDataRow.Holding = thisPortfolioData.Position
								PortfolioDataRow.Weight = 0
								PortfolioDataRow.XMLData = ""
								PortfolioDataRow.MonitorItem = False

								PortfolioDataDS.tblPortfolioData.AddtblPortfolioDataRow(PortfolioDataRow)

							End If
						Next

						' Delete Redundant Portfolio Rows.
						If (PortfolioDataSelectedRows IsNot Nothing) Then
							For Each PortfolioDataRow In PortfolioDataSelectedRows
								If Not (PortfolioDataRow.IsDateDeletedNull) Then
									PortfolioDataRow.Delete()
								End If
							Next
						End If

						' Commit Changes

						PortfolioDataAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = DBNull.Value
						PortfolioDataAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = DBNull.Value
						' PortfolioDataAdaptor.Update(PortfolioDataDS.tblPortfolioData)
						AdaptorUpdate(PortfolioDataAdaptor, PortfolioDataDS.tblPortfolioData)

						PortfolioDataUpdateFlag = True

					Catch ex As Exception
						LogError(DebugLevels.Minimal, "DataBaseClient, ProcessPortfolioUpdate", LOG_LEVELS.Error, ex.Message, "Error Processing Portfolio.", ex.StackTrace)
					End Try


        End If

        ' Post Table Update Messages as necessary

        If (PortfolioIndexUpdateFlag = True) Then
					If (NaplesInitiator.Naples_TCPClient IsNot Nothing) Then
						Try
							NaplesInitiator.Naples_TCPClient.PostApplicationUpdate((TCPServer_Common.FCP_Application.Renaissance Or TCPServer_Common.FCP_Application.Venice), RenaissanceGlobals.RenaissanceChangeID.tblPortfolioIndex.ToString)
						Catch ex As Exception
						End Try
					End If
        End If

        If (PortfolioDataUpdateFlag = True) Then
					If (NaplesInitiator.Naples_TCPClient IsNot Nothing) Then
						Try
							NaplesInitiator.Naples_TCPClient.PostApplicationUpdate((TCPServer_Common.FCP_Application.Renaissance Or TCPServer_Common.FCP_Application.Venice), RenaissanceGlobals.RenaissanceChangeID.tblPortfolioData.ToString)
						Catch ex As Exception
						End Try
					End If
        End If

			Catch ex As Exception
				LogError(DebugLevels.Minimal, "DataBaseClient, ProcessPortfolioUpdate", LOG_LEVELS.Error, ex.Message, "Error Processing Portfolio.", ex.StackTrace)
			End Try

      ' Get NAME Updates for Any Instruments that were New

      If MissingTickers.Count > 0 Then
        Dim NameDataServerRequest As New DataServerRequest

        For TickerCount = 0 To (MissingTickers.Count - 1)
          NameDataServerRequest = New DataServerRequest(DS_RequestType.API_Static, MissingTickers(TickerCount).ToString & "|NAME")
          AddHandler NameDataServerRequest.DataUpdate, AddressOf Me.DataUpdate
          Me.NaplesInitiator.Naples_DataServer.PostNewRequest(NameDataServerRequest)
          Threading.Thread.Sleep(10)
        Next
      End If

    Catch ex As Exception
    Finally
      Try
        If (thisConnection IsNot Nothing) Then
          thisConnection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

  End Sub

  Private Sub DataUpdate(ByVal sender As Object, ByVal e As DataServerUpdate)
    Try
      If sender.GetType Is GetType(DataServerRequest) Then

        ' This Object is the Generic internal Request Object

        Dim thisDataServerRequest As DataServerRequest
        Dim thisDataServerUpdate As DataServerUpdate
        Dim RequestString As String

        thisDataServerRequest = CType(sender, DataServerRequest)
        thisDataServerUpdate = e

        ' ************************************
        ' React according to the Request Type...
        ' ************************************

        Select Case thisDataServerRequest.RequestType

          Case DS_RequestType.API_Static
            ' ************************************
            ' 
            ' OK, this should be an Update delivering the requested NAME data. 
            ' ************************************

            Select Case thisDataServerRequest.RequestStatus
              Case DS_RequestStatus.RequestComplete
                If thisDataServerRequest.ReturnObject.GetType Is GetType(Hashtable) Then
                  RequestString = thisDataServerRequest.Request

                  If (RequestString Is Nothing) Then Exit Sub
                  If InStr(RequestString, "|") <= 0 Then
                    Exit Sub
                  End If

									Dim MktInstrumentsDS As New RenaissanceDataClass.DSMarketInstruments
									Dim MktInstrumentsTbl As RenaissanceDataClass.DSMarketInstruments.tblMarketInstrumentsDataTable
									' Dim MktInstrumentsRow As RenaissanceDataClass.DSMarketInstruments.tblMarketInstrumentsRow
                  Dim MktInstrumentsAdaptor As SqlDataAdapter
									Dim SelectedMktInstrumentRows() As RenaissanceDataClass.DSMarketInstruments.tblMarketInstrumentsRow
                  Dim Ticker As String
                  Dim FieldValue As String
                  Dim FieldsHashTable As Hashtable
                  Dim RowCount As Integer

                  FieldsHashTable = CType(thisDataServerRequest.ReturnObject, Hashtable)
                  If FieldsHashTable.ContainsKey("NAME") = False Then
                    Exit Sub
                  End If
                  If (FieldsHashTable("NAME").GetType Is GetType(DataServerRTField)) Then
                    FieldValue = CType(FieldsHashTable("NAME"), DataServerRTField).FieldValue.ToString
                  Else
                    FieldValue = FieldsHashTable("NAME").ToString
                  End If

                  Ticker = RequestString.Substring(0, InStr(RequestString, "|") - 1)

                  MktInstrumentsDS = Me.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblMarketInstruments)
                  If (MktInstrumentsDS Is Nothing) Then Exit Sub
                  If (MktInstrumentsDS.tblMarketInstruments Is Nothing) Then Exit Sub

                  MktInstrumentsAdaptor = thisDataHandler.Get_Adaptor(RenaissanceGlobals.RenaissanceStandardDatasets.tblMarketInstruments.Adaptorname, NAPLES_CONNECTION, RenaissanceGlobals.RenaissanceStandardDatasets.tblMarketInstruments.TableName)
                  MktInstrumentsAdaptor.InsertCommand.Parameters("@KnowledgeDate").Value = KNOWLEDGEDATE_NOW
                  MktInstrumentsAdaptor.UpdateCommand.Parameters("@KnowledgeDate").Value = KNOWLEDGEDATE_NOW
                  MktInstrumentsTbl = MktInstrumentsDS.tblMarketInstruments

                  SelectedMktInstrumentRows = MktInstrumentsTbl.Select("Ticker='" & Ticker & "'")
                  If (SelectedMktInstrumentRows Is Nothing) OrElse (SelectedMktInstrumentRows.Length <= 0) Then Exit Sub

                  For RowCount = 0 To (SelectedMktInstrumentRows.Length - 1)
                    SelectedMktInstrumentRows(RowCount).Description = FieldValue
                  Next

                  Me.AdaptorUpdate(MktInstrumentsAdaptor, SelectedMktInstrumentRows)
                End If
            End Select
        End Select
      End If
    Catch ex As Exception
    End Try

  End Sub

#End Region

#Region " Get Portfolio Instruments"

	Public Function GetPortfolioMarketInstruments(ByVal pPortfolioID As Integer, ByVal pLimitToMonitorItems As Boolean, ByVal UseLatestPortfolio As Boolean, Optional ByVal pRecursePortfolios As Boolean = True, Optional ByVal PortfolioTree As String = "") As String()
		' *************************************************************************************
		' Returns a sorted list of Market Instrument Tickers from a given portfolio.
		' Optionally recurses through Sub-Portfolios.
		'
		' *************************************************************************************


		Dim ReturnArray As New ArrayList()

		Dim thisPortfolioIndexDS As RenaissanceDataClass.DSPortfolioIndex
		Dim thisPortfolioIndexTbl As RenaissanceDataClass.DSPortfolioIndex.tblPortfolioIndexDataTable
		Dim MatchingPortfolios() As RenaissanceDataClass.DSPortfolioIndex.tblPortfolioIndexRow
		Dim PortfolioTicker As String
		Dim PortfolioID As Integer
		Dim PortfolioDataID As Integer

		' Resolve Portfolio to use.

		Try
			thisPortfolioIndexDS = Me.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblPortfolioIndex, False)
			thisPortfolioIndexTbl = Nothing
			If (thisPortfolioIndexDS Is Nothing) Then
				thisPortfolioIndexDS = Get_SeparateDataSet(RenaissanceStandardDatasets.tblPortfolioIndex)
				If (thisPortfolioIndexDS IsNot Nothing) Then
					thisPortfolioIndexTbl = thisPortfolioIndexDS.tblPortfolioIndex
				End If
			Else
				thisPortfolioIndexTbl = thisPortfolioIndexDS.tblPortfolioIndex
			End If
			If (thisPortfolioIndexTbl Is Nothing) Then
				Return New String() {}
			End If

			MatchingPortfolios = thisPortfolioIndexTbl.Select("PortfolioID=" & pPortfolioID.ToString)
			If (MatchingPortfolios Is Nothing) OrElse (MatchingPortfolios.Length <= 0) Then
				Return New String() {}
			End If

			PortfolioID = pPortfolioID
			PortfolioTicker = MatchingPortfolios(0).PortfolioTicker
			PortfolioDataID = MatchingPortfolios(0).PortfolioDataID

			If (UseLatestPortfolio) Then
				MatchingPortfolios = thisPortfolioIndexTbl.Select("PortfolioTicker='" & PortfolioTicker & "'", "PortfolioFilingDate DESC")
				If (MatchingPortfolios.Length > 0) Then
					PortfolioID = MatchingPortfolios(0).PortfolioID
					PortfolioDataID = MatchingPortfolios(0).PortfolioDataID
				End If
			End If

		Catch ex As Exception

		End Try

		Try
			' Check for portfolio recursion.

			If (PortfolioTree.Contains("|" & PortfolioID.ToString & "|")) Then
				Return New String() {}
			End If
		Catch ex As Exception
		End Try

		' Get Portfolio Constituents

		Dim ThisCommand As SqlCommand

		' Dim thisPortfolioDataDS As RenaissanceDataClass.DSPortfolioData
		Dim thisPortfolioDataTbl As RenaissanceDataClass.DSPortfolioData.tblPortfolioDataDataTable
		Dim PortfolioData() As RenaissanceDataClass.DSPortfolioData.tblPortfolioDataRow = Nothing
		Dim thisPortfolioDataRow As RenaissanceDataClass.DSPortfolioData.tblPortfolioDataRow
		Dim PortfolioDataCount As Integer

		thisPortfolioDataTbl = Nothing
		ThisCommand = Nothing

		Try

			ThisCommand = New SqlCommand
			ThisCommand.CommandType = CommandType.StoredProcedure
			ThisCommand.CommandText = "adp_tblPortfolioData_SpecificPortfolio"
			ThisCommand.Connection = Me.GetNaplesConnection
			ThisCommand.Parameters.Add("@PortfolioDataID", SqlDbType.Int).Value = PortfolioDataID
			ThisCommand.Parameters.Add("@Knowledgedate", SqlDbType.DateTime).Value = KNOWLEDGEDATE_NOW

			thisPortfolioDataTbl = New RenaissanceDataClass.DSPortfolioData.tblPortfolioDataDataTable
      SyncLock ThisCommand.Connection
        thisPortfolioDataTbl.Load(ThisCommand.ExecuteReader)
      End SyncLock

			If (thisPortfolioDataTbl Is Nothing) Then
				Return New String() {}
			End If

			PortfolioData = thisPortfolioDataTbl.Select("PortfolioDataID=" & PortfolioDataID.ToString, "Ticker")

			If (PortfolioData Is Nothing) OrElse (PortfolioData.Length <= 0) Then
				Return New String() {}
			End If

		Catch ex As Exception
			Me.LogError(DebugLevels.Maximum, "DataBaseClient, GetPortfolioMarketInstruments", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace)
			Return New String() {}

		Finally
			Try
				If (ThisCommand IsNot Nothing) AndAlso (ThisCommand.Connection IsNot Nothing) Then
					ThisCommand.Connection.Close()
				End If
			Catch ex As Exception
			End Try
		End Try

		' Iterate through Constituents, adding them to the results set and recursing Portfolios as necessary

		Dim thisMarketInstrumentDS As RenaissanceDataClass.DSMarketInstruments = Nothing
		Dim SelectedMarketInstruments() As RenaissanceDataClass.DSMarketInstruments.tblMarketInstrumentsRow

		Try
			thisMarketInstrumentDS = Me.Load_Table(RenaissanceStandardDatasets.tblMarketInstruments)
			If (thisMarketInstrumentDS Is Nothing) Then
				Return New String() {}
			End If
		Catch ex As Exception
			Me.LogError(DebugLevels.Maximum, "DataBaseClient, GetPortfolioMarketInstruments", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace)
			Return New String() {}
		End Try

		Try
			Dim InstrumentTicker As String = ""

			For PortfolioDataCount = 0 To (PortfolioData.Length - 1)
				thisPortfolioDataRow = PortfolioData(PortfolioDataCount)

				Try
					If (pLimitToMonitorItems = False) OrElse (thisPortfolioDataRow.MonitorItem = True) Then

						Select Case CType(thisPortfolioDataRow.PortfolioItemType, PortfolioItemType)

							Case PortfolioItemType.MarketInstrument
								SelectedMarketInstruments = thisMarketInstrumentDS.tblMarketInstruments.Select("MarketInstrumentID=" & thisPortfolioDataRow.MarketInstrumentID)
								If (SelectedMarketInstruments IsNot Nothing) AndAlso (SelectedMarketInstruments.Length > 0) Then
									Dim TickerDecompose() As String

									InstrumentTicker = SelectedMarketInstruments(0).Ticker.Trim

									If (InstrumentTicker.Length > 0) Then
										TickerDecompose = InstrumentTicker.Split

										' 'M_Mkt' fix.
										If TickerDecompose(TickerDecompose.Length - 1) = "M-Mkt" Then
											TickerDecompose(TickerDecompose.Length - 1) = "M_Mkt"
										End If

										If System.Enum.IsDefined(GetType(DataProviderInstrumentType), TickerDecompose(TickerDecompose.Length - 1)) = False Then
											' Append Market Instrument defined type 
											Dim InstType As String

											InstType = CType(SelectedMarketInstruments(0).InstrumentType, DataProviderInstrumentType).ToString
											If InstType = "M_Mkt" Then
												InstType = "M-Mkt"
											End If

											InstrumentTicker &= " " & InstType
										End If

										If (ReturnArray.IndexOf(InstrumentTicker) < 0) Then
											ReturnArray.Add(InstrumentTicker)
										End If

									End If
								End If

							Case PortfolioItemType.Portfolio
								Dim SubPortfolio() As String
								SubPortfolio = Me.GetPortfolioMarketInstruments(thisPortfolioDataRow.MarketInstrumentID, pLimitToMonitorItems, UseLatestPortfolio, pRecursePortfolios, PortfolioTree & "|" & PortfolioID.ToString & "|")
								If (SubPortfolio IsNot Nothing) AndAlso (SubPortfolio.Length > 0) Then
									Dim SubPortfolioCounter As Integer

									For SubPortfolioCounter = 0 To (SubPortfolio.Length - 1)
										If (ReturnArray.IndexOf(SubPortfolio(SubPortfolioCounter)) < 0) Then
											ReturnArray.Add(SubPortfolio(SubPortfolioCounter))
										End If
									Next
								End If

							Case PortfolioItemType.VeniceInstrument

							Case PortfolioItemType.Default

						End Select

					End If
				Catch ex As Exception
					Me.LogError(DebugLevels.Maximum, "DataBaseClient, GetPortfolioMarketInstruments", LOG_LEVELS.Error, ex.Message, "Error getting Instrument Ticker. PortfolioDataID=" & PortfolioDataID.ToString & ", PortfolioDataCount=" & PortfolioDataCount.ToString, ex.StackTrace)
				End Try
	
			Next

		Catch ex As Exception
			Me.LogError(DebugLevels.Maximum, "DataBaseClient, GetPortfolioMarketInstruments", LOG_LEVELS.Error, ex.Message, "Error getting Instrument Ticker.", ex.StackTrace)
		End Try

		' Return Market Instrument ID or a constructed ticker ??

		If (ReturnArray.Count <= 0) Then
			Return New String() {}
		End If

		Try
			ReturnArray.Sort()
		Catch ex As Exception
		End Try

		Return ReturnArray.ToArray(GetType(String))

	End Function

#End Region

#Region " Get / Set Reference Prices"

	Public Function GetReferencePrice(ByVal pTaskID As Integer, ByVal pInstrumentTicker As String, ByRef pReferencePrice As Double, ByRef pDateEntered As Date) As Double

		SyncLock GetReferencePrice_Lockobject
			Dim QueryCommand As New SqlCommand
			Dim InstrumentTicker As String
			Dim ResultsTable As New DataTable

			If (pInstrumentTicker.Contains("|")) Then
				InstrumentTicker = pInstrumentTicker.Substring(0, pInstrumentTicker.IndexOf("|"))
			Else
				InstrumentTicker = pInstrumentTicker
			End If

			Try
				QueryCommand.CommandType = CommandType.Text
				QueryCommand.CommandText = "SELECT ISNULL(MAX(ReferencePrice), 0) AS ReferencePrice, ISNULL(MAX(DateEntered), '1 Jan 1900') AS DateEntered  FROM tblTriggerReferencePrices WHERE (TaskID=@TaskID) AND (Ticker = @InstrumentTicker)"
				QueryCommand.Parameters.Add("@TaskID", SqlDbType.Int).Value = pTaskID
				QueryCommand.Parameters.Add("@InstrumentTicker", SqlDbType.VarChar).Value = InstrumentTicker

				QueryCommand.Connection = Me.GetNaplesConnection

        SyncLock QueryCommand.Connection
          ResultsTable.Load(QueryCommand.ExecuteReader)
        End SyncLock

				If ResultsTable.Rows.Count > 0 Then
					pReferencePrice = CDbl(ResultsTable.Rows(0)("ReferencePrice"))
					pDateEntered = CDate(ResultsTable.Rows(0)("DateEntered"))
					Return CDbl(ResultsTable.Rows(0)(0))

				Else
					pReferencePrice = 0
					pDateEntered = Now()
					Return pReferencePrice
				End If

			Catch ex As Exception
				Me.LogError(DebugLevels.Maximum, "DataBaseClient, GetReferencePrice", LOG_LEVELS.Error, ex.Message, "Error getting Reference price.", ex.StackTrace)
			Finally
				If (QueryCommand.Connection IsNot Nothing) Then
					QueryCommand.Connection.Close()
				End If
			End Try

		End SyncLock

		Return 0
	End Function

	Public Sub SetReferencePrice(ByVal pTaskID As Integer, ByVal pInstrumentTicker As String, ByVal ReferenceValue As Double)

		SyncLock SetReferencePrice_Lockobject
			Dim QueryCommand As New SqlCommand
			Dim InstrumentTicker As String

			Try
				If (pInstrumentTicker.Contains("|")) Then
					InstrumentTicker = pInstrumentTicker.Substring(0, pInstrumentTicker.IndexOf("|"))
				Else
					InstrumentTicker = pInstrumentTicker
				End If

				QueryCommand.CommandType = CommandType.Text
				QueryCommand.CommandText = "SELECT ISNULL(MAX(RN), 0) AS RN FROM tblTriggerReferencePrices WHERE (TaskID=@TaskID) AND (Ticker = @InstrumentTicker)"
				QueryCommand.Parameters.Add("@TaskID", SqlDbType.Int).Value = pTaskID
				QueryCommand.Parameters.Add("@InstrumentTicker", SqlDbType.VarChar).Value = InstrumentTicker

				QueryCommand.Connection = Me.GetNaplesConnection

        Try
          SyncLock QueryCommand.Connection
            If CDbl(QueryCommand.ExecuteScalar) = 0 Then
              QueryCommand.CommandText = "INSERT tblTriggerReferencePrices(TaskID, Ticker, ReferencePrice) VALUES(@TaskID, @InstrumentTicker, 0)"
              QueryCommand.ExecuteNonQuery()
           End If
          End SyncLock
        Catch ex As Exception
          Me.LogError(DebugLevels.Minimal, "DataBaseClient(), SetReferencePrice()", LOG_LEVELS.Error, ex.Message, "Error INSERTING Reference price. " & QueryCommand.CommandText, ex.StackTrace, 0)
        End Try

        QueryCommand.CommandText = "UPDATE tblTriggerReferencePrices SET ReferencePrice = @ReferencePrice, DateEntered=GetDate() WHERE (TaskID=@TaskID) AND (Ticker = @InstrumentTicker)"
        QueryCommand.Parameters.Add("@ReferencePrice", SqlDbType.VarChar).Value = ReferenceValue
        SyncLock QueryCommand.Connection
          QueryCommand.ExecuteNonQuery()
        End SyncLock

      Catch ex As Exception
        Me.LogError(DebugLevels.Minimal, "DataBaseClient(), SetReferencePrice()", LOG_LEVELS.Error, ex.Message, "Error saving Reference price. " & QueryCommand.CommandText, ex.StackTrace, 0)
      Finally
        Try
          If (QueryCommand.Connection IsNot Nothing) Then
            QueryCommand.Connection.Close()
          End If
        Catch ex As Exception
        End Try
      End Try

			Try
				Me.LogError(DebugLevels.Maximum, "DataBaseClient(), SetReferencePrice()", LOG_LEVELS.Audit, "", QueryCommand.CommandText, "", 0)
			Catch ex As Exception
			End Try
		End SyncLock
	End Sub

	Public Sub ClearPendingEmails(ByVal pPortfolioID As Integer, ByVal pMaxRN As Integer)

		Dim thisSQLCommand As New SqlCommand
		thisSQLCommand.CommandType = CommandType.Text
		thisSQLCommand.CommandText = "DELETE FROM tblPendingDataTaskEMails WHERE (DataTaskID = " & pPortfolioID.ToString & ") AND (RN <= " & pMaxRN.ToString & ")"

    Try
      thisSQLCommand.Connection = Me.GetNaplesConnection
      SyncLock thisSQLCommand.Connection
        thisSQLCommand.ExecuteNonQuery()
      End SyncLock
    Catch ex As Exception
    Finally
      Try
        If (thisSQLCommand.Connection IsNot Nothing) Then
          thisSQLCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try
	End Sub

#End Region

#Region " Get EMail Addresses for Person IDs "

	Friend Function GetPersonIdEmailAddresses(ByVal pPersionIDs() As Integer) As String()
		Dim RVal(-1) As String
		Dim EMailArray As New ArrayList

		Dim PersonCounter As Integer = 0

		Dim DStblPerson As RenaissanceDataClass.DSPerson = Nothing
		Dim tblPerson As RenaissanceDataClass.DSPerson.tblPersonDataTable = Nothing
		Dim SelectedPeople(-1) As RenaissanceDataClass.DSPerson.tblPersonRow
		Dim thisPerson As RenaissanceDataClass.DSPerson.tblPersonRow = Nothing

		' Validate
		If (pPersionIDs Is Nothing) OrElse (pPersionIDs.Length <= 0) Then
			Return RVal
		End If

		' Get DataSet
		DStblPerson = Me.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblPerson)
		If (DStblPerson Is Nothing) OrElse (DStblPerson.tblPerson Is Nothing) OrElse (DStblPerson.tblPerson.Rows.Count <= 0) Then
			Return RVal
		End If

		' get table
		tblPerson = DStblPerson.tblPerson

		For PersonCounter = 0 To (pPersionIDs.Length - 1)
			If (pPersionIDs(PersonCounter) > 0) Then
				SelectedPeople = tblPerson.Select("PersonID=" & pPersionIDs(PersonCounter).ToString)

				If (SelectedPeople IsNot Nothing) AndAlso (SelectedPeople.Length > 0) AndAlso (SelectedPeople(0).PersonEmail.Length > 0) Then
					EMailArray.Add(SelectedPeople(0).PersonEmail)
				End If
			End If
		Next

		If (EMailArray.Count > 0) Then
			ReDim RVal(EMailArray.Count - 1)

			For PersonCounter = 0 To (EMailArray.Count - 1)
				RVal(PersonCounter) = EMailArray(PersonCounter)
			Next
		End If
		Return RVal
	End Function
#End Region

#Region " Save DataTaskEmails"

	Friend Sub SaveDataTaskEmail(ByVal DataTaskID As Integer, ByVal EMailLine As String)
		' ************************************************************************************************
		'
		'
		' ************************************************************************************************

		Dim thisCommand As New SqlCommand

		Try
			thisCommand.CommandType = CommandType.StoredProcedure
			thisCommand.CommandText = "adp_tblPendingDataTaskEmails_InsertCommand"
			thisCommand.Parameters.Add("@DataTaskID", SqlDbType.Int).Value = DataTaskID
			thisCommand.Parameters.Add("@EMailLine", SqlDbType.VarChar, 500).Value = EMailLine
		Catch ex As Exception
		End Try

		Try
			thisCommand.Connection = GetNaplesConnection()
			thisCommand.ExecuteNonQuery()
		Catch ex As Exception
		Finally
			Try
				If (thisCommand.Connection IsNot Nothing) Then
					thisCommand.Connection.Close()
				End If
			Catch ex As Exception
			End Try
		End Try
	End Sub

	Friend Function GetDataTaskEmail(ByVal DataTaskID As Integer) As DataTable
		' ************************************************************************************************
		' Return a table of Pending EMail lines for the given Data Task.
		'
		'
		' ************************************************************************************************

		Dim thisCommand As New SqlCommand
		Dim ReturnDataTable As New DataTable

		Try
			thisCommand.CommandType = CommandType.StoredProcedure
			thisCommand.CommandText = "adp_tblPendingDataTaskEmails_SelectCommand"
			thisCommand.Parameters.Add("@DataTaskID", SqlDbType.Int).Value = DataTaskID
		Catch ex As Exception
		End Try

		Try
			thisCommand.Connection = GetNaplesConnection()

			ReturnDataTable.Load(thisCommand.ExecuteReader)

			Return ReturnDataTable

		Catch ex As Exception
		Finally
			Try
				If (thisCommand.Connection IsNot Nothing) Then
					thisCommand.Connection.Close()
				End If
			Catch ex As Exception
			End Try
		End Try

		Return New DataTable

	End Function


#End Region

End Class


