

Module Gobals

  ' Minimum time to wait before trying to start the BBG Server again.
  ' Used in the DataServer Class
  Public Const BBGServerRetryTimeout As Integer = 60
	Public Const BBGAutoLoginHeader As String = "<auto-login><blp-0><cancel><dflt>"
	Public Const BBGAutoLoginDefaultUser As String = "XXXXXXX"
	Public Const BBGAutoLoginDefaultPassword As String = "XXXXXXX"
	Public Const BBGAutoLoginTail As String = "<go><auto-login>"
	' Public Const BBGAutoLoginString As String = "<auto-login><blp-0><cancel><dflt>XXXXXXXXXXXXXX<go><auto-login>"

	Public Const BBGScreenToUse As String = "<blp-3>"
  Public Const BBGClipboardCopyTimeout As Integer = 5000

  ' Some Bloomberg Field Names

  Public Const BBG_LAST_PRICE As String = "LAST_PRICE"
  Public Const BBG_BID As String = "BID"
  Public Const BBG_ASK As String = "ASK"
  Public Const BBG_VOLUME As String = "VOLUME"


  Public Const KNOWLEDGEDATE_NOW As Date = #1/1/1900#

  ' RegistryBase for the Naples Application
  '
  ' Things such as the Default Message Server name may be kept here.

	Public Const REGISTRY_BASE As String = "Software\Primonial\Naples"

  Public Const NAPLES_CONNECTION As String = "cnnNaples"

  Public Const DISPLAYMEMBER_DATEFORMAT As String = "dd MMM yyyy"
  Public Const DISPLAYMEMBER_LONGDATEFORMAT As String = "dd MMM yyyy HH:mm:ss"
	Public Const DISPLAYMEMBER_TIMEFORMAT As String = "HH:mm:ss"
	Public Const REPORT_DATEFORMAT As String = "dd MMMM yyyy"
  Public Const REPORT_LONGDATEFORMAT As String = "dd MMM yyyy HH:mm:ss"
  Public Const QUERY_SHORTDATEFORMAT As String = "dd MMM yyyy"
  Public Const QUERY_LONGDATEFORMAT As String = "dd MMM yyyy HH:mm:ss"

  ' Arbitrary, but standard Names for things :-

	' Public Const TEMP_Get_MI_tblPortfolioIndex As String = "TEMP_Get_MI_tblPortfolioIndex"
	' Public Const TEMP_Get_MI_tblPortfolioData As String = "TEMP_Get_MI_tblPortfolioData"

#Region "EMail Server Globals"
	Public Const EMAIL_DefaultFromAddress As String = "Naples@primonial.com"
	Public Const EMAIL_DefaultSMTPServer As String = "mailer.patrimoine.net" ' Not Correct ?!?

#End Region

  Public Enum NaplesChangeID As Integer
    None = 0



    DeathToNaples = 101
  End Enum

  ' DEFINED IN RENAISSANCEGLOBALS !!!
  '
  'Public Enum LOG_LEVELS As Integer
  '  [Warning] = 0     'Global Const LOG_Warning As Integer = 0
  '  [Audit] = (-1)    'Global Const LOG_Audit As Integer = (-1)
  '  [Changes] = (-2)  'Global Const LOG_Changes As Integer = (-2)
  '  [Error] = (-10)   'Global Const LOG_Error As Integer = (-10)

  'End Enum

  Public Enum DebugLevels As Integer
    [None] = 0
    [Minimal] = 1
    [Medium] = 2
    [Maximum] = 3
  End Enum

End Module


