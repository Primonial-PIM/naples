Imports System.Net
Imports System.Net.Sockets
Imports System.IO
Imports Microsoft.Win32
Imports System.Windows.Forms

Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.Serialization
Imports TCPServer_Common
Imports TCPServer_Common.PostMessageUtils
Imports NaplesGlobals


Public Class TCP_Client

  ' *****************************************************************************************************
  ' TCP Client CLASS
  ' -----------------
  '
  ' The TCP Client Class provides the interface between Naples and the MessageServer, Whilst direct connections
  ' Between Applications, such as Venice, and Naples are catered for to some degree, that was not the original
  ' intention and such an approach may be less well tested.
  '
  ' At it's simplest, TCP Client maintains a collection of live TCP Connections, checking each one in turn for 
  ' new messages. When a Message is received an appropriate Data Request will be created and passed to the 
  ' appropriate part of Naples, the message server will then combine the Incoming message parts with the Internal 
  ' DataRequest in a Work Object and add this item to a collection of outstanding Work.
  '
  ' As Data is received from Naples components, the outstanding work items are updated as appropriate and reply 
  ' messages are sent back to the Message Server.  Maintenance of the Work list is done as needed, the maintenance 
  ' (cleanup) of the internal DataServer objects is also catered for. To a large extent the internal DataServer(s)
  ' should maintain their own objects, but the TCP Client co-operates in this endevour.
  '
  ' -----------------
  '
  ' Imcoming messages are, in the first part, standard 'TCPServerHeader' objects. The TCP Client will handle various of
  ' the standard header Message types, such as TouchBase, RequestConnect, Disconnect etc, as appropriate.
  '
  ' Incoming data requests are usually made as a combination of two objects. The First Object is a standard 'TCPServerHeader'
  ' object with a message type of 'Server_NaplesDataRequest'. This message may be followed by any number of objects as 
  ' determined by the TCPServerHeader.FollowingMessageCount property. 
  ' Thus far there will usually be a single following object of type 'NaplesDataRequest'. In the absence of any following 
  ' objects, or if the following object is not of the appropriate type, then TCPClient will attempt to construct a 
  ' StaticAPI Data Request using the MessageHeader.MessageString as a default options.
  '
  ' The next steps are :
  '
  '	  1) Construct a 'DataServerRequest' object and pass it to the appropriate Naples Component.
  '	  2) Combine the Connection, Incoming Request object(s) and Internal DataRequest object into a work item that
  '		   can be queried as data updates are received from internal data sources.
  '	  3) As updates are received, generate appropriate responses for the MessageServer.
  '	  4) Handle and respond to Data updates and Errors as appropriate by sending out Messages and tidying up internal
  '		   structures.
  '
  ' API Data Requests, that is conventional Security Information requests such as Bid, Ask or Last, may be marked
  ' as either Static or Realtime.
  ' Static Requests involve a One-Off return of information, after which the internal Naples objects and Requests
  ' will be disposed of.
  ' Realtime Requests will return a Data Message each the Requested items are updated.
  ' A failed response message or an error will result in a NaplesData Request, Static or RealTime, being discarded.
  '
  ' Note that some care must be taken in correctly creating MessageHeader objects for return to the Message Server.
  ' In order to accomodate the concept of passing messages and requests along a chain of servers, the 
  ' MessageHeader.RequestingMessage property may be a nested Sequence of Message Headers. Care must be taken 
  ' to return these in the way that the Message Server Expects. If there is no chain of Message Servers, then this is
  ' not an issue, but you should code accordingly !
  '
  ' *****************************************************************************************************

  Inherits ThreadWrapperBase

  Private _CloseDownFlag As Boolean
  Private NaplesInitiator As Initiator

  Private ConnectionCollection As New Hashtable
  Private WorkCollection As New Hashtable
  Private Listener As TcpListener
  Private DataUpdateLockObject As New TCPClientLockObject
  Private NaplesClientID As Guid = Guid.NewGuid

  Public Event FCPApplicationUpdate(ByVal sender As Object, ByVal e As FCPApplicationUpdateEventArgs)

  Public Sub New(ByVal pInitiator As Initiator)
		' *****************************************************************************************************
		'
		' *****************************************************************************************************
		MyBase.New()

    Me.Thread.Name = "TCP_Client"
    NaplesInitiator = pInitiator
  End Sub


  Public Property CloseDown() As Boolean
		' *****************************************************************************************************
		'
		' *****************************************************************************************************
		Get
			Return _CloseDownFlag
		End Get
    Set(ByVal Value As Boolean)
      _CloseDownFlag = Value
    End Set
  End Property

  Protected Overrides Sub DoTask()
		' *****************************************************************************************************
		'
		' *****************************************************************************************************

    Initialise()

    RunDataServer()

    TidyUp()

  End Sub

  Private Sub Initialise()
		' *****************************************************************************************************
		' Initialise Data Server
		'
		' *****************************************************************************************************
		
    _CloseDownFlag = False

    ' Start Initial, Default, server connection
    Dim DefaultServerConnection As New ServerConnectionClass

    ' The 'ConnectToMessageServer' function will resolve an empty Commection object to the Default
    ' Server.

    Call ConnectToMessageServer(DefaultServerConnection)

    If Not (DefaultServerConnection.MsgServerIPAddress Is IPAddress.None) Then
      ' An IP Address was resolved, add the Default connection to the Connection collection
      ConnectionCollection.Add(DefaultServerConnection.ConnectionId, DefaultServerConnection)
    End If

    ' Initialise the TCP Listener
    Try
      Listener = New TcpListener(TCPServer_Common.NetworkFunctions.GetServerAddress(System.Environment.MachineName), TCPServerGlobals.FCP_ServerPort)
      Listener.Start()
    Catch ex As Exception
      _CloseDownFlag = True
    End Try



  End Sub


  Private Sub TidyUp()
		' *****************************************************************************************************
		'
		' *****************************************************************************************************

		Dim thisHashtableEntry As DictionaryEntry
    Dim thisServerConnection As ServerConnectionClass

    ' On ending the Server






    ' Stop the Listener
    Try
      Listener.Stop()
    Catch ex As Exception
    End Try


    ' Close the MessageServer connection(s)
    For Each thisHashtableEntry In ConnectionCollection

      Try
        thisServerConnection = CType(thisHashtableEntry.Value, ServerConnectionClass)
      Catch ex As Exception
        ' In the event of an error, Null the ServerConnection object and remove this connection from the 
        ' collection.

        ConnectionCollection.Remove(thisHashtableEntry.Key)
        thisServerConnection = Nothing
      End Try

      If (Not (thisServerConnection Is Nothing)) Then
        Try
          SyncLock DataUpdateLockObject
            If Not (thisServerConnection.MsgServerStream Is Nothing) Then
              PostServerMessage(thisServerConnection.MsgServerStream, TCP_ServerMessageType.Client_Disconnect)
            End If
          End SyncLock
          thisServerConnection.MsgServerClient.Close()

        Catch ex As Exception
        Finally
          thisServerConnection.MsgServerClient = Nothing
          thisServerConnection.MsgServerStream = Nothing
      End Try
      End If
    Next


  End Sub

  Private Sub RunDataServer()
		' *****************************************************************************************************
		'
		' *****************************************************************************************************

		Dim LoopWorkDone As Boolean
    Dim KeysCollection As ICollection
    Dim ThisKey As Object
    Dim thisServerConnection As ServerConnectionClass

    ' Dim ConnectionsToRemove As Collection

    Dim MessageHeader As TCPServerHeader
    Dim MaxMessageCounter As Integer
    Dim thisServerRequest As DataServerRequest = Nothing
    Dim thisWorkItem As ClientWorkClass = Nothing

    Dim SerializeFormatter As New BinaryFormatter

    Dim WorkCollectionCancelList As New ArrayList
    Dim ConnectionCollectionCancelList As New ArrayList

    Dim ServerConnectionExists As Boolean = False


    ' Work loop.
    While (_CloseDownFlag = False)
      LoopWorkDone = False

      ' *************************************************
      ' Check for New Connections pending
      ' *************************************************

      ' If there is a connection pending...

      If Listener.Pending = True Then
        LoopWorkDone = True

        thisServerConnection = New ServerConnectionClass
        Try
          thisServerConnection.MsgServerClient = Listener.AcceptTcpClient()
          thisServerConnection.MsgServerClient.ReceiveTimeout = TCPServerGlobals.FCP_Server_ReceiveTimeout
          thisServerConnection.MsgServerClient.SendTimeout = TCPServerGlobals.FCP_Server_SendTimeout
					thisServerConnection.MsgServerClient.NoDelay = True
          thisServerConnection.MsgServerClient.ReceiveBufferSize = CInt((2 ^ 14)) ' 16K
          thisServerConnection.MsgServerClient.SendBufferSize = CInt((2 ^ 14))  ' 16K
          thisServerConnection.MsgServerStream = thisServerConnection.MsgServerClient.GetStream()
          thisServerConnection.MsgServerName = "<Unknown>"
          thisServerConnection.MsgServerIPAddress = IPAddress.None

          ConnectionCollection.Add(thisServerConnection.ConnectionId, thisServerConnection)
        Catch ex As Exception
        End Try
      End If


      ' *************************************************
      ' Process existing Connections
      ' *************************************************

      SyncLock ConnectionCollection
        KeysCollection = ConnectionCollection.Keys
      End SyncLock

      Try
        WorkCollectionCancelList.Clear()
      Catch ex As Exception
      End Try

      Try
        ConnectionCollectionCancelList.Clear()
			Catch ex As Exception
			End Try

      Try
        ServerConnectionExists = False

        For Each ThisKey In KeysCollection
          Try
            SyncLock ConnectionCollection
              If ConnectionCollection.ContainsKey(ThisKey) Then
                thisServerConnection = CType(ConnectionCollection(ThisKey), ServerConnectionClass)
              Else
                thisServerConnection = Nothing
              End If
            End SyncLock
          Catch ex As Exception
            ' ******************************************************
            ' In the event of an error, Null the ServerConnection object and remove this connection from the 
            ' collection.
            ' ******************************************************

            Try
              ConnectionCollectionCancelList.Add(ThisKey)
              thisServerConnection = Nothing
              Exit For
            Catch ex2 As Exception
            End Try

            thisServerConnection = Nothing
          End Try

          Try
            If (thisServerConnection IsNot Nothing) AndAlso ConnectToMessageServer(thisServerConnection) Then

              ' Check that the standard message server connection exists
              If (thisServerConnection.MsgServerIPAddress.Equals(NaplesInitiator.MessageServerAddress)) Then
                ServerConnectionExists = True
              End If

              ' *************************************************
              ' Check for Incoming messages
              ' *************************************************

              MaxMessageCounter = 0

              While (Not (thisServerConnection Is Nothing)) AndAlso (thisServerConnection.MsgServerStream.DataAvailable = True) AndAlso (MaxMessageCounter < 5)
                Try
                  MessageHeader = DirectCast(SerializeFormatter.Deserialize(thisServerConnection.MsgServerStream), TCPServerHeader)
                Catch ex As Exception
                  MessageHeader = Nothing
                End Try

                If (Not (MessageHeader Is Nothing)) Then
                  thisServerConnection.LastWakeupTime = Now()

                  Dim FollowingObjects(-1) As Object

                  ' ******************************************************
                  ' Take in Following objects, if any.
                  ' Strip objects out of 'TCPServerObjectWrapper' wrappers as necessary.
                  ' ******************************************************

                  If MessageHeader.FollowingMessageCount > 0 Then
                    Dim Counter As Integer

                    ReDim FollowingObjects(MessageHeader.FollowingMessageCount)

                    For Counter = 1 To MessageHeader.FollowingMessageCount
                      Try
                        FollowingObjects(Counter - 1) = SerializeFormatter.Deserialize(thisServerConnection.MsgServerStream)

                        ' Strip off Wrapper objects as necessary.
                        If (Not (FollowingObjects(Counter - 1) Is Nothing)) AndAlso (FollowingObjects(Counter - 1).GetType Is GetType(TCPServerObjectWrapper)) Then

                          Dim ThisObjectWrapper As TCPServerObjectWrapper
                          Dim NewObject As Object

                          ThisObjectWrapper = CType(FollowingObjects(Counter - 1), TCPServerObjectWrapper)
                          NewObject = FollowingObjects(Counter - 1)

                          Select Case ThisObjectWrapper.ObjectFormat.ToUpper
                            Case "BINARYFORMATTER"       ' BinaryFormatter
                              Try
                                FollowingObjects(Counter - 1) = SerializeFormatter.Deserialize(New MemoryStream(ThisObjectWrapper.ObjectData))
                              Catch ex As Exception
                                FollowingObjects(Counter - 1) = ThisObjectWrapper
                              End Try

                            Case "XMLFORMATTER"
                              Select Case ThisObjectWrapper.ObjectType
                                Case "RenaissanceUpdateClass"
                                  ' More Code Here ....
                                  '
                                  '
                                  '

                                Case "ANOther"
                                  ' More Code Here ....
                                  '
                                  '
                                  '
                              End Select

                          End Select

                        End If
                      Catch ex As Exception
                        FollowingObjects(Counter - 1) = Nothing
                      End Try
                    Next
                  End If


                  ' ******************************************************
                  ' Process Message
                  ' ******************************************************

                  Select Case MessageHeader.MessageType

                    Case TCP_ServerMessageType.Server_NaplesDataRequest, TCP_ServerMessageType.Client_NaplesDataRequest
                      ' ******************************************************
                      ' NaplesDataRequest
                      ' ******************************************************

                      Dim thisNaplesDataRequest As NaplesDataRequest = Nothing

                      ' If a Naples Data request object was given....
                      If (MessageHeader.FollowingMessageCount > 0) AndAlso _
                      (FollowingObjects IsNot Nothing) AndAlso _
                      (FollowingObjects.Length > 0) AndAlso _
                      (TypeOf FollowingObjects(0) Is NaplesDataRequest) Then

                        ' Then Use it.
                        Try

                          thisNaplesDataRequest = CType(FollowingObjects(0), NaplesDataRequest)

                        Catch ex As Exception

                          ' Default to Static API Request using the Message String
                          thisNaplesDataRequest = New NaplesDataRequest(MessageHeader.MessageString, DS_RequestType.API_Static)

                        End Try
                      Else

                        ' Default to Static API Request using the Message String

                        thisNaplesDataRequest = New NaplesDataRequest(MessageHeader.MessageString, DS_RequestType.API_Static)
                      End If

                      ' ********************************************************
                      ' If this is not a 'Cancel' request, Create the Data Request and the Work item 
                      ' and add it to the TCPClient Work collection.
                      ' ********************************************************

                      Select Case thisNaplesDataRequest.RequestType
                        Case DS_RequestType.Cancel
                          ' No action required at this point 

                        Case DS_RequestType.Debug_AggregatorDataTasks

                        Case DS_RequestType.Debug_PendingItems

                        Case DS_RequestType.Debug_RTItems

                        Case DS_RequestType.Set_DebugLevel

                        Case Else ' If (Not (thisNaplesDataRequest.RequestType = DS_RequestType.Cancel)) Then
                          ' ********************************************************
                          ' OK, Now Construct a DataServer Request Object.
                          ' ********************************************************

                          thisServerRequest = New DataServerRequest(thisNaplesDataRequest.RequestType, thisNaplesDataRequest.RequestText)
                          AddHandler thisServerRequest.DataUpdate, AddressOf Me.DataUpdate

                          ' ********************************************************
                          ' Add this Work Item to the WorkCollection
                          ' ********************************************************

                          thisWorkItem = New ClientWorkClass(thisServerConnection, MessageHeader, thisNaplesDataRequest, thisServerRequest.RequestId, thisServerRequest)
                          SyncLock WorkCollection
                            WorkCollection.Add(thisWorkItem.ClientRequestId, thisWorkItem)
                          End SyncLock

                      End Select

                      ' ********************************************************
                      ' Post the Data Request to the appropriate Data Source, or process the 
                      ' 'Cancel request as appropriate.
                      ' ********************************************************

                      Select Case thisNaplesDataRequest.RequestType
                        Case DS_RequestType.API_Realtime, DS_RequestType.API_Static

                          ' ********************************************************
                          ' Pass the Request to the BBG Api Data Server.
                          ' ********************************************************

                          NaplesInitiator.Naples_DataServer.PostNewRequest(thisServerRequest)

												Case DS_RequestType.DDE_ScreenShot, DS_RequestType.DDE_ScreenCopy, DS_RequestType.DDE_PostCommand, DS_RequestType.DDE_GetPortfolio, DS_RequestType.BBG_Login

													' ********************************************************
													' Pass the Request to the BBG DDE Data Server.
													' ********************************************************

													NaplesInitiator.Naples_DDECapture.PostNewRequest(thisServerRequest)

                        Case DS_RequestType.Cancel

                          ' ********************************************************
                          ' Cancel a request
                          ' ********************************************************

                          ' Find the associated Work Item
                          ' thisNaplesDataRequest


                          Dim thisCancelKey As Object
                          Dim CancelKeyList As ICollection

                          Dim tempWorkItem As ClientWorkClass
                          Dim WorkItemDataRequest As NaplesDataRequest

                          CancelKeyList = WorkCollection.Keys
                          Try
                            For Each thisCancelKey In CancelKeyList
                              Try
                                tempWorkItem = CType(WorkCollection(thisCancelKey), ClientWorkClass)

                                If (Not (tempWorkItem.MessageBody Is Nothing)) AndAlso (tempWorkItem.MessageBody.GetType Is GetType(NaplesDataRequest)) Then
                                  WorkItemDataRequest = CType(tempWorkItem.MessageBody, NaplesDataRequest)

                                  If thisNaplesDataRequest.RequestID.Equals(WorkItemDataRequest.RequestID) Then
                                    ' Remove from WorkCollection
                                    Try
                                      WorkCollectionCancelList.Add(thisCancelKey)
                                      'SyncLock WorkCollection
                                      '  WorkCollection.Remove(thisCancelKey)
                                      'End SyncLock
                                    Catch ex As Exception
                                    End Try

                                    ' ***************************************************
                                    ' Cancel ServerObjects.
                                    '
                                    ' First resolve which server, then cancel objects as appropriate
                                    '
                                    ' ***************************************************

                                    Select Case WorkItemDataRequest.RequestType

                                      Case DS_RequestType.API_Realtime, DS_RequestType.API_Static
                                        ' ***************************************************
                                        '
                                        ' ***************************************************

                                        Dim ThisAPIDataServerRequestObject As DataServerRequest
                                        Dim ThisAPIDataServerRTObject As DataServerRTItem

                                        If (tempWorkItem.DataRequest.GetType Is GetType(DataServerRequest)) Then
                                          ThisAPIDataServerRequestObject = CType(tempWorkItem.DataRequest, DataServerRequest)

                                          Me.NaplesInitiator.Naples_DataServer.RemoveRequest(ThisAPIDataServerRequestObject)

                                        End If

                                        If (tempWorkItem.DataItem.GetType Is GetType(DataServerRTItem)) Then
                                          ThisAPIDataServerRTObject = CType(tempWorkItem.DataItem, DataServerRTItem)

                                          Me.NaplesInitiator.Naples_DataServer.RemoveRequest(ThisAPIDataServerRTObject)

                                        End If

                                      Case DS_RequestType.DDE_ScreenCopy, DS_RequestType.DDE_ScreenShot
                                        ' ***************************************************
                                        '
                                        ' ***************************************************






                                    End Select



                                  End If

                                End If



                              Catch ex As Exception
                              End Try

                            Next
                          Catch ex As Exception
                          End Try

                        Case DS_RequestType.Debug_AggregatorDataTasks
                          ' ********************************************************
                          ' 
                          ' ********************************************************

                          Try
                            ' Post Debug Message.
                            Dim DebugMessage As New TCPServerHeader(TCP_ServerMessageType.Server_NaplesDataAnswer, FCP_Application.NaplesDataServer, thisNaplesDataRequest.RequestType.ToString, 1, 0, New ArrayList, MessageHeader.PathTravelled)
                            Dim DebugDataAnswer As New NaplesDataAnswer(thisNaplesDataRequest, NaplesAnswerType.Debug, NaplesInitiator.Naples_Aggregator.Debug_GetDataTaskDetails(thisNaplesDataRequest.RequestText))

                            SyncLock DataUpdateLockObject
                              If PostServerMessage(thisServerConnection.MsgServerStream, DebugMessage) = True Then
                                Call PostServerMessage(thisServerConnection.MsgServerStream, AddTCPServerWrapperClass(DebugDataAnswer))

                                ' thisServerConnection.LastWakeupTime = Now()
                              End If
                            End SyncLock

                          Catch ex As Exception
                          End Try

                        Case DS_RequestType.Debug_PendingItems
                          ' ********************************************************
                          ' 
                          ' ********************************************************
                          Try
                            ' Post Debug Message.
                            Dim DebugMessage As New TCPServerHeader(TCP_ServerMessageType.Server_NaplesDataAnswer, FCP_Application.NaplesDataServer, thisNaplesDataRequest.RequestType.ToString, 1, 0, New ArrayList, MessageHeader.PathTravelled)
                            Dim DebugDataAnswer As New NaplesDataAnswer(thisNaplesDataRequest, NaplesAnswerType.Debug, NaplesInitiator.Naples_DataServer.Debug_GetPendingItemDetails)

                            SyncLock DataUpdateLockObject
                              If PostServerMessage(thisServerConnection.MsgServerStream, DebugMessage) = True Then
                                Call PostServerMessage(thisServerConnection.MsgServerStream, AddTCPServerWrapperClass(DebugDataAnswer))

                                ' thisServerConnection.LastWakeupTime = Now()
                              End If
                            End SyncLock

                          Catch ex As Exception
                          End Try

                        Case DS_RequestType.Debug_RTItems
                          ' ********************************************************
                          ' 
                          ' ********************************************************

                          Try
                            ' Post Debug Message.
                            Dim DebugMessage As New TCPServerHeader(TCP_ServerMessageType.Server_NaplesDataAnswer, FCP_Application.NaplesDataServer, thisNaplesDataRequest.RequestType.ToString, 1, 0, New ArrayList, MessageHeader.PathTravelled)
                            Dim DebugDataAnswer As New NaplesDataAnswer(thisNaplesDataRequest, NaplesAnswerType.Debug, NaplesInitiator.Naples_DataServer.Debug_GetRTItemDetails)

                            SyncLock DataUpdateLockObject
                              If PostServerMessage(thisServerConnection.MsgServerStream, DebugMessage) = True Then
                                Call PostServerMessage(thisServerConnection.MsgServerStream, AddTCPServerWrapperClass(DebugDataAnswer))

                                ' thisServerConnection.LastWakeupTime = Now()
                              End If
                            End SyncLock
                          Catch ex As Exception
                          End Try

                        Case DS_RequestType.Debug_RePostRTRequests
                          ' ********************************************************
                          ' 
                          ' ********************************************************

                          Try
                            ' Post Debug Message.
                            Dim DebugMessage As New TCPServerHeader(TCP_ServerMessageType.Server_NaplesDataAnswer, FCP_Application.NaplesDataServer, thisNaplesDataRequest.RequestType.ToString, 0, 0, New ArrayList, MessageHeader.PathTravelled)

                            Call NaplesInitiator.Naples_DataServer.RePostAllRTDataItemRequests()

                            SyncLock DataUpdateLockObject
                              Call PostServerMessage(thisServerConnection.MsgServerStream, DebugMessage)
                            End SyncLock

                            'If PostServerMessage(thisServerConnection.MsgServerStream, DebugMessage) = True Then
                            '  thisServerConnection.LastWakeupTime = Now()
                            'End If
                          Catch ex As Exception
                          End Try

                        Case DS_RequestType.Set_DebugLevel
                          ' ********************************************************
                          ' Set Debug Level
                          ' ********************************************************

                          If IsNumeric(MessageHeader.MessageString) AndAlso (CInt(MessageHeader.MessageString) >= 0) Then
                            Try
                              Me.NaplesInitiator.DebugLevel = CInt(MessageHeader.MessageString)
                            Catch ex As Exception
                            End Try
                          End If

                          Try
                            ' Post Debug Message.
                            Dim DebugMessage As New TCPServerHeader(TCP_ServerMessageType.Server_NaplesDataAnswer, FCP_Application.NaplesDataServer, thisNaplesDataRequest.RequestType.ToString, 0, 0, New ArrayList, MessageHeader.PathTravelled)
                            DebugMessage.MessageString = "NaplesInitiator.DebugLevel = " & NaplesInitiator.DebugLevel.ToString

                            SyncLock DataUpdateLockObject
                              Call PostServerMessage(thisServerConnection.MsgServerStream, DebugMessage)
                            End SyncLock


                            'If PostServerMessage(thisServerConnection.MsgServerStream, DebugMessage) = True Then
                            '	thisServerConnection.LastWakeupTime = Now()
                            'End If
                          Catch ex As Exception
                          End Try

                        Case Else
                          ' ********************************************************
                          ' No Appropriate Data Source ...
                          ' ********************************************************

                          Try
                            ' Post Rejected Message.

                            Dim errMessage As New TCPServerHeader(TCP_ServerMessageType.Server_NaplesDataAnswer, FCP_Application.NaplesDataServer, "No DataServer for this Request", 1, 0, New ArrayList, MessageHeader.PathTravelled)
                            Dim errDataAnswer As New NaplesDataAnswer(thisNaplesDataRequest, NaplesAnswerType.Rejected, Nothing)

                            SyncLock DataUpdateLockObject
                              If PostServerMessage(thisServerConnection.MsgServerStream, errMessage) = True Then
                                Call PostServerMessage(thisServerConnection.MsgServerStream, AddTCPServerWrapperClass(errDataAnswer))

                                ' thisServerConnection.LastWakeupTime = Now()
                              End If
                            End SyncLock
                          Catch ex As Exception
                          End Try

                          Try
                            ' Remove from the Work Collection
                            If (Not (thisWorkItem Is Nothing)) Then
                              WorkCollectionCancelList.Add(thisWorkItem.ClientRequestId)
                            End If
                            'WorkCollection.Remove(thisWorkItem.ClientRequestId)
                          Catch ex As Exception
                          End Try

                      End Select

                    Case TCP_ServerMessageType.Server_MessageRejected
                      ' ******************************************************
                      ' Reply Rejected ?
                      ' 
                      ' If a data reply was rejected, then cancel the related work object, if
                      ' it exists.
                      ' ******************************************************

                      Dim RejectedDataAnswer As NaplesDataAnswer

                      ' If a Naples Data Answer object was given....

                      If (MessageHeader.FollowingMessageCount > 0) AndAlso _
                      (Not (FollowingObjects Is Nothing)) AndAlso _
                      (FollowingObjects.Length > 0) AndAlso _
                      (FollowingObjects(0).GetType Is GetType(NaplesDataAnswer)) Then

                        Try
                          RejectedDataAnswer = CType(FollowingObjects(0), NaplesDataAnswer)
                          If (Not (Guid.Empty.Equals(RejectedDataAnswer.WorkItemObjectID))) Then
                            If WorkCollection.ContainsKey(RejectedDataAnswer.WorkItemObjectID) Then
                              Dim RejectedWorkItem As ClientWorkClass
                              RejectedWorkItem = CType(WorkCollection(RejectedDataAnswer.WorkItemObjectID), ClientWorkClass)

                              If (Not (RejectedWorkItem.DataRequest Is Nothing)) Then
                                If (RejectedWorkItem.DataRequest.GetType Is GetType(DataServerRequest)) Then
                                  NaplesInitiator.Naples_DataServer.RemoveRequest(CType(RejectedWorkItem.DataRequest, DataServerRequest))
                                ElseIf (RejectedWorkItem.DataRequest.GetType Is GetType(DataServerRTItem)) Then
                                  NaplesInitiator.Naples_DataServer.RemoveRequest(CType(RejectedWorkItem.DataRequest, DataServerRTItem))
                                End If
                              End If

                              If (Not (RejectedWorkItem.DataItem Is Nothing)) Then
                                If (RejectedWorkItem.DataItem.GetType Is GetType(DataServerRequest)) Then
                                  NaplesInitiator.Naples_DataServer.RemoveRequest(CType(RejectedWorkItem.DataItem, DataServerRequest))
                                ElseIf (RejectedWorkItem.DataItem.GetType Is GetType(DataServerRTItem)) Then
                                  NaplesInitiator.Naples_DataServer.RemoveRequest(CType(RejectedWorkItem.DataItem, DataServerRTItem))
                                End If
                              End If

                              WorkCollectionCancelList.Add(RejectedDataAnswer.WorkItemObjectID)
                              'WorkCollection.Remove(RejectedDataAnswer.WorkItemObjectID)
                            End If
                          End If

                        Catch ex As Exception
                        End Try
                      End If


                    Case TCP_ServerMessageType.Server_Touchbase
                      ' ******************************************************
                      ' TouchBase Message
                      ' ******************************************************

                      SyncLock DataUpdateLockObject
                        Call PostServerMessage(thisServerConnection.MsgServerStream, TCP_ServerMessageType.Client_AcknowledgeOK, FCP_Application.Naples)
                      End SyncLock

                    Case TCP_ServerMessageType.Server_Disconnect, TCP_ServerMessageType.Client_Disconnect
                      ' ******************************************************
                      ' Disconnect, Remove this Connection from the collection
                      ' ******************************************************

                      ConnectionCollectionCancelList.Add(ThisKey)
                      'ConnectionCollection.Remove(ThisKey)

                    Case TCP_ServerMessageType.Client_RequestConnect, TCP_ServerMessageType.Server_RequestConnect
                      ' ******************************************************
                      ' Connection Requested
                      ' ******************************************************

                      ' Start a dialogue.

                      SyncLock DataUpdateLockObject
                        PostServerMessage(thisServerConnection.MsgServerStream, TCP_ServerMessageType.Client_RegisterApplication, FCP_Application.Naples)
                        PostServerMessage(thisServerConnection.MsgServerStream, TCP_ServerMessageType.Client_RegisterApplication, FCP_Application.NaplesDataServer)
                        PostServerMessage(thisServerConnection.MsgServerStream, TCP_ServerMessageType.Client_RegisterApplication, FCP_Application.Renaissance)
                        PostServerMessage(thisServerConnection.MsgServerStream, TCP_ServerMessageType.Client_RegisterApplication, FCP_Application.Venice)
                        PostServerMessage(thisServerConnection.MsgServerStream, TCP_ServerMessageType.Client_SetGUID, FCP_Application.Naples, NaplesClientID.ToString)
                      End SyncLock

                    Case TCP_ServerMessageType.Client_ApplicationUpdateMessage, TCP_ServerMessageType.Server_ApplicationUpdateMessage
                      ' ******************************************************
                      ' Application Update Message.
                      '
                      ' Expose this Message through an event.
                      ' ******************************************************

                      RaiseEvent FCPApplicationUpdate(Me, New FCPApplicationUpdateEventArgs(MessageHeader, FollowingObjects))

                    Case Else    ' MessageHeader.MessageType
                      ' Ignore other Messages

                  End Select    ' MessageHeader.MessageType


                End If    ' Not (MessageHeader Is Nothing)

                MaxMessageCounter += 1
              End While

              ' *************************************************
              ' Check for outgoing messages
              ' *************************************************

              ' *************************************************
              ' Send a TouchBase Message to the Server if no conversation recently.
              ' Close and tidy the connection objects if there is an error
              ' *************************************************

              ' 

              If (Now().Subtract(thisServerConnection.LastWakeupTime).TotalSeconds > TCPServerGlobals.FCP_CLIENT_RENEW_CONNECTION_TIMER) Then
                SyncLock DataUpdateLockObject
                  SyncLock thisServerConnection
                    Try
                      thisServerConnection.MsgServerClient.Close()
                    Catch ex As Exception
                    Finally
                      thisServerConnection.MsgServerClient = Nothing
                      thisServerConnection.MsgServerStream = Nothing
                    End Try

                    Try
                      ConnectionCollectionCancelList.Add(ThisKey)
                    Catch ex As Exception
                    End Try
                  End SyncLock
                End SyncLock

              ElseIf (Now().Subtract(thisServerConnection.LastWakeupTime).TotalSeconds > TCPServerGlobals.FCP_CLIENT_WAKEUP_TIMER) Then

                Try
                  If Now().Subtract(thisServerConnection.LastWakeupSentTime).TotalSeconds > TCPServerGlobals.FCP_CLIENT_WAKEUP_TIMER Then
                    thisServerConnection.LastWakeupSentTime = Now()

                    SyncLock DataUpdateLockObject
                      If PostServerMessage(thisServerConnection.MsgServerStream, TCP_ServerMessageType.Client_Touchbase) = False Then
                        Try
                          thisServerConnection.MsgServerClient.Close()
                        Catch ex As Exception
                        Finally
                          Try
                            thisServerConnection.MsgServerClient = Nothing
                            thisServerConnection.MsgServerStream = Nothing
                          Catch ex As Exception
                          End Try
                        End Try
                        Try
                          ConnectionCollectionCancelList.Add(ThisKey)
                        Catch ex As Exception
                        End Try
                        'Else
                        '	' Post OK
                        '	thisServerConnection.LastWakeupTime = Now()
                      End If
                    End SyncLock
                  End If

                Catch ex As Exception
                End Try

              End If
            End If

          Catch ex As Exception
          End Try

        Next ' ConnectionObject

      Catch ex As Exception
      End Try

      ' Remove work items marked for removal.

      Try
        If WorkCollectionCancelList.Count > 0 Then
          Dim CancelCounter As Integer

          SyncLock WorkCollection
            For CancelCounter = 0 To (WorkCollectionCancelList.Count - 1)
              Try
                If Not (WorkCollectionCancelList(CancelCounter) Is Nothing) Then
                  WorkCollection.Remove(WorkCollectionCancelList(CancelCounter))
                End If
              Catch ex As Exception
              End Try
            Next
          End SyncLock
        End If

        WorkCollectionCancelList.Clear()
      Catch ex As Exception
      End Try

      ' Remove Connections marked for removal
      Try
        ' Close marked connections
        If (ConnectionCollectionCancelList.Count > 0) Then
          SyncLock (ConnectionCollection)
            For Each ThisKey In ConnectionCollectionCancelList
              If (ConnectionCollection.ContainsKey(ThisKey)) Then
                thisServerConnection = ConnectionCollection(ThisKey)

                If (thisServerConnection IsNot Nothing) Then
                  Try
                    thisServerConnection.MsgServerClient.Close()
                  Catch ex As Exception
                  Finally
                    Try
                      thisServerConnection.MsgServerClient = Nothing
                      thisServerConnection.MsgServerStream = Nothing
                    Catch ex As Exception
                    End Try
                  End Try
                End If

                ConnectionCollection.Remove(ThisKey)
              End If

            Next
          End SyncLock

          ConnectionCollectionCancelList.Clear()
        End If
      Catch ex As Exception
      End Try

      ' Ensure that there is always atleast One Connection, or try repeatedly to 
      ' Open the default one if no connections exist.

      If (ConnectionCollection.Count <= 0) Then
        ' Start Initial, Default, server connection
        Dim DefaultServerConnection As New ServerConnectionClass

        ' The 'ConnectToMessageServer' function will resolve an empty Commection object to the Default
        ' Server.

        Call ConnectToMessageServer(DefaultServerConnection)

        If Not (DefaultServerConnection.MsgServerIPAddress Is IPAddress.None) Then
          ' An IP Address was resolved, add the Default connection to the Connection collection
          ConnectionCollection.Add(DefaultServerConnection.ConnectionId, DefaultServerConnection)
        End If
      ElseIf (ServerConnectionExists = False) Then

        SyncLock ConnectionCollection
          KeysCollection = ConnectionCollection.Keys

          Try
            For Each ThisKey In KeysCollection
              If ConnectionCollection.ContainsKey(ThisKey) Then
                thisServerConnection = CType(ConnectionCollection(ThisKey), ServerConnectionClass)
              Else
                thisServerConnection = Nothing
              End If

              If (thisServerConnection IsNot Nothing) Then
                ' Check that the standard message server connection exists
                If (thisServerConnection.MsgServerIPAddress.Equals(NaplesInitiator.MessageServerAddress)) Then
                  ServerConnectionExists = True
                  Exit For
                End If
              End If
            Next

            If (ServerConnectionExists = False) Then
              ' Start Initial, Default, server connection
              Dim DefaultServerConnection As New ServerConnectionClass

              ' The 'ConnectToMessageServer' function will resolve an empty Commection object to the Default
              ' Server.

              Call ConnectToMessageServer(DefaultServerConnection)

              If Not (DefaultServerConnection.MsgServerIPAddress Is IPAddress.None) Then
                ' An IP Address was resolved, add the Default connection to the Connection collection
                ConnectionCollection.Add(DefaultServerConnection.ConnectionId, DefaultServerConnection)
              End If
            End If

          Catch ex As Exception
          End Try


        End SyncLock

      End If

      ' Pause the thread briefly if the loop was idle.

      If (LoopWorkDone = False) Then
        Threading.Thread.Sleep(25)
      End If

    End While

  End Sub

  Public Sub PostApplicationUpdate(ByVal pApplication As FCP_Application, ByVal pMessageText As String)
    Dim thisConnectionObject As Object
    Dim thisServerConnection As ServerConnectionClass

    Try

      SyncLock DataUpdateLockObject
        For Each thisConnectionObject In ConnectionCollection
          Try

            thisServerConnection = CType(thisConnectionObject, ServerConnectionClass)

            PostServerMessage(thisServerConnection.MsgServerStream, TCP_ServerMessageType.Client_ApplicationUpdateMessage, pApplication, pMessageText)

          Catch ex As Exception
          End Try
        Next

      End SyncLock

    Catch ex As Exception
    End Try

  End Sub

  Private Sub DataUpdate(ByVal sender As Object, ByVal e As DataServerUpdate)

    ' *******************************************************
    ' Process Update Messages 
    '
    ' Sender is the Object that has been updated
    ' e.UpdateObjectID  = ID of the Sender Object
    ' e.ReplyObject = The Object which supplied the Data.
    ' e.BBgReply = The BBG Reply Object (Cast of the ReplyObject if appropriate)
    ' *******************************************************

    Dim thisKey As Object = Nothing
    Dim KeyList As ICollection

    Dim tempWorkItem As ClientWorkClass
    Dim thisWorkItem As ClientWorkClass

    thisWorkItem = Nothing

    ' Lock the dummy UpdateLockObject to prevent concurrent Update Events being processed.
    ' 
    ' At this time I do not want concurrent Updates processed in order to prevent out-of-sequence messages
    ' being sent to a server in the event of two requests being responded to simultaneously.
    '
    SyncLock DataUpdateLockObject

      ' ************************************
      ' If no Work Objects exist, then cancel the request.
      ' ************************************

      If WorkCollection.Count <= 0 Then
        ' RemoveRequest
        If (sender.GetType Is GetType(DataServerRequest)) Then
          NaplesInitiator.Naples_DataServer.RemoveRequest(CType(sender, DataServerRequest))
        ElseIf (sender.GetType Is GetType(DataServerRTItem)) Then
          NaplesInitiator.Naples_DataServer.RemoveRequest(CType(sender, DataServerRTItem))
        End If

        Exit Sub
      End If

      ' ************************************
      ' Find the Related Work Object
      ' ************************************

      SyncLock WorkCollection
        KeyList = WorkCollection.Keys
        For Each thisKey In KeyList
          Try
            tempWorkItem = CType(WorkCollection(thisKey), ClientWorkClass)

            If (Not (tempWorkItem.DataRequestID.Equals(Guid.Empty))) AndAlso (tempWorkItem.DataRequestID.Equals(e.UpdateObjectID)) Then
              thisWorkItem = tempWorkItem
              Exit For
            End If

            If (Not (tempWorkItem.DataItemID.Equals(Guid.Empty))) AndAlso (tempWorkItem.DataItemID.Equals(e.UpdateObjectID)) Then
              thisWorkItem = tempWorkItem
              Exit For
            End If

          Catch ex As Exception
          End Try
        Next
      End SyncLock

      If (thisWorkItem Is Nothing) Then
        ' Can't find a related work item, Cancel the event

        If (sender.GetType Is GetType(DataServerRequest)) Then
          NaplesInitiator.Naples_DataServer.RemoveRequest(CType(sender, DataServerRequest))
        ElseIf (sender.GetType Is GetType(DataServerRTItem)) Then
          NaplesInitiator.Naples_DataServer.RemoveRequest(CType(sender, DataServerRTItem))
        End If

        Exit Sub
      End If

      ' ************************************
      ' OK, Work Item Found.
      ' ************************************

      If sender.GetType Is GetType(DataServerRequest) Then

        ' This Object is the Generic internal Request Object

        Dim thisDataServerRequest As DataServerRequest
        Dim thisDataServerUpdate As DataServerUpdate

        thisDataServerRequest = CType(sender, DataServerRequest)
        thisDataServerUpdate = e

        ' ************************************
        ' React according to the Request Type...
        ' ************************************

        Select Case thisDataServerRequest.RequestType

          Case DS_RequestType.API_Static
            ' ************************************
            ' 
            ' OK, this should be an Update delivering the requested data. 
            ' ************************************
            Dim ReplyHeader As TCPServerHeader
            Dim ReplyData As NaplesDataAnswer

            Select Case thisDataServerRequest.RequestStatus
              Case DS_RequestStatus.RequestComplete
                ReplyHeader = New TCPServerHeader(TCP_ServerMessageType.Server_NaplesDataAnswer, FCP_Application.NaplesDataServer, thisWorkItem.MessageHeader.MessageString, 1, 0, New ArrayList, thisWorkItem.MessageHeader.PathTravelled)
                ReplyData = New NaplesDataAnswer(thisWorkItem.MessageBody, NaplesAnswerType.API_Static, thisDataServerRequest.ReturnObject, thisWorkItem.ClientRequestId)
                ' ReturnObject should be a Hashtable of DataServerRTField objects

                SyncLock DataUpdateLockObject
                  If PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, ReplyHeader) Then
                    PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, AddTCPServerWrapperClass(ReplyData))
                  End If
                End SyncLock


              Case DS_RequestStatus.RequestFailed
                ReplyHeader = New TCPServerHeader(TCP_ServerMessageType.Server_NaplesDataAnswer, FCP_Application.NaplesDataServer, thisWorkItem.MessageHeader.MessageString, 1, 0, New ArrayList, thisWorkItem.MessageHeader.PathTravelled)
                ReplyData = New NaplesDataAnswer(thisWorkItem.MessageBody, NaplesAnswerType.Failed, thisDataServerRequest.ReturnObject, thisWorkItem.ClientRequestId)

                SyncLock DataUpdateLockObject
                  If PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, ReplyHeader) Then
                    PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, AddTCPServerWrapperClass(ReplyData))
                  End If
                End SyncLock

              Case DS_RequestStatus.RequestRejected
                ReplyHeader = New TCPServerHeader(TCP_ServerMessageType.Server_NaplesDataAnswer, FCP_Application.NaplesDataServer, thisWorkItem.MessageHeader.MessageString, 1, 0, New ArrayList, thisWorkItem.MessageHeader.PathTravelled)
                ReplyData = New NaplesDataAnswer(thisWorkItem.MessageBody, NaplesAnswerType.Rejected, thisDataServerRequest.ReturnObject, thisWorkItem.ClientRequestId)

                SyncLock DataUpdateLockObject
                  If PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, ReplyHeader) Then
                    PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, AddTCPServerWrapperClass(ReplyData))
                  End If
                End SyncLock

              Case DS_RequestStatus.RequestPending, DS_RequestStatus.RequestSubmitted
                ' Ignore, shouldn't happen, don't care (?)

              Case DS_RequestStatus.None
                ' Ignore, shouldn't happen.

              Case Else
                ' Ignore, shouldn't happen.

            End Select

            ' Remove Work Item ?

            Select Case thisDataServerRequest.RequestStatus
              Case DS_RequestStatus.RequestPending, DS_RequestStatus.RequestSubmitted, DS_RequestStatus.None
                ' Let it live.

              Case Else
                ' Item Done, 
                Try
                  If (Not (thisKey Is Nothing)) Then
                    SyncLock WorkCollection
                      WorkCollection.Remove(thisKey)
                    End SyncLock
                  End If
                Catch ex As Exception
                End Try

                ' Remove From Data Server. Should be superfluous !

                Me.NaplesInitiator.Naples_DataServer.RemoveRequest(thisDataServerRequest)
            End Select

          Case DS_RequestType.API_Realtime
            ' ************************************
            ' 
            ' This should be an update delivering the reference to the Realtime data object or 
            ' ************************************
            Dim ReplyHeader As TCPServerHeader
            Dim ReplyData As NaplesDataAnswer

            Select Case thisDataServerRequest.RequestStatus

              Case DS_RequestStatus.RequestComplete
                ' OK We have a Realtime Request marked as complete. 
                ' This should not happen as Realtime requests will drop the DataServerRequest with a 'Submitted' Status or
                ' an error statis.
                ' Since this has happened, treat it like a static request.
                ReplyHeader = New TCPServerHeader(TCP_ServerMessageType.Server_NaplesDataAnswer, FCP_Application.NaplesDataServer, thisWorkItem.MessageHeader.MessageString, 1, 0, New ArrayList, thisWorkItem.MessageHeader.PathTravelled)
                ReplyData = New NaplesDataAnswer(thisWorkItem.MessageBody, NaplesAnswerType.API_Static, thisDataServerRequest.ReturnObject, thisWorkItem.ClientRequestId)

                SyncLock DataUpdateLockObject
                  If PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, ReplyHeader) Then
                    PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, AddTCPServerWrapperClass(ReplyData))
                  End If
                End SyncLock


              Case DS_RequestStatus.RequestPending, DS_RequestStatus.RequestSubmitted
                ' This Event should be delivering a reference to the RT Data Object.
                ' Anything else will be taken badly...

                If thisDataServerRequest.ReturnObject.GetType Is GetType(DataServerRTItem) Then
                  Dim thisDataServerRTItem As DataServerRTItem
                  Try
                    thisDataServerRTItem = CType(thisDataServerRequest.ReturnObject, DataServerRTItem)

                    thisWorkItem.DataItem = thisDataServerRTItem
                    thisWorkItem.DataItemID = thisDataServerRTItem.ItemId
                    AddHandler thisDataServerRTItem.DataUpdate, AddressOf Me.DataUpdate

                  Catch ex As Exception
                    thisDataServerRequest.RequestStatus = DS_RequestStatus.RequestFailed
                    ReplyHeader = New TCPServerHeader(TCP_ServerMessageType.Server_NaplesDataAnswer, FCP_Application.NaplesDataServer, thisWorkItem.MessageHeader.MessageString, 1, 0, New ArrayList, thisWorkItem.MessageHeader.PathTravelled)
                    ReplyData = New NaplesDataAnswer(thisWorkItem.MessageBody, NaplesAnswerType.Failed, thisDataServerRequest.ReturnObject, thisWorkItem.ClientRequestId)

                    SyncLock DataUpdateLockObject
                      If PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, ReplyHeader) Then
                        PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, AddTCPServerWrapperClass(ReplyData))
                      End If
                    End SyncLock

                  End Try

                Else
                  ' Error ?
                  thisDataServerRequest.RequestStatus = DS_RequestStatus.RequestFailed
                  ReplyHeader = New TCPServerHeader(TCP_ServerMessageType.Server_NaplesDataAnswer, FCP_Application.NaplesDataServer, thisWorkItem.MessageHeader.MessageString, 1, 0, New ArrayList, thisWorkItem.MessageHeader.PathTravelled)
                  ReplyData = New NaplesDataAnswer(thisWorkItem.MessageBody, NaplesAnswerType.Failed, thisDataServerRequest.ReturnObject, thisWorkItem.ClientRequestId)

                  SyncLock DataUpdateLockObject
                    If PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, ReplyHeader) Then
                      PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, AddTCPServerWrapperClass(ReplyData))
                    End If
                  End SyncLock

                End If

              Case DS_RequestStatus.RequestFailed
                ReplyHeader = New TCPServerHeader(TCP_ServerMessageType.Server_NaplesDataAnswer, FCP_Application.NaplesDataServer, thisWorkItem.MessageHeader.MessageString, 1, 0, New ArrayList, thisWorkItem.MessageHeader.PathTravelled)
                ReplyData = New NaplesDataAnswer(thisWorkItem.MessageBody, NaplesAnswerType.Failed, thisDataServerRequest.ReturnObject, thisWorkItem.ClientRequestId)

                SyncLock DataUpdateLockObject
                  If PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, ReplyHeader) Then
                    PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, AddTCPServerWrapperClass(ReplyData))
                  End If
                End SyncLock

              Case DS_RequestStatus.RequestRejected
                ReplyHeader = New TCPServerHeader(TCP_ServerMessageType.Server_NaplesDataAnswer, FCP_Application.NaplesDataServer, thisWorkItem.MessageHeader.MessageString, 1, 0, New ArrayList, thisWorkItem.MessageHeader.PathTravelled)
                ReplyData = New NaplesDataAnswer(thisWorkItem.MessageBody, NaplesAnswerType.Rejected, thisDataServerRequest.ReturnObject, thisWorkItem.ClientRequestId)

                SyncLock DataUpdateLockObject
                  If PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, ReplyHeader) Then
                    PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, AddTCPServerWrapperClass(ReplyData))
                  End If
                End SyncLock

              Case DS_RequestStatus.None
                ' Ignore.

            End Select

            ' Remove Item ?
            Select Case thisDataServerRequest.RequestStatus
              Case DS_RequestStatus.RequestPending, DS_RequestStatus.RequestSubmitted, DS_RequestStatus.None
                ' Let it live.

              Case Else
                ' Item Done, 
                Try
                  SyncLock WorkCollection
                    WorkCollection.Remove(thisKey)
                  End SyncLock
                Catch ex As Exception
                End Try

                Me.NaplesInitiator.Naples_DataServer.RemoveRequest(thisDataServerRequest)

            End Select

          Case DS_RequestType.DDE_ScreenShot
            ' ************************************
            ' 
            ' ************************************



          Case DS_RequestType.DDE_ScreenCopy
            ' ************************************
            ' 
            ' ************************************
            Dim ReplyHeader As TCPServerHeader
            Dim ReplyData As NaplesDataAnswer

            ' Remove Work Item ?

            Select Case thisDataServerRequest.RequestStatus
              Case DS_RequestStatus.RequestPending, DS_RequestStatus.RequestSubmitted, DS_RequestStatus.None
                ' Let it live.

              Case Else

                ' Send Request Response
                Try
                  ReplyHeader = New TCPServerHeader(TCP_ServerMessageType.Server_NaplesDataAnswer, FCP_Application.NaplesDataServer, thisWorkItem.MessageHeader.MessageString, 1, 0, New ArrayList, thisWorkItem.MessageHeader.PathTravelled)

                  If (thisDataServerRequest.RequestStatus = DS_RequestStatus.RequestComplete) Then
                    ReplyData = New NaplesDataAnswer(thisWorkItem.MessageBody, NaplesAnswerType.Complete, thisDataServerUpdate.ReplyObject, thisWorkItem.ClientRequestId)
                  Else
                    ReplyData = New NaplesDataAnswer(thisWorkItem.MessageBody, NaplesAnswerType.Failed, thisDataServerUpdate.ReplyObject, thisWorkItem.ClientRequestId)
                  End If

                  SyncLock DataUpdateLockObject
                    If PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, ReplyHeader) Then
                      PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, AddTCPServerWrapperClass(ReplyData))
                    End If
                  End SyncLock

                Catch ex As Exception
                End Try

                ' Item Done, remove from collection
                Try
                  SyncLock WorkCollection
                    WorkCollection.Remove(thisKey)
                  End SyncLock
                Catch ex As Exception
                End Try

                '' Remove From Data Server. Should be superfluous !
                'Me.NaplesInitiator.Naples_DataServer.RemoveRequest(thisDataServerRequest)
            End Select



          Case DS_RequestType.DDE_PostCommand, DS_RequestType.DDE_GetPortfolio
            ' ************************************
            ' 
            ' ************************************
            Dim ReplyHeader As TCPServerHeader
            Dim ReplyData As NaplesDataAnswer

            ' Remove Work Item ?

            Select Case thisDataServerRequest.RequestStatus
              Case DS_RequestStatus.RequestPending, DS_RequestStatus.RequestSubmitted, DS_RequestStatus.None
                ' Let it live.

              Case Else

                ' Send Request Response
                Try
                  ReplyHeader = New TCPServerHeader(TCP_ServerMessageType.Server_NaplesDataAnswer, FCP_Application.NaplesDataServer, thisWorkItem.MessageHeader.MessageString, 1, 0, New ArrayList, thisWorkItem.MessageHeader.PathTravelled)

                  If (thisDataServerRequest.RequestStatus = DS_RequestStatus.RequestComplete) Then
                    ReplyData = New NaplesDataAnswer(thisWorkItem.MessageBody, NaplesAnswerType.Complete, thisDataServerUpdate.ReplyObject, thisWorkItem.ClientRequestId)
                  Else
                    ReplyData = New NaplesDataAnswer(thisWorkItem.MessageBody, NaplesAnswerType.Failed, thisDataServerUpdate.ReplyObject, thisWorkItem.ClientRequestId)
                  End If

                  SyncLock DataUpdateLockObject
                    If PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, ReplyHeader) Then
                      PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, AddTCPServerWrapperClass(ReplyData))
                    End If
                  End SyncLock

                Catch ex As Exception
                End Try

                ' Item Done, remove from collection
                Try
                  SyncLock WorkCollection
                    WorkCollection.Remove(thisKey)
                  End SyncLock
                Catch ex As Exception
                End Try

                '' Remove From Data Server. Should be superfluous !
                'Me.NaplesInitiator.Naples_DataServer.RemoveRequest(thisDataServerRequest)
            End Select


          Case Else
            ' ************************************
            ' 
            ' ************************************


        End Select



      ElseIf sender.GetType Is GetType(DataServerRTItem) Then
        ' This Object is specific to the BBG API Data Server
        ' It delivers an RT Update.

        Dim thisRTItem As DataServerRTItem
        thisRTItem = CType(sender, DataServerRTItem)

        Dim ReplyHeader As TCPServerHeader
        Dim ReplyData As NaplesDataAnswer

        Select Case thisRTItem.ItemStatus
          Case DS_ObjectStatus.Live
            ReplyHeader = New TCPServerHeader(TCP_ServerMessageType.Server_NaplesDataAnswer, FCP_Application.NaplesDataServer, thisWorkItem.MessageHeader.MessageString, 1, 0, New ArrayList, thisWorkItem.MessageHeader.PathTravelled)
            ReplyData = New NaplesDataAnswer(thisWorkItem.MessageBody, NaplesAnswerType.API_Realtime, thisRTItem.Fields, thisWorkItem.ClientRequestId)
            ' ReturnObject should be a Hashtable of DataServerRTField objects

            SyncLock DataUpdateLockObject
              If PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, ReplyHeader) Then
                PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, AddTCPServerWrapperClass(ReplyData))
              End If
            End SyncLock


          Case DS_ObjectStatus.Pending

          Case DS_ObjectStatus.Dead
            ReplyHeader = New TCPServerHeader(TCP_ServerMessageType.Server_NaplesDataAnswer, FCP_Application.NaplesDataServer, thisWorkItem.MessageHeader.MessageString, 1, 0, New ArrayList, thisWorkItem.MessageHeader.PathTravelled)
            ReplyData = New NaplesDataAnswer(thisWorkItem.MessageBody, NaplesAnswerType.Complete, thisRTItem.Fields, thisWorkItem.ClientRequestId)

            SyncLock DataUpdateLockObject
              If PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, ReplyHeader) Then
                PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, AddTCPServerWrapperClass(ReplyData))
              End If
            End SyncLock

          Case DS_ObjectStatus.None
            ' Since this object is about to removed, send a 'Failed' reply
            ReplyHeader = New TCPServerHeader(TCP_ServerMessageType.Server_NaplesDataAnswer, FCP_Application.NaplesDataServer, thisWorkItem.MessageHeader.MessageString, 1, 0, New ArrayList, thisWorkItem.MessageHeader.PathTravelled)
            ReplyData = New NaplesDataAnswer(thisWorkItem.MessageBody, NaplesAnswerType.Failed, thisRTItem.Fields, thisWorkItem.ClientRequestId)

            SyncLock DataUpdateLockObject
              If PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, ReplyHeader) Then
                PostServerMessage(thisWorkItem.ServerConnection.MsgServerStream, AddTCPServerWrapperClass(ReplyData))
              End If
            End SyncLock

          Case Else

        End Select

        ' Remove Objects if dead.
        Select Case thisRTItem.ItemStatus
          Case DS_ObjectStatus.Dead, DS_ObjectStatus.None
            ' Item Done, 
            Try
              SyncLock WorkCollection
                WorkCollection.Remove(thisKey)
              End SyncLock
            Catch ex As Exception
            End Try

            Me.NaplesInitiator.Naples_DataServer.RemoveRequest(thisRTItem)

        End Select


        'ElseIf sender.GetType Is GetType(DDEServerData) Then
        ' *************************************************
        '
        ' *************************************************




      Else
        ' *************************************************
        '
        ' *************************************************




      End If

    End SyncLock

  End Sub

  Private Function ConnectToMessageServer(ByVal pServerConnection As ServerConnectionClass) As Boolean

    ' *******************************************************
    ' If the Stream appears to be valid, then Exit OK.
    ' *******************************************************

    If Not (pServerConnection.MsgServerStream Is Nothing) Then
      Return True
      Exit Function
    End If

    ' *******************************************************
    ' If there is no IP Address Set then try to resolve one.
    ' *******************************************************

    If (pServerConnection.MsgServerIPAddress.Equals(IPAddress.None)) Then

      ' *******************************************************
      ' Use the startup parameter if resolved. Held in the Initiator Object
      ' *******************************************************

      Try
        If Not NaplesInitiator.MessageServerAddress.Equals(IPAddress.None) Then
          pServerConnection.MsgServerIPAddress = NaplesInitiator.MessageServerAddress
          pServerConnection.MsgServerName = NaplesInitiator.MessageServerName
        End If
      Catch ex As Exception
        pServerConnection.MsgServerIPAddress = IPAddress.None
        pServerConnection.MsgServerName = ""
      End Try

      ' *******************************************************
      ' If the Startup parameter was not given, or it did not resolve
      ' to a valid IP Address, then look in the Registry.
      ' Default the registry value to 'localhost'.
      ' *******************************************************

      If (pServerConnection.MsgServerIPAddress.Equals(IPAddress.None)) Then
        ' Try the Registry
        pServerConnection.MsgServerName = CType(Get_RegistryItem(Registry.CurrentUser, REGISTRY_BASE & "\MessageServer", "ServerName", "localhost"), String)
        If (pServerConnection.MsgServerName = "localhost") Then
          pServerConnection.MsgServerName = CType(Get_RegistryItem(Registry.LocalMachine, REGISTRY_BASE & "\MessageServer", "ServerName", "localhost"), String)
        End If

        Try
          pServerConnection.MsgServerIPAddress = TCPServer_Common.NetworkFunctions.GetServerAddress(pServerConnection.MsgServerName)
        Catch ex As Exception
          pServerConnection.MsgServerIPAddress = IPAddress.None
          pServerConnection.MsgServerName = ""
        End Try
      End If

      ' *******************************************************
      ' If there is still no IP Address resolved, then Exit with False.
      ' *******************************************************

      If (pServerConnection.MsgServerIPAddress.Equals(IPAddress.None)) Then
        Return False
        Exit Function
      End If
    End If

    ' *******************************************************
    ' Initiate the Connection.
    ' *******************************************************

    Try
      pServerConnection.MsgServerClient = New TcpClient
      pServerConnection.MsgServerClient.ReceiveTimeout = TCPServerGlobals.FCP_Server_ReceiveTimeout
      pServerConnection.MsgServerClient.SendTimeout = TCPServerGlobals.FCP_Server_SendTimeout
			pServerConnection.MsgServerClient.NoDelay = True
      pServerConnection.MsgServerClient.ReceiveBufferSize = CInt(2 ^ 14) ' 16K
      pServerConnection.MsgServerClient.SendBufferSize = CInt(2 ^ 14)  ' 16K

      pServerConnection.MsgServerClient.Connect(pServerConnection.MsgServerIPAddress, TCPServerGlobals.FCP_ServerPort)

      pServerConnection.MsgServerStream = pServerConnection.MsgServerClient.GetStream()

      ' Start a dialogue.

      SyncLock DataUpdateLockObject
        PostServerMessage(pServerConnection.MsgServerStream, TCP_ServerMessageType.Client_RequestConnect, FCP_Application.Naples)
        PostServerMessage(pServerConnection.MsgServerStream, TCP_ServerMessageType.Client_RegisterApplication, FCP_Application.Naples)
        PostServerMessage(pServerConnection.MsgServerStream, TCP_ServerMessageType.Client_RegisterApplication, FCP_Application.NaplesDataServer)
        PostServerMessage(pServerConnection.MsgServerStream, TCP_ServerMessageType.Client_RegisterApplication, FCP_Application.Renaissance)
        PostServerMessage(pServerConnection.MsgServerStream, TCP_ServerMessageType.Client_RegisterApplication, FCP_Application.Venice)
        PostServerMessage(pServerConnection.MsgServerStream, TCP_ServerMessageType.Client_SetGUID, FCP_Application.Naples, NaplesClientID.ToString)
      End SyncLock


    Catch ex As Exception

      Try
        pServerConnection.MsgServerClient.Close()
      Catch ex1 As Exception
      Finally
        pServerConnection.MsgServerStream = Nothing
      End Try

      Return False
      Exit Function
    End Try

    Return True

  End Function

  Public Function AddTCPServerWrapperClass(ByRef pSourceObject As Object) As TCPServerObjectWrapper
    Dim SerializeFormatter As New BinaryFormatter
    Dim ThisWrapperObject As TCPServerObjectWrapper

    If (Not (pSourceObject.GetType Is GetType(TCPServerObjectWrapper))) Then
      Try
        ThisWrapperObject = New TCPServerObjectWrapper
        ThisWrapperObject.ObjectType = pSourceObject.GetType.Name
        ThisWrapperObject.ObjectFormat = "BinaryFormatter"

        Dim ms As New MemoryStream
        SerializeFormatter.Serialize(ms, pSourceObject)
        ms.Position = 0
        ThisWrapperObject.ObjectData = ms.ToArray()
        ms.Close()
      Catch ex As Exception
        ThisWrapperObject = New TCPServerObjectWrapper
      End Try

    Else
      ThisWrapperObject = pSourceObject
    End If

    Return ThisWrapperObject

  End Function


  Private Class ServerConnectionClass
    ' *************************************************
    ' Simple class used to manage connection to the TCP Cient.
    ' *************************************************

    Private _ConnectionId As Guid

    Public MsgServerName As String
    Public MsgServerIPAddress As IPAddress
    Public MsgServerStream As NetworkStream
    Public MsgServerClient As TcpClient
    Public LastWakeupTime As Date
    Public LastWakeupSentTime As Date

    Public Sub New()
      Me._ConnectionId = Guid.NewGuid
      MsgServerName = ""
      MsgServerIPAddress = IPAddress.None
      MsgServerStream = Nothing
      MsgServerClient = Nothing
      LastWakeupTime = Now()
      LastWakeupSentTime = Now()
    End Sub

    Public Sub New(ByVal pMsgServerName As String, ByVal pMsgServerIPAddress As IPAddress)
      Me.New()
      Me.MsgServerName = pMsgServerName
      Me.MsgServerIPAddress = pMsgServerIPAddress
    End Sub

    Public ReadOnly Property ConnectionId() As Guid
      Get
        Return Me._ConnectionId
      End Get
    End Property
  End Class

  Private Class ClientWorkClass
    ' *************************************************
    ' SImple Class used to manage active work items being processed bt the TCP Client / Data Servers
    ' *************************************************

    Public ClientRequestId As Guid

    Public ServerConnection As ServerConnectionClass

    Public MessageHeader As TCPServerHeader   ' 
    Public MessageBody As Object  ' e.g. NaplesDataRequest object

    Public DataRequestID As Guid
    Public DataRequest As Object  ' e.g. DataServerRequest object
    Public DataItemID As Guid
    Public DataItem As Object  ' e.g. DataServerRTItem object

    Public Sub New(ByRef pServerConnection As ServerConnectionClass, ByVal pMessageHeader As TCPServerHeader, ByVal pMessageBody As Object, ByVal pClientRequestId As Guid)
      Me.ClientRequestId = pClientRequestId
      Me.ServerConnection = pServerConnection
      Me.MessageHeader = pMessageHeader
      Me.MessageBody = pMessageBody
    End Sub

    Public Sub New(ByRef pServerConnection As ServerConnectionClass, ByVal pMessageHeader As TCPServerHeader, ByVal pMessageBody As Object)
      Me.MessageHeader = pMessageHeader
      Me.MessageBody = pMessageBody
      Me.ServerConnection = pServerConnection
      Me.ClientRequestId = Guid.NewGuid

      Me.DataRequestID = Guid.Empty
      Me.DataRequest = Nothing
      Me.DataItemID = Guid.Empty
      Me.DataItem = Nothing

      If Not (pMessageBody Is Nothing) Then
        If (pMessageBody.GetType Is GetType(NaplesDataRequest)) Then
          Me.ClientRequestId = CType(pMessageBody, NaplesDataRequest).RequestID
        End If
      End If
    End Sub

    Public Sub New(ByRef pServerConnection As ServerConnectionClass, ByRef pMessageHeader As TCPServerHeader, ByRef pMessageBody As Object, ByVal pRequestID As Guid, ByRef pDataRequest As Object)
      Me.New(pServerConnection, pMessageHeader, pMessageBody)
      Me.DataRequest = pDataRequest
      Me.DataRequestID = pRequestID
    End Sub

    Private Sub New()
      ' Enforce the use of Constructor arguments
    End Sub

  End Class

  Private Class TCPClientLockObject
    ' *************************************************
    ' Dummy object, used as an object to lock.
    ' *************************************************

    Private x As Integer

  End Class


  Public Class FCPApplicationUpdateEventArgs
    '
    Inherits EventArgs

    Private _MessageHeader As TCPServerHeader
    Private _FollowingObjects As Object()

    ' Basic Constructor
    Private Sub New()
      MyBase.new()
    End Sub

    ' Constructor that also sets the initial Table-Changed value
    Public Sub New(ByVal pMessageHeader As TCPServerHeader, ByVal pFollowingObjects As Object())
      MyBase.new()
      _MessageHeader = pMessageHeader
      _FollowingObjects = pFollowingObjects
    End Sub

    Public ReadOnly Property MessageHeader() As TCPServerHeader
      Get
        Return _MessageHeader
      End Get
    End Property

    Public ReadOnly Property FollowingObjects() As Object()
      Get
        Return _FollowingObjects
      End Get
    End Property

  End Class

End Class
