EXEC adp_tblMarketInstruments_InsertCommand 0, 'A UN Equity', 'AGILENT TECHNOLOGIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'A3TV SQ Equity', 'ANTENA 3 TELEVISION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AA UN Equity', 'ALCOA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AABA NA Equity', 'ABN AMRO HOLDING NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AACC UW Equity', 'ASSET ACCEPTANCE CAPITAL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AAL LN Equity', 'ANGLO AMERICAN PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AAME UQ Equity', 'ATLANTIC AMERICAN CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AANB UQ Equity', 'ABIGAIL ADAMS NATL BANCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AAON UW Equity', 'AAON INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AAP UN Equity', 'ADVANCE AUTO PARTS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AAPL UW Equity', 'APPLE COMPUTER INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AATI UW Equity', 'ADVANCED ANALOGIC TECHNOLOGI', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AATK UR Equity', 'AMERICAN ACCESS TECHNOLOGIES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AAUK UR Equity', 'ANGLO AMERICAN PLC-UNSP ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AAWW UW Equity', 'ATLAS AIR WORLDWIDE HOLDINGS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AB/ LN Equity', 'ALLIANCE BOOTS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ABAX UW Equity', 'ABAXIS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ABBC UQ Equity', 'ABINGTON COMMUNITY BANCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ABBI UW Equity', 'ABRAXIS BIOSCIENCE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ABBN VX Equity', 'ABB LTD-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ABC UN Equity', 'AMERISOURCEBERGEN CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ABCB UW Equity', 'AMERIS BANCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ABCO UW Equity', 'ADVISORY BOARD CO/THE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ABCW UW Equity', 'ANCHOR BANCORP WISCONSIN INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ABE SQ Equity', 'ABERTIS INFRAESTRUCTURAS SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ABER UR Equity', 'ABER DIAMOND CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ABF LN Equity', 'ASSOCIATED BRITISH FOODS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ABFS UW Equity', 'ARKANSAS BEST CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ABI UN Equity', 'APPLIED BIOSYSTEMS GROUP-APP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ABIX UR Equity', 'ABATIX CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ABK UN Equity', 'AMBAC FINANCIAL GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ABMC UR Equity', 'AMERICAN BIO MEDICA CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ABMD UQ Equity', 'ABIOMED INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ABNJ UQ Equity', 'AMERICAN BANCORP OF NJ', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ABPI UQ Equity', 'ACCENTIA BIOPHARMACEUTICALS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ABT UN Equity', 'ABBOTT LABORATORIES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ABTL UQ Equity', 'AUTOBYTEL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ABVA UR Equity', 'ALLIANCE BANKSHARES CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ABXA UQ Equity', 'ABX AIR INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AC FP Equity', 'ACCOR SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACA FP Equity', 'CREDIT AGRICOLE SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACAD UQ Equity', 'ACADIA PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACAM UQ Equity', 'ACAMBIS PLC-SPONS ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACAP UQ Equity', 'AMERICAN PHYSICIANS CAP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACAS UW Equity', 'AMERICAN CAPITAL STRATEGIES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACAT UW Equity', 'ARCTIC CAT INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACBA UR Equity', 'AMERICAN COMMUNITY BNCSHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACCL UQ Equity', 'ACCELRYS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACE UN Equity', 'ACE LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACEC UR Equity', 'ACE*COMM CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACEL UR Equity', 'ALFACELL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACET UW Equity', 'ACETO CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACF UN Equity', 'AMERICREDIT CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACFC UQ Equity', 'ATLANTIC COAST FEDERAL/WAYCR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACGL UW Equity', 'ARCH CAPITAL GROUP LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACGY UW Equity', 'ACERGY SA-SPON ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACHN UQ Equity', 'ACHILLION PHARMACEUTICALS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACI UN Equity', 'ARCH COAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACLI UW Equity', 'AMERICAN COMMERCIAL LINES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACLS UQ Equity', 'AXCELIS TECHNOLOGIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACME UQ Equity', 'ACME COMMUNICATIONS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACMR UW Equity', 'AC MOORE ARTS & CRAFTS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACOR UQ Equity', 'ACORDA THERAPEUTICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACPW UQ Equity', 'ACTIVE POWER INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACS SQ Equity', 'ACS ACTIVIDADES CONS Y SERV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACS UN Equity', 'AFFILIATED COMPUTER SVCS-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACSEF UR Equity', 'ACS MOTION CONTROL LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACTG UQ Equity', 'ACACIA RESEARCH - ACACIA TEC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACTI UQ Equity', 'ACTIVIDENTITY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACTL UQ Equity', 'ACTEL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACTS UQ Equity', 'ACTIONS SEMICONDUCTOR CO-ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACTU UQ Equity', 'ACTUATE CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACUS UQ Equity', 'ACUSPHERE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACV UN Equity', 'ALBERTO-CULVER CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACX SQ Equity', 'ACERINOX SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACXM UW Equity', 'ACXIOM CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ACY NO Equity', 'ACERGY SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADAM UR Equity', 'A.D.A.M. INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADAT UQ Equity', 'AUTHENTIDATE HOLDING CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADBE UW Equity', 'ADOBE SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADBL UQ Equity', 'AUDIBLE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADCT UW Equity', 'ADC TELECOMMUNICATIONS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADEN VX Equity', 'ADECCO SA-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADEP UQ Equity', 'ADEPT TECHNOLOGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADES UR Equity', 'ADA-ES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADI UN Equity', 'ANALOG DEVICES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADLR UQ Equity', 'ADOLOR CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADLS UQ Equity', 'ADVANCED LIFE SCIENCES HOLD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADM LN Equity', 'ADMIRAL GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADM UN Equity', 'ARCHER-DANIELS-MIDLAND CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADP UN Equity', 'AUTOMATIC DATA PROCESSING', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADPI UW Equity', 'AMERICAN DENTAL PARTNERS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADPT UQ Equity', 'ADAPTEC INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADS GY Equity', 'ADIDAS AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADS UN Equity', 'ALLIANCE DATA SYSTEMS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADSK UW Equity', 'AUTODESK INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADST UR Equity', 'ADSTAR INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADSX UR Equity', 'APPLIED DIGITAL SOLUTIONS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADTN UW Equity', 'ADTRAN INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADVNA UW Equity', 'ADVANTA CORP-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADVNB UW Equity', 'ADVANTA CORP-CL B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADVS UW Equity', 'ADVENT SOFTWARE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ADZA UW Equity', 'ADEZA BIOMEDICAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AEE UN Equity', 'AMEREN CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AEHR UQ Equity', 'AEHR TEST SYSTEMS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AEIS UQ Equity', 'ADVANCED ENERGY INDUSTRIES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AEM IM Equity', 'AEM SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AEOS UW Equity', 'AMERICAN EAGLE OUTFITTERS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AEP UN Equity', 'AMERICAN ELECTRIC POWER', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AEPI UQ Equity', 'AEP INDUSTRIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AERT UR Equity', 'ADVANCED ENVIRONMENTAL REC-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AES UN Equity', 'AES CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AET UN Equity', 'AETNA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AEZS UQ Equity', 'AETERNA ZENTARIS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AF FP Equity', 'AIR FRANCE-KLM', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AF UN Equity', 'ASTORIA FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AFAM UR Equity', 'ALMOST FAMILY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AFCE UQ Equity', 'AFC ENTERPRISES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AFFM UW Equity', 'AFFIRMATIVE INSURANCE HOLDIN', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AFFX UW Equity', 'AFFYMETRIX INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AFG UN Equity', 'AMERICAN FINANCIAL GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AFL UN Equity', 'AFLAC INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AFOP UR Equity', 'ALLIANCE FIBER OPTIC PRODUCT', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AFSI UQ Equity', 'AMTRUST FINANCIAL SERVICES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AG UN Equity', 'AGCO CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AGE UN Equity', 'EDWARDS (A.G.) INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AGEN UQ Equity', 'ANTIGENICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AGF FP Equity', 'AGF - ASSUR GEN DE FRANCE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AGFB BB Equity', 'AGFA GEVAERT NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AGII UW Equity', 'ARGONAUT GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AGIL UQ Equity', 'AGILE SOFTWARE CORP /DE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AGIX UQ Equity', 'ATHEROGENICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AGL IM Equity', 'AUTOGRILL SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AGN NA Equity', 'AEGON NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AGN UN Equity', 'ALLERGAN INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AGR UN Equity', 'AGERE SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AGS LN Equity', 'AEGIS GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AGYS UW Equity', 'AGILYSYS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AH NA Equity', 'KONINKLIJKE AHOLD NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AH UN Equity', 'ARMOR HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AHCI UW Equity', 'ALLIED HEALTHCARE INTL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AHGP UW Equity', 'ALLIANCE HOLDINGS GP LP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AHPI UQ Equity', 'ALLIED HEALTHCARE PRODUCTS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AI FP Equity', 'AIR LIQUIDE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AIG UN Equity', 'AMERICAN INTERNATIONAL GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AINN UQ Equity', 'APPLIED INNOVATION INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AIRM UW Equity', 'AIR METHODS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AIRN UQ Equity', 'AIRSPAN NETWORKS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AIRT UR Equity', 'AIR T INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AIV UN Equity', 'APARTMENT INVT & MGMT CO -A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AIXD UQ Equity', 'ACCESS INTEGRATED TECHNOLO-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AIXG UR Equity', 'AIXTRON AG-SPONSORED ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AIZ UN Equity', 'ASSURANT INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AJG UN Equity', 'ARTHUR J GALLAGHER & CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AKAM UW Equity', 'AKAMAI TECHNOLOGIES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AKE FP Equity', 'ARKEMA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AKVER NO Equity', 'AKER KVAERNER', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AKZA NA Equity', 'AKZO NOBEL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AKZOY UW Equity', 'AKZO NOBEL NV-SPON ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AL IM Equity', 'ALLEANZA ASSICURAZIONI', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AL/ LN Equity', 'ALLIANCE & LEICESTER PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALAB UW Equity', 'ALABAMA NATIONAL BANCORP/DEL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALAN UR Equity', 'ALANCO TECHNOLOGIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALB SQ Equity', 'CORPORACION FINANCIERA ALBA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALB UN Equity', 'ALBEMARLE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALBK ID Equity', 'ALLIED IRISH BANKS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALBY UR Equity', 'COMMUNITY CAP BANCSHARES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALCO UW Equity', 'ALICO INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALD UN Equity', 'ALLIED CAPITAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALDA UQ Equity', 'ALDILA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALDN UQ Equity', 'ALADDIN KNOWLEDGE SYSTEMS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALEX UW Equity', 'ALEXANDER & BALDWIN INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALFA SS Equity', 'ALFA LAVAL AB', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALFA UW Equity', 'ALFA CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALGN UQ Equity', 'ALIGN TECHNOLOGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALGT UQ Equity', 'ALLEGIANT TRAVEL CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALKS UW Equity', 'ALKERMES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALL UN Equity', 'ALLSTATE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALLI UQ Equity', 'ALLION HEALTHCARE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALLT UQ Equity', 'ALLOT COMMUNICATIONS LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALNC UQ Equity', 'ALLIANCE FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALNY UQ Equity', 'ALNYLAM PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALO FP Equity', 'ALSTOM', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALOG UW Equity', 'ANALOGIC CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALOT UQ Equity', 'ASTRO-MED INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALOY UQ Equity', 'ALLOY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALPHA GA Equity', 'ALPHA BANK A.E.', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALSK UQ Equity', 'ALASKA COMM SYSTEMS GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALT GY Equity', 'ALTANA AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALT SQ Equity', 'ALTADIS SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALTH UQ Equity', 'ALLOS THERAPEUTICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALTI UR Equity', 'ALTAIR NANOTECHNOLOGIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALTR UQ Equity', 'ALTERA CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALTU UQ Equity', 'ALTUS PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALU FP Equity', 'ALCATEL-LUCENT', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALV GY Equity', 'ALLIANZ SE-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALV UN Equity', 'AUTOLIV INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALVR UQ Equity', 'ALVARION LIMITED', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALXA UQ Equity', 'ALEXZA PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ALXN UQ Equity', 'ALEXION PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMAB UR Equity', 'AMERICASBANK CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMAC UR Equity', 'AMER MEDICAL ALERT CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMAG UQ Equity', 'ADVANCED MAGNETICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMAT UW Equity', 'APPLIED MATERIALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMB UN Equity', 'AMB PROPERTY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMBK UR Equity', 'AMERICAN BANK INC(PA)', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMCC UW Equity', 'APPLIED MICRO CIRCUITS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMCE UR Equity', 'AMERICAN CLAIMS EVALUATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMCP UQ Equity', 'AMCOMP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMCR UW Equity', 'AMCOR LTD-SPONS ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMCS UQ Equity', 'AMICAS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMD UN Equity', 'ADVANCED MICRO DEVICES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AME UN Equity', 'AMETEK INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMEC LN Equity', 'AMEC PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMED UW Equity', 'AMEDISYS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMEN UR Equity', 'AMEN PROPERTIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMFI UW Equity', 'AMCORE FINANCIAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMG UN Equity', 'AFFILIATED MANAGERS GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMGI UR Equity', 'AMERICAN MOLD GUARD INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMGN UW Equity', 'AMGEN INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMIC UQ Equity', 'AMERICAN INDEPENDENCE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMIE UQ Equity', 'AMBASSADORS INTERNATIONAL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMIS UW Equity', 'AMIS HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMKR UW Equity', 'AMKOR TECHNOLOGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AML LN Equity', 'AMLIN PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMLN UW Equity', 'AMYLIN PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMMD UW Equity', 'AMERICAN MEDICAL SYS HLDGS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMNB UW Equity', 'AMER NATL BNKSHS/DANVILLE VA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMOT UR Equity', 'ALLIED MOTION TECHNOLOGIES I', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMOV UW Equity', 'AMERICA MOVIL-ADR SERIES A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMP UN Equity', 'AMERIPRISE FINANCIAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMPH UR Equity', 'AMERICAN PHYSICIANS SVC GP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMPL UQ Equity', 'AMPAL-AMERICAN ISRAEL CORP-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMPX UR Equity', 'AMPEX CORP-CLASS A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMR UN Equity', 'AMR CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMRB UW Equity', 'AMERICAN RIVER BANKSHRS (CA)', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMRI UQ Equity', 'ALBANY MOLECULAR RESEARCH', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMRN UR Equity', 'AMARIN CORPORATION PLC -ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMSC UQ Equity', 'AMERICAN SUPERCONDUCTOR CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMSF UW Equity', 'AMERISAFE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMSG UW Equity', 'AMSURG CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMSWA UW Equity', 'AMERICAN SOFTWARE INC-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMT UN Equity', 'AMERICAN TOWER CORP-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMTC UR Equity', 'AMERITRANS CAPTL CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMTD UW Equity', 'TD AMERITRADE HOLDING CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMTY UR Equity', 'AMERITYRE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMWD UW Equity', 'AMERICAN WOODMARK CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AMZN UW Equity', 'AMAZON.COM INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AN UN Equity', 'AUTONATION INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANA SQ Equity', 'ACCIONA SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANAD UQ Equity', 'ANADIGICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANAT UW Equity', 'AMERICAN NATIONAL INSURANCE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANCX UQ Equity', 'ACCESS NATIONAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANDE UW Equity', 'ANDERSONS INC/THE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANDS UQ Equity', 'ANADYS PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANDW UW Equity', 'ANDREW CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANEN UQ Equity', 'ANAREN INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANF UN Equity', 'ABERCROMBIE & FITCH CO-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANGL ID Equity', 'ANGLO IRISH BANK CORP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANGN UR Equity', 'ANGEION CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANGO UW Equity', 'ANGIODYNAMICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANIK UW Equity', 'ANIKA THERAPEUTICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANLT UR Equity', 'ANALYTICAL SURVEYS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANLY UQ Equity', 'ANALYSTS INTERNATIONAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANN UN Equity', 'ANNTAYLOR STORES CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANNB UR Equity', 'ANNAPOLIS BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANPI UW Equity', 'ANGIOTECH PHARMACEUTICALS IN', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANSR UQ Equity', 'ANSWERTHINK INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANSS UW Equity', 'ANSYS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANST UW Equity', 'ANSOFT CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANSV UQ Equity', 'ANESIVA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANSW UQ Equity', 'ANSWERS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ANTO LN Equity', 'ANTOFAGASTA PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AOC UN Equity', 'AON CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'APA UN Equity', 'APACHE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'APAB UQ Equity', 'APPALACHIAN BANCSHARES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'APAC UQ Equity', 'APAC CUSTOMER SERVICES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'APAGF UR Equity', 'APCO ARGENTINA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'APAT UQ Equity', 'APA ENTERPRISES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'APC UN Equity', 'ANADARKO PETROLEUM CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'APCC UW Equity', 'AMERICAN POWER CONVERSION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'APCFY UR Equity', 'ATLAS SOUTH SEA PEARL-SP ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'APD UN Equity', 'AIR PRODUCTS & CHEMICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'APFC UQ Equity', 'AMERICAN PACIFIC CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'APH UN Equity', 'AMPHENOL CORP-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'APII UR Equity', 'ACTION PRODUCTS INTL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'APKT UQ Equity', 'ACME PACKET INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'APLX UW Equity', 'APPLIX INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'APOG UW Equity', 'APOGEE ENTERPRISES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'APOL UW Equity', 'APOLLO GROUP INC-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'APPA UQ Equity', 'AP PHARMA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'APPB UW Equity', 'APPLEBEE`S INTL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'APRO UQ Equity', 'AMERICA FIRST APARTMENT INV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'APSA UQ Equity', 'ALTO PALERMO S.A.-ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'APSG UW Equity', 'APPLIED SIGNAL TECHNOLOGY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'APTM UQ Equity', 'APTIMUS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AQNT UW Equity', 'AQUANTIVE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARBA UQ Equity', 'ARIBA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARBX UQ Equity', 'ARBINET-THEXCHANGE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARCAF UW Equity', 'ARCADIS NV - NY REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARCI UR Equity', 'APPLIANCE RECYCLING CTRS AMR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARDI UQ Equity', '@ROAD INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARDNA UQ Equity', 'ARDEN GROUP INC  -CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARG UN Equity', 'AIRGAS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARGN UR Equity', 'AMERIGON INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARI LN Equity', 'ARRIVA PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARIA UQ Equity', 'ARIAD PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARII UW Equity', 'AMERICAN RAILCAR INDUSTRIES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARKR UQ Equity', 'ARK RESTAURANTS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARLP UW Equity', 'ALLIANCE RESOURCE PARTNERS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARM LN Equity', 'ARM HOLDINGS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARMHY UW Equity', 'ARM HOLDINGS PLC-SPONS ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARNA UQ Equity', 'ARENA PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AROW UW Equity', 'ARROW FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARQL UQ Equity', 'ARQULE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARR FP Equity', 'SOCIETE DES AUTOROUTES PARIS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARRO UW Equity', 'ARROW INTERNATIONAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARRS UW Equity', 'ARRIS GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARRY UQ Equity', 'ARRAY BIOPHARMA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARTC UW Equity', 'ARTHROCARE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARTG UQ Equity', 'ART TECHNOLOGY GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARTL UR Equity', 'ARISTOTLE CORP/THE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARTNA UQ Equity', 'ARTESIAN RESOURCES CORP-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARTW UR Equity', 'ART`S-WAY MANUFACTURING CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARTX UQ Equity', 'AROTECH CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARW UN Equity', 'ARROW ELECTRONICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARWR UR Equity', 'ARROWHEAD RESEARCH CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARXT UW Equity', 'ADAMS RESPIRATORY THERA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ARXX UW Equity', 'AEROFLEX INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASBC UW Equity', 'ASSOCIATED BANC-CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASBI UQ Equity', 'AMERIANA BANCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASCA UW Equity', 'AMERISTAR CASINOS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASD UN Equity', 'AMERICAN STANDARD COS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASEI UQ Equity', 'AMERICAN SCIENCE & ENGINEERI', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASFI UW Equity', 'ASTA FUNDING INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASGR UW Equity', 'AMERICA SERVICE GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASH UN Equity', 'ASHLAND INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASHW UQ Equity', 'ASHWORTH INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASIA UQ Equity', 'ASIAINFO HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASMI UW Equity', 'ASM INTERNATIONAL N.V.', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASML NA Equity', 'ASML HOLDING NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASML UW Equity', 'ASML HOLDING NV-NY REG SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASN UN Equity', 'ARCHSTONE-SMITH TRUST', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASPM UQ Equity', 'ASPECT MEDICAL SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASPV UW Equity', 'ASPREVA PHARMACEUTICALS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASRV UQ Equity', 'AMERISERV FINANCIAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASSAB SS Equity', 'ASSA ABLOY AB-B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASTE UW Equity', 'ASTEC INDUSTRIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASTI UR Equity', 'ASCENT SOLAR TECHNOLOGIES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASTM UR Equity', 'AASTROM BIOSCIENCES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASTSF UW Equity', 'ASE TEST LIMITED', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASTT UR Equity', 'ASAT HOLDINGS LTD -ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASVI UW Equity', 'A.S.V. INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASYS UQ Equity', 'AMTECH SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ASYT UQ Equity', 'ASYST TECHNOLOGIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AT UN Equity', 'ALLTEL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATAC UW Equity', 'AFTERMARKET TECHNOLOGY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATAR UQ Equity', 'ATARI INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATAXZ UQ Equity', 'AMER FST TAX EX INVESTORS LP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATBC UR Equity', 'ATLANTIC BANCGROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATCO UR Equity', 'AMERICAN TECHNOLOGY CORP/DEL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATCOA SS Equity', 'ATLAS COPCO AB-A SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATEA UR Equity', 'ASTEA INTL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATEC UQ Equity', 'ALPHATEC HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATG UN Equity', 'AGL RESOURCES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATGN UR Equity', 'ALTIGEN COMMUNICATIONS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATHR UW Equity', 'ATHEROS COMMUNICATIONS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATI UN Equity', 'ALLEGHENY TECHNOLOGIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATK UN Equity', 'ALLIANT TECHSYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATLN SE Equity', 'ACTELION LTD-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATLO UR Equity', 'AMES NATIONAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATLS UW Equity', 'ATLAS AMERICA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATMI UW Equity', 'ATMI INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATML UW Equity', 'ATMEL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATNI UQ Equity', 'ATLANTIC TELE-NETWORK INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATO FP Equity', 'ATOS ORIGIN SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATO UN Equity', 'ATMOS ENERGY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATPG UW Equity', 'ATP OIL & GAS CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATPL UQ Equity', 'ATLANTIS PLASTICS INC-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATRC UQ Equity', 'ATRICURE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATRI UQ Equity', 'ATRION CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATRM UQ Equity', 'AETRIUM INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATRO UQ Equity', 'ASTRONICS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATRS UW Equity', 'ALTIRIS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATSI UQ Equity', 'ATS MEDICAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATTU UQ Equity', 'ATTUNITY LIMITED', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ATVI UW Equity', 'ACTIVISION INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AUBN UR Equity', 'AUBURN NATL BANCORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AUDC UQ Equity', 'AUDIOCODES LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AUTO IM Equity', 'AUTOSTRADE SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AUXL UQ Equity', 'AUXILIUM PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AV UN Equity', 'AVAYA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AV/ LN Equity', 'AVIVA PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AVAN UQ Equity', 'AVANT IMMUNOTHERAPEUTICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AVB UN Equity', 'AVALONBAY COMMUNITIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AVCA UR Equity', 'ADVOCAT INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AVCI UQ Equity', 'AVICI SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AVCT UW Equity', 'AVOCENT CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AVGN UQ Equity', 'AVIGEN INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AVID UW Equity', 'AVID TECHNOLOGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AVII UQ Equity', 'AVI BIOPHARMA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AVNC UQ Equity', 'ADVANCIS PHARMACEUTICAL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AVNR UQ Equity', 'AVANIR PHARMACEUTICALS-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AVNX UQ Equity', 'AVANEX CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AVP UN Equity', 'AVON PRODUCTS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AVRX UQ Equity', 'AVALON PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AVSR UR Equity', 'AVISTAR COMMUNICATIONS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AVT UN Equity', 'AVNET INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AVTR UW Equity', 'AVATAR HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AVX UN Equity', 'AVX CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AVY UN Equity', 'AVERY DENNISON CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AVZ LN Equity', 'AMVESCAP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AVZA UQ Equity', 'AVIZA TECHNOLOGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AW UN Equity', 'ALLIED WASTE INDUSTRIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AWBC UW Equity', 'AMERICANWEST BANCORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AWG LN Equity', 'AWG PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AWRE UQ Equity', 'AWARE INC/MASS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AXCA UW Equity', 'AXCAN PHARMA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AXP UN Equity', 'AMERICAN EXPRESS CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AXTI UQ Equity', 'AXT INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AXYS UW Equity', 'AXSYS TECHNOLOGIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AYE UN Equity', 'ALLEGHENY ENERGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AZN LN Equity', 'ASTRAZENECA PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AZO UN Equity', 'AUTOZONE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'AZPN UQ Equity', 'ASPEN TECHNOLOGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BA UN Equity', 'BOEING CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BA/ LN Equity', 'BAE SYSTEMS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BAC UN Equity', 'BANK OF AMERICA CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BAER VX Equity', 'JULIUS BAER HOLDING AG-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BALN VX Equity', 'BALOISE HOLDING-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BAMM UW Equity', 'BOOKS-A-MILLION INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BAMNB NA Equity', 'KONINKLIJKE BAM GROEP NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BANF UW Equity', 'BANCFIRST CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BANR UW Equity', 'BANNER CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BARC LN Equity', 'BARCLAYS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BARE UW Equity', 'BARE ESCENTUALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BARI UW Equity', 'BANCORP RHODE ISLAND INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BAS GY Equity', 'BASF AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BASI UQ Equity', 'BIOANALYTICAL SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BATS LN Equity', 'BRITISH AMERICAN TOBACCO PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BAX UN Equity', 'BAXTER INTERNATIONAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BAY GY Equity', 'BAYER AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BAY LN Equity', 'BRITISH AIRWAYS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BAYN UR Equity', 'BAY NATIONAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BB FP Equity', 'BIC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BB/ LN Equity', 'BRADFORD & BINGLEY PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BBA LN Equity', 'BBA AVIATION PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BBBB UW Equity', 'BLACKBOARD INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BBBY UW Equity', 'BED BATH & BEYOND INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BBEP UQ Equity', 'BREITBURN ENERGY PARTNERS LP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BBGI UQ Equity', 'BEASLEY BROADCAST GRP INC -A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BBNK UR Equity', 'BRIDGE CAPITAL HOLDINGS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BBOX UW Equity', 'BLACK BOX CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BBSI UW Equity', 'BARRETT BUSINESS SVCS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BBT UN Equity', 'BB&T CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BBVA SQ Equity', 'BANCO BILBAO VIZCAYA ARGENTA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BBY LN Equity', 'BALFOUR BEATTY PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BBY UN Equity', 'BEST BUY CO INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BC UN Equity', 'BRUNSWICK CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BCAR UR Equity', 'BANK OF THE CAROLINAS(NC)', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BCBP UQ Equity', 'BCB BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BCGI UQ Equity', 'BOSTON COMMUNICATIONS GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BCO UN Equity', 'BRINK`S CO/THE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BCON UR Equity', 'BEACON POWER CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BCP PL Equity', 'BANCO COMERCIAL PORTUGUES-R', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BCR UN Equity', 'BARD (C.R.) INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BCRX UQ Equity', 'BIOCRYST PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BCSB UQ Equity', 'BCSB BANKCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BCSI UQ Equity', 'BLUE COAT SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BDAY UQ Equity', 'CELEBRATE EXPRESS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BDCO UR Equity', 'BLUE DOLPHIN ENERGY CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BDEV LN Equity', 'BARRATT DEVELOPMENTS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BDK UN Equity', 'BLACK & DECKER CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BDMS UR Equity', 'BIRNER DENTAL MGMT SERVICES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BDN UN Equity', 'BRANDYWINE REALTY TRUST', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BDOG UQ Equity', 'BIG DOG HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BDSI UR Equity', 'BIODELIVERY SCIENCES INTL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BDX UN Equity', 'BECTON DICKINSON & CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BEAS UW Equity', 'BEA SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BEAV UW Equity', 'BE AEROSPACE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BEBE UW Equity', 'BEBE STORES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BEC UN Equity', 'BECKMAN COULTER INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BECN UW Equity', 'BEACON ROOFING SUPPLY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BEI GY Equity', 'BEIERSDORF AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BELFA UW Equity', 'BEL FUSE INC-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BELFB UW Equity', 'BEL FUSE INC-CL B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BELG BB Equity', 'BELGACOM SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BELM UQ Equity', 'BELL MICROPRODUCTS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BEN UN Equity', 'FRANKLIN RESOURCES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BER UN Equity', 'WR BERKLEY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BERK UQ Equity', 'BERKSHIRE BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BESI UQ Equity', 'BE SEMICONDUCTOR INDS-NY SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BESNN PL Equity', 'BANCO ESPIRITO SANTO-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BEXP UW Equity', 'BRIGHAM EXPLORATION CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BF/B UN Equity', 'BROWN-FORMAN CORP -CL B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BFAM UW Equity', 'BRIGHT HORIZONS FAMILY SOLUT', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BFBC UQ Equity', 'BENJAMIN FRANKLIN BANCORP IN', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BFI IM Equity', 'BANCA FIDEURAM SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BFIN UQ Equity', 'BANKFINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BFLY UR Equity', 'BLUEFLY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BFNB UQ Equity', 'BEACH FIRST NATL BANCSHARES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BFSB UQ Equity', 'BROOKLYN FEDERAL BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BG/ LN Equity', 'BG GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BGFV UW Equity', 'BIG 5 SPORTING GOODS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BGY LN Equity', 'BRITISH ENERGY GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BHBC UW Equity', 'BEVERLY HILLS BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BHI UN Equity', 'BAKER HUGHES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BHLB UW Equity', 'BERKSHIRE HILLS BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BIDU UQ Equity', 'BAIDU.COM - ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BIFF LN Equity', 'BIFFA PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BIG UN Equity', 'BIG LOTS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BIIB UW Equity', 'BIOGEN IDEC INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BIL IM Equity', 'BANCA ITALEASE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BIN IM Equity', 'BANCA INTESA SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BIO SE Equity', 'BB BIOTECH AG-BR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BIOM UQ Equity', 'BIOMIRA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BIOS UQ Equity', 'BIOSCRIP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BIOV UQ Equity', 'BIOVERIS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BITI UQ Equity', 'BIO-IMAGING TECHNOLOGIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BITS UR Equity', 'BITSTREAM INC-CLASS A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BIVN UQ Equity', 'BIOENVISION INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BJ UN Equity', 'BJ`S WHOLESALE CLUB INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BJCT UR Equity', 'BIOJECT MEDICAL TECHNOLOGIES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BJGP UR Equity', 'BEIJING MED-PHARM CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BJRI UW Equity', 'BJ`S RESTAURANTS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BJS UN Equity', 'BJ SERVICES CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BK UN Equity', 'BANK OF NEW YORK CO INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BKBK UR Equity', 'BRITTON & KOONTZ CAPITAL CRP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BKC UN Equity', 'BURGER KING HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BKD UN Equity', 'BROOKDALE SENIOR LIVING INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BKG LN Equity', 'BERKELEY GROUP HOLDINGS-UNIT', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BKHM UQ Equity', 'BOOKHAM INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BKIR ID Equity', 'BANK OF IRELAND', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BKMU UW Equity', 'BANK MUTUAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BKOR UR Equity', 'BANK OF OAK RIDGE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BKRS UQ Equity', 'BAKERS FOOTWEAR GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BKS UN Equity', 'BARNES & NOBLE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BKSC UR Equity', 'BANK OF SOUTH CAROLINA CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BKT SQ Equity', 'BANKINTER SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BKUNA UW Equity', 'BANKUNITED FINANCIAL CORP-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BL IM Equity', 'BANCA LOMBARDA E PIEMONTESE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BLDP UQ Equity', 'BALLARD POWER SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BLDR UW Equity', 'BUILDERS FIRSTSOURCE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BLK UN Equity', 'BLACKROCK INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BLKB UW Equity', 'BLACKBAUD INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BLL UN Equity', 'BALL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BLND LN Equity', 'BRITISH LAND CO PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BLS UN Equity', 'BELLSOUTH CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BLSI UR Equity', 'BOSTON LIFE SCIENCES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BLT LN Equity', 'BHP BILLITON PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BLTI UQ Equity', 'BIOLASE TECHNOLOGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BLUE UR Equity', 'BLUE HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BMC UN Equity', 'BMC SOFTWARE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BME SQ Equity', 'BOLSAS Y MERCADOS ESPANOLES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BMET UW Equity', 'BIOMET INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BMPS IM Equity', 'BANCA MONTE DEI PASCHI SIENA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BMRC UR Equity', 'BANK OF MARIN/CA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BMRN UQ Equity', 'BIOMARIN PHARMACEUTICAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BMS UN Equity', 'BEMIS COMPANY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BMTC UQ Equity', 'BRYN MAWR BANK CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BMTI UQ Equity', 'BIOMIMETIC THERAPEUTICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BMW GY Equity', 'BAYERISCHE MOTOREN WERKE AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BMY UN Equity', 'BRISTOL-MYERS SQUIBB CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BN FP Equity', 'GROUPE DANONE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BNCC UQ Equity', 'BNCCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BNCN UR Equity', 'BNC BANCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BNHN UQ Equity', 'BENIHANA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BNHNA UQ Equity', 'BENIHANA INC - CLASS A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BNI UN Equity', 'BURLINGTON NORTHERN SANTA FE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BNK UN Equity', 'TD BANKNORTH INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BNP FP Equity', 'BNP PARIBAS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BNSO UQ Equity', 'BONSO ELECTRONICS INTL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BNZL LN Equity', 'BUNZL PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BOB FP Equity', 'BUSINESS OBJECTS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BOBE UW Equity', 'BOB EVANS FARMS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BOBJ UW Equity', 'BUSINESS OBJECTS SA-SP ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BOCH UQ Equity', 'BANK OF COMMERCE HOLDINGS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BOFI UQ Equity', 'BOFI HOLDING INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BOFL UQ Equity', 'BANCSHARES OF FLORIDA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BOH UN Equity', 'BANK OF HAWAII CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BOKF UW Equity', 'BOK FINANCIAL CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BOL SS Equity', 'BOLIDEN AB', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BOL UN Equity', 'BAUSCH & LOMB INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BOMK UR Equity', 'BANK OF MCKENNEY/VA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BONT UW Equity', 'BON-TON STORES INC/THE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BOOM UW Equity', 'DYNAMIC MATERIALS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BORD UR Equity', 'BOARDWALK BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BORL UQ Equity', 'BORLAND SOFTWARE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BOSC UQ Equity', 'BOS BETTER ON-LINE SOLUTIONS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BOT UN Equity', 'CBOT HOLDINGS INC-CLASS A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BOVA UR Equity', 'BANK OF VIRGINIA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BP/ LN Equity', 'BP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BPFH UW Equity', 'BOSTON PRIVATE FINL HOLDING', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BPHX UQ Equity', 'BLUEPHOENIX SOLUTIONS LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BPI IM Equity', 'BANCA POPOLARE ITALIANA SCRL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BPIN PL Equity', 'BANCO BPI SA.- REG SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BPM IM Equity', 'BANCA POPOLARE DI MILANO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BPOP UW Equity', 'POPULAR INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BPRG UQ Equity', 'BIOPROGRESS PLC-ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BPU IM Equity', 'BANCHE POPOLARI UNITE SCPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BPUR UQ Equity', 'BIOPURE CORP-CLASS A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BPVN IM Equity', 'BANCO POPOLARE DI VERONA E N', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BRBI UR Equity', 'BLUE RIVER BANCSHARES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BRBY LN Equity', 'BURBERRY GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BRCD UW Equity', 'BROCADE COMMUNICATIONS SYS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BRCM UW Equity', 'BROADCOM CORP-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BRE LN Equity', 'BRIT INSURANCE HOLDINGS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BRE UN Equity', 'BRE PROPERTIES  -CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BRID UQ Equity', 'BRIDGFORD FOODS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BRISA PL Equity', 'BRISA AUTO-ESTRADAS-PRIV SHR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BRKL UW Equity', 'BROOKLINE BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BRKR UQ Equity', 'BRUKER BIOSCIENCES CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BRKS UQ Equity', 'BROOKS AUTOMATION INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BRL UN Equity', 'BARR PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BRLI UW Equity', 'BIO-REFERENCE LABS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BRNC UQ Equity', 'BRONCO DRILLING CO INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BRO UN Equity', 'BROWN & BROWN INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BSC UN Equity', 'BEAR STEARNS COMPANIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BSET UW Equity', 'BASSETT FURNITURE INDS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BSMD UQ Equity', 'BIOSPHERE MEDICAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BSML UR Equity', 'BSML INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BSQR UQ Equity', 'BSQUARE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BSTE UW Equity', 'BIOSITE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BSX UN Equity', 'BOSTON SCIENTIFIC CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BSXT UR Equity', 'BOE FINANCIAL SERVICES OF VA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BSY LN Equity', 'BRITISH SKY BROADCASTING GRO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BT/A LN Equity', 'BT GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BTFG UW Equity', 'BANCTRUST FINANCIAL GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BTRX UQ Equity', 'BARRIER THERAPEUTICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BTU UN Equity', 'PEABODY ENERGY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BTUI UQ Equity', 'BTU INTERNATIONAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BUCA UQ Equity', 'BUCA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BUCY UW Equity', 'BUCYRUS INTERNATIONAL INC-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BUD AV Equity', 'BOEHLER-UDDEHOLM', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BUD UN Equity', 'ANHEUSER-BUSCH COS INC.', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BUHR NA Equity', 'BUHRMANN N.V.', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BUL IM Equity', 'BULGARI SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BUR LN Equity', 'BURREN ENERGY PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BUSE UW Equity', 'FIRST BUSEY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BVA SQ Equity', 'BANCO DE VALENCIA SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BVS LN Equity', 'BOVIS HOMES GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BWA UN Equity', 'BORGWARNER INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BWEB UR Equity', 'BACKWEB TECHNOLOGIES LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BWINA UQ Equity', 'BALDWIN & LYONS INC -CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BWINB UQ Equity', 'BALDWIN & LYONS INC -CL B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BWLD UW Equity', 'BUFFALO WILD WINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BWNG UW Equity', 'BROADWING CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BWTR UQ Equity', 'BASIN WATER INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BWY LN Equity', 'BELLWAY PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BXP UN Equity', 'BOSTON PROPERTIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BXS UN Equity', 'BANCORPSOUTH INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BXTN LN Equity', 'BRIXTON PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BXXX UQ Equity', 'BROOKE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BYBI UR Equity', 'BACK YARD BURGERS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BYD UN Equity', 'BOYD GAMING CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BYFC UR Equity', 'BROADWAY FINANCIAL CORP/DEL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'BZH UN Equity', 'BEAZER HOMES USA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'C UN Equity', 'CITIGROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CA FP Equity', 'CARREFOUR SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CA UN Equity', 'CA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CAAS UR Equity', 'CHINA AUTOMOTIVE SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CACB UR Equity', 'CASCADE BANCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CACC UQ Equity', 'CREDIT ACCEPTANCE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CACH UW Equity', 'CACHE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CACS UQ Equity', 'CARRIER ACCESS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CADA UQ Equity', 'CAM COMMERCE SOLUTIONS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CADX UQ Equity', 'CADENCE PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CAFI UQ Equity', 'CAMCO FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CAG UN Equity', 'CONAGRA FOODS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CAH UN Equity', 'CARDINAL HEALTH INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CAKE UW Equity', 'CHEESECAKE FACTORY (THE)', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CAL UN Equity', 'CONTINENTAL AIRLINES-CLASS B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CALC UQ Equity', 'CALIFORNIA COASTAL COMM INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CALD UQ Equity', 'CALLIDUS SOFTWARE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CALL UQ Equity', 'CALLWAVE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CALM UQ Equity', 'CAL-MAINE FOODS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CALP UQ Equity', 'CALIPER LIFE SCIENCES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CAM UN Equity', 'CAMERON INTERNATIONAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CAMD UQ Equity', 'CALIFORNIA MICRO DEVICES CP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CAMP UW Equity', 'CALAMP CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CAMT UQ Equity', 'CAMTEK LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CANI UQ Equity', 'CARREKER CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CAP FP Equity', 'CAP GEMINI SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CAP IM Equity', 'CAPITALIA SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CAPA UQ Equity', 'CAPTARIS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CAPB UQ Equity', 'CAPITALSOUTH BANCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CAPE UR Equity', 'CAPE FEAR BANK CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CAPX UQ Equity', 'CAPITAL CROSSING BANK', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CAR UN Equity', 'AVIS BUDGET GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CARLB DC Equity', 'CARLSBERG AS-B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CARN UR Equity', 'CARRINGTON LABS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CART UR Equity', 'CAROLINA TRUST BANK', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CASB UR Equity', 'CASCADE FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CASM UR Equity', 'CAS MEDICAL SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CASS IM Equity', 'CATTOLICA ASSICURAZIONI SCRL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CASS UQ Equity', 'CASS INFORMATION SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CAST SS Equity', 'CASTELLUM AB', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CASY UW Equity', 'CASEY`S GENERAL STORES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CAT UN Equity', 'CATERPILLAR INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CATS UQ Equity', 'CATALYST SEMICONDUCTOR INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CATT UW Equity', 'CATAPULT COMMUNICATIONS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CATY UW Equity', 'CATHAY GENERAL BANCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CB UN Equity', 'CHUBB CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBAK UQ Equity', 'CHINA BAK BATTERY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBAN UQ Equity', 'COLONY BANKCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBBO UW Equity', 'COLUMBIA BANCORP/OR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBCF UW Equity', 'CITIZENS BANKING CORP MICH', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBE UN Equity', 'COOPER INDUSTRIES LTD-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBEY UQ Equity', 'CBEYOND INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBG LN Equity', 'CLOSE BROTHERS GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBG UN Equity', 'CB RICHARD ELLIS GROUP INC-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBH UN Equity', 'COMMERCE BANCORP INC/NJ', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBHI UW Equity', 'CENTENNIAL BANK HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBIN UR Equity', 'COMMUNITY BK SHARES/INDIANA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBK GY Equity', 'COMMERZBANK AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBKN UW Equity', 'CAPITAL BANK CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBL UN Equity', 'CBL & ASSOCIATES PROPERTIES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBLI UR Equity', 'CLEVELAND BIOLABS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBMX UQ Equity', 'ACACIA RESEARCH - COMBIMATRI', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBNK UQ Equity', 'CHICOPEE BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBON UQ Equity', 'COMMUNITY BANCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBOU UQ Equity', 'CARIBOU COFFEE CO INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBRL UW Equity', 'CBRL GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBRX UQ Equity', 'COLUMBIA LABORATORIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBRY LN Equity', 'CADBURY SCHWEPPES PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBS UN Equity', 'CBS CORP-CLASS B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBSH UW Equity', 'COMMERCE BANCSHARES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBSS UW Equity', 'COMPASS BANCSHARES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBST UW Equity', 'CUBIST PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBT UN Equity', 'CABOT CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBTE UR Equity', 'COMMONWEALTH BIOTECHNOLOGIES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CBUK UQ Equity', 'CUTTER & BUCK INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CC UN Equity', 'CIRCUIT CITY STORES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CCBD UQ Equity', 'COMMUNITY CENTRAL BANK CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CCBG UW Equity', 'CAPITAL CITY BANK GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CCBL UQ Equity', 'C-COR INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CCBP UQ Equity', 'COMM BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CCE UN Equity', 'COCA-COLA ENTERPRISES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CCFH UR Equity', 'CCF HOLDING COMPANY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CCI UN Equity', 'CROWN CASTLE INTL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CCK UN Equity', 'CROWN HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CCL LN Equity', 'CARNIVAL PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CCL UN Equity', 'CARNIVAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CCMP UW Equity', 'CABOT MICROELECTRONICS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CCNE UW Equity', 'CNB FINANCIAL CORP/PA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CCO UN Equity', 'CLEAR CHANNEL OUTDOOR-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CCOI UQ Equity', 'COGENT COMMUNICATIONS GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CCOW UW Equity', 'CAPITAL CORP OF THE WEST', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CCRN UW Equity', 'CROSS COUNTRY HEALTHCARE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CCRT UW Equity', 'COMPUCREDIT CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CCU UN Equity', 'CLEAR CHANNEL COMMUNICATIONS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CCUR UQ Equity', 'CONCURRENT COMPUTER CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CDCY UQ Equity', 'COMPUDYNE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CDI FP Equity', 'CHRISTIAN DIOR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CDIC UQ Equity', 'CARDIODYNAMICS INTL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CDMS UQ Equity', 'CADMUS COMMUNICATIONS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CDNS UW Equity', 'CADENCE DESIGN SYS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CDWC UW Equity', 'CDW CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CDZI UQ Equity', 'CADIZ INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CE UN Equity', 'CELANESE CORP-SERIES A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CEBK UQ Equity', 'CENTRAL BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CECE UQ Equity', 'CECO ENVIRONMENTAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CECO UW Equity', 'CAREER EDUCATION CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CEDC UW Equity', 'CENTRAL EURO DISTRIBUTION CP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CEG UN Equity', 'CONSTELLATION ENERGY GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CEGE UQ Equity', 'CELL GENESYS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CELG UW Equity', 'CELGENE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CELL UW Equity', 'BRIGHTPOINT INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CEM UN Equity', 'CHEMTURA CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CEN UN Equity', 'CERIDIAN CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CENT UW Equity', 'CENTRAL GARDEN & PET CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CENX UW Equity', 'CENTURY ALUMINUM COMPANY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CEP SQ Equity', 'CIA ESPANOLA DE PETROLEOS SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CEPH UW Equity', 'CEPHALON INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CERN UW Equity', 'CERNER CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CERS UQ Equity', 'CERUS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CESI UQ Equity', 'CATALYTICA ENERGY SYSTEMS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CETV UW Equity', 'CENTRAL EUROPEAN MEDIA ENT-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CEVA UQ Equity', 'CEVA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CFBK UR Equity', 'CENTRAL FEDERAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CFC UN Equity', 'COUNTRYWIDE FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CFCP UR Equity', 'COASTAL FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CFFC UR Equity', 'COMMUNITY FINL CORP/VA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CFFI UW Equity', 'C & F FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CFFN UW Equity', 'CAPITOL FEDERAL FINANCIAL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CFHI UQ Equity', 'COAST FINANCIAL HOLDINGS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CFNB UQ Equity', 'CALIFORNIA FIRST NATL BANCOR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CFNL UW Equity', 'CARDINAL FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CFR UN Equity', 'CULLEN/FROST BANKERS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CFR VX Equity', 'CIE FINANCIERE RICHEMON-BR A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CG UN Equity', 'LOEWS CORP - CAROLINA GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CGCBV FH Equity', 'CARGOTEC CORP-B SHARE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CGEN UQ Equity', 'COMPUGEN LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CGNX UW Equity', 'COGNEX CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CGPI UQ Equity', 'COLLAGENEX PHARMACEUTICALS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHAP UW Equity', 'CHAPARRAL STEEL CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHCI UQ Equity', 'COMSTOCK HOMEBUILDING COS-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHCO UW Equity', 'CITY HOLDING CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHD UN Equity', 'CHURCH & DWIGHT CO INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHDN UW Equity', 'CHURCHILL DOWNS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHDX UR Equity', 'CHINDEX INTERNATIONAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHEV UR Equity', 'CHEVIOT FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHFC UW Equity', 'CHEMICAL FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHFN UQ Equity', 'CHARTER FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHH UN Equity', 'CHOICE HOTELS INTL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHIC UW Equity', 'CHARLOTTE RUSSE HOLDING INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHINA UQ Equity', 'CDC CORP-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHK UN Equity', 'CHESAPEAKE ENERGY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHKE UW Equity', 'CHEROKEE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHKP UW Equity', 'CHECK POINT SOFTWARE TECH', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHMP UQ Equity', 'CHAMPION INDUSTRIES INC/WV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHNL UQ Equity', 'CHANNELL COMMERCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHNR UR Equity', 'CHINA NATURAL RESOURCES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHRD UQ Equity', 'CHORDIANT SOFTWARE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHRK UQ Equity', 'CHEROKEE INTERNATIONAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHRN SE Equity', 'CONVERIUM HOLDING AG-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHRS UW Equity', 'CHARMING SHOPPES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHRT UW Equity', 'CHARTERED SEMICONDUCTOR-ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHRW UW Equity', 'C.H. ROBINSON WORLDWIDE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHRZ UQ Equity', 'COMPUTER HORIZONS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHS UN Equity', 'CHICO`S FAS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHTP UR Equity', 'CHELSEA THERAPEUTICS INTERNA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHTR LN Equity', 'CHARTER PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHTR UQ Equity', 'CHARTER COMMUNICATIONS-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CHTT UW Equity', 'CHATTEM INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CI UN Equity', 'CIGNA CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CIBN VX Equity', 'CIBA SPECIALTY CHEMICALS-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CIEN UW Equity', 'CIENA CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CIMP PL Equity', 'CIMPOR-CIMENTOS DE PORTUGAL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CIMT UR Equity', 'CIMATRON LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CIN SQ Equity', 'CINTRA CONCESIONES DE INFRAE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CINF UW Equity', 'CINCINNATI FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CIPH UR Equity', 'CIPHERGEN BIOSYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CIT UN Equity', 'CIT GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CITP UQ Equity', 'COMSYS IT PARTNERS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CITZ UQ Equity', 'CFS BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CIZN UQ Equity', 'CITIZENS HOLDING COMPANY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CJBK UR Equity', 'CENTRAL JERSEY BANCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CKEC UQ Equity', 'CARMIKE CINEMAS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CKFR UW Equity', 'CHECKFREE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CKH UN Equity', 'SEACOR HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CKNN UQ Equity', 'CASH SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CKSW UR Equity', 'CLICKSOFTWARE TECHNOLOGIES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CKXE UW Equity', 'CKX INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CL UN Equity', 'COLGATE-PALMOLIVE CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CLAY UQ Equity', 'CLAYTON HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CLBH UR Equity', 'CAROLINA BANK HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CLBK UQ Equity', 'COMMERCIAL BANKSHARES INC/FL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CLCT UQ Equity', 'COLLECTORS UNIVERSE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CLDA UQ Equity', 'CLINICAL DATA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CLDN UW Equity', 'CELADON GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CLE UN Equity', 'CLAIRE`S STORES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CLFC UW Equity', 'CENTER FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CLHB UW Equity', 'CLEAN HARBORS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CLI UN Equity', 'MACK-CALI REALTY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CLMS UW Equity', 'CALAMOS ASSET MANAGEMENT-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CLMT UQ Equity', 'CALUMET SPECIALTY PRODUCTS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CLN VX Equity', 'CLARIANT AG-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CLP UN Equity', 'COLONIAL PROPERTIES TRUST', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CLRK UQ Equity', 'COLOR KINETICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CLRT UR Equity', 'CLARIENT INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CLS1 GY Equity', 'CELESIO AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CLWT UR Equity', 'EURO TECH HOLDINGS CO LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CLX UN Equity', 'CLOROX COMPANY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CLZR UW Equity', 'CANDELA CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CMA UN Equity', 'COMERICA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CMC UN Equity', 'COMMERCIAL METALS CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CMCO UQ Equity', 'COLUMBUS MCKINNON CORP/NY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CMCSA UW Equity', 'COMCAST CORP-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CMCSK UW Equity', 'COMCAST CORP-SPECIAL CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CME UN Equity', 'CHICAGO MERCANTILE EXCHANG-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CMED UW Equity', 'CHINA MEDICAL TECH-SPON ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CMFB UR Equity', 'COMMERCEFIRST BANCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CMGI UQ Equity', 'CMGI INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CMI UN Equity', 'CUMMINS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CMKG UR Equity', 'COACTIVE MARKETING GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CMLS UW Equity', 'CUMULUS MEDIA INC-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CMOS UW Equity', 'CREDENCE SYSTEMS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CMPP UQ Equity', 'CHAMPPS ENTERTAINMENT INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CMRG UQ Equity', 'CASUAL MALE RETAIL GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CMRO UQ Equity', 'COMARCO INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CMS UN Equity', 'CMS ENERGY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CMTL UW Equity', 'COMTECH TELECOMMUNICATIONS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CMTY UW Equity', 'COMMUNITY BANKS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CMVT UQ Equity', 'COMVERSE TECHNOLOGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CMX UN Equity', 'CAREMARK RX INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNA LN Equity', 'CENTRICA PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNA UN Equity', 'CNA FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNAF UQ Equity', 'COMMERCIAL NATL FINL CORP/PA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNB UN Equity', 'COLONIAL BANCGROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNBC UW Equity', 'CENTER BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNBKA UQ Equity', 'CENTURY BANCORP INC -CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNCP UR Equity', 'CAROLINA NATIONAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNCT UQ Equity', 'CONNETICS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNE LN Equity', 'CAIRN ENERGY PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNET UW Equity', 'CNET NETWORKS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNFL UR Equity', 'CITIZENS FINANCIAL CORP-A/KY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNLA UR Equity', 'COMMUNITY NATIONAL BANK OF T', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNLG UR Equity', 'CONOLOG CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNMD UW Equity', 'CONMED CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNO UN Equity', 'CONSECO INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNP FP Equity', 'CNP ASSURANCES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNP UN Equity', 'CENTERPOINT ENERGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNQR UQ Equity', 'CONCUR TECHNOLOGIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNSL UQ Equity', 'CONSOLIDATED COMMUNICATIONS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNST UQ Equity', 'CONSTAR INTL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNTF UQ Equity', 'CHINA TECHFAITH WIRELESS-ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNTY UR Equity', 'CENTURY CASINOS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNVR UQ Equity', 'CONVERA CORP-CLASS A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNW UN Equity', 'CON-WAY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNX UN Equity', 'CONSOL ENERGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNXS UW Equity', 'CNS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CNXT UW Equity', 'CONEXANT SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CO FP Equity', 'CASINO GUICHARD PERRACHON', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COB LN Equity', 'COBHAM PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COBK UQ Equity', 'COLONIAL BANKSHARES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COBR UQ Equity', 'COBRA ELECTRONICS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COBZ UW Equity', 'COBIZ INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COCO UW Equity', 'CORINTHIAN COLLEGES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CODI UW Equity', 'COMPASS DIVERSIFIED TRUST', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COF UN Equity', 'CAPITAL ONE FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COG UN Equity', 'CABOT OIL & GAS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COGN UW Equity', 'COGNOS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COGO UW Equity', 'COMTECH GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COGT UW Equity', 'COGENT INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COH UN Equity', 'COACH INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COHR UW Equity', 'COHERENT INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COHT UR Equity', 'COHESANT TECHNOLOGIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COHU UW Equity', 'COHU INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COKE UQ Equity', 'COCA-COLA BOTTLING CO CONSOL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COL SQ Equity', 'INMOBILIARIA COLONIAL SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COL UN Equity', 'ROCKWELL COLLINS INC.', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COLB UW Equity', 'COLUMBIA BANKING SYSTEM INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COLM UW Equity', 'COLUMBIA SPORTSWEAR CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COLOB DC Equity', 'COLOPLAST-B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COLR BB Equity', 'COLRUYT SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COLY UQ Equity', 'COLEY PHARMACEUTICAL GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COMS UW Equity', '3COM CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CON GY Equity', 'CONTINENTAL AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CONN UW Equity', 'CONN`S INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CONR UQ Equity', 'CONOR MEDSYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COO UN Equity', 'THE COOPER COS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COOP UQ Equity', 'COOPERATIVE BANKSHARES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COP UN Equity', 'CONOCOPHILLIPS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CORA NA Equity', 'CORIO NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CORE UW Equity', 'CORE-MARK HOLDING CO INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CORI UW Equity', 'CORILLIAN CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CORS UW Equity', 'CORUS BANKSHARES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CORT UQ Equity', 'CORCEPT THERAPEUTICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COSI UQ Equity', 'COSI INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COSMO GA Equity', 'COSMOTE MOBILE TELECOMMUNICA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COST UW Equity', 'COSTCO WHOLESALE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'COWN UQ Equity', 'COWEN GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPAK UQ Equity', 'CPAC INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPB UN Equity', 'CAMPBELL SOUP CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPBC UR Equity', 'COMMUNITY PARTNERS BANCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPBK UQ Equity', 'COMMUNITY CAPITAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPCI UQ Equity', 'CIPRICO INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPG LN Equity', 'COMPASS GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPHD UQ Equity', 'CEPHEID INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPI LN Equity', 'CAPITA GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPII UQ Equity', 'CPI INTERNATIONAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPKI UW Equity', 'CALIFORNIA PIZZA KITCHEN INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPLA UQ Equity', 'CAPELLA EDUCATION CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPNO UW Equity', 'COPANO ENERGY LLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPO UN Equity', 'CORN PRODUCTS INTL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPRT UW Equity', 'COPART INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPRX UQ Equity', 'CATALYST PHARMACEUTICAL PART', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPS UN Equity', 'CHOICEPOINT INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPSI UW Equity', 'COMPUTER PROGRAMS & SYSTEMS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPSS UQ Equity', 'CONSUMER PORTFOLIO SERVICES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPST UQ Equity', 'CAPSTONE TURBINE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPT UN Equity', 'CAMDEN PROPERTY TRUST', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPTS UQ Equity', 'CONCEPTUS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPW LN Equity', 'CARPHONE WAREHOUSE GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPWM UW Equity', 'COST PLUS INC/CALIFORNIA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CPWR UW Equity', 'COMPUWARE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CR UN Equity', 'CRANE CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRAI UW Equity', 'CRA INTERNATIONAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRAY UQ Equity', 'CRAY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRDC UQ Equity', 'CARDICA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRDN UW Equity', 'CERADYNE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CREAF UW Equity', 'CREATIVE TECHNOLOGY LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRED UR Equity', 'CREDO PETROLEUM CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CREE UW Equity', 'CREE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CREL UQ Equity', 'COREL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRESY UW Equity', 'CRESUD S.A.-SPONS ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRFN UQ Equity', 'CRESCENT FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRFT UQ Equity', 'CRAFTMADE INTERNATIONAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRG IM Equity', 'BANCA CARIGE SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRGI UQ Equity', 'CORGI INTERNATIONAL-SPON ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRGN UQ Equity', 'CURAGEN CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRH ID Equity', 'CRH PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRIS UQ Equity', 'CURIS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRL UN Equity', 'CHARLES RIVER LABORATORIES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRM UN Equity', 'SALESFORCE.COM INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRME UQ Equity', 'CARDIOME PHARMA CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRMH UW Equity', 'CRM HOLDINGS LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRMT UW Equity', 'AMERICA`S CAR-MART INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRNS UQ Equity', 'CRONOS GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRNT UQ Equity', 'CERAGON NETWORKS LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CROX UW Equity', 'CROCS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRRB UQ Equity', 'CARROLLTON BANCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRRC UW Equity', 'COURIER CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRS UN Equity', 'CARPENTER TECHNOLOGY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRTX UQ Equity', 'CRITICAL THERAPEUTICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRUS UW Equity', 'CIRRUS LOGIC INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRVL UW Equity', 'CORVEL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRWN UQ Equity', 'CROWN MEDIA HOLDINGS-CLASS A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRXL UQ Equity', 'CRUCELL-ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRXX UQ Equity', 'COMBINATORX INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRYO UQ Equity', 'CRYOCOR INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRYP UW Equity', 'CRYPTOLOGIC INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CRZO UW Equity', 'CARRIZO OIL & GAS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CS FP Equity', 'AXA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CS/ LN Equity', 'CORUS GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSAR UQ Equity', 'CARAUSTAR INDUSTRIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSBC UQ Equity', 'CITIZENS SOUTH BANKING CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSBK UW Equity', 'CLIFTON SAVINGS BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSC UN Equity', 'COMPUTER SCIENCES CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSCD UQ Equity', 'CASCADE MICROTECH INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSCO UW Equity', 'CISCO SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSCX UQ Equity', 'CARDIAC SCIENCE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSE UN Equity', 'CAPITALSOURCE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSFL UW Equity', 'CENTERSTATE BANKS OF FLORIDA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSGN VX Equity', 'CREDIT SUISSE GROUP-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSGP UW Equity', 'COSTAR GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSGS UW Equity', 'CSG SYSTEMS INTL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSHB UR Equity', 'COMMUNITY SHORES BANK CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSIQ UQ Equity', 'CANADIAN SOLAR INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSL UN Equity', 'CARLISLE COS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSLMF UR Equity', 'CONSOLIDATED MERCANTILE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSLR UR Equity', 'CONSULIER ENGINEERING INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSMNC NA Equity', 'CSM-CVA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSNC UR Equity', 'CARDINAL STATE BANK', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSNT UR Equity', 'CRESCENT BANKING CO/GA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSPI UQ Equity', 'CSP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSR LN Equity', 'CSR PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSTL LN Equity', 'COLLINS STEWART TULLETT PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSTL UR Equity', 'CASTELLE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSTR UW Equity', 'COINSTAR INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSWC UQ Equity', 'CAPITAL SOUTHWEST CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CSX UN Equity', 'CSX CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTAC UQ Equity', '1-800 CONTACTS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTAS UW Equity', 'CINTAS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTBC UR Equity', 'CONNECTICUT BANK & TRUST', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTBI UW Equity', 'COMMUNITY TRUST BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTBK UR Equity', 'CITY BANK LYNNWOOD WA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTCH UR Equity', 'COMMTOUCH SOFTWARE LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTCI UW Equity', 'CT COMMUNICATIONS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTCM UQ Equity', 'CTC MEDIA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTCO UW Equity', 'COMMONWEALTH TELEPHONE ENTRP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTDC UR Equity', 'CHINA TECHNOLOGY DEVELOPMENT', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTEC UQ Equity', 'CHOLESTECH CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTEL UQ Equity', 'CITY TELECOM (H.K.) LTD-ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTGX UR Equity', 'COMPUTER TASK GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTHR UW Equity', 'CHARLES & COLVARD LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTIB UR Equity', 'CTI INDUSTRIES CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTIC UQ Equity', 'CELL THERAPEUTICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTL UN Equity', 'CENTURYTEL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTLM UQ Equity', 'CENTILLIUM COMMUNICATIONS IN', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTRN UW Equity', 'CITI TRENDS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTRP UW Equity', 'CTRIP.COM INTERNATIONAL-ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTRX UQ Equity', 'COTHERIX INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTSH UW Equity', 'COGNIZANT TECH SOLUTIONS-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTT LN Equity', 'CATTLES PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTTY UR Equity', 'CATUITY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTWS UW Equity', 'CONNECTICUT WATER SVC INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTX UN Equity', 'CENTEX CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTXS UW Equity', 'CITRIX SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CTZN UW Equity', 'CITIZENS FIRST BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CULS UR Equity', 'COST-U-LESS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CUTR UW Equity', 'CUTERA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CVA UN Equity', 'COVANTA HOLDING CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CVBF UW Equity', 'CVB FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CVBG UQ Equity', 'CIVITAS BANKGROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CVBK UQ Equity', 'CENTRAL VIRGINIA BANKSHARES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CVC UN Equity', 'CABLEVISION SYSTEMS-NY GRP-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CVCO UW Equity', 'CAVCO INDUSTRIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CVCY UR Equity', 'CENTRAL VALLEY COMM BANCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CVD UN Equity', 'COVANCE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CVG UN Equity', 'CONVERGYS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CVGI UW Equity', 'COMMERCIAL VEHICLE GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CVGW UQ Equity', 'CALAVO GROWERS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CVH UN Equity', 'COVENTRY HEALTH CARE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CVLL UR Equity', 'COMMUNITY VALLEY BANCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CVLT UQ Equity', 'COMMVAULT SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CVLY UQ Equity', 'CODORUS VALLEY BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CVNS UW Equity', 'COVANSYS CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CVS UN Equity', 'CVS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CVTI UW Equity', 'COVENANT TRANSPORT INC-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CVTX UQ Equity', 'CV THERAPEUTICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CVX UN Equity', 'CHEVRON CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CW/ LN Equity', 'CABLE & WIRELESS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CWBC UQ Equity', 'COMMUNITY WEST BANCSHARES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CWBS UQ Equity', 'COMMONWEALTH BANKSHARES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CWCO UW Equity', 'CONSOLIDATED WATER CO-ORD SH', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CWEI UQ Equity', 'CLAYTON WILLIAMS ENERGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CWLZ UQ Equity', 'COWLITZ BANCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CWST UW Equity', 'CASELLA WASTE SYSTEMS INC-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CWTR UW Equity', 'COLDWATER CREEK INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CXG UN Equity', 'CNX GAS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CXSP UR Equity', 'CHEMGENEX PHARMACEU-SP ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CXW UN Equity', 'CORRECTIONS CORP OF AMERICA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CY UN Equity', 'CYPRESS SEMICONDUCTOR CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CYAN UR Equity', 'CYANOTECH CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CYBE UQ Equity', 'CYBEROPTICS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CYBI UQ Equity', 'CYBEX INTERNATIONAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CYBS UQ Equity', 'CYBERSOURCE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CYBX UQ Equity', 'CYBERONICS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CYCC UQ Equity', 'CYCLACEL PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CYCL UW Equity', 'CENTENNIAL COMMUNICATIONS CP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CYDS UR Equity', 'CYGNE DESIGNS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CYH UN Equity', 'COMMUNITY HEALTH SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CYMI UW Equity', 'CYMER INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CYN UN Equity', 'CITY NATIONAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CYNO UQ Equity', 'CYNOSURE INC-CLASS A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CYPB UQ Equity', 'CYPRESS BIOSCIENCE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CYT UN Equity', 'CYTEC INDUSTRIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CYTC UW Equity', 'CYTYC CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CYTK UQ Equity', 'CYTOKINETICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CYTO UQ Equity', 'CYTOGEN CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CYTR UR Equity', 'CYTRX CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CYTX UQ Equity', 'CYTORI THERAPEUTICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CZFC UQ Equity', 'CITIZENS FIRST CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CZN UN Equity', 'CITIZENS COMMUNICATIONS CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CZNC UR Equity', 'CITIZENS & NORTHERN CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'CZWI UQ Equity', 'CITIZENS COMMUNITY BANCORP I', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'D UN Equity', 'DOMINION RESOURCES INC/VA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DADE UW Equity', 'DADE BEHRING HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DAGM UR Equity', 'DAG MEDIA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DAIEY UR Equity', 'DAIEI INC -SPON ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DAIO UR Equity', 'DATA I/O CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DAKT UW Equity', 'DAKTRONICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DANKY UR Equity', 'DANKA BUSINESS SYS -SPON ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DANSKE DC Equity', 'DANSKE BANK A/S', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DASTY UW Equity', 'DASSAULT SYSTEMES SA-ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DATA UR Equity', 'DATATRAK INTERNATIONAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DAVE UQ Equity', 'FAMOUS DAVE`S OF AMERICA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DB1 GY Equity', 'DEUTSCHE BOERSE AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DBD UN Equity', 'DIEBOLD INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DBK GY Equity', 'DEUTSCHE BANK AG-REGISTERED', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DBLE UR Equity', 'DOUBLE EAGLE PETROLEUM CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DBRN UW Equity', 'DRESS BARN INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DCAI UR Equity', 'DIALYSIS CORP OF AMERICA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DCAP UR Equity', 'DCAP GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DCBK UQ Equity', 'DESERT COMMUNITY BANK', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DCC ID Equity', 'DCC PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DCEL UW Equity', 'DOBSON COMMUNICATIONS CORP-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DCGN UQ Equity', 'DECODE GENETICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DCI UN Equity', 'DONALDSON CO INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DCO DC Equity', 'DANISCO A/S', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DCOM UW Equity', 'DIME COMMUNITY BANCSHARES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DCTH UR Equity', 'DELCATH SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DCX GY Equity', 'DAIMLERCHRYSLER AG-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DD UN Equity', 'DU PONT (E.I.) DE NEMOURS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DDDC UR Equity', 'DELTATHREE INC-CLASS A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DDIC UQ Equity', 'DDI CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DDMX UW Equity', 'DYNAMEX INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DDR UN Equity', 'DEVELOPERS DIVERSIFIED RLTY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DDRX UQ Equity', 'DIEDRICH COFFEE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DDS UN Equity', 'DILLARDS INC-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DE UN Equity', 'DEERE & CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DEAR UQ Equity', 'DEARBORN BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DEB LN Equity', 'DEBENHAMS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DEBS UW Equity', 'DEB SHOPS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DEC FP Equity', 'JC DECAUX SA .', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DECC UQ Equity', 'D & E COMMUNICATIONS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DECK UW Equity', 'DECKERS OUTDOOR CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DECT UR Equity', 'DECTRON INTERNATIONALE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DEIX UQ Equity', 'DIRECTED ELECTRONICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DELB BB Equity', 'DELHAIZE GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DELL UW Equity', 'DELL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DELT UQ Equity', 'DELTA GALIL INDUSTRIES-ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DENN UR Equity', 'DENNY`S CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DEP GY Equity', 'DEPFA BANK PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DEPO UQ Equity', 'DEPOMED INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DESC UQ Equity', 'DISTRIBUTED ENERGY SYSTEMS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DEVC UQ Equity', 'DEVCON INTERNATIONAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DEXB BB Equity', 'DEXIA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DF UN Equity', 'DEAN FOODS CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DG FP Equity', 'VINCI SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DG UN Equity', 'DOLLAR GENERAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DGAS UQ Equity', 'DELTA NATURAL GAS CO INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DGE LN Equity', 'DIAGEO PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DGICA UW Equity', 'DONEGAL GROUP INC-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DGICB UW Equity', 'DONEGAL GROUP INC-B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DGII UW Equity', 'DIGI INTERNATIONAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DGIN UW Equity', 'DIGITAL INSIGHT CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DGIT UQ Equity', 'DG FASTCHANNEL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DGSE UR Equity', 'DGSE COMPANIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DGX UN Equity', 'QUEST DIAGNOSTICS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DHI UN Equity', 'DR HORTON INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DHIL UR Equity', 'DIAMOND HILL INVESTMENT GRP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DHOM UQ Equity', 'DOMINION HOMES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DHR UN Equity', 'DANAHER CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DIET UR Equity', 'EDIETS.COM INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DIGE UW Equity', 'DIGENE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DIIB UW Equity', 'DOREL INDUSTRIES-CL B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DIOD UW Equity', 'DIODES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DIS UN Equity', 'THE WALT DISNEY CO.', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DISCA UW Equity', 'DISCOVERY HOLDING CO-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DISCB UW Equity', 'DISCOVERY HOLDING CO-B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DISH UW Equity', 'ECHOSTAR COMMUNICATIONS - A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DITC UQ Equity', 'DITECH NETWORKS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DIVX UQ Equity', 'DIVX INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DJ UN Equity', 'DOW JONES & CO INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DJCO UR Equity', 'DAILY JOURNAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DKS UN Equity', 'DICK`S SPORTING GOODS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DLB UN Equity', 'DOLBY LABORATORIES INC-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DLGS UQ Equity', 'DIALOG SEMICONDUCTOR PLC-ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DLIA UQ Equity', 'DELIA*S INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DLLR UW Equity', 'DOLLAR FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DLM UN Equity', 'DEL MONTE FOODS CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DLPX UQ Equity', 'DELPHAX TECHNOLOGIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DLTR UW Equity', 'DOLLAR TREE STORES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DMGI UQ Equity', 'DIGITAL MUSIC GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DMGT LN Equity', 'DAILY MAIL&GENERAL TST-A NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DMLP UQ Equity', 'DORCHESTER MINERALS LP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DMND UW Equity', 'DIAMOND FOODS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DMRC UQ Equity', 'DIGIMARC CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DNA UN Equity', 'GENENTECH INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DNB UN Equity', 'DUN & BRADSTREET CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DNBNOR NO Equity', 'DNB NOR ASA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DNDN UQ Equity', 'DENDREON CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DNEX UW Equity', 'DIONEX CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DNR UN Equity', 'DENBURY RESOURCES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DO UN Equity', 'DIAMOND OFFSHORE DRILLING', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DOCC UQ Equity', 'DOCUCORP INTERNATIONAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DOCX UR Equity', 'DOCUMENT SCIENCES CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DORM UQ Equity', 'DORMAN PRODUCTS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DOV UN Equity', 'DOVER CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DOVR UQ Equity', 'DOVER SADDLERY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DOW UN Equity', 'DOW CHEMICAL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DPB GY Equity', 'DEUTSCHE POSTBANK AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DPL UN Equity', 'DPL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DPTR UQ Equity', 'DELTA PETROLEUM CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DPW GY Equity', 'DEUTSCHE POST AG-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DRAD UQ Equity', 'DIGIRAD CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DRAM UQ Equity', 'DATARAM CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DRAX UW Equity', 'DRAXIS HEALTH INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DRC UN Equity', 'DRESSER-RAND GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DRCO UQ Equity', 'DYNAMICS RESEARCH CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DRCT UW Equity', 'DIRECT GENERAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DRE UN Equity', 'DUKE REALTY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DRI UN Equity', 'DARDEN RESTAURANTS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DRIV UW Equity', 'DIGITAL RIVER INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DROOY UR Equity', 'DRDGOLD LTD-SPONSORED ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DRRX UQ Equity', 'DURECT CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DRS UN Equity', 'DRS TECHNOLOGIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DRTE UW Equity', 'DENDRITE INTERNATIONAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DRX LN Equity', 'DRAX GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DRYS UW Equity', 'DRYSHIPS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DSCM UQ Equity', 'DRUGSTORE.COM INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DSCO UQ Equity', 'DISCOVERY LABORATORIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DSCP UW Equity', 'DATASCOPE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DSGI LN Equity', 'DSG INTERNATIONAL PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DSGX UQ Equity', 'DESCARTES SYSTEMS GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DSM NA Equity', 'KONINKLIJKE DSM NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DSPG UW Equity', 'DSP GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DST UN Equity', 'DST SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DSTI UR Equity', 'DAYSTAR TECHNOLOGIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DSV DC Equity', 'DSV A/S', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DSWL UQ Equity', 'DESWELL INDUSTRIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DSY FP Equity', 'DASSAULT SYSTEMES SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DTAS UW Equity', 'DIGITAS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DTE GY Equity', 'DEUTSCHE TELEKOM AG-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DTE UN Equity', 'DTE ENERGY COMPANY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DTLK UQ Equity', 'DATALINK CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DTPI UQ Equity', 'DIAMOND MANAGEMENT & TECHNOL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DTSI UW Equity', 'DTS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DTV UN Equity', 'DIRECTV GROUP INC/THE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DUCK UQ Equity', 'DUCKWALL-ALCO STORES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DUK UN Equity', 'DUKE ENERGY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DUSA UQ Equity', 'DUSA PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DVA UN Equity', 'DAVITA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DVAX UQ Equity', 'DYNAVAX TECHNOLOGIES CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DVN UN Equity', 'DEVON ENERGY CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DVSA UQ Equity', 'DIVERSA CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DWA UN Equity', 'DREAMWORKS ANIMATION SKG-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DWCH UR Equity', 'DATAWATCH CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DWRI UQ Equity', 'DESIGN WITHIN REACH INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DWSN UQ Equity', 'DAWSON GEOPHYSICAL CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DXCM UQ Equity', 'DEXCOM INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DXPE UR Equity', 'DXP ENTERPRISES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DXYN UQ Equity', 'DIXIE GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DYAX UQ Equity', 'DYAX CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DYII UR Equity', 'DYNACQ HEALTHCARE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DYN UN Equity', 'DYNEGY INC-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'DYNT UR Equity', 'DYNATRONICS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EAD FP Equity', 'EUROPEAN AERONAUTIC DEFENCE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EAGL UW Equity', 'EGL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EAS UN Equity', 'ENERGY EAST CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EASY UR Equity', 'EASYLINK SERVICES CORP-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EAT UN Equity', 'BRINKER INTERNATIONAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EBAY UW Equity', 'EBAY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EBHI UQ Equity', 'EDDIE BAUER HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EBIX UR Equity', 'EBIX INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EBS AV Equity', 'ERSTE BANK DER OESTER SPARK', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EBTC UQ Equity', 'ENTERPRISE BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ECBE UQ Equity', 'ECB BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ECGI UR Equity', 'ENVOY COMMUNICATIONS GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ECHO UR Equity', 'ELECTRONIC CLEARING HOUSE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ECIL UW Equity', 'ECI TELECOM LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ECL UN Equity', 'ECOLAB INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ECLG UQ Equity', 'ECOLLEGE.COM INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ECLP UW Equity', 'ECLIPSYS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ECM LN Equity', 'ELECTROCOMPONENTS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ECMV UR Equity', 'E COM VENTURES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ECOL UQ Equity', 'AMERICAN ECOLOGY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ECPG UW Equity', 'ENCORE CAPITAL GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ECTX UQ Equity', 'ECTEL LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ED UN Equity', 'CONSOLIDATED EDISON INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EDAP UQ Equity', 'EDAP TMS SA -ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EDEN UR Equity', 'EDEN BIOSCIENCE CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EDF FP Equity', 'ELECTRICITE DE FRANCE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EDGR UQ Equity', 'EDGAR ONLINE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EDGW UQ Equity', 'EDGEWATER TECHNOLOGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EDP PL Equity', 'ENERGIAS DE PORTUGAL SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EDS UN Equity', 'ELECTRONIC DATA SYSTEMS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EDUC UQ Equity', 'EDUCATIONAL DEVELOPMENT CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EEEE UW Equity', 'EDUCATE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EEEI UR Equity', 'ELECTRO ENERGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EEEK GA Equity', 'COCA-COLA HELLENIC BOTTLING', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EEFT UW Equity', 'EURONET WORLDWIDE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EF FP Equity', 'ESSILOR INTERNATIONAL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EFII UW Equity', 'ELECTRONICS FOR IMAGING', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EFJI UQ Equity', 'EFJ INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EFSC UQ Equity', 'ENTERPRISE FINANCIAL SERVICE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EFUT UR Equity', 'EFUTURE INFORMATION TECHNOLO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EFX UN Equity', 'EQUIFAX INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EGBN UR Equity', 'EAGLE BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EGHT UR Equity', '8X8 INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EGLE UW Equity', 'EAGLE BULK SHIPPING INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EGLS UQ Equity', 'ELECTROGLAS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EGLT UQ Equity', 'EAGLE TEST SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EGN UN Equity', 'ENERGEN CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EHTH UQ Equity', 'EHEALTH INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EIHI UQ Equity', 'EASTERN INSURANCE HOLDINGS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EIX UN Equity', 'EDISON INTERNATIONAL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EK UN Equity', 'EASTMAN KODAK CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EL UN Equity', 'ESTEE LAUDER COMPANIES-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ELE SQ Equity', 'ENDESA SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ELGX UQ Equity', 'ENDOLOGIX INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ELI1V FH Equity', 'ELISA OYJ', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ELMG UW Equity', 'EMS TECHNOLOGIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ELN ID Equity', 'ELAN CORP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ELNK UW Equity', 'EARTHLINK INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ELON UQ Equity', 'ECHELON CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ELOY UQ Equity', 'ELOYALTY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ELRC UQ Equity', 'ELECTRO RENT CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ELRN UQ Equity', 'ELRON ELECTRONIC INDS  -ORD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ELSE UR Equity', 'ELECTRO-SENSORS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ELTK UR Equity', 'ELTEK LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ELUXB SS Equity', 'ELECTROLUX AB-SER B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EMA LN Equity', 'EMAP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EMAG UQ Equity', 'EMAGEON INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EMAK UQ Equity', 'EMAK WORLDWIDE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EMBT UW Equity', 'EMBARCADERO TECHNOLOGIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EMBX UQ Equity', 'EMBREX INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EMC UN Equity', 'EMC CORP/MASS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EMCI UW Equity', 'EMC INS GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EMG LN Equity', 'MAN GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EMI LN Equity', 'EMI GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EMIS UQ Equity', 'EMISPHERE TECHNOLOGIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EMITF UQ Equity', 'ELBIT MEDICAL IMAGING LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EMKR UQ Equity', 'EMCORE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EMMS UW Equity', 'EMMIS COMMUNICATIONS-CLASS A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EMN UN Equity', 'EASTMAN CHEMICAL COMPANY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EMR UN Equity', 'EMERSON ELECTRIC CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EMRG UR Equity', 'EMERGE INTERACTIVE INC -CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EN FP Equity', 'BOUYGUES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ENCO UR Equity', 'ENCORIUM GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ENCY UQ Equity', 'ENCYSIVE PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ENDP UW Equity', 'ENDO PHARMACEUT HLDGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ENEL IM Equity', 'ENEL SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ENER UW Equity', 'ENERGY CONVERSION DEVICES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ENG SQ Equity', 'ENAGAS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ENI IM Equity', 'ENI SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ENMD UQ Equity', 'ENTREMED INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ENPT UR Equity', 'EN POINTE TECHNOLOGIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ENR UN Equity', 'ENERGIZER HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ENRO SS Equity', 'ENIRO AB', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ENSI UW Equity', 'ENERGYSOUTH INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ENTG UW Equity', 'ENTEGRIS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ENTU UQ Equity', 'ENTRUST INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ENWV UQ Equity', 'ENDWAVE CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ENZN UQ Equity', 'ENZON PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EOA GY Equity', 'E.ON AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EOG UN Equity', 'EOG RESOURCES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EONC UR Equity', 'EON COMMUNICATIONS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EOP UN Equity', 'EQUITY OFFICE PROPERTIES TR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EP UN Equity', 'EL PASO CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EPAX UW Equity', 'AMBASSADORS GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EPAY UQ Equity', 'BOTTOMLINE TECHNOLOGIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EPCT UQ Equity', 'EPICEPT CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EPEN UR Equity', 'EAST PENN FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EPEX UW Equity', 'EDGE PETROLEUM CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EPHC UR Equity', 'EPOCH HOLDING CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EPIC UW Equity', 'EPICOR SOFTWARE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EPIK UR Equity', 'EPIC BANCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EPIQ UW Equity', 'EPIQ SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EPIX UQ Equity', 'EPIX PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EPMD UR Equity', 'EP MEDSYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EQ UN Equity', 'EMBARQ CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EQIX UW Equity', 'EQUINIX INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EQR UN Equity', 'EQUITY RESIDENTIAL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EQT UN Equity', 'EQUITABLE RESOURCES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ERES UW Equity', 'ERESEARCH TECHNOLOGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ERIC UW Equity', 'ERICSSON (LM) TEL-SP ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ERICB SS Equity', 'ERICSSON LM-B SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ERIE UW Equity', 'ERIE INDEMNITY COMPANY-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EROC UQ Equity', 'EAGLE ROCK ENERGY PARTNERS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ERTS UW Equity', 'ELECTRONIC ARTS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ESBF UW Equity', 'ESB FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ESBK UR Equity', 'ELMIRA SAVINGS BANK FSB', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ESCA UQ Equity', 'ESCALADE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ESCC UQ Equity', 'EVANS & SUTHERLAND CMP CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ESCH UQ Equity', 'ESCHELON TELECOM INC.', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ESCL UW Equity', 'ESCALA GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ESGR UW Equity', 'ENSTAR GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ESI UN Equity', 'ITT EDUCATIONAL SERVICES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ESIO UQ Equity', 'ELECTRO SCIENTIFIC INDS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ESLR UQ Equity', 'EVERGREEN SOLAR INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ESLT UW Equity', 'ELBIT SYSTEMS LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ESMC UR Equity', 'ESCALON MEDICAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ESPD UQ Equity', 'ESPEED INC-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ESRX UW Equity', 'EXPRESS SCRIPTS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ESS UN Equity', 'ESSEX PROPERTY TRUST INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ESST UQ Equity', 'ESS TECHNOLOGY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ESV UN Equity', 'ENSCO INTERNATIONAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ET UN Equity', 'E*TRADE FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ETE GA Equity', 'NATIONAL BANK OF GREECE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ETI LN Equity', 'ENTERPRISE INNS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ETL FP Equity', 'EUTELSAT COMMUNICATIONS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ETN UN Equity', 'EATON CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ETR UN Equity', 'ENTERGY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ETWC UQ Equity', 'ETRIALS WORLDWIDE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EUBK UW Equity', 'EUROBANCSHARES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EURO UR Equity', 'EUROTRUST A/S-SPONS ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EUROB GA Equity', 'EFG EUROBANK ERGASIAS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EV UN Equity', 'EATON VANCE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EVA SQ Equity', 'EBRO PULEVA SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EVBN UQ Equity', 'EVANS BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EVBS UQ Equity', 'EASTERN VIRGINIA BANKSHARES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EVCI UR Equity', 'EVCI CAREER COLLEGES HOLDING', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EVEP UW Equity', 'EV ENERGY PARTNER LP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EVOL UR Equity', 'EVOLVING SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EVST UR Equity', 'EVERLAST WORLDWIDE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EVVV UW Equity', 'EV3 INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EW UN Equity', 'EDWARDS LIFESCIENCES CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EWBC UW Equity', 'EAST WEST BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EWEB UR Equity', 'EUROWEB INTERNATIONAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EWST UQ Equity', 'ENERGY WEST INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EXAC UQ Equity', 'EXACTECH INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EXAR UQ Equity', 'EXAR CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EXAS UQ Equity', 'EXACT SCIENCES CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EXBD UW Equity', 'CORPORATE EXECUTIVE BOARD CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EXC UN Equity', 'EXELON CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EXEL UW Equity', 'EXELIXIS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EXFO UQ Equity', 'EXFO ELECTRO-OPTICAL ENGINEE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EXJF UQ Equity', 'EXCHANGE NATL BANCSHARES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EXLS UW Equity', 'EXLSERVICE HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EXP UN Equity', 'EAGLE MATERIALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EXPD UW Equity', 'EXPEDITORS INTL WASH INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EXPE UW Equity', 'EXPEDIA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EXPN LN Equity', 'EXPERIAN GROUP LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EXPO UW Equity', 'EXPONENT INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EXTR UQ Equity', 'EXTREME NETWORKS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EYE UN Equity', 'ADVANCED MEDICAL OPTICS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EZEM UW Equity', 'E-Z-EM INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'EZPW UW Equity', 'EZCORP INC-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'F IM Equity', 'FIAT SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'F UN Equity', 'FORD MOTOR CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FABG SS Equity', 'FABEGE AB', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FACT UQ Equity', 'FIRST ALBANY COMPANIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FAD SQ Equity', 'FADESA INMOBILIARIA SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FADV UW Equity', 'FIRST ADVANTAGE CORP-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FAF UN Equity', 'FIRST AMERICAN CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FALC UQ Equity', 'FALCONSTOR SOFTWARE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FARM UQ Equity', 'FARMER BROS CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FARO UQ Equity', 'FARO TECHNOLOGIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FAST UW Equity', 'FASTENAL CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FAVS UR Equity', 'FIRST AVIATION SERVICES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FBEI UQ Equity', 'FIRST BANCORP OF INDIANA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FBIZ UQ Equity', 'FIRST BUSINESS FINANCIAL SER', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FBMS UQ Equity', 'FIRST BANCSHARES INC/MS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FBNC UW Equity', 'FIRST BANCORP/NC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FBSI UQ Equity', 'FIRST BANCSHARES INC (MO)', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FBSS UR Equity', 'FAUQUIER BANKSHARES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FBST UQ Equity', 'FIBERSTARS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FBTC UR Equity', 'FIRST BANCTRUST CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FCAP UR Equity', 'FIRST CAPITAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FCBC UW Equity', 'FIRST COMMUNITY BANCSHARES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FCBP UW Equity', 'FIRST COMMUNITY BANCORP /CA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FCC SQ Equity', 'FOMENTO DE CONSTRUC Y CONTRA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FCCO UR Equity', 'FIRST COMMUNITY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FCCY UQ Equity', '1ST CONSTITUTION BANCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FCE/A UN Equity', 'FOREST CITY ENTERPRISES-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FCFL UR Equity', 'FIRST COMMUNITY BANK CORP OF', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FCFS UW Equity', 'FIRST CASH FINL SVCS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FCGI UQ Equity', 'FIRST CONSULTING GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FCL UN Equity', 'FOUNDATION COAL HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FCLF UR Equity', 'FIRST CLOVER LEAF FINANCIAL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FCNCA UW Equity', 'FIRST CITIZENS BCSHS  -CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FCPO UQ Equity', 'FACTORY CARD & PARTY OUTLET', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FCS UN Equity', 'FAIRCHILD SEMICON INTERNATIO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FCTR UW Equity', 'FIRST CHARTER CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FCX UN Equity', 'FREEPORT-MCMORAN COPPER-B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FCZA UR Equity', 'FIRST CITIZENS BANC CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FD UN Equity', 'FEDERATED DEPARTMENT STORES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FDC UN Equity', 'FIRST DATA CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FDEF UW Equity', 'FIRST DEFIANCE FINL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FDO UN Equity', 'FAMILY DOLLAR STORES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FDS UN Equity', 'FACTSET RESEARCH SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FDX UN Equity', 'FEDEX CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FE UN Equity', 'FIRSTENERGY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FEIC UQ Equity', 'FEI COMPANY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FER SQ Equity', 'GRUPO FERROVIAL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FFBC UW Equity', 'FIRST FINANCIAL BANCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FFBH UQ Equity', 'FIRST FED BNCSHS OF ARKANSAS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FFBI UQ Equity', 'FIRST FEDERAL BANCSHARES /DE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FFCH UW Equity', 'FIRST FINANCIAL HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FFCO UR Equity', 'FEDFIRST FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FFDF UR Equity', 'FFD FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FFFL UW Equity', 'FIDELITY BANKSHARES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FFHS UQ Equity', 'FIRST FRANKLIN CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FFIN UW Equity', 'FIRST FINL BANKSHARES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FFIV UW Equity', 'F5 NETWORKS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FFKT UR Equity', 'FARMERS CAPITAL BANK CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FFKY UQ Equity', 'FIRST FINANCIAL SERVICE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FFNM UQ Equity', 'FIRST FEDERAL OF NORTHERN MI', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FFSW UR Equity', 'FIRST FEDERAL BANC OF THE SO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FFSX UQ Equity', 'FIRST FED BANKSHARES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FGP LN Equity', 'FIRSTGROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FGR FP Equity', 'EIFFAGE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FHN UN Equity', 'FIRST HORIZON NATIONAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FIC UN Equity', 'FAIR ISAAC CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FICC UW Equity', 'FIELDSTONE INVESTMENT CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FIFG UQ Equity', '1ST INDEPENDENCE FINANCIAL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FII UN Equity', 'FEDERATED INVESTORS INC-CL B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FINB UW Equity', 'FIRST INDIANA CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FINL UW Equity', 'THE FINISH LINE-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FIS UN Equity', 'FIDELITY NATIONAL INFORMATIO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FISI UW Equity', 'FINANCIAL INSTITUTIONS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FISV UW Equity', 'FISERV INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FITB UW Equity', 'FIFTH THIRD BANCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FKFS UQ Equity', 'FIRST KEYSTONE FINANCIAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FL UN Equity', 'FOOT LOCKER INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FLIC UR Equity', 'FIRST OF LONG ISLAND CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FLR UN Equity', 'FLUOR CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FLS DC Equity', 'FLSMIDTH & CO A/S', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FLS UN Equity', 'FLOWSERVE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FLWS UW Equity', '1-800-FLOWERS.COM INC-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FMAR UQ Equity', 'FIRST MARINER BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FMBI UW Equity', 'FIRST MIDWEST BANCORP INC/IL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FMC UN Equity', 'FMC CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FMD UN Equity', 'FIRST MARBLEHEAD CORP/THE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FME GY Equity', 'FRESENIUS MEDICAL CARE AG &', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FMFC UW Equity', 'FIRST M & F CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FMRX UR Equity', 'FAMILYMEDS GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FMSB UQ Equity', 'FIRST MUTUAL BANCSHARES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FNC IM Equity', 'FINMECCANICA SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FNF UN Equity', 'FIDELITY NATIONAL FINL-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FNFG UW Equity', 'FIRST NIAGARA FINANCIAL GRP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FNFI UR Equity', 'FIRST NILES FINANCIAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FNLC UW Equity', 'FIRST NATIONAL LINCOLN CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FNLY UQ Equity', 'FINLAY ENTERPRISES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FNM UN Equity', 'FANNIE MAE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FNSC UQ Equity', 'FIRST NATIONAL BANCSHARES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FNSR UW Equity', 'FINISAR CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FO UN Equity', 'FORTUNE BRANDS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FORA NA Equity', 'FORTIS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FP FP Equity', 'TOTAL SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FP/ LN Equity', 'FRIENDS PROVIDENT PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FPFC UW Equity', 'FIRST PLACE FINANCIAL /OHIO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FPL UN Equity', 'FPL GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FPTB UQ Equity', 'FIRST PACTRUST BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FR FP Equity', 'VALEO SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FRA GY Equity', 'FRAPORT AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FRE UN Equity', 'FREDDIE MAC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FRE3 GY Equity', 'FRESENIUS AG-PFD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FRGB UQ Equity', 'FIRST REGIONAL BANCORP/CAL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FRK UN Equity', 'FLORIDA ROCK INDS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FRME UW Equity', 'FIRST MERCHANTS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FRT UN Equity', 'FEDERAL REALTY INVS TRUST', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FRX UN Equity', 'FOREST LABORATORIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FSA IM Equity', 'FONDIARIA-SAI SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FSBI UQ Equity', 'FIDELITY BANCORP INC/PENN', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FSBK UW Equity', 'FIRST SOUTH BANCORP INC /NC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FSGI UW Equity', 'FIRST SECURITY GROUP INC/TN', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FSLR UQ Equity', 'FIRST SOLAR INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FSNM UW Equity', 'FIRST STATE BANCORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FST UN Equity', 'FOREST OIL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FSTF UQ Equity', 'FIRST STATE FINANCIAL CORP/F', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FTE FP Equity', 'FRANCE TELECOM SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FTGX UR Equity', 'FIBERNET TELECOM GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FTI UN Equity', 'FMC TECHNOLOGIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FTO UN Equity', 'FRONTIER OIL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FTWR UQ Equity', 'FIBERTOWER CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FULT UW Equity', 'FULTON FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FUM1V FH Equity', 'FORTUM OYJ', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FUR NA Equity', 'FUGRO NV-CVA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FVRL UQ Equity', 'FAVRILLE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'FWB NI Equity', 'FASTWEB', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'G IM Equity', 'ASSICURAZIONI GENERALI', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'G1A GY Equity', 'GEA GROUP AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GA FP Equity', 'CIE GENERALE DE GEOPHYSIQUE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GAM SQ Equity', 'GAMESA CORP TECNOLOGICA SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GAS SQ Equity', 'GAS NATURAL SDG SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GAS UN Equity', 'NICOR INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GAZ FP Equity', 'GAZ DE FRANCE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GBB FP Equity', 'BOURBON SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GBF GY Equity', 'BILFINGER BERGER AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GBL BB Equity', 'GROUPE BRUXELLES LAMBERT SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GCC ID Equity', 'C&C GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GCI UN Equity', 'GANNETT CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GD UN Equity', 'GENERAL DYNAMICS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GDI UN Equity', 'GARDNER DENVER INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GE UN Equity', 'GENERAL ELECTRIC CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GEBN SE Equity', 'GEBERIT AG-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GENZ UW Equity', 'GENZYME CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GETIB SS Equity', 'GETINGE AB-B SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GFC FP Equity', 'GECINA SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GFS LN Equity', 'GROUP 4 SECURICOR PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GGG UN Equity', 'GRACO INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GGP UN Equity', 'GENERAL GROWTH PROPERTIES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GILD UW Equity', 'GILEAD SCIENCES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GIS UN Equity', 'GENERAL MILLS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GIVN VX Equity', 'GIVAUDAN-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GKN LN Equity', 'GKN PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GLBL UW Equity', 'GLOBAL INDUSTRIES LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GLE FP Equity', 'SOCIETE GENERALE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GLH LN Equity', 'GALLAHER GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GLOV UR Equity', 'AHPC HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GLW UN Equity', 'CORNING INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GM UN Equity', 'GENERAL MOTORS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GME UN Equity', 'GAMESTOP CORP-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GMT UN Equity', 'GATX CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GN DC Equity', 'GN STORE NORD A/S', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GN5 ID Equity', 'GRAFTON GRP PLC-UTS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GNK LN Equity', 'GREENE KING PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GNTX UW Equity', 'GENTEX CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GNW UN Equity', 'GENWORTH FINANCIAL INC-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GOOG UW Equity', 'GOOGLE INC-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GPC UN Equity', 'GENUINE PARTS CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GPN UN Equity', 'GLOBAL PAYMENTS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GPRO UW Equity', 'GEN-PROBE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GPS UN Equity', 'GAP INC/THE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GR UN Equity', 'GOODRICH CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GRAN UW Equity', 'BANK OF GRANITE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GRP UN Equity', 'GRANT PRIDECO INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GRRF UQ Equity', 'CHINA GRENTECH CORP LTD-ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GS UN Equity', 'GOLDMAN SACHS GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GSK LN Equity', 'GLAXOSMITHKLINE PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GSL UN Equity', 'GLOBAL SIGNAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GT UN Equity', 'GOODYEAR TIRE & RUBBER CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GTLS UQ Equity', 'CHART INDUSTRIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GWW UN Equity', 'WW GRAINGER INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GXP UN Equity', 'GREAT PLAINS ENERGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'GYI UN Equity', 'GETTY IMAGES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'H UN Equity', 'REALOGY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HAL UN Equity', 'HALLIBURTON CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HANS UR Equity', 'HANSEN NATURAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HAR UN Equity', 'HARMAN INTERNATIONAL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HAS LN Equity', 'HAYS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HAS UN Equity', 'HASBRO INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HB UN Equity', 'HILLENBRAND INDUSTRIES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HBAN UW Equity', 'HUNTINGTON BANCSHARES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HBI UN Equity', 'HANESBRANDS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HBOS LN Equity', 'HBOS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HCBK UW Equity', 'HUDSON CITY BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HCC UN Equity', 'HCC INSURANCE HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HCN UN Equity', 'HEALTH CARE REIT INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HCP UN Equity', 'HEALTH CARE PPTYS INVEST INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HCR UN Equity', 'MANOR CARE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HD UN Equity', 'HOME DEPOT INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HDD GY Equity', 'HEIDELBERGER DRUCKMASCHINEN', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HE UN Equity', 'HAWAIIAN ELECTRIC INDS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HEI GY Equity', 'HEIDELBERGCEMENT AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HEIA NA Equity', 'HEINEKEN NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HEIO NA Equity', 'HEINEKEN HOLDING NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HEN3 GY Equity', 'HENKEL KGAA-VORZUG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HES UN Equity', 'HESS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HET UN Equity', 'HARRAH`S ENTERTAINMENT INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HEW UN Equity', 'HEWITT ASSOCIATES INC-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HEXAB SS Equity', 'HEXAGON AB-B SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HGM NA Equity', 'HAGEMEYER NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HHS UN Equity', 'HARTE-HANKS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HIG UN Equity', 'HARTFORD FINANCIAL SVCS GRP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HILL UQ Equity', 'DOT HILL SYSTEMS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HLT UN Equity', 'HILTON HOTELS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HLTH UW Equity', 'EMDEON CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HLX UN Equity', 'HELIX ENERGY SOLUTIONS GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HMA UN Equity', 'HEALTH MGMT ASSOCIATES INC-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HMB SS Equity', 'HENNES & MAURITZ AB-B SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HMSO LN Equity', 'HAMMERSON PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HNI UN Equity', 'HNI CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HNR1 GY Equity', 'HANNOVER RUECKVERSICHERU-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HNS LN Equity', 'HANSON PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HNT UN Equity', 'HEALTH NET INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HNZ UN Equity', 'HJ HEINZ CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HO FP Equity', 'THALES SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HOC UN Equity', 'HOLLY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HOG UN Equity', 'HARLEY-DAVIDSON INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HOLMB SS Equity', 'HOLMEN AB-B SHARES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HOLN VX Equity', 'HOLCIM LTD-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HOME LN Equity', 'HOME RETAIL GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HON UN Equity', 'HONEYWELL INTERNATIONAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HOT GY Equity', 'HOCHTIEF AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HOT UN Equity', 'STARWOOD HOTELS & RESORTS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HP UN Equity', 'HELMERICH & PAYNE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HPC UN Equity', 'HERCULES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HPQ UN Equity', 'HEWLETT-PACKARD CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HPT UN Equity', 'HOSPITALITY PROPERTIES TRUST', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HRB UN Equity', 'H&R BLOCK INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HRL UN Equity', 'HORMEL FOODS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HRP UN Equity', 'HRPT PROPERTIES TRUST', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HRS UN Equity', 'HARRIS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HRX GY Equity', 'HYPO REAL ESTATE HOLDING', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HSBA LN Equity', 'HSBC HOLDINGS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HSC UN Equity', 'HARSCO CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HSIC UW Equity', 'HENRY SCHEIN INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HSP UN Equity', 'HOSPIRA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HST UN Equity', 'HOST HOTELS & RESORTS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HSY UN Equity', 'HERSHEY CO/THE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HTO GA Equity', 'HELLENIC TELECOMMUN ORGANIZA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HTV UN Equity', 'HEARST-ARGYLE TELEVISION INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HUB/B UN Equity', 'HUBBELL INC -CL B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HUH1V FH Equity', 'HUHTAMAKI OYJ', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HUM UN Equity', 'HUMANA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HUN UN Equity', 'HUNTSMAN CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'HUSQB SS Equity', 'HUSQVARNA AB-B SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IACI UW Equity', 'IAC/INTERACTIVECORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IAP LN Equity', 'ICAP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IAR UN Equity', 'IDEARC INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IAW ID Equity', 'IAWS GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IBE SQ Equity', 'IBERDROLA SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IBM UN Equity', 'INTL BUSINESS MACHINES CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ICE UN Equity', 'INTERCONTINENTALEXCHANGE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ICI LN Equity', 'IMPERIAL CHEMICAL INDS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ICP LN Equity', 'INTERMEDIATE CAPITAL GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IDR SQ Equity', 'INDRA SISTEMAS SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IDTI UW Equity', 'INTEGRATED DEVICE TECH INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IDXX UW Equity', 'IDEXX LABORATORIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IEA AV Equity', 'IMMOEAST IMMOBILIEN ANLAGEN', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IEX UN Equity', 'IDEX CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IFF UN Equity', 'INTL FLAVORS & FRAGRANCES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IFIN UW Equity', 'INVESTORS FINANCIAL SVCS CP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IFL IM Equity', 'IFIL-INVESTMENTS SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IFX GY Equity', 'INFINEON TECHNOLOGIES AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IGT UN Equity', 'INTL GAME TECHNOLOGY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IHG LN Equity', 'INTERCONTINENTAL HOTELS GROU', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IIA AV Equity', 'IMMOFINANZ IMMOBILIEN ANLAGE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'III LN Equity', '3I GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IM UN Equity', 'INGRAM MICRO INC-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IMCL UW Equity', 'IMCLONE SYSTEMS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IMI LN Equity', 'IMI PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IMOS UW Equity', 'CHIPMOS TECHNOLOGIES BERMUDA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IMT LN Equity', 'IMPERIAL TOBACCO GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'INB BB Equity', 'INBEV NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'INCH LN Equity', 'INCHCAPE PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'INDUA SS Equity', 'INDUSTRIVARDEN AB-A SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'INF LN Equity', 'INFORMA PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'INGA NA Equity', 'ING GROEP NV-CVA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'INTC UW Equity', 'INTEL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'INTU UW Equity', 'INTUIT INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'INVEB SS Equity', 'INVESTOR AB-B SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'INVP LN Equity', 'INVESTEC PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'INWS ID Equity', 'INDEPENDENT NEWS & MEDIA PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IP UN Equity', 'INTERNATIONAL PAPER CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IPG UN Equity', 'INTERPUBLIC GROUP OF COS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IPM ID Equity', 'IRISH LIFE & PERMANENT PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IPR LN Equity', 'INTERNATIONAL POWER PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IR UN Equity', 'INGERSOLL-RAND CO LTD-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IRF UN Equity', 'INTL RECTIFIER CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IRM UN Equity', 'IRON MOUNTAIN INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ISAT LN Equity', 'INMARSAT PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ISCA UW Equity', 'INTL SPEEDWAY CORP-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ISIL UW Equity', 'INTERSIL CORP -CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ISRG UW Equity', 'INTUITIVE SURGICAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ISYS LN Equity', 'INVENSYS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IT IM Equity', 'ITALCEMENTI SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ITG UN Equity', 'INVESTMENT TECHNOLOGY GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ITRK LN Equity', 'INTERTEK GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ITT UN Equity', 'ITT CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ITV LN Equity', 'ITV PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ITW UN Equity', 'ILLINOIS TOOL WORKS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ITX SQ Equity', 'INDITEX', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IVG GY Equity', 'IVG IMMOBILIEN AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'IVGN UW Equity', 'INVITROGEN CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JAH UN Equity', 'JARDEN CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JBHT UW Equity', 'HUNT (JB) TRANSPRT SVCS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JBL UN Equity', 'JABIL CIRCUIT INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JCI UN Equity', 'JOHNSON CONTROLS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JCP UN Equity', 'J.C. PENNEY CO INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JDSU UW Equity', 'JDS UNIPHASE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JEC UN Equity', 'JACOBS ENGINEERING GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JEF UN Equity', 'JEFFERIES GROUP INC (NEW)', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JLL UN Equity', 'JONES LANG LASALLE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JMAT LN Equity', 'JOHNSON MATTHEY PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JNC UN Equity', 'NUVEEN INVESTMENTS-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JNJ UN Equity', 'JOHNSON & JOHNSON', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JNPR UW Equity', 'JUNIPER NETWORKS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JNS UN Equity', 'JANUS CAPITAL GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JNY UN Equity', 'JONES APPAREL GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JOBS UW Equity', '51JOB INC-ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JOE UN Equity', 'THE ST JOE COMPANY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JOYG UW Equity', 'JOY GLOBAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JPM UN Equity', 'JPMORGAN CHASE & CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JPR LN Equity', 'JOHNSTON PRESS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JRJC UQ Equity', 'CHINA FINANCE ONLINE CO-ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JW/A UN Equity', 'WILEY (JOHN) & SONS  -CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JWN UN Equity', 'NORDSTROM INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'JYSK DC Equity', 'JYSKE BANK-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'K UN Equity', 'KELLOGG CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KAR UN Equity', 'ADESA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KBC BB Equity', 'KBC GROEP NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KBH UN Equity', 'KB HOME', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KCI UN Equity', 'KINETIC CONCEPTS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KEL LN Equity', 'KELDA GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KESA LN Equity', 'KESA ELECTRICALS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KESBV FH Equity', 'KESKO OYJ-B SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KEX UN Equity', 'KIRBY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KEY UN Equity', 'KEYCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KEYW UW Equity', 'ESSEX CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KFT UN Equity', 'KRAFT FOODS INC-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KG UN Equity', 'KING PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KGF LN Equity', 'KINGFISHER PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KIM UN Equity', 'KIMCO REALTY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KINVB SS Equity', 'KINNEVIK INVESTMENT AB-B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KLAC UW Equity', 'KLA-TENCOR CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KMB UN Equity', 'KIMBERLY-CLARK CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KMI UN Equity', 'KINDER MORGAN INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KMT UN Equity', 'KENNAMETAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KMX UN Equity', 'CARMAX INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KN FP Equity', 'NATIXIS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KNEBV FH Equity', 'KONE OYJ-B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KNIN SE Equity', 'KUEHNE & NAGEL INTL AG-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KO UN Equity', 'COCA-COLA CO/THE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KOSP UW Equity', 'KOS PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KPN NA Equity', 'KONINKLIJKE KPN NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KR UN Equity', 'KROGER CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KRC UN Equity', 'KILROY REALTY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KSE UN Equity', 'KEYSPAN CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KSP ID Equity', 'KINGSPAN GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KSS UN Equity', 'KOHLS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KSU UN Equity', 'KANSAS CITY SOUTHERN', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KWK UN Equity', 'QUICKSILVER RESOURCES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'KYG ID Equity', 'KERRY GROUP PLC-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LAD LN Equity', 'LADBROKES PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LAMR UW Equity', 'LAMAR ADVERTISING CO-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LAND LN Equity', 'LAND SECURITIES GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LAUR UW Equity', 'LAUREATE EDUCATION INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LBTYA UW Equity', 'LIBERTY GLOBAL INC-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LCAPA UW Equity', 'LIBERTY MEDIA CORP-CAP SER A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LCC UN Equity', 'US AIRWAYS GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LEAP UW Equity', 'LEAP WIRELESS INTL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LECO UW Equity', 'LINCOLN ELECTRIC HOLDINGS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LEG UN Equity', 'LEGGETT & PLATT INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LEH UN Equity', 'LEHMAN BROTHERS HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LEN UN Equity', 'LENNAR CORP-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LEND UW Equity', 'ACCREDITED HOME LENDERS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LENSD UQ Equity', 'CONCORD CAMERA CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LG FP Equity', 'LAFARGE SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LGEN LN Equity', 'LEGAL & GENERAL GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LH UN Equity', 'LABORATORY CRP OF AMER HLDGS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LHA GY Equity', 'DEUTSCHE LUFTHANSA-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LI FP Equity', 'KLEPIERRE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LI UN Equity', 'LAIDLAW INTERNATIONAL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LII LN Equity', 'LIBERTY INTERNATIONAL PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LII UN Equity', 'LENNOX INTERNATIONAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LIN GY Equity', 'LINDE AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LINTA UW Equity', 'LIBERTY MEDIA-INTERACTIVE A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LION UW Equity', 'FIDELITY SOUTHERN CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LISN SE Equity', 'LINDT & SPRUENGLI AG-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LIZ UN Equity', 'LIZ CLAIBORNE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LLL UN Equity', 'L-3 COMMUNICATIONS HOLDINGS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LLOY LN Equity', 'LLOYDS TSB GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LLTC UW Equity', 'LINEAR TECHNOLOGY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LLY UN Equity', 'ELI LILLY & CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LM UN Equity', 'LEGG MASON INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LMI LN Equity', 'LONMIN PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LMT UN Equity', 'LOCKHEED MARTIN CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LNC UN Equity', 'LINCOLN NATIONAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LNCR UW Equity', 'LINCARE HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LNG UA Equity', 'CHENIERE ENERGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LNT UN Equity', 'ALLIANT ENERGY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LOG LN Equity', 'LOGICACMG PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LOGN SE Equity', 'LOGITECH INTERNATIONAL-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LONG UQ Equity', 'ELONG INC-SPONSORED ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LONN VX Equity', 'LONZA GROUP AG-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LOW UN Equity', 'LOWE`S COS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LPNT UW Equity', 'LIFEPOINT HOSPITALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LPX UN Equity', 'LOUISIANA-PACIFIC CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LR FP Equity', 'LEGRAND SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LRCX UW Equity', 'LAM RESEARCH CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LRY UN Equity', 'LIBERTY PROPERTY TRUST', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LSE LN Equity', 'LONDON STOCK EXCHANGE GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LSI UN Equity', 'LSI LOGIC CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LSTR UW Equity', 'LANDSTAR SYSTEM INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LTD UN Equity', 'LIMITED BRANDS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LTO IM Equity', 'LOTTOMATICA SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LTR UN Equity', 'LOEWS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LUK UN Equity', 'LEUCADIA NATIONAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LUPE SS Equity', 'LUNDIN PETROLEUM AB', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LUV UN Equity', 'SOUTHWEST AIRLINES CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LUX IM Equity', 'LUXOTTICA GROUP SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LVLT UW Equity', 'LEVEL 3 COMMUNICATIONS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LVS UN Equity', 'LAS VEGAS SANDS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LXK UN Equity', 'LEXMARK INTERNATIONAL INC-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LXS GY Equity', 'LANXESS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LYO UN Equity', 'LYONDELL CHEMICAL COMPANY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'LZ UN Equity', 'LUBRIZOL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MA UN Equity', 'MASTERCARD INC-CLASS A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MAB LN Equity', 'MITCHELLS & BUTLERS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MAC UN Equity', 'MACERICH CO/THE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MAERSKB DC Equity', 'A P MOLLER - MAERSK A/S', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MAN GY Equity', 'MAN AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MAN UN Equity', 'MANPOWER INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MAP SQ Equity', 'CORPORACION MAPFRE SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MAR UN Equity', 'MARRIOTT INTERNATIONAL-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MAS UN Equity', 'MASCO CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MAT UN Equity', 'MATTEL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MAU FP Equity', 'MAUREL ET PROM', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MB IM Equity', 'MEDIOBANCA SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MBI UN Equity', 'MBIA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MC FP Equity', 'LVMH MOET HENNESSY LOUIS VUI', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MCD UN Equity', 'MCDONALD`S CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MCHP UW Equity', 'MICROCHIP TECHNOLOGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MCK UN Equity', 'MCKESSON CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MCO UN Equity', 'MOODY`S CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MCY UN Equity', 'MERCURY GENERAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MDC UN Equity', 'MDC HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MDP UN Equity', 'MEREDITH CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MDRX UW Equity', 'ALLSCRIPTS HEALTHCARE SOLUT', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MDT UN Equity', 'MEDTRONIC INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MDU UN Equity', 'MDU RESOURCES GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MED IM Equity', 'MEDIOLANUM SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MEDI UW Equity', 'MEDIMMUNE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MEE UN Equity', 'MASSEY ENERGY CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MEL AV Equity', 'MEINL EUROPEAN LAND LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MEL UN Equity', 'MELLON FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MELA UR Equity', 'ELECTRO-OPTICAL SCIENCES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MEO GY Equity', 'METRO AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MEO1V FH Equity', 'METSO OYJ', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MER UN Equity', 'MERRILL LYNCH & CO INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MET UN Equity', 'METLIFE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MF FP Equity', 'WENDEL INVESTISSEMENT', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MFE UN Equity', 'MCAFEE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MGGT LN Equity', 'MEGGITT PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MGI UN Equity', 'MONEYGRAM INTERNATIONAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MGM UN Equity', 'MGM MIRAGE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MHK UN Equity', 'MOHAWK INDUSTRIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MHP UN Equity', 'MCGRAW-HILL COMPANIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MHS UN Equity', 'MEDCO HEALTH SOLUTIONS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MI UN Equity', 'MARSHALL & ILSLEY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MIL UN Equity', 'MILLIPORE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MIR UN Equity', 'MIRANT CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MKC UN Equity', 'MCCORMICK & CO-NON VTG SHRS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MKL UN Equity', 'MARKEL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MKS LN Equity', 'MARKS & SPENCER GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ML FP Equity', 'MICHELIN (CGDE)-B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MLM UN Equity', 'MARTIN MARIETTA MATERIALS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MLNM UW Equity', 'MILLENNIUM PHARMACEUTICALS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MMB FP Equity', 'LAGARDERE S.C.A.', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MMC UN Equity', 'MARSH & MCLENNAN COS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MMM UN Equity', '3M CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MMT FP Equity', 'M6-METROPOLE TELEVISION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MNI UN Equity', 'MCCLATCHY CO-CLASS A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MNST UW Equity', 'MONSTER WORLDWIDE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MO UN Equity', 'ALTRIA GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MOBB BB Equity', 'MOBISTAR SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MOLX UW Equity', 'MOLEX INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MON UN Equity', 'MONSANTO CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MOS UN Equity', 'MOSAIC CO/THE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MOT UN Equity', 'MOTOROLA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MPI LN Equity', 'MICHAEL PAGE INTERNATIONAL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MRBK UW Equity', 'MERCANTILE BANKSHARES CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MRK GY Equity', 'MERCK KGAA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MRK UN Equity', 'MERCK & CO. INC.', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MRO UN Equity', 'MARATHON OIL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MRW LN Equity', 'MORRISON <WM.> SUPERMARKETS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MS IM Equity', 'MEDIASET SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MS UN Equity', 'MORGAN STANLEY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MSFT UW Equity', 'MICROSOFT CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MSM UN Equity', 'MSC INDUSTRIAL DIRECT CO-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MSY LN Equity', 'MISYS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MTB UN Equity', 'M & T BANK CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MTD UN Equity', 'METTLER-TOLEDO INTERNATIONAL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MTG UN Equity', 'MGIC INVESTMENT CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MTGB SS Equity', 'MODERN TIMES GROUP-B SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MTP FP Equity', 'MITTAL STEEL CO NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MTW UN Equity', 'MANITOWOC COMPANY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MTX GY Equity', 'MTU AERO ENGINES HOLDING AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MU UN Equity', 'MICRON TECHNOLOGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MUR UN Equity', 'MURPHY OIL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MUV2 GY Equity', 'MUENCHENER RUECKVER AG-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MVC SQ Equity', 'METROVACESA SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MWV UN Equity', 'MEADWESTVACO CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MXIM UW Equity', 'MAXIM INTEGRATED PRODUCTS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'MYL UN Equity', 'MYLAN LABORATORIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NATI UW Equity', 'NATIONAL INSTRUMENTS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NAV UN Equity', 'NAVISTAR INTERNATIONAL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NBL UN Equity', 'NOBLE ENERGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NBR UN Equity', 'NABORS INDUSTRIES LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NCC UN Equity', 'NATIONAL CITY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NCR UN Equity', 'NCR CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NDA SS Equity', 'NORDEA BANK AB', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NDAQ UW Equity', 'NASDAQ STOCK MARKET INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NDE UN Equity', 'INDYMAC BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NE UN Equity', 'NOBLE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NEM UN Equity', 'NEWMONT MINING CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NEO FP Equity', 'NEOPOST SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NES1V FH Equity', 'NESTE OIL OYJ', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NESN VX Equity', 'NESTLE SA-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NEW UN Equity', 'NEW CENTURY FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NEX LN Equity', 'NATIONAL EXPRESS GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NFG UN Equity', 'NATIONAL FUEL GAS CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NFS UN Equity', 'NATIONWIDE FINANCIAL SERV- A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NFX UN Equity', 'NEWFIELD EXPLORATION CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NG/ LN Equity', 'NATIONAL GRID PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NHY NO Equity', 'NORSK HYDRO ASA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NI UN Equity', 'NISOURCE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NIHD UW Equity', 'NII HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NILE UW Equity', 'BLUE NILE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NK FP Equity', 'IMERYS SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NKE UN Equity', 'NIKE INC -CL B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NLC UN Equity', 'NALCO HOLDING CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NLY UN Equity', 'ANNALY CAPITAL MANAGEMENT IN', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NNI UN Equity', 'NELNET INC-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NOBE VX Equity', 'NOBEL BIOCARE HOLDING AG-BR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NOC UN Equity', 'NORTHROP GRUMMAN CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NOK1V FH Equity', 'NOKIA OYJ', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NOV UN Equity', 'NATIONAL OILWELL VARCO INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NOVL UW Equity', 'NOVELL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NOVN VX Equity', 'NOVARTIS AG-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NOVOB DC Equity', 'NOVO NORDISK A/S-B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NPTH UQ Equity', 'ENPATH MEDICAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NRG UN Equity', 'NRG ENERGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NRK LN Equity', 'NORTHERN ROCK PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NSC UN Equity', 'NORFOLK SOUTHERN CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NSG NO Equity', 'NORSKE SKOGINDUSTRIER ASA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NSM UN Equity', 'NATIONAL SEMICONDUCTOR CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NSR UN Equity', 'NEUSTAR INC-CLASS A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NST UN Equity', 'NSTAR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NTAP UW Equity', 'NETWORK APPLIANCE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NTLI UW Equity', 'NTL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NTRI UW Equity', 'NUTRI/SYSTEM INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NTRS UW Equity', 'NORTHERN TRUST CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NU UN Equity', 'NORTHEAST UTILITIES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NUE UN Equity', 'NUCOR CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NUM NA Equity', 'KONINKLIJKE NUMICO NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NVDA UW Equity', 'NVIDIA CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NVLS UW Equity', 'NOVELLUS SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NVR UA Equity', 'NVR INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NVT UN Equity', 'NAVTEQ CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NWG LN Equity', 'NORTHUMBRIAN WATER GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NWL UN Equity', 'NEWELL RUBBERMAID INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NWS/A UN Equity', 'NEWS CORP-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NXL UN Equity', 'NEW PLAN EXCEL REALTY TRUST', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NXT FP Equity', 'EURONEXT NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NXT LN Equity', 'NEXT PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NYB UN Equity', 'NEW YORK COMMUNITY BANCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NYNY UR Equity', 'EMPIRE RESORTS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NYT UN Equity', 'NEW YORK TIMES CO -CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NYX UN Equity', 'NYSE GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'NZYMB DC Equity', 'NOVOZYMES A/S-B SHARES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'OCR UN Equity', 'OMNICARE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ODP UN Equity', 'OFFICE DEPOT INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'OGE UN Equity', 'OGE ENERGY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'OI UN Equity', 'OWENS-ILLINOIS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'OII UN Equity', 'OCEANEERING INTL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'OKE UN Equity', 'ONEOK INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'OKOAS FH Equity', 'OKO BANK PLC-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'OLED UQ Equity', 'CAMBRIDGE DISPLAY TECHNOLOGY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'OMC UN Equity', 'OMNICOM GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'OML LN Equity', 'OLD MUTUAL PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'OMV AV Equity', 'OMV AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'OMX UN Equity', 'OFFICEMAX INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'OPAP GA Equity', 'OPAP SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'OR FP Equity', 'L`OREAL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ORCL UW Equity', 'ORACLE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ORI UN Equity', 'OLD REPUBLIC INTL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ORK NO Equity', 'ORKLA ASA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ORLY UW Equity', 'O`REILLY AUTOMOTIVE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'OSG UN Equity', 'OVERSEAS SHIPHOLDING GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'OSI UN Equity', 'OSI RESTAURANT PARTNERS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'OSK UN Equity', 'OSHKOSH TRUCK CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'OUT1V FH Equity', 'OUTOKUMPU OYJ', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'OXY UN Equity', 'OCCIDENTAL PETROLEUM CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'OZRK UW Equity', 'BANK OF THE OZARKS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PAJ FP Equity', 'PAGESJAUNES GROUPE SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PAN NO Equity', 'PAN FISH ASA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PARG SE Equity', 'PARGESA HOLDING SA-BR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PAS SQ Equity', 'BANCO PASTOR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PAS UN Equity', 'PEPSIAMERICAS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PAY UN Equity', 'VERIFONE HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PAYX UW Equity', 'PAYCHEX INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PBCT UW Equity', 'PEOPLES BANK', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PBG UN Equity', 'PEPSI BOTTLING GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PBI UN Equity', 'PITNEY BOWES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PC IM Equity', 'PIRELLI & C.', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PCAR UW Equity', 'PACCAR INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PCG UN Equity', 'P G & E CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PCL UN Equity', 'PLUM CREEK TIMBER CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PCP UN Equity', 'PRECISION CASTPARTS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PCU UN Equity', 'SOUTHERN COPPER CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PD UN Equity', 'PHELPS DODGE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PDCO UW Equity', 'PATTERSON COS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PDE UN Equity', 'PRIDE INTERNATIONAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PDLI UW Equity', 'PDL BIOPHARMA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PDX UN Equity', 'PEDIATRIX MEDICAL GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PEG UN Equity', 'PUBLIC SERVICE ENTERPRISE GP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PENN UW Equity', 'PENN NATIONAL GAMING INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PEP UN Equity', 'PEPSICO INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PETM UW Equity', 'PETSMART INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PFE UN Equity', 'PFIZER INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PFG LN Equity', 'PROVIDENT FINANCIAL PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PFG UN Equity', 'PRINCIPAL FINANCIAL GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PG IM Equity', 'SEAT PAGINE GIALLE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PG UN Equity', 'PROCTER & GAMBLE CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PGL UN Equity', 'PEOPLES ENERGY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PGN UN Equity', 'PROGRESS ENERGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PGR UN Equity', 'PROGRESSIVE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PGS NO Equity', 'PETROLEUM GEO-SERVICES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PH UN Equity', 'PARKER HANNIFIN CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PHBN SE Equity', 'PHONAK HOLDING AG-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PHIA NA Equity', 'PHILIPS ELECTRONICS NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PHLY UW Equity', 'PHILADELPHIA CONS HLDG CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PHM UN Equity', 'PULTE HOMES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PKG UN Equity', 'PACKAGING CORP OF AMERICA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PKI UN Equity', 'PERKINELMER INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PL UN Equity', 'PROTECTIVE LIFE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PLCE UW Equity', 'CHILDREN`S PLACE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PLD UN Equity', 'PROLOGIS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PLL UN Equity', 'PALL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PLT IM Equity', 'PARMALAT SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PLUS UQ Equity', 'EPLUS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PMCS UW Equity', 'PMC - SIERRA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PMI UN Equity', 'PMI GROUP INC/THE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PMTC UW Equity', 'PARAMETRIC TECHNOLOGY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PNC UN Equity', 'PNC FINANCIAL SERVICES GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PNN LN Equity', 'PENNON GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PNR UN Equity', 'PENTAIR INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PNRA UW Equity', 'PANERA BREAD COMPANY-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PNW UN Equity', 'PINNACLE WEST CAPITAL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'POM UN Equity', 'PEPCO HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'POOL UW Equity', 'POOL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'POP SQ Equity', 'BANCO POPULAR ESPANOL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'POR3 GY Equity', 'PORSCHE AG-PFD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'POSH UR Equity', 'BABY UNIVERSE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PP FP Equity', 'PPR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PPC GA Equity', 'PUBLIC POWER CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PPDI UW Equity', 'PHARMACEUTICAL PRODUCT DEVEL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PPG UN Equity', 'PPG INDUSTRIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PPL UN Equity', 'PPL CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PPP UN Equity', 'POGO PRODUCING CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PRS NO Equity', 'PROSAFE ASA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PRTY LN Equity', 'PARTYGAMING PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PRU LN Equity', 'PRUDENTIAL PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PRU UN Equity', 'PRUDENTIAL FINANCIAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PSA UN Equity', 'PUBLIC STORAGE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PSD UN Equity', 'PUGET ENERGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PSM GY Equity', 'PROSIEBEN SAT.1 MEDIA AG-PFD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PSN LN Equity', 'PERSIMMON PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PSON LN Equity', 'PEARSON PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PSPN SE Equity', 'PSP SWISS PROPERTY AG-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PTC PL Equity', 'PORTUGAL TELECOM SGPS SA-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PTEN UW Equity', 'PATTERSON-UTI ENERGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PTV UN Equity', 'PACTIV CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PUB FP Equity', 'PUBLICIS GROUPE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PUB LN Equity', 'PUNCH TAVERNS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PUM GY Equity', 'PUMA AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PWR UN Equity', 'QUANTA SERVICES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PX UN Equity', 'PRAXAIR INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PXD UN Equity', 'PIONEER NATURAL RESOURCES CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'PXP UN Equity', 'PLAINS EXPLORATION & PRODUCT', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'Q UN Equity', 'QWEST COMMUNICATIONS INTL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'QCE GY Equity', 'Q-CELLS AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'QCOM UW Equity', 'QUALCOMM INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'QIA GY Equity', 'QIAGEN N.V.', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'QLGC UW Equity', 'QLOGIC CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'R UN Equity', 'RYDER SYSTEM INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RA UN Equity', 'RECKSON ASSOC REALTY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RAD UN Equity', 'RITE AID CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RAI UN Equity', 'REYNOLDS AMERICAN INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RAMS UQ Equity', 'ARIES MARITIME TRANSPORT LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RAND NA Equity', 'RANDSTAD HOLDING NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RATE UW Equity', 'BANKRATE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RB/ LN Equity', 'RECKITT BENCKISER PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RBS LN Equity', 'ROYAL BANK OF SCOTLAND GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RCEA NA Equity', 'RODAMCO EUROPE NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RDC UN Equity', 'ROWAN COMPANIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RDEN UW Equity', 'ELIZABETH ARDEN INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RDN UN Equity', 'RADIAN GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RDSA NA Equity', 'ROYAL DUTCH SHELL PLC-A SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'REC NO Equity', 'RENEWABLE ENERGY CORP AS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'REE SQ Equity', 'RED ELECTRICA DE ESPANA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'REG UN Equity', 'REGENCY CENTERS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'REL LN Equity', 'REED ELSEVIER PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'REN NA Equity', 'REED ELSEVIER NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'REP SQ Equity', 'REPSOL YPF SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RESP UW Equity', 'RESPIRONICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'REX LN Equity', 'REXAM PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RF FP Equity', 'EURAZEO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RF UN Equity', 'REGIONS FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RGA UN Equity', 'REINSURANCE GROUP OF AMERICA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RGC UN Equity', 'REGAL ENTERTAINMENT GROUP-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RHA FP Equity', 'RHODIA SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RHAT UW Equity', 'RED HAT INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RHD UN Equity', 'R.H. DONNELLEY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RHI UN Equity', 'ROBERT HALF INTL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RHM GY Equity', 'RHEINMETALL AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RI FP Equity', 'PERNOD-RICARD SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RIBH AV Equity', 'RAIFFEISEN INTL BANK HOLDING', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RIG UN Equity', 'TRANSOCEAN INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RIO LN Equity', 'RIO TINTO PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RJF UN Equity', 'RAYMOND JAMES FINANCIAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RL UN Equity', 'POLO RALPH LAUREN CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RMBS UW Equity', 'RAMBUS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RMD UN Equity', 'RESMED INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RMK UN Equity', 'ARAMARK CORP-CL B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RMS FP Equity', 'HERMES INTERNATIONAL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RNK LN Equity', 'RANK GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RNO FP Equity', 'RENAULT SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ROG VX Equity', 'ROCHE HOLDING AG-GENUSSCHEIN', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ROH UN Equity', 'ROHM AND HAAS CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ROK UN Equity', 'ROCKWELL AUTOMATION INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ROP UN Equity', 'ROPER INDUSTRIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ROST UW Equity', 'ROSS STORES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RPM UN Equity', 'RPM INTERNATIONAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RR/ LN Equity', 'ROLLS-ROYCE GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RRC UN Equity', 'RANGE RESOURCES CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RRD UN Equity', 'RR DONNELLEY & SONS CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RRI UN Equity', 'RELIANT ENERGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RS UN Equity', 'RELIANCE STEEL & ALUMINUM', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RSA LN Equity', 'ROYAL & SUN ALLIANCE INS GRP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RSG UN Equity', 'REPUBLIC SERVICES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RSH UN Equity', 'RADIOSHACK CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RSL LN Equity', 'RESOLUTION PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RTN UN Equity', 'RAYTHEON COMPANY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RTO LN Equity', 'RENTOKIL INITIAL PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RTR LN Equity', 'REUTERS GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RTRKS FH Equity', 'RAUTARUUKKI OYJ', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RUKN VX Equity', 'SWISS RE-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RWE GY Equity', 'RWE AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RX UN Equity', 'IMS HEALTH INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RYA ID Equity', 'RYANAIR HOLDINGS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RYL UN Equity', 'RYLAND GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'RYN UN Equity', 'RAYONIER INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'S UN Equity', 'SPRINT NEXTEL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SAB LN Equity', 'SABMILLER PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SAB SQ Equity', 'BANCO DE SABADELL SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SAF FP Equity', 'SAFRAN SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SAF UN Equity', 'SAFECO CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SAMAS FH Equity', 'SAMPO OYJ-A SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SAN FP Equity', 'SANOFI-AVENTIS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SAN SQ Equity', 'BANCO SANTANDER CENTRAL HISP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SAND SS Equity', 'SANDVIK AB', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SANM UW Equity', 'SANMINA-SCI CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SAP GY Equity', 'SAP AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SAZ GY Equity', 'STADA ARZNEIMITTEL AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SBAC UW Equity', 'SBA COMMUNICATIONS CORP-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SBH UN Equity', 'SALLY BEAUTY HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SBL UN Equity', 'SYMBOL TECHNOLOGIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SBMO NA Equity', 'SBM OFFSHORE NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SBRY LN Equity', 'SAINSBURY (J) PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SBUX UW Equity', 'STARBUCKS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SCAB SS Equity', 'SVENSKA CELLULOSA AB-B SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SCG UN Equity', 'SCANA CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SCHP SE Equity', 'SCHINDLER HOLDING-PART CERT', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SCHW UW Equity', 'SCHWAB (CHARLES) CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SCI UN Equity', 'SERVICE CORP INTERNATIONAL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SCMN VX Equity', 'SWISSCOM AG-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SCO FP Equity', 'SCOR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SCS UN Equity', 'STEELCASE INC-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SCTN LN Equity', 'SCOTTISH & NEWCASTLE PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SCVB SS Equity', 'SCANIA AB-B SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SDF GY Equity', 'K+S AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SDR LN Equity', 'SCHRODERS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SDRL NO Equity', 'SEADRILL LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SEBA SS Equity', 'SKANDINAVISKA ENSKILDA BAN-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SECUB SS Equity', 'SECURITAS AB-B SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SEE UN Equity', 'SEALED AIR CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SEIC UW Equity', 'SEI INVESTMENTS COMPANY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SEMI UQ Equity', 'ALL AMERICAN SEMICONDUCTOR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SEO VX Equity', 'SERONO SA-BR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SEPR UW Equity', 'SEPRACOR INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SESG FP Equity', 'SES GLOBAL-FDR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SFD UN Equity', 'SMITHFIELD FOODS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SFG UN Equity', 'STANCORP FINANCIAL GROUP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SFI UN Equity', 'ISTAR FINANCIAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SGC LN Equity', 'STAGECOACH GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SGC SQ Equity', 'SOGECABLE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SGE LN Equity', 'SAGE GROUP PLC (THE)', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SGMS UW Equity', 'SCIENTIFIC GAMES CORP-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SGO FP Equity', 'COMPAGNIE DE SAINT-GOBAIN', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SGP UN Equity', 'SCHERING-PLOUGH CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SGR UN Equity', 'SHAW GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SGSN VX Equity', 'SGS SA-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SHBA SS Equity', 'SVENSKA HANDELSBANKEN-A SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SHLD UW Equity', 'SEARS HOLDINGS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SHP LN Equity', 'SHIRE PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SHW UN Equity', 'SHERWIN-WILLIAMS CO/THE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SIAL UW Equity', 'SIGMA-ALDRICH', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SIE GY Equity', 'SIEMENS AG-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SIE UN Equity', 'SIERRA HEALTH SERVICES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SIG LN Equity', 'SIGNET GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SII UN Equity', 'SMITH INTERNATIONAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SIK SE Equity', 'SIKA AG-BR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SIRI UW Equity', 'SIRIUS SATELLITE RADIO INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SJM UN Equity', 'JM SMUCKER CO/THE-NEW COMMON', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SKAB SS Equity', 'SKANSKA AB-B SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SKFB SS Equity', 'SKF AB-B SHARES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SKS UN Equity', 'SAKS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SKYF UW Equity', 'SKY FINANCIAL GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SLAB UW Equity', 'SILICON LABORATORIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SLB UN Equity', 'SCHLUMBERGER LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SLE UN Equity', 'SARA LEE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SLG UN Equity', 'SL GREEN REALTY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SLHN VX Equity', 'SWISS LIFE HOLDING-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SLM UN Equity', 'SLM CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SLOU LN Equity', 'SLOUGH ESTATES PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SLR UN Equity', 'SOLECTRON CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SM UN Equity', 'ST MARY LAND & EXPLORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SMG UN Equity', 'SCOTTS MIRACLE-GRO CO-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SMIN LN Equity', 'SMITHS GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SN/ LN Equity', 'SMITH & NEPHEW PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SNA UN Equity', 'SNAP-ON INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SNDK UW Equity', 'SANDISK CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SNPS UW Equity', 'SYNOPSYS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SNV UN Equity', 'SYNOVUS FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SO UN Equity', 'SOUTHERN CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SOLB BB Equity', 'SOLVAY SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SON UN Equity', 'SONOCO PRODUCTS CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SOV UN Equity', 'SOVEREIGN BANCORP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SPF UN Equity', 'STANDARD-PACIFIC CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SPG UN Equity', 'SIMON PROPERTY GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SPI IM Equity', 'SANPAOLO IMI SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SPLS UW Equity', 'STAPLES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SPM IM Equity', 'SAIPEM', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SPN UN Equity', 'SUPERIOR ENERGY SERVICES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SPSN UW Equity', 'SPANSION INC-CLASS A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SPW LN Equity', 'SCOTTISH POWER PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SPW UN Equity', 'SPX CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SRCE UW Equity', '1ST SOURCE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SRCL UW Equity', 'STERICYCLE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SRE UN Equity', 'SEMPRA ENERGY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SRG IM Equity', 'SNAM RETE GAS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SRP LN Equity', 'SERCO GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SRP UN Equity', 'SIERRA PACIFIC RESOURCES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SSABA SS Equity', 'SSAB SVENSKT STAAL AB-SER A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SSCC UW Equity', 'SMURFIT-STONE CONTAINER CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SSE LN Equity', 'SCOTTISH & SOUTHERN ENERGY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SSP UN Equity', 'EW SCRIPPS CO-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'STA UN Equity', 'ST PAUL TRAVELERS COS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'STAN LN Equity', 'STANDARD CHARTERED PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'STB NO Equity', 'STOREBRAND ASA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'STERV FH Equity', 'STORA ENSO OYJ-R SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'STI UN Equity', 'SUNTRUST BANKS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'STJ UN Equity', 'ST JUDE MEDICAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'STL NO Equity', 'STATOIL ASA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'STLD UW Equity', 'STEEL DYNAMICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'STM IM Equity', 'STMICROELECTRONICS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'STMN SE Equity', 'STRAUMANN HOLDING AG-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'STN UN Equity', 'STATION CASINOS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'STO NA Equity', 'STORK NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'STR UN Equity', 'QUESTAR CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'STST UW Equity', 'ARGON ST INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'STT UN Equity', 'STATE STREET CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'STU UN Equity', 'STUDENT LOAN CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'STZ UN Equity', 'CONSTELLATION BRANDS INC-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SU FP Equity', 'SCHNEIDER ELECTRIC SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SUG UN Equity', 'SOUTHERN UNION CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SUN SW Equity', 'SULZER AG-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SUN UN Equity', 'SUNOCO INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SUNW UW Equity', 'SUN MICROSYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SVM UN Equity', 'SERVICEMASTER COMPANY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SVT LN Equity', 'SEVERN TRENT PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SVU UN Equity', 'SUPERVALU INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SW FP Equity', 'SODEXHO ALLIANCE SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SWEDA SS Equity', 'SWEDBANK AB - A SHARES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SWFT UW Equity', 'SWIFT TRANSPORTATION CO INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SWK UN Equity', 'STANLEY WORKS/THE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SWMA SS Equity', 'SWEDISH MATCH AB', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SWN UN Equity', 'SOUTHWESTERN ENERGY CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SWS1V FH Equity', 'SANOMAWSOY OYJ', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SWV GY Equity', 'SOLARWORLD AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SWY UN Equity', 'SAFEWAY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SYDB DC Equity', 'SYDBANK A/S', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SYK UN Equity', 'STRYKER CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SYMC UW Equity', 'SYMANTEC CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SYNN VX Equity', 'SYNGENTA AG-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SYST VX Equity', 'SYNTHES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SYV SQ Equity', 'SACYR VALLEHERMOSO SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SYY UN Equity', 'SYSCO CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SZE FP Equity', 'SUEZ SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SZG GY Equity', 'SALZGITTER AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'SZU GY Equity', 'SUEDZUCKER AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'T UN Equity', 'AT&T INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TACX UR Equity', 'A CONSULTING TEAM INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TAP UN Equity', 'MOLSON COORS BREWING CO -B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TATE LN Equity', 'TATE & LYLE PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TBBK UQ Equity', 'BANCORP INC/THE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TBUS UR Equity', 'DIGITAL RECORDERS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TCB UN Equity', 'TCF FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TCHC UQ Equity', '21ST CENTURY HOLDING CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TCO UN Equity', 'TAUBMAN CENTERS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TDS UA Equity', 'TELEPHONE AND DATA SYSTEMS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TDSC UQ Equity', '3D SYSTEMS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TDW UN Equity', 'TIDEWATER INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TE UN Equity', 'TECO ENERGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TEC FP Equity', 'TECHNIP SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TECD UW Equity', 'TECH DATA CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TECH UW Equity', 'TECHNE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TEF SQ Equity', 'TELEFONICA SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TEK UN Equity', 'TEKTRONIX INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TEL NO Equity', 'TELENOR ASA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TEL2B SS Equity', 'TELE2 AB-B SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TELL GA Equity', 'BANK OF GREECE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TEN IM Equity', 'TENARIS SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TER UN Equity', 'TERADYNE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TEX UN Equity', 'TEREX CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TFI FP Equity', 'TELEVISION FRANCAISE (T.F.1)', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TFSM UQ Equity', '24/7 REAL MEDIA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TFX UN Equity', 'TELEFLEX INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TGT UN Equity', 'TARGET CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'THC UN Equity', 'TENET HEALTHCARE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'THE UN Equity', 'TODCO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'THFF UW Equity', 'FIRST FINANCIAL CORP INDIANA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'THG UN Equity', 'HANOVER INSURANCE GROUP INC/', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'THI UN Equity', 'TIM HORTONS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'THO UN Equity', 'THOR INDUSTRIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TIE UN Equity', 'TITANIUM METALS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TIE1V FH Equity', 'TIETOENATOR OYJ', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TIF UN Equity', 'TIFFANY & CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TIN UN Equity', 'TEMPLE-INLAND INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TIT IM Equity', 'TELECOM ITALIA SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TITK GA Equity', 'TITAN CEMENT CO. S.A.', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TJX UN Equity', 'TJX COMPANIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TKA AV Equity', 'TELEKOM AUSTRIA AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TKA GY Equity', 'THYSSENKRUPP AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TKR UN Equity', 'TIMKEN CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TL5 SQ Equity', 'GESTEVISION TELECINCO SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TLAB UW Equity', 'TELLABS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TLSN SS Equity', 'TELIASONERA AB', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TLW LN Equity', 'TULLOW OIL PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TMA UN Equity', 'THORNBURG MORTGAGE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TMK UN Equity', 'TORCHMARK CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TMO UN Equity', 'THERMO FISHER SCIENTIFIC INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TMS FP Equity', 'THOMSON (EX-TMM)', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TNB UN Equity', 'THOMAS & BETTS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TNI LN Equity', 'TRINITY MIRROR PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TNN LN Equity', 'TAYLOR NELSON SOFRES PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TNT NA Equity', 'TNT NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TOL UN Equity', 'TOLL BROTHERS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TOMK LN Equity', 'TOMKINS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TOP DC Equity', 'TOPDANMARK A/S', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TPEIR GA Equity', 'PIRAEUS BANK S.A.', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TPK LN Equity', 'TRAVIS PERKINS PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TRAK UQ Equity', 'DEALERTRACK HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TRB UN Equity', 'TRIBUNE CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TRELB SS Equity', 'TRELLEBORG AB-B SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TRH UN Equity', 'TRANSATLANTIC HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TRI UN Equity', 'TRIAD HOSPITALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TRMB UW Equity', 'TRIMBLE NAVIGATION LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TRMD UW Equity', 'DAMPSKIBSSELSKABET TORM-ADR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TRN IM Equity', 'TERNA SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TRN UN Equity', 'TRINITY INDUSTRIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TROW UW Equity', 'T ROWE PRICE GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TRUED UQ Equity', 'CENTRUE FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TRW UN Equity', 'TRW AUTOMOTIVE HOLDINGS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TSCO LN Equity', 'TESCO PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TSCO UW Equity', 'TRACTOR SUPPLY COMPANY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TSFG UW Equity', 'SOUTH FINANCIAL GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TSG UN Equity', 'SABRE HOLDINGS CORP-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TSN UN Equity', 'TYSON FOODS INC-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TSO UN Equity', 'TESORO CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TSS UN Equity', 'TOTAL SYSTEM SERVICES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TTC UN Equity', 'TORO CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TTI UN Equity', 'TETRA TECHNOLOGIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TUI1 GY Equity', 'TUI AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TWOD LN Equity', 'TAYLOR WOODROW PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TWX UN Equity', 'TIME WARNER INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TXCO UW Equity', 'EXPLORATION CO OF DELAWARE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TXN UN Equity', 'TEXAS INSTRUMENTS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TXT UN Equity', 'TEXTRON INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TXU UN Equity', 'TXU CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'TYC UN Equity', 'TYCO INTERNATIONAL LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UAG UN Equity', 'UNITED AUTO GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UAUA UW Equity', 'UAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UB UN Equity', 'UNIONBANCAL CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UBM LN Equity', 'UNITED BUSINESS MEDIA PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UBSN VX Equity', 'UBS AG-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UC IM Equity', 'UNICREDITO ITALIANO SPA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UCB BB Equity', 'UCB SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UDR UN Equity', 'UNITED DOMINION REALTY TRUST', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UG FP Equity', 'PEUGEOT SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UGI UN Equity', 'UGI CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UHAL UW Equity', 'AMERCO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UHR VX Equity', 'THE SWATCH GROUP AG-BR', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UHS UN Equity', 'UNIVERSAL HEALTH SERVICES-B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UIS UN Equity', 'UNISYS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UL FP Equity', 'UNIBAIL', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ULVR LN Equity', 'UNILEVER PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UMI BB Equity', 'UMICORE', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UNA NA Equity', 'UNILEVER NV-CVA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UNF SQ Equity', 'UNION FENOSA SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UNH UN Equity', 'UNITEDHEALTH GROUP INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UNIP IM Equity', 'UNIPOL-PFD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UNM UN Equity', 'UNUMPROVIDENT CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UNP UN Equity', 'UNION PACIFIC CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UNT UN Equity', 'UNIT CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UPM1V FH Equity', 'UPM-KYMMENE OYJ', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UPS UN Equity', 'UNITED PARCEL SERVICE-CL B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'URBN UW Equity', 'URBAN OUTFITTERS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'URI UN Equity', 'UNITED RENTALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'URS UN Equity', 'URS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'USB UN Equity', 'US BANCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'USG UN Equity', 'USG CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'USM UA Equity', 'US CELLULAR CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UST UN Equity', 'UST INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UTR UN Equity', 'UNITRIN INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UTX UN Equity', 'UNITED TECHNOLOGIES CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UU/ LN Equity', 'UNITED UTILITIES PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'UVN UN Equity', 'UNIVISION COMMUNICATIONS-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VAL UN Equity', 'VALSPAR CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VAR UN Equity', 'VARIAN MEDICAL SYSTEMS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VATN SE Equity', 'VALIANT HOLDING-REG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VED LN Equity', 'VEDANTA RESOURCES PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VEDR NA Equity', 'VEDIOR NV-CVA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VEGF UR Equity', 'CORAUTUS GENETICS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VER AV Equity', 'OEST ELEKTRIZITATSWIRTS-A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VFC UN Equity', 'VF CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VHI UN Equity', 'VALHI INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VIA/B UN Equity', 'VIACOM INC-CLASS B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VIE FP Equity', 'VEOLIA ENVIRONNEMENT', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VIV FP Equity', 'VIVENDI', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VK FP Equity', 'VALLOUREC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VLO UN Equity', 'VALERO ENERGY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VLY UN Equity', 'VALLEY NATIONAL BANCORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VMC UN Equity', 'VULCAN MATERIALS CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VNO UN Equity', 'VORNADO REALTY TRUST', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VOD LN Equity', 'VODAFONE GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VOE AV Equity', 'VOESTALPINE AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VOLVB SS Equity', 'VOLVO AB-B SHS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VOW GY Equity', 'VOLKSWAGEN AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VOXX UQ Equity', 'AUDIOVOX CORP -CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VRSN UW Equity', 'VERISIGN INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VRTX UW Equity', 'VERTEX PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VSH UN Equity', 'VISHAY INTERTECHNOLOGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VTR UN Equity', 'VENTAS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VVC UN Equity', 'VECTREN CORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VWS DC Equity', 'VESTAS WIND SYSTEMS A/S', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'VZ UN Equity', 'VERIZON COMMUNICATIONS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WAG UN Equity', 'WALGREEN CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WAT UN Equity', 'WATERS CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WB UN Equity', 'WACHOVIA CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WBMD UW Equity', 'WEBMD HEALTH CORP-CLASS A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WBS UN Equity', 'WEBSTER FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WCC UN Equity', 'WESCO INTERNATIONAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WCG UN Equity', 'WELLCARE HEALTH PLANS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WDC UN Equity', 'WESTERN DIGITAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WDH DC Equity', 'WILLIAM DEMANT HOLDING', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WEC UN Equity', 'WISCONSIN ENERGY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WEN UN Equity', 'WENDY`S INTERNATIONAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WFC UN Equity', 'WELLS FARGO & COMPANY', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WFMI UW Equity', 'WHOLE FOODS MARKET INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WFR UN Equity', 'MEMC ELECTRONIC MATERIALS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WFSL UW Equity', 'WASHINGTON FEDERAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WFT UN Equity', 'WEATHERFORD INTL LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WHA NA Equity', 'WERELDHAVE NV', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WHR UN Equity', 'WHIRLPOOL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WIE AV Equity', 'WIENERBERGER AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WIN GY Equity', 'WINCOR NIXDORF AG', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WIN UN Equity', 'WINDSTREAM CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WIRE UW Equity', 'ENCORE WIRE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WL UN Equity', 'WILMINGTON TRUST CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WLB LN Equity', 'WILSON BOWDEN PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WLK UN Equity', 'WESTLAKE CHEMICAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WLP UN Equity', 'WELLPOINT INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WLSNC NA Equity', 'WOLTERS KLUWER', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WLT UN Equity', 'WALTER INDUSTRIES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WM UN Equity', 'WASHINGTON MUTUAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WMB UN Equity', 'WILLIAMS COS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WMG UN Equity', 'WARNER MUSIC GROUP CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WMH LN Equity', 'WILLIAM HILL PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WMI UN Equity', 'WASTE MANAGEMENT INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WMPY LN Equity', 'WIMPEY (GEORGE)  PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WMT UN Equity', 'WAL-MART STORES INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WOLV LN Equity', 'WOLVERHAMPTON & DUDLEY BREW', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WOOF UW Equity', 'VCA ANTECH INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WOS LN Equity', 'WOLSELEY PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WPI UN Equity', 'WATSON PHARMACEUTICALS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WPO UN Equity', 'WASHINGTON POST  -CL B', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WPP LN Equity', 'WPP GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WPS UN Equity', 'WPS RESOURCES CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WRI UN Equity', 'WEINGARTEN REALTY INVESTORS', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WRTBV FH Equity', 'WARTSILA OYJ-B SHARES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WSC UA Equity', 'WESCO FINANCIAL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WSM UN Equity', 'WILLIAMS-SONOMA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WTB LN Equity', 'WHITBREAD PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WTI UN Equity', 'W&T OFFSHORE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WTNY UW Equity', 'WHITNEY HOLDING CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WTR UN Equity', 'AQUA AMERICA INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WTW UN Equity', 'WEIGHT WATCHERS INTL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WU UN Equity', 'WESTERN UNION CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WWY UN Equity', 'WRIGLEY WM JR CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WY UN Equity', 'WEYERHAEUSER CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WYE UN Equity', 'WYETH', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WYN UN Equity', 'WYNDHAM WORLDWIDE CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'WYNN UW Equity', 'WYNN RESORTS LTD', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'X UN Equity', 'UNITED STATES STEEL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'XEC UN Equity', 'CIMAREX ENERGY CO', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'XEL UN Equity', 'XCEL ENERGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'XIDE UQ Equity', 'EXIDE TECHNOLOGIES', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'XL UN Equity', 'XL CAPITAL LTD -CLASS A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'XLNX UW Equity', 'XILINX INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'XLTC UW Equity', 'EXCEL TECHNOLOGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'XMSR UW Equity', 'XM SATELLITE RADIO HOLD-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'XOM UN Equity', 'EXXON MOBIL CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'XRAY UW Equity', 'DENTSPLY INTERNATIONAL INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'XRX UN Equity', 'XEROX CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'XTA LN Equity', 'XSTRATA PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'XTEX UW Equity', 'CROSSTEX ENERGY LP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'XTO UN Equity', 'XTO ENERGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'XTXI UW Equity', 'CROSSTEX ENERGY INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'Y UN Equity', 'ALLEGHANY CORP', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'YAR NO Equity', 'YARA INTERNATIONAL ASA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'YELL LN Equity', 'YELL GROUP PLC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'YHOO UW Equity', 'YAHOO! INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'YRCW UW Equity', 'YRC WORLDWIDE INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'YTY1V FH Equity', 'YIT OYJ', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'YUM UN Equity', 'YUM! BRANDS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ZBRA UW Equity', 'ZEBRA TECHNOLOGIES CORP-CL A', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ZC FP Equity', 'ZODIAC SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ZION UW Equity', 'ZIONS BANCORPORATION', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ZMH UN Equity', 'ZIMMER HOLDINGS INC', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ZOT SQ Equity', 'ZARDOYA OTIS SA', 7, 1, Null
EXEC adp_tblMarketInstruments_InsertCommand 0, 'ZURN VX Equity', 'ZURICH FINANCIAL SERVICE-REG', 7, 1, Null
GO
