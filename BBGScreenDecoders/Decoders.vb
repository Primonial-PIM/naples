Imports System.Globalization

Public Class ScreenDecoders

  Public Shared Function BBGScreenDecoder(ByVal ScreenType As BBGScreenType, ByVal ReturnValue As BBGReturnValue, ByVal ScreenCopy As String) As String
    If (ScreenCopy Is Nothing) Then
      Return ""
      Exit Function
    End If

    Dim ScreenArray() As String
    Dim Separators() As Char = {Chr(10)}
    Dim ReturnString As String = ""

    ScreenArray = ScreenCopy.Split(Separators)

    Select Case ScreenType

      Case BBGScreenType.PortfolioPDSP

        Select Case ReturnValue
          Case BBGReturnValue.PDSP_Currency
            Dim LineText As String
            ' Dim thisString As String

            For Each LineText In ScreenArray
              If InStr(LineText, "Rpt Crncy") > 0 Then
                ReturnString = LineText.Substring(InStr(LineText, "Rpt Crncy") + 8, 5).Trim
                Exit For
              End If
            Next

          Case BBGReturnValue.PDSP_HolderName
            Dim LineText As String
            Dim thisString As String

            For Each LineText In ScreenArray
              If InStr(LineText, "Holder Name:") > 0 Then
                thisString = LineText.Substring(InStr(LineText, "Holder Name:") + 11).Trim
                If InStr(thisString, "Port:") > 0 Then
                  ReturnString = thisString.Substring(0, InStr(thisString, "Port:") - 1).Trim
                Else
                  ReturnString = thisString
                End If
                Exit For
              End If
            Next

          Case BBGReturnValue.PSDP_PortfolioID
            Dim LineText As String

            For Each LineText In ScreenArray
              If InStr(LineText, "Port:") > 0 Then
                ReturnString = LineText.Substring(InStr(LineText, "Port:") + 4).Trim
                Exit For
              End If
            Next

          Case BBGReturnValue.PDSP_CurrentPage, BBGReturnValue.PDSP_TotalPages
            Dim LineText As String
            Dim PagesString As String
            Dim NumberString As String
            ReturnString = "0"

            For Each LineText In ScreenArray
              If (InStr(LineText, " Page ") > 0) Then
                PagesString = LineText.Substring(InStr(LineText, " Page ") + 5).Trim
                If InStr(PagesString, "/") > 0 Then
                  If (ReturnValue = BBGReturnValue.PDSP_CurrentPage) Then
                    NumberString = PagesString.Substring(0, InStr(PagesString, "/") - 1).Trim
                  Else
                    NumberString = PagesString.Substring(InStr(PagesString, "/")).Trim
                  End If

                  If IsNumeric(NumberString) Then
                    ReturnString = NumberString
                  End If

                  Exit For
                End If
              End If
            Next

          Case BBGReturnValue.PDSP_ReportDate
            Dim LineText As String
            Dim thisDateString As String
            ReturnString = "1 Jan 1900"

            For Each LineText In ScreenArray
              If InStr(LineText, "Filing Date") > 0 Then
                thisDateString = LineText.Substring(InStr(LineText, "Filing Date") + 10).Trim
                ReturnString = Date.Parse(thisDateString, New CultureInfo("en-US").DateTimeFormat).ToString("dd MMM yyyy")
                Exit For
              End If
            Next

          Case BBGReturnValue.PDSP_SettleOn
            Dim LineText As String
            Dim thisDateString As String
            ReturnString = "1 Jan 1900"

            For Each LineText In ScreenArray
              If InStr(LineText, " Settle on") > 0 Then
                thisDateString = LineText.Substring(InStr(LineText, " Settle on") + 9).Trim
                ReturnString = Date.Parse(thisDateString, New CultureInfo("en-US").DateTimeFormat).ToString("dd MMM yyyy")
                Exit For
              End If
            Next

          Case BBGReturnValue.PDSP_Header
            Dim Counter As Integer

            For Counter = 0 To 8
              If ScreenArray.GetLength(0) >= (Counter + 1) Then
                ReturnString &= ScreenArray(Counter) & vbCrLf
              End If
            Next

          Case BBGReturnValue.PDSP_InstrumentLines

            Dim Counter As Integer

            For Counter = 9 To (ScreenArray.GetLength(0) - 1)
              If ScreenArray.GetLength(0) >= (Counter + 1) Then
                If InStr(ScreenArray(Counter), ")") > 0 Then
                  ReturnString &= ScreenArray(Counter).Trim(New Char() {Chr(10), Chr(13)}) & vbCrLf ' .Substring(InStr(ScreenArray(Counter), ")")).Trim & vbCrLf
                End If
              End If
            Next

          Case BBGReturnValue.PDSP_Ticker
            ReturnString = ScreenArray(7).Substring(2, 1)
          Case Else
            ReturnString = ""
        End Select






      Case Else
        ReturnString = ""
    End Select

    Return ReturnString

  End Function

  Public Enum BBGScreenType As Integer
    PortfolioPDSP

  End Enum

  Public Enum BBGReturnValue As Integer
    PDSP_Currency
    PDSP_HolderName
    PSDP_PortfolioID
    PDSP_SettleOn
    PDSP_ReportDate
    PDSP_Ticker
    PDSP_CurrentPage
    PDSP_TotalPages
    PDSP_Header
    PDSP_InstrumentLines
  End Enum
End Class
